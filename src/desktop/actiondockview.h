#ifndef ACTIONDOCKVIEW_H
#define ACTIONDOCKVIEW_H

#include <QtCore>
#include <QtGui>
#include <QtSvg>
#include "core/actionview.h"

namespace Core {
class ActionModel;

}


namespace Desktop {

class ActionDockView:
        public QGraphicsSvgItem,
        public Core::ActionView
{
    Q_OBJECT;
    Q_PROPERTY(int modelId READ modelId);
public:
    explicit ActionDockView(Core::ActionModel *model, QGraphicsItem *parent=0);
    Core::ActionModel *model();
    int modelId() const;
    inline QGraphicsSvgItem* graphicsItem() { return this; }
    bool isEnabled() const;
    QSize originalSize() const;
    QSize currentSize() const;
public slots:
    void setActive(bool active);
signals:
    void clicked();
    void dragged();
    void hovered(const QString &title);
    void unhovered();
private:
    virtual void mouseReleaseEvent ( QGraphicsSceneMouseEvent * event );
    virtual void mousePressEvent ( QGraphicsSceneMouseEvent * event );
#ifndef QT_NO_CURSOR
    virtual void mouseMoveEvent( QGraphicsSceneMouseEvent * event );
    virtual void hoverEnterEvent ( QGraphicsSceneHoverEvent * event );
    virtual void hoverLeaveEvent ( QGraphicsSceneHoverEvent * event );
#endif
    Core::ActionModel *m_model;
    bool b_mouseClick;
    QPropertyAnimation *an_jumping;
    QPropertyAnimation *an_stopJumping;
    bool b_active;

};

}

#endif // ACTIONDOCKVIEW_H
