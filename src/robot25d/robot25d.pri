HEADERS += robot25d/cellgraphicsitem.h robot25d/default_plugin.h \
    robot25d/graphicsimageitem.h \
    robot25d/robotcell.h robot25d/roboterrors.h robot25d/robotview.h \
    robot25d/util.h robot25d/robotitem.h
SOURCES += robot25d/cellgraphicsitem.cpp robot25d/default_plugin.cpp \
    robot25d/graphicsimageitem.cpp \
    robot25d/robotview.cpp \
    robot25d/util.cpp robot25d/robotitem.cpp
