#include "newenvironmentdialog.h"
#include "ui_newenvironmentdialog.h"

namespace Creator {

NewEnvironmentDialog::NewEnvironmentDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewEnvironmentDialog)
{
    ui->setupUi(this);
}

NewEnvironmentDialog::~NewEnvironmentDialog()
{
    delete ui;
}

QSize NewEnvironmentDialog::environemntSize() const
{
    return QSize(ui->width->value(), ui->height->value());
}

}
