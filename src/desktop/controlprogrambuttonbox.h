#ifndef CONTROLPROGRAMBUTTONBOX_H
#define CONTROLPROGRAMBUTTONBOX_H

#include <QtCore>
#include <QtGui>
#include <QtSvg>

#include "core/enums.h"

namespace Util {
class SvgButton;

}

namespace Desktop {


class ControlProgramButtonBox : public QObject, public QGraphicsRectItem
{
    Q_OBJECT
public:
    explicit ControlProgramButtonBox(QGraphicsItem *parent = 0);

signals:
    void startClicked();
    void stopClicked();
    void pauseClicked();
    void resetClicked();
    void stepClicked();
    void debugClicked();
    void fastRunClicked();

public slots:
    void setProgramState(Core::ProgramState state);
private:
    Util::SvgButton *g_start;
    Util::SvgButton *g_stop;
    Util::SvgButton *g_pause;
    Util::SvgButton *g_reset;
//    Util::SvgButton *g_step;
    Util::SvgButton *g_debug;
    Util::SvgButton *g_fastRun;

    Core::ProgramState e_currentState;

};

}
#endif // CONTROLPROGRAMBUTTONBOX_H
