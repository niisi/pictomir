#include "mobile/m_dockwidget.h"
#include "core/actionmodel.h"
#include "core/actionview.h"
#include "mobile/actionminidockview.h"
#include "core/actionmanager.h"

using namespace Core;

namespace Mobile {

DockWidgetTouchscreen::DockWidgetTouchscreen(const QSize &iconSize, QWidget *parent)
    : QGraphicsView(parent)
    , sz_iconSize(iconSize)
{
    m_actionManager = NULL;
    setMouseTracking(true);
    setScene(new QGraphicsScene());
    setFixedHeight(90);
    QLinearGradient gr(0,1,0,0);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setFrameShape(QFrame::NoFrame);
    gr_background.setCoordinateMode(QGradient::ObjectBoundingMode);
    gr_background.setColorAt(0.0, QColor("cadetblue"));
    gr_background.setColorAt(1.0, QColor("black"));
    r_offset = 0;
    b_pressed = 0;
    g_pressed = 0;
}

void DockWidgetTouchscreen::setActionManager(ActionManager *manager)
{
    m_actionManager = manager;
}

ActionManager * DockWidgetTouchscreen::actionManager()
{
    return m_actionManager;
}

void DockWidgetTouchscreen::addSpacer()
{
    l_spacers << l_items.size();
}

void DockWidgetTouchscreen::relayoutDock()
{
    Q_ASSERT ( l_actions.size() == l_items.size() );
    setSceneRect(0,0,width(),height());
    r_offset = 0;
    int cur = r_offset;
    for (int i=0; i<l_items.count(); i++ ) {
        if (l_spacers.contains(i))
            cur += 25;
        if (e_currentOrientation==Qt::Horizontal) {
            l_items[i]->graphicsItem()->setPos(cur,5);
            cur += l_items[i]->currentSize().width();
        }
        else {
            l_items[i]->graphicsItem()->setPos(5, cur);
            cur += l_items[i]->currentSize().height();
        }
        cur += 8;
    }
    update();
}

void DockWidgetTouchscreen::updateVisiblity()
{
    l_actions.clear();
    for (int i=0; i<l_items.size(); i++) {
        l_items[i]->graphicsItem()->deleteLater();
    }
    l_items.clear();
    l_spacers.clear();
    QList<ActionModel*> repeaters;
    QList<ActionModel*> conditions;
    QList<ActionModel*> moduleActions;
    QList<ActionModel*> functions;
    repeaters = m_actionManager->models(true, ActionRepeat);
    conditions = m_actionManager->models(true, ActionCondition);
    moduleActions = m_actionManager->models(true, ActionPlain, Module);
    functions = m_actionManager->models(true, ActionPlain, Functions);
    for (int i=0; i<repeaters.size(); i++) {
        l_items << new ActionMiniDockView(repeaters[i]);
    }
    addSpacer();
    for (int i=0; i<conditions.size(); i++) {
        l_items << new ActionMiniDockView(conditions[i]);
    }
    for (int i=0; i<moduleActions.size(); i++) {
        l_items << new ActionMiniDockView(moduleActions[i]);
    }
    addSpacer();
    for (int i=0; i<functions.size(); i++) {
        l_items << new ActionMiniDockView(functions[i]);
    }
    l_actions = repeaters + conditions + moduleActions + functions;
    for (int i=0; i<l_items.size(); i++) {
        QGraphicsSvgItem *item = l_items[i]->graphicsItem();
        QSizeF itemSize = item->boundingRect().size();
        qreal scaleX = sz_iconSize.width() / itemSize.width();
        qreal scaleY = sz_iconSize.height() / itemSize.height();
        qreal scale = qMin(scaleX, scaleY);
        item->scale(scale, scale);
        item->setPos(0, 5);
        //        connect (item, SIGNAL(clicked()), this, SLOT(handleItemClicked()));
        //        connect (item, SIGNAL(hovered(QString)), this, SLOT(handleItemText(QString)));
        scene()->addItem(item);
    }
    relayoutDock();
}



void DockWidgetTouchscreen::resizeEvent(QResizeEvent *event)
{
    i_dockWidth = event->size().width();
    relayoutDock();
    event->accept();
}

QWidget* DockWidgetTouchscreen::thisWidget()
{
    return this;
}

void DockWidgetTouchscreen::connectSignalTouched(QObject *obj, const char *method)
{
    connect (this, SIGNAL(touched()), obj, method, Qt::DirectConnection);
}

void DockWidgetTouchscreen::connectSignalActivationChanged(QObject *obj, const char *method)
{
    connect (this, SIGNAL(activationChanged()), obj, method, Qt::DirectConnection);
}

void DockWidgetTouchscreen::handleItemClicked()
{
    ActionMiniDockView *item = qobject_cast<ActionMiniDockView*>(sender());
    Q_CHECK_PTR(item);

    if (m_actionManager->activeView()==item) {
        m_actionManager->setActiveView(NULL, true);
    }
    else {
        m_actionManager->setActiveView(item, true);
    }
    emit touched();
    emit activationChanged();
}



void DockWidgetTouchscreen::setOrientation(Qt::Orientation orientation)
{
    e_currentOrientation = orientation;
    if (orientation==Qt::Horizontal) {
        gr_background.setStart(0.5, 1.0);
        gr_background.setFinalStop(0.5, 0.0);
    }
    else {
        gr_background.setStart(0.0, 0.5);
        gr_background.setFinalStop(1.0, 0.5);
    }
    relayoutDock();
    update();
}

void DockWidgetTouchscreen::paintEvent(QPaintEvent *event)
{
    QPainter p(viewport());
    p.save();
    p.setRenderHint(QPainter::Antialiasing, true);
    p.setPen(Qt::NoPen);
    p.setBrush(QColor("limegreen"));
    p.drawRoundedRect(0,0,width(),height(),10,10);
    if (e_currentOrientation==Qt::Vertical) {
        p.drawRect(0,0,10,height());
    }
    else {
        p.drawRect(0, height()-10, width(), 10);
    }
    p.restore();
    QGraphicsView::paintEvent(event);
}

void DockWidgetTouchscreen::mousePressEvent(QMouseEvent *event)
{
    qDebug() << "Mouse press";
    pnt_pressed = event->pos();
    b_pressed = true;
    g_pressed = scene()->itemAt(event->pos());
    event->accept();
}

void DockWidgetTouchscreen::mouseReleaseEvent(QMouseEvent *event)
{
    qDebug() << "Mouse release";
    if (!b_pressed) {
        event->ignore();
        return;
    }
    if (g_pressed && scene()->itemAt(event->pos())==g_pressed) {
        // item click
#ifndef QT_NO_DYNAMIC_CAST
        ActionMiniDockView *item = dynamic_cast<ActionMiniDockView*>(g_pressed);
#else
		ActionMiniDockView *item = (ActionMiniDockView*)(g_pressed);
#endif
        if (m_actionManager->activeView()!=item)
            m_actionManager->setActiveView(item, true);
        else
            m_actionManager->setActiveView(NULL, true);
        emit touched();
        emit activationChanged();
    }
    else {
        // just stop moving
    }
    event->accept();
}

void DockWidgetTouchscreen::mouseMoveEvent(QMouseEvent *event)
{
    if (!b_pressed) {
        event->ignore();
        return;
    }
    if (e_currentOrientation==Qt::Horizontal)
        r_offset += event->pos().x() - pnt_pressed.x();
    else
        r_offset += event->pos().y() - pnt_pressed.y();
    pnt_pressed = event->pos();
    relayoutDock();
    event->accept();
}
}
