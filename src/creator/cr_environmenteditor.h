#ifndef CR_ENVIRONMENTEDITOR_H
#define CR_ENVIRONMENTEDITOR_H

#include <QtCore>
#include <QtGui>
#include "schema/sch_game.h"

namespace Robot25D {
    class RobotView;
    class Plugin;
}


namespace Creator {

class NewEnvironmentDialog;

namespace Ui {
    class EnvironmentEditor;
}

class EnvironmentEditor : public QWidget
{
    Q_OBJECT

public:
    explicit EnvironmentEditor(QWidget *parent = 0);
    ~EnvironmentEditor();
    inline Robot25D::Plugin * module() { return robotPlugin; }

    inline QAction * actionNew() { return a_new; }
    inline QAction * actionImport() { return a_import; }
    inline QAction * actionExport() { return a_export; }

public slots:
    inline Schema::Game * game() { return sch_game; }
    void setEnvironment(Schema::Game * game, int index);
    void setEnvironment(const Schema::Environment &env);
    Schema::Environment environment() const;

    inline void setTaskIndex(int v) { i_taskIndex = v; }
    inline void checkTaskIndex(const QMap<int,int> map) { if (map.contains(i_taskIndex)) i_taskIndex = map[i_taskIndex]; }
    inline int taskIndex() const { return i_taskIndex; }
    void updateWindowTitle();

    void newEnvironment();
    void importEnvironment();
    void exportEnvironment();
    void sync();

private slots:
    void handleAction();


private:
    void closeEvent(QCloseEvent *event);
    QAction * a_new;
    QAction * a_import;
    QAction * a_export;
    Schema::Game * sch_game;
    int i_taskIndex;
    void setIcons();
    Ui::EnvironmentEditor *ui;
    Robot25D::RobotView * robot;
    Robot25D::Plugin * robotPlugin;
    NewEnvironmentDialog *  newDialog;
};


} // namespace Creator
#endif // CR_ENVIRONMENTEDITOR_H
