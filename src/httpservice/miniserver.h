#ifndef MINISERVER_H
#define MINISERVER_H

#include <QtCore>
#include <QtNetwork>

namespace HTTPService {

class MiniServer : public QObject
{
    Q_OBJECT
public:
    explicit MiniServer(QObject *parent = 0);
    QNetworkReply * serveRequest(QNetworkAccessManager * manager, QNetworkAccessManager::Operation op, const QNetworkRequest &request, QIODevice * outgoingData);
    void setLocalRoot(const QString &path);
    QString localRoot() const;
    ~MiniServer();
private:
    class MiniServerPrivate * d;

};

} // namespace HTTPService

#endif // MINISERVER_H
