#include "cr_hinteditor.h"
#include "ui_cr_hinteditor.h"
#include "schema/sch_game.h"
#include "core/platform_configuration.h"

namespace Creator {

HintEditor::HintEditor(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::HintEditor)
{
    ui->setupUi(this);
    ui->selector->setCurrentIndex(0);

    a_clear = new QAction(tr("Clear hint"), this);
    a_setText = new QAction(tr("Set plain text as hint"), this);
    a_setPicture = new QAction(tr("Load picture as hint..."), this);

    connect(a_clear, SIGNAL(triggered()), this, SLOT(clearHint()));
    connect(a_setText, SIGNAL(triggered()), this, SLOT(setText()));
    connect(a_setPicture, SIGNAL(triggered()), this, SLOT(setImage()));

    ui->btnClear1->setIcon(QIcon(Core::PlatformConfiguration::resourcesPath()+"/ui_creator/edit-delete.png"));
    ui->btnClear2->setIcon(QIcon(Core::PlatformConfiguration::resourcesPath()+"/ui_creator/edit-delete.png"));
    ui->btnClear3->setIcon(QIcon(Core::PlatformConfiguration::resourcesPath()+"/ui_creator/edit-delete.png"));

    ui->btnSetText->setIcon(QIcon(Core::PlatformConfiguration::resourcesPath()+"/ui_creator/insert-text.png"));
    ui->btnSEtPicture->setIcon(QIcon(Core::PlatformConfiguration::resourcesPath()+"/ui_creator/insert-image.png"));

    connect(ui->btnClear1, SIGNAL(clicked()), this, SLOT(clearHint()));
    connect(ui->btnClear3, SIGNAL(clicked()), this, SLOT(clearHint()));
    connect(ui->btnClear2, SIGNAL(clicked()), this, SLOT(clearHint()));

    connect(ui->btnSetText, SIGNAL(clicked()), this, SLOT(setText()));
    connect(ui->btnSEtPicture, SIGNAL(clicked()), this, SLOT(setImage()));


}


void HintEditor::sync()
{
    m_game->tasks[i_taskIndex].hintMimeType = s_mimetype;
    if (s_mimetype.isEmpty()) {
        m_game->tasks[i_taskIndex].hintData.clear();
    }
    if (s_mimetype.startsWith("text")) {
        m_game->tasks[i_taskIndex].hintData = ui->plainTextEdit->toPlainText().toLocal8Bit();
    }
    else {
        m_game->tasks[i_taskIndex].hintData = m_data;
    }
}

void HintEditor::setHint(Schema::Game *game, int index)
{
    m_game = game;
    i_taskIndex = index;
    s_mimetype = game->tasks[i_taskIndex].hintMimeType;
    m_data = game->tasks[i_taskIndex].hintData;
    if (s_mimetype=="text/plain") {
        ui->selector->setCurrentWidget(ui->plainTextHint);
        ui->plainTextEdit->setPlainText(QString::fromLocal8Bit(m_data));
        a_clear->setEnabled(true);
        a_setText->setEnabled(false);
        a_setPicture->setEnabled(false);
    }
    else if (s_mimetype=="image/png") {
        QImage img = QImage::fromData(m_data);
        QPixmap px = QPixmap::fromImage(img);
        ui->selector->setCurrentWidget(ui->imageHint);
        ui->image->setPixmap(px);
        a_clear->setEnabled(true);
        a_setText->setEnabled(false);
        a_setPicture->setEnabled(false);
    }
    else if (s_mimetype.startsWith("image/svg")) {
        ui->selector->setCurrentWidget(ui->drawingHint);
        ui->drawing->renderer()->load(m_data);
        a_clear->setEnabled(true);
        a_setText->setEnabled(false);
        a_setPicture->setEnabled(false);
    }
    else {
        ui->selector->setCurrentWidget(ui->noHint);
        a_clear->setEnabled(false);
        a_setText->setEnabled(true);
        a_setPicture->setEnabled(true);
    }
    updateWindowTitle();
}

void HintEditor::closeEvent(QCloseEvent *event)
{
    sync();
    event->accept();

}

void HintEditor::clearHint()
{
    ui->selector->setCurrentIndex(0);
    s_mimetype = "";
    m_data.clear();
    a_clear->setEnabled(false);
    a_setText->setEnabled(true);
    a_setPicture->setEnabled(true);
}

void HintEditor::setImage()
{
    QSettings settings;
    QString lastDir = settings.value("Creator/HintPicture/LastDir", QDesktopServices::storageLocation(QDesktopServices::PicturesLocation)).toString();
    const QString filter = tr("Supported pictures (*.png *.svg);;Raster images (*.png);;Vector drawings (*.svg)");
    const QString fileName = QFileDialog::getOpenFileName(this, tr("Load picture..."), lastDir, filter);
    if (!fileName.isEmpty()) {
        settings.setValue("Creator/HintPicture/LastDir", QFileInfo(fileName).dir().absolutePath());
        if (fileName.endsWith(".png")) {
            QFile f(fileName);
            if (!f.open(QIODevice::ReadOnly)) {
                QMessageBox::critical(this, tr("Can't load image"), tr("File not readable"));
                return;
            }
            m_data = f.readAll();
            f.close();
            QImage image;
            if (!image.loadFromData(m_data)) {
                QMessageBox::critical(this, tr("Can't load image"), tr("File is not PNG iamge"));
                return;
            }
            QPixmap px = QPixmap::fromImage(image);
            ui->image->setPixmap(px);
            ui->selector->setCurrentWidget(ui->imageHint);
            s_mimetype = "image/png";
            a_clear->setEnabled(true);
            a_setText->setEnabled(false);
            a_setPicture->setEnabled(false);
        }
        else if (fileName.endsWith(".svg")) {
            QFile f(fileName);
            if (!f.open(QIODevice::ReadOnly)) {
                QMessageBox::critical(this, tr("Can't load drawing"), tr("File not readable"));
                return;
            }
            m_data = f.readAll();
            f.close();
            if (!ui->drawing->renderer()->load(m_data)) {
                QMessageBox::critical(this, tr("Can't load drawing"), tr("File is not SVG drawing"));
                return;
            }
            ui->selector->setCurrentWidget(ui->drawingHint);
            s_mimetype = "image/svg+xml";
            a_clear->setEnabled(true);
            a_setText->setEnabled(false);
            a_setPicture->setEnabled(false);
        }
    }
}

void HintEditor::setText()
{
    ui->selector->setCurrentIndex(1);
    ui->plainTextEdit->clear();
    s_mimetype = "text/plain";
    a_clear->setEnabled(true);
    a_setText->setEnabled(false);
    a_setPicture->setEnabled(false);
}


void HintEditor::updateWindowTitle()
{
    setWindowTitle(m_game->title + " : " + m_game->tasks[i_taskIndex].title);
}

HintEditor::~HintEditor()
{
    delete ui;
}

} // namespace Creator
