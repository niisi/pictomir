#include "webkit/pageglobalobject.h"
#include "robot25d/default_plugin.h"
#include "robot25d/robotview.h"
#include "core/actionmanager.h"
#include "desktop/dockwidget.h"
#include "desktop/functionwidget.h"
#include "core/pictomirfunction.h"
#include "runtime/executorkrt.h"
#include "core/pictomirprogram.h"
#include "schema/sch_program.h"

#include <QNetworkRequest>
#include <QNetworkReply>

namespace Webkit {

class PageGlobalObjectPrivate
        : public QObject
{
    Q_OBJECT
public:
    QMap<QString, Robot25D::Plugin*> robots;
    QMap<QString, Core::ActionManager*> managers;
    QMap<QString, Core::PictomirProgram*> programs;
    QMap<QString, QList<Core::FunctionWidgetInterface*> > forms;
    QMap<QString, QList<QGraphicsView*> > formViews;
    QMap<QString, Desktop::DockWidget*> shelfs;
    QMap<QString, Runtime::ExecutorKRT*> executors;
    PageGlobalObject * q;
    QWebPage * page;

    bool clearing;
    QMutex *mutex_clearing;

    Core::ActionManager * createManager(const QString &group);
    Core::PictomirProgram * createProgram(const QString &group);
    Runtime::ExecutorKRT * createExecutor(const QString &group);

public slots:
    void handleRobotSync();
    void handleExecutionFinished(const QString &status);
    void handleHighlightRequest(int func, int cell);
    void clear();
    void createExecuableRuntimes();
};


void PageGlobalObjectPrivate::clear()
{
    mutex_clearing->lock();
    clearing = true;
    mutex_clearing->unlock();
    foreach (const QString &key, executors.keys()) {
        executors[key]->dispose();
        executors[key]->waitForStopped();
        executors[key]->deleteLater();
    }
    foreach (const QString &key, robots.keys()) {
        robots[key]->dispose();
        robots[key]->deleteLater();
    }
    foreach (const QString &key, forms.keys()) {
        for (int i=0; i<forms[key].size(); i++)
            forms[key][i]->deleteLater();
//        for (int i=0; i<formViews[key].size(); i++)
//            delete formViews[key][i];
    }
    foreach (const QString &key, programs.keys()) {
        programs[key]->deleteLater();
    }
    foreach (const QString &key, shelfs.keys()) {
        shelfs[key]->deleteLater();
    }
    foreach (const QString &key, managers.keys()) {
        managers[key]->deleteLater();
    }
    robots.clear();
    forms.clear();
    programs.clear();
    shelfs.clear();
    executors.clear();
    managers.clear();
}

PageGlobalObject::PageGlobalObject(QWebPage *page) :
    QObject(page)
{
    d = new PageGlobalObjectPrivate;
    d->q = this;
    d->page = page;
    d->clearing = false;
    d->mutex_clearing = new QMutex;

}

PageGlobalObject::~PageGlobalObject()
{
    delete d;
}

Core::ActionManager * PageGlobalObjectPrivate::createManager(const QString &group)
{
    Core::ActionManager * manager = NULL;
    if (managers.contains(group)) {
        manager = managers[group];
    }
    else {
        manager = new Core::ActionManager(q);
        managers[group] = manager;
        q->createRobot(group, QUrl());
        QList<Robot25D::Plugin*> ps;
        ps << robots[group];
        manager->createActions(ps);
    }
    return manager;
}

Core::PictomirProgram * PageGlobalObjectPrivate::createProgram(const QString &group)
{
    Core::PictomirProgram * program = NULL;
    if (programs.contains(group)) {
        program = programs[group];
    }
    else {
        Core::ActionManager * manager = createManager(group);
        Q_ASSERT(robots.contains(group));
        Robot25D::Plugin * actor = robots[group];
        Q_CHECK_PTR(manager);
        Q_CHECK_PTR(actor);
        Schema::Program emptyProgram;
        program = new Core::PictomirProgram(emptyProgram,
                                            actor,
                                            manager,
                                            q);
        programs[group] = program;
    }
    Q_CHECK_PTR(program);
    return program;
}

Runtime::ExecutorKRT * PageGlobalObjectPrivate::createExecutor(const QString &group)
{
    Runtime::ExecutorKRT * ex = NULL;
    if (executors.contains(group)) {
        ex = executors[group];
    }
    else {
        ex = new Runtime::ExecutorKRT(q);
        connect(ex, SIGNAL(executionFinished(QString)), this, SLOT(handleExecutionFinished(QString)));
        connect(ex, SIGNAL(highlightRequest(int,int)), this, SLOT(handleHighlightRequest(int,int)));

        executors[group] = ex;
    }
    Q_CHECK_PTR(ex);
    return ex;
}

QObject * PageGlobalObject::createRobot(const QString &group, const QUrl &env)
{
    Robot25D::Plugin * p = NULL;
    bool shouldConnect = false;
    if (!d->robots.contains(group)) {
        p = new Robot25D::Plugin();
        shouldConnect = true;
        d->robots.insert(group, p);
    }
    else {
        p = d->robots[group];
    }
    Q_CHECK_PTR(p);
    Robot25D::RobotView * w = p->mainWidget();
    if (shouldConnect) {
        connect(w, SIGNAL(sync()), d, SLOT(handleRobotSync()));
    }
    if (!env.isEmpty()) {
        QNetworkRequest req(env);
        QNetworkReply * reply = d->page->networkAccessManager()->get(req);
        connect(reply, SIGNAL(finished()), p, SLOT(handleNetworkLoadEnvFinished()));
        connect(reply, SIGNAL(readyRead()), p, SLOT(handleNetworkLoadEnvReadyRead()));
        connect(reply, SIGNAL(uploadProgress(qint64,qint64)), p, SLOT(handleNetworkLoadProgress(qint64, qint64)));
    }
    QGraphicsView * view =  new QGraphicsView();
    view->setFrameShape(QFrame::NoFrame);
    view->setRenderHint(QPainter::Antialiasing, true);
    view->setScene(new QGraphicsScene);
    view->scene()->addItem(w);
    return view;
}

QObject * PageGlobalObject::createShelf( const QString &group
                                        , bool plain
                                        , bool cond
                                        , bool rep
                                        , int fc
                                        , int tableWidth
                                        , const QColor &tableColor
                                        )
{
    Core::ActionManager * manager = NULL;
    if (d->managers.contains(group)) {
        manager = d->managers[group];
    }
    else {
        manager = new Core::ActionManager(this);
        d->managers[group] = manager;
        createRobot(group, QUrl());
        QList<Robot25D::Plugin*> ps;
        ps << d->robots[group];
        manager->createActions(ps);
    }
    Q_CHECK_PTR(manager);
    manager->setEnabledActionsList(d->robots[group], plain, cond, rep, fc);
    Desktop::DockWidget * dock= NULL;
    if (!d->shelfs.contains(group)) {
        dock = new Desktop::DockWidget();
        dock->setDrawBackground(false);
    }
    else {
        dock = d->shelfs[group];
    }
    QGraphicsView * dockView = new QGraphicsView();
    dockView->setFrameShape(QFrame::NoFrame);
    dockView->setRenderHint(QPainter::Antialiasing, true);
    dockView->setScene(new QGraphicsScene);
    dockView->scene()->addItem(dock);
    dock->setRect(QRectF(0,0,tableWidth>=0? tableWidth : 400,90));
    dock->setTableColor(tableColor);
    dock->setActionManager(manager);
    dock->updateVisiblity();
    return dockView;
}

QObject * PageGlobalObject::createForm(const QString &group,
                                      int no,
                                      bool cnd, bool rep, int w, int h,
                                      const QString &title,
                                      const QStringList &hints,
                                      const QStringList &initial)
{
    Q_UNUSED(title);
    Q_UNUSED(hints);
    Core::ActionManager * manager = d->createManager(group);
    Core::PictomirProgram * program = d->createProgram(group);
    Q_CHECK_PTR(manager);
    Q_CHECK_PTR(program);
    QList<Core::FunctionWidgetInterface*> forms = d->forms.value(group, QList<Core::FunctionWidgetInterface*>());
    QList<QGraphicsView*> formViews = d->formViews.value(group, QList<QGraphicsView*>());
    Q_UNUSED(no);
    Core::FunctionModifiers mods;
    mods |= Core::PlainFunction;
    if (cnd)
        mods |= Core::ConditionFunction;
    if (rep)
        mods |= Core::RepeatFunction;
    Core::PictomirFunction * f = program->addAlgorhitm(QSize(w,h), mods);
    int index = 0;
    for (int i=0; i<initial.size(); i++) {
        const QString sc = initial[i];
        const Core::ActionModel * model = manager->modelByShortcut(sc);
        if (rep && i==0) {
            f->setRepeater(model);
        }
        else if (cnd && i==1) {
            f->setCondition(model);
        }
        else {
            index = i + (rep?-1:0) + (cnd?-1:0);
            if (index>=0) {
                f->setAction(index, model);
            }
        }
    }

    Desktop::FunctionWidget * fw = new Desktop::FunctionWidget(f);

    QGraphicsView * view = new QGraphicsView(d->page->view());
    view->setFrameShape(QFrame::NoFrame);
    view->setRenderHint(QPainter::Antialiasing, true);
    view->setScene(new QGraphicsScene);
    view->scene()->addItem(fw);

    Q_CHECK_PTR(fw->scene());
    Q_ASSERT(!fw->scene()->views().isEmpty());
    Q_CHECK_PTR(fw->scene()->views()[0]);
    QGraphicsView * container = fw->scene()->views()[0];
    forms << fw;
    formViews << container;

    d->forms[group] = forms;
    d->formViews[group] = formViews;

    return container;
}

void PageGlobalObjectPrivate::createExecuableRuntimes()
{
    QSet<QString> fgroups = QSet<QString>::fromList( programs.keys() );
    QSet<QString> rgroups = QSet<QString>::fromList( robots.keys() );
    QSet<QString> groups = (fgroups & rgroups) - QSet<QString>::fromList(QList<QString>() << "");
    foreach (const QString &group, groups) {
        createExecutor(group);
    }
}

void PageGlobalObjectPrivate::handleHighlightRequest(int func, int cell)
{
    mutex_clearing->lock();
    bool skip = clearing;
    mutex_clearing->unlock();
    if (skip)
        return;
    Runtime::ExecutorKRT * ex = qobject_cast<Runtime::ExecutorKRT*>(sender());
    Q_CHECK_PTR(ex);
    QString groupName;
    Runtime::ExecutorKRT * exx = NULL;
    for (int i=0; i<executors.keys().size(); i++) {
        const QString group = executors.keys()[i];
        exx = executors[group];
        if (exx==ex) {
            groupName = group;
            break;
        }
    }
    QList<Core::FunctionWidgetInterface*> fws = forms[groupName];
    Q_ASSERT(func < fws.size());
    fws[func]->highlightPlainCell(cell);
}

void PageGlobalObjectPrivate::handleRobotSync()
{
    mutex_clearing->lock();
    bool skip = clearing;
    mutex_clearing->unlock();
    if (skip)
        return;
//    QObject * o = sender();
//    qDebug() << o->metaObject()->className();

    Robot25D::RobotView * v = qobject_cast<Robot25D::RobotView*>(sender());
    Q_CHECK_PTR(v);
    Robot25D::Plugin * p = v->plugin();
    Q_CHECK_PTR(p);
    Robot25D::Plugin * pp = NULL;
    QString groupName;
    for (int i=0; i<robots.keys().size(); i++) {
        const QString group = robots.keys()[i];
        pp = robots[group];
        if (pp==p) {
            groupName = group;
            break;
        }
    }

    if (executors.contains(groupName)) {
        Runtime::ExecutorKRT * executor = executors[groupName];
        Q_CHECK_PTR(executor);
        executor->commandFinished();
    }
    else {
        QString handlerFunc;
        QString argument;
        if (p->lastError().isEmpty()) {
            handlerFunc = "onRobotCommandFinished";
            argument = p->lastResult().toBool()? "true" : "false";
        }
        else {
            handlerFunc = "onRobotCommandFailed";
            argument = "\""+p->lastError()+"\"";
        }
        groupName = "\"" + groupName + "\"";
        const QString call = handlerFunc+"("+groupName+","+argument+")";
        page->currentFrame()->evaluateJavaScript(call);
    }

}

void PageGlobalObjectPrivate::handleExecutionFinished(const QString &status)
{
    mutex_clearing->lock();
    bool skip = clearing;
    mutex_clearing->unlock();
    if (skip)
        return;
    Q_UNUSED(status);
    Runtime::ExecutorKRT * ex = qobject_cast<Runtime::ExecutorKRT*>(sender());
    Q_CHECK_PTR(ex);
    QString groupName;
    Runtime::ExecutorKRT * exx;
    for (int i=0; i<executors.keys().size(); i++) {
        const QString group = executors.keys()[i];
        exx = executors[group];
        if (exx==ex) {
            groupName = group;
            break;
        }
    }
    Q_ASSERT(forms.contains(groupName));
    QList<Core::FunctionWidgetInterface*> myforms = forms[groupName];
    for (int i=0; i<myforms.size(); i++) {
        myforms[i]->unlock();
    }
    Robot25D::Plugin * p = robots[groupName];
    Q_CHECK_PTR(p);
    bool error = !p->lastError().isEmpty();
    int unpaintedCells = p->checkEvaluationResult().unpaintedCells;
    QList<Core::FunctionWidgetInterface*> fws = forms[groupName];
    Core::ActionManager * manager = managers[groupName];
    Q_CHECK_PTR(manager);
    manager->unlock();

    if (error) {
        const QStack< QPair<int,int> > backtrace = ex->backtrace();
        for (int i=0; i<backtrace.size(); i++) {
            int fn = backtrace[i].first;
            int cn = backtrace[i].second;
            Q_ASSERT(fn<fws.size());
            fws[fn]->highlightErrorCell(cn);
        }
    }
    else {
        for (int i=0; i<fws.size(); i++) {
            fws[i]->unhighlight();
        }
    }

    QString handlerFunc;
    QString argument;
    handlerFunc = "onEvaluationFinished";
    argument = error? "\"crash\"" : QString::number(unpaintedCells);

    groupName = "\"" + groupName + "\"";
    const QString call = handlerFunc+"("+groupName+","+argument+")";
    page->currentFrame()->evaluateJavaScript(call);
}

void PageGlobalObject::evaluateCommand(const QString &groupName, const QString &command)
{
    if (d->robots.contains(groupName)) {
        Robot25D::Plugin * p = d->robots[groupName];
        Q_CHECK_PTR(p);
        Robot25D::RobotView * v = p->mainWidget();
        v->evaluateCommand(command);
    }
}

void PageGlobalObject::resetActor(const QString &groupName)
{
    if (d->robots.contains(groupName)) {
        Robot25D::Plugin * p = d->robots[groupName];
        Q_CHECK_PTR(p);
        p->reset();
    }
}

int PageGlobalObject::underdoneCellsLeft(const QString &groupName) const
{
    int result = 0;
    if (d->robots.contains(groupName)) {
        Robot25D::Plugin * p = d->robots[groupName];
        Q_CHECK_PTR(p);
        result = p->mainWidget()->unpaintedPoints();
    }
    return result;
}

void PageGlobalObject::reload()
{
    d->clear();
}

void PageGlobalObject::prepare()
{
    d->createExecuableRuntimes();
    d->mutex_clearing->lock();
    d->clearing = false;
    d->mutex_clearing->unlock();
}

void PageGlobalObject::start(const QString &groupName)
{
    Runtime::ExecutorKRT * ex = d->executors.value(groupName, NULL);
    if (ex) {
        Q_ASSERT(d->forms.contains(groupName));
        QList<Core::FunctionWidgetInterface*> myforms = d->forms[groupName];
        for (int i=0; i<myforms.size(); i++) {
            myforms[i]->lock();
        }
        Robot25D::Plugin * actor = d->robots[groupName];
        Core::PictomirProgram * program = d->programs[groupName];
        Core::ActionManager * manager = d->managers[groupName];
        Q_CHECK_PTR(actor);
        Q_CHECK_PTR(program);
        Q_CHECK_PTR(manager);
        const QStringList pt = program->m68kProgram();
        manager->lock();
        ex->init(actor, pt);
        ex->continuousRun();
    }
}

void PageGlobalObject::stop(const QString &groupName)
{
    Runtime::ExecutorKRT * ex = d->executors.value(groupName, NULL);
    if (ex) {
        ex->stop();
    }
}

} // namespace Webkit

#include "pageglobalobject.moc"
