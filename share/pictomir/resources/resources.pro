TEMPLATE=subdirs
SUBDIRS=

r.files=*.*
r.path=/share/pictomir/resources/

icons_1.files=icons/actions/*.*
icons_1.path=/share/pictomir/resources/icons/actions/

icons_2.files=icons/conditions/*.*
icons_2.path=/share/pictomir/resources/icons/conditions/

icons_3.files=icons/repeat/*.*
icons_3.path=/share/pictomir/resources/icons/repeat/

ui_d.files=ui_desktop/*.*
ui_d.path=/share/pictomir/resources/ui_desktop/

ui_d_t.files=ui_desktop/teacher_mode/*.*
ui_d_t.path=/share/pictomir/resources/ui_desktop/teacher_mode/

ui_s.files=ui_smartphone_and_mid/*.*
ui_s.path=/share/pictomir/resources/ui_smartphone_and_mid/

ui_w.files=ui_webkit/icons/*.png
ui_w.path=/share/pictomir/resources/ui_webkit/icons/

ui_c.files=ui_creator/*.*
ui_c.path=/share/pictomir/resources/ui_creator/

cc_lic.files=cc_licensees/*.png
cc_lic.path=/share/pictomir/resources/cc_licensees/

robot.files=robot_frames/*.*
robot.path=/share/pictomir/resources/robot_frames/

INSTALLS += r icons_1 icons_2 icons_3 ui_d ui_d_t ui_s ui_w robot ui_c cc_lic

