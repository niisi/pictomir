#ifndef WEBMAINWINDOW_H
#define WEBMAINWINDOW_H

#include <QtGui>
#include <QtWebKit>

namespace Core {
    class PictomirCommon;
}

namespace Util {
    class AboutDialog;
}

namespace Webkit {

class WebTab;

namespace Ui {
    class MainWindow;
}


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(Core::PictomirCommon *pictomir, QWidget *parent = 0);
    ~MainWindow();

protected slots:
    void btnShowMenuClicked();
    void closeCurrentTab();
    void toggleMenubar(bool v);
    void toggleFullscreen(bool v);
    void tabIconChanged();
    void tabTitleChanged(const QString &t);
    void tabChanged(int no);
    void tabCloseRequest(int no);
    void saveSettings();
    void restoreSettings();
    void showUsage();
    void showAbout();
    WebTab * addTab(QWebPage * page);
    void createNewTab();
    void createNewTab(QWebPage * page);


private:
    Ui::MainWindow *ui;
    Core::PictomirCommon * m_pictomir;
    Util::AboutDialog * m_about;
};


} // namespace Webkit
#endif // WEBMAINWINDOW_H
