#ifndef CONFIG_H
#define CONFIG_H

//#define USE_CUSTOM_WINDOW_DECORATION

#define USE_ANTIALIASING true

#ifdef Q_WS_S60
    #undef USE_ANTIALIASING
    #define USE_ANTIALIASING false
#endif

#ifdef Q_WS_QWS
    #undef USE_ANTIALIASING
    #define USE_ANTIALIASING false
#endif

#ifdef Q_OS_SYMBIAN
    #undef USE_ANTIALIASING
    #define USE_ANTIALIASING false
#endif

#ifdef Q_OS_WINCE
    #undef USE_ANTIALIASING
    #define USE_ANTIALIASING false
#endif

#endif // CONFIG_H
