#ifndef PICTOMIRINFO_H
#define PICTOMIRINFO_H

#include <QtCore>

namespace Util {

class PictomirInfo :
        public QObject
{
    Q_OBJECT
    Q_PROPERTY ( QString version READ version )
    Q_PROPERTY ( QString revision READ revision )
public:
    PictomirInfo(QObject *parent = 0);
    static PictomirInfo *instance();
public slots:
    QString version() const;
    QString revision() const;
private:
    static PictomirInfo *m_instance;
};

}

#endif // PICTOMIRINFO_H
