HEADERS += util/constants.h util/contentprovider.h util/pictomirinfo.h util/svgbutton.h util/svgrenderfarm.h \
    util/fp.h \
    util/svgsimplebutton.h \
    util/aboutdialog.h
SOURCES += util/contentprovider.cpp util/pictomirinfo.cpp util/svgbutton.cpp util/svgrenderfarm.cpp \
    util/svgsimplebutton.cpp \
    util/aboutdialog.cpp

FORMS += \
    util/aboutdialog.ui
