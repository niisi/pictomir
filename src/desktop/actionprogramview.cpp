#include "desktop/actionprogramview.h"
#include "core/actionmodel.h"
#include "core/actionmanager.h"

using namespace Core;

namespace Desktop {

ActionProgramView::ActionProgramView(ActionModel *model, QGraphicsItem *parent)
    : QGraphicsSvgItem(parent)
{
    m_model = model;
    setSharedRenderer(model->m_renderer);
    b_mouseClick = false;
    setAcceptHoverEvents(true);
    b_active = false;

    an_pulsing = new QPropertyAnimation(this);
    an_pulsing->setPropertyName("pulse");
    an_pulsing->setTargetObject(this);
    an_pulsing->setKeyValueAt(0.5, 1.0);
    an_pulsing->setStartValue(0.0);
    an_pulsing->setEndValue(0.0);
    an_pulsing->setDuration(800);
    an_pulsing->setLoopCount(-1);

    an_stopPulsing = new QPropertyAnimation(this);
    an_stopPulsing->setPropertyName("pulse");
    an_stopPulsing->setTargetObject(this);
    an_stopPulsing->setEndValue(0.0);
    an_stopPulsing->setDuration(150);
    an_stopPulsing->setLoopCount(1);

    an_fadeOut = new QPropertyAnimation(this, "opacity", this);
    an_fadeOut->setStartValue(1.0);
    an_fadeOut->setEndValue(0.0);
    an_fadeOut->setDuration(300);

    an_fadeIn = new QPropertyAnimation(this, "opacity", this);
    an_fadeIn->setEndValue(1.0);
    an_fadeIn->setDuration(300);
    connect (an_fadeOut, SIGNAL(finished()), this, SLOT(handleFadeOutFinished()));
}

void ActionProgramView::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if (m_model->manager()->isLocked()) {
        event->ignore();
        return;
    }
    b_mouseClick = true;
    an_fadeOut->setDuration(2000);
    an_fadeOut->start();
    event->accept();
}

void ActionProgramView::setPulse(qreal value)
{
    r_pulse = value;
    qreal scaleFactor = 1.0 - 0.3 * r_pulse;
    setTransformOriginPoint(boundingRect().center());
    setScale(scaleFactor);
//    qreal angle = (1.0-cos(r_pulse*3.14159)) * 10;
//    setRotation(angle);
    update();
}

void ActionProgramView::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    if (m_model->manager()->isLocked() || !b_mouseClick) {
        b_mouseClick = false;
        event->ignore();
        return;
    }
    event->accept();
    an_fadeOut->stop();
    setProperty(an_fadeIn->propertyName(), an_fadeIn->endValue());
    emit clicked(event->button()==Qt::RightButton);
}

void ActionProgramView::setActive(bool active)
{
    if (b_active && !active) {
        an_pulsing->stop();
        an_stopPulsing->setStartValue(an_pulsing->currentValue());
        an_stopPulsing->start();
    }
    b_active = active;
    if (b_active) {
        an_pulsing->start();
    }
}

void ActionProgramView::dispose()
{
    if (isVisible()) {
        an_pulsing->stop();
        an_stopPulsing->stop();
        if (!b_mouseClick) {
            an_fadeOut->setDuration(300);
            an_fadeOut->setStartValue(an_fadeOut->currentValue());
            an_fadeOut->start();
        }
        else {
            deleteLater();
        }
    }
    else {
        deleteLater();
    }
}

void ActionProgramView::handleFadeOutFinished()
{
    qreal value = an_fadeOut->currentValue().toDouble();
    if (value!=0.0)
        return;
    if (!b_mouseClick) {
        deleteLater();
    }
    else {
        emit clicked(true);
    }
}
#ifndef QT_NO_CURSOR
void ActionProgramView::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    if ( QLineF(event->screenPos(), event->buttonDownScreenPos(Qt::LeftButton))
        .length() < QApplication::startDragDistance() ) {
        return;
    }
    b_mouseClick = false;
    an_fadeOut->stop();
    setVisible(false);
    QDrag *drag = new QDrag(event->widget());
    QPixmap px = QPixmap::fromImage(m_model->image());
    drag->setPixmap(px);
#ifndef Q_WS_X11
    drag->setHotSpot(QPoint(px.width()/2, px.height()/2));
#endif
    QMimeData *data = new QMimeData;
    data->setProperty("mimetype","ActionItem");
    data->setProperty("modelId", m_model->modelId());
    drag->setMimeData(data);
    emit dragged();
    drag->exec();
}
#endif

ActionModel* ActionProgramView::model()
{
    return m_model;
}

int ActionProgramView::modelId() const
{
    return m_model->modelId();
}

bool ActionProgramView::isEnabled() const
{
    return m_model->isEnabled();
}

QSize ActionProgramView::originalSize() const
{
    return m_model->m_renderer->defaultSize();
}

QSize ActionProgramView::currentSize() const
{
    return boundingRect().size().toSize();
}

}
