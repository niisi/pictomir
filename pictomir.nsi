!include MUI2.nsh
 
OutFile "pictomir-win32-install.exe"

Name "�������� �� ������"
InstallDir "$PROGRAMFILES\Pictomir"
RequestExecutionLevel admin


!insertmacro MUI_PAGE_LICENSE "LICENSE_RU.rtf"

!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH
!insertmacro MUI_LANGUAGE "Russian"

Section "Pictomir" Pictomir

	SetOutPath "$INSTDIR"
	File LICENSE_RU.rtf
	File vcredist_x86.exe
	SetOutPath "$INSTDIR\bin"
	File /r bin\*
	SetOutPath "$INSTDIR\share"
	File /r share\*
	SetOutPath "$INSTDIR\win32"
	File /r win32\*.ico
	
	ExecWait '"$INSTDIR\vcredist_x86.exe" /passive'
	Delete /REBOOTOK "$INSTDIR\vcredist_x86.exe"

	CreateDirectory "$SMPROGRAMS\�������� �� ������"
	CreateShortCut  "$SMPROGRAMS\�������� �� ������\��������.lnk" "$INSTDIR\bin\pictomir.exe" "" "$INSTDIR\win32\pictomir.ico"
	CreateShortCut  "$SMPROGRAMS\�������� �� ������\��������-�����������.lnk" "$INSTDIR\bin\pictomir.exe" "--ui=creator" "$INSTDIR\win32\pictomir-creator.ico"
	CreateShortCut  "$SMPROGRAMS\�������� �� ������\��������-���������.lnk" "$INSTDIR\bin\pictomir.exe" "--ui=navigator" "$INSTDIR\win32\pictomir-navigator.ico"

	
SectionEnd