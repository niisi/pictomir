#include "cr_environmenteditor.h"
#include "ui_cr_environmenteditor.h"
#include "robot25d/default_plugin.h"
#include "robot25d/robotview.h"
#include "core/platform_configuration.h"
#include "creator/newenvironmentdialog.h"

namespace Creator {

EnvironmentEditor::EnvironmentEditor(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EnvironmentEditor)
{
    ui->setupUi(this);
    setMinimumWidth(400);
    sch_game = 0;
    i_taskIndex = 0;

    connect(ui->btnHand, SIGNAL(clicked()), this, SLOT(handleAction()));
    connect(ui->btnRotate, SIGNAL(clicked()), this, SLOT(handleAction()));
    connect(ui->btnWall, SIGNAL(clicked()), this, SLOT(handleAction()));
    connect(ui->btnPaint, SIGNAL(clicked()), this, SLOT(handleAction()));
    connect(ui->btnPoint, SIGNAL(clicked()), this, SLOT(handleAction()));


    robotPlugin = new Robot25D::Plugin();
    robotPlugin->setTeacherMode(true);
    robot = robotPlugin->mainWidget();
    ui->view->setBackgroundBrush(palette().brush(QPalette::Window));
    ui->view->setRenderHint(QPainter::Antialiasing, true);
    ui->view->setScene(new QGraphicsScene);
    ui->view->scene()->addItem(robot);


    a_new = new QAction(tr("New..."), this);
    connect(a_new, SIGNAL(triggered()), this, SLOT(newEnvironment()));

    a_import = new QAction(tr("Import from Kumir..."), this);
    connect(a_import, SIGNAL(triggered()), this, SLOT(importEnvironment()));

    a_export = new QAction(tr("Export to Kumir..."), this);
    connect(a_export, SIGNAL(triggered()), this, SLOT(exportEnvironment()));

    newDialog = new NewEnvironmentDialog(this);

    connect(ui->btnNew, SIGNAL(clicked()), this, SLOT(newEnvironment()));
    connect(ui->btnLoad, SIGNAL(clicked()), this, SLOT(importEnvironment()));

    setIcons();
}



void EnvironmentEditor::setIcons()
{
    ui->btnHand->setIcon(QIcon(Core::PlatformConfiguration::resourcesPath()+"ui_desktop/teacher_mode/arrow.svg"));
    ui->btnRotate->setIcon(QIcon(Core::PlatformConfiguration::resourcesPath()+"ui_desktop/teacher_mode/robot.svg"));
    ui->btnWall->setIcon(QIcon(Core::PlatformConfiguration::resourcesPath()+"ui_desktop/teacher_mode/walls.svg"));
    ui->btnPaint->setIcon(QIcon(Core::PlatformConfiguration::resourcesPath()+"ui_desktop/teacher_mode/paint.svg"));
    ui->btnPoint->setIcon(QIcon(Core::PlatformConfiguration::resourcesPath()+"ui_desktop/teacher_mode/point.svg"));

    ui->btnNew->setIcon(QIcon(Core::PlatformConfiguration::resourcesPath()+"/ui_creator/document-new.png"));
    ui->btnLoad->setIcon(QIcon(Core::PlatformConfiguration::resourcesPath()+"/ui_creator/document-import.png"));

    ui->frame->setFixedWidth(56);
}

void EnvironmentEditor::setEnvironment(Schema::Game *game, int index)
{
    Q_CHECK_PTR(game);
    Q_ASSERT(index>=0);
    Q_ASSERT(index<game->tasks.size());
    sch_game = game;
    i_taskIndex = index;
    Schema::Environment env = game->tasks[index].environment;
    setEnvironment(env);
    updateWindowTitle();
}

void EnvironmentEditor::updateWindowTitle()
{
    if (sch_game)
        setWindowTitle(sch_game->title + " : " + sch_game->tasks[i_taskIndex].title);
}

void EnvironmentEditor::setEnvironment(const Schema::Environment &env)
{
    robot->loadEnvironment(env);
}

Schema::Environment EnvironmentEditor::environment() const
{
    return robot->environment();
}

void EnvironmentEditor::handleAction()
{
    QToolButton * btn = qobject_cast<QToolButton*>(sender());
    Q_CHECK_PTR(btn);

    QList<QToolButton*> btns;
    btns << ui->btnHand << ui->btnRotate << ui->btnWall << ui->btnWall << ui->btnPaint << ui->btnPoint;
    btns.removeAll(btn);

    btn->setChecked(true);
    foreach (QToolButton *b, btns) {
        b->setChecked(false);
    }
    if (btn==ui->btnHand)
        robot->setEditMode("hand");
    else if (btn==ui->btnRotate)
        robot->setEditMode("robot");
    else if (btn==ui->btnWall)
        robot->setEditMode("wall");
    else if (btn==ui->btnPaint)
        robot->setEditMode("paint");
    else if (btn==ui->btnPoint)
        robot->setEditMode("point");
}

void EnvironmentEditor::newEnvironment()
{
    if (newDialog->exec()==QDialog::Accepted) {
        QSize sz = newDialog->environemntSize();
        Schema::Environment e;
        e.size = sz;
        e.position = QPoint(0,0);
        e.direction = Schema::Environment::South;
        setEnvironment(e);
    }
}

void EnvironmentEditor::importEnvironment()
{
    const QString filter = tr("Kumir robot environment files (*.fil)");
    const QString errTitle = tr("Can't import environment from Kumir");
    QSettings settings;
    QString lastDir = settings.value("Creator/Env/LastDir", QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation)).toString();
    const QString fileName = QFileDialog::getOpenFileName(this, tr("Load environment..."), lastDir, filter);
    if (!fileName.isEmpty()) {
        Schema::Environment e;
        QFile f(fileName);

        if (!f.open(QIODevice::ReadOnly)) {
            QMessageBox::critical(this, errTitle, tr("File is not readable"));
            return;
        }

        QByteArray data = f.readAll();
        f.close();

        if (!Schema::parceKumirFil(data, e)) {
            QMessageBox::critical(this, errTitle, tr("File is not robot environment or file damaged"));
            return;
        }

        setEnvironment(e);
        settings.setValue("Creator/Env/LastDir", fileName);
    }
}

void EnvironmentEditor::exportEnvironment()
{
    const QString filter = tr("Kumir robot environment files (*.fil)");
    const QString errTitle = tr("Can't export environment to Kumir");
    QSettings settings;
    QString lastDir = settings.value("Creator/Env/LastDir", QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation)).toString();
    const QString fileName = QFileDialog::getSaveFileName(this, tr("Save environment..."), lastDir, filter);
    if (!fileName.isEmpty()) {
        const Schema::Environment e = environment();
        QFile f(fileName);

        if (!f.open(QIODevice::WriteOnly)) {
            QMessageBox::critical(this, errTitle, tr("File is not writable"));
            return;
        }

        QByteArray data = Schema::generateKumFil(e).toUtf8();
        f.write(data);
        f.close();
        settings.setValue("Creator/Env/LastDir", fileName);
    }
}

void EnvironmentEditor::sync()
{
    Schema::Environment current = robot->environment();
    sch_game->tasks[i_taskIndex].environment = current;
}

void EnvironmentEditor::closeEvent(QCloseEvent *event)
{
    sync();
    event->accept();
}

EnvironmentEditor::~EnvironmentEditor()
{
    delete ui;
}

} // namespace Creator
