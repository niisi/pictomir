#ifndef ACTIONMODEL_H
#define ACTIONMODEL_H

#include <QtCore>
#include <QtGui>
#include <QtSvg>

#include "actiontype.h"
#include "enums.h"
#include "schema/sch_command.h"

namespace Desktop {
    class ActionDockView;
    class ActionProgramView;
}

namespace Mobile {
    class ActionMiniDockView;
}

namespace Robot25D {
    class Plugin;
}

namespace Core {

class ActionView;
class ActionManager;

class ActionModel
    : public QObject
{
    friend class Desktop::ActionDockView;
    friend class Desktop::ActionProgramView;
    friend class Mobile::ActionMiniDockView;
    friend class ActionManager;
    Q_OBJECT;
    Q_PROPERTY(int modelId READ modelId);
public:

    int modelId() const;

    explicit ActionModel(const QString &actionName,
                        int actionId,
                        const QString &iconFileName,
                        const QString &actionText,
                        Schema::Command stardartCommand,
                        Robot25D::Plugin *module,
                        ActionManager * manager,
                        ActionType actionType,
                        ActionCategory category);
    QString svgData() const;
    ActionManager * manager() const;
    QString imageData() const;
    QImage image() const;
    inline void setEnabled(bool v) { b_enabled = v; }
    inline bool isEnabled() { return b_enabled; }
    inline Schema::Command standartCommand() const { return sch_command; }
    inline QString actionName() const { return s_actionName; }
    inline ActionType actionType() const { return e_type; }
    inline ActionCategory actionCategory() const { return e_category; }
    QString text() const;
    inline Robot25D::Plugin* module() const { return ptr_module; }

    inline int actionId() const { return i_actionId; }



protected:
    void initModelId();

private:
    bool b_enabled;
    QString s_actionName;
    QString s_text;
    QString s_iconFileName;
    ActionType e_type;
    ActionCategory e_category;
    Schema::Command sch_command;
    int i_actionId;
    int i_modelId;
    QSvgRenderer *m_renderer;
    Robot25D::Plugin *ptr_module;
    ActionManager * ptr_manager;

};

}
#endif // ACTIONMODEL_H
