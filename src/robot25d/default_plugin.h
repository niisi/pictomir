#ifndef PLUGIN_H
#define PLUGIN_H

#include <QtCore>
#include "robot25d/roboterrors.h"
#include "schema/sch_command.h"
#include "schema/sch_environment.h"
#include <QtGui>

namespace Robot25D {

class RobotView;

struct Command {
    int id;
    QString name;
    QString tooltip;
    QMap<QLocale::Language, QString> name_i18n;
    QMap<QLocale::Language, QString> tooltip_i18n;
    QString shortcut;
    QByteArray svgImageData;
    Schema::Command standartCommand;
};

struct TaskScore {
    quint8 value;
    quint16 unpaintedCells;
    QString comment;
};

class Plugin :
        public QObject
 {
    Q_OBJECT;
public:
    explicit Plugin();

    QString name(const QLocale::Language language = QLocale::C) const;
    QList<Command> conditions() const;
    QList<Command> actions() const;


    void init();
    bool loadData(const Schema::Environment &env);
    bool loadData(const QByteArray &data);
    QByteArray saveData() const;
    Schema::Environment saveData2() const;

    QVariant lastResult() const;
    QString lastError(const QLocale::Language language = QLocale::C) const;

    TaskScore checkEvaluationResult(const QLocale::Language language = QLocale::C) const;


    RobotView* mainWidget();
//    QGraphicsRectItem* editorWidget();

    inline void setTeacherMode(bool value) { b_teacherMode = value; }

    QList<QMenu*> menuBar();
    static QString moduleImagesRoot();


public slots:
    void reset();
    void evaluateCommandAnimated(int id);
    void evaluateCommandHidden(int id);
    void finishEvaluation();
    void dispose();

    void loadEnvironmentClicked();
    void loadDefaultEnvironment();

    void handleNetworkLoadEnvStarted();
    void handleNetworkLoadEnvFinished();
    void handleNetworkLoadEnvReadyRead();
    void handleNetworkLoadProgress(qint64, qint64);

private:
    void createMenuBar();
    void createMainWidget();
    void createEditor();
    void evaluateCommand(int id, bool animate);

    RobotView *m_robotView;
    QWidget *m_editor;
    bool m_lastResult;
    QMap<QString,QByteArray> m_icons;
    int m_stepsToDone;
	QString s_name_ru;
	QString s_name_en;
	bool b_teacherMode;


	QByteArray ba_networkData;


    QAction *a_actionLoadEnvironment;
    QAction *a_actionDefaultEnvironment;
    QList<QMenu*> m_menuBar;

signals:
    void signalSync();
};

}
#endif // PLUGIN_H
