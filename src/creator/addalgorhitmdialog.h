#ifndef ADDALGORHITMDIALOG_H
#define ADDALGORHITMDIALOG_H

#include <QDialog>
namespace Ui {
    class AddAlgorhitmDialog;
}

namespace Creator {


class AddAlgorhitmDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AddAlgorhitmDialog(QWidget *parent = 0);
    ~AddAlgorhitmDialog();
    QSize programSize() const;
    bool hasCondition() const;
    bool hasRepeater() const;
    void setProgramSize(const QSize &sz);
    void setCondition(bool);
    void setRepeater(bool);
    static AddAlgorhitmDialog* instance();

private:
    Ui::AddAlgorhitmDialog *ui;
    static AddAlgorhitmDialog* m_instance;
};

}

#endif // ADDALGORHITMDIALOG_H
