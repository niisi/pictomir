#ifndef PAGEGLOBALOBJECT_H
#define PAGEGLOBALOBJECT_H

#include <QtCore>
#include <QtScript>
#include <QtWebKit>


namespace Webkit {

class PageGlobalObject
        : public QObject
        , protected QScriptable
{
    friend class PageGlobalObjectPrivate;
    Q_OBJECT
public:
    explicit PageGlobalObject(QWebPage * page);
    ~PageGlobalObject();
public slots:
    void reload();
    void prepare();
    void start(const QString &groupName);
    void stop(const QString &groupName);
    void evaluateCommand(const QString &groupName, const QString &command);
    int underdoneCellsLeft(const QString &groupName) const;
    void resetActor(const QString &groupName);

    QObject * createRobot(const QString &group, const QUrl & env);
    QObject * createShelf(const QString &group, bool pl, bool cnd,
                          bool rep, int fc,
                          int tableWidth, const QColor &tableColor
                          );
    QObject * createForm(const QString &group,
                         int no, bool cnd, bool rep, int w, int h,
                         const QString &title,
                         const QStringList &hints,
                         const QStringList &initial);

protected:
    class PageGlobalObjectPrivate * d;

};

} // namespace Webkit

#endif // PAGEGLOBALOBJECT_H
