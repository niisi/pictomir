#ifndef PROGRAMTYPE_H
#define PROGRAMTYPE_H

namespace Core {
    enum ProgramType {
        ProgramInitial,
        ProgramSaved,
        ProgramCurrent
    };
}

#endif // PROGRAMTYPE_H
