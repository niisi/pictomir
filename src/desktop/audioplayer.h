#ifndef AUDIOPLAYER_H
#define AUDIOPLAYER_H
#include <QtCore>
#include <phonon/mediaobject.h>
#include <phonon/mediasource.h>
#include <phonon/audiooutput.h>
#include <phonon/path.h>

namespace Desktop {

class AudioPlayer :
        public QObject
{
    Q_OBJECT;
public:
    AudioPlayer(QObject *parent);
public slots:
    void play(const QString &soundName);
private slots:
    void handleStateChanged(Phonon::State, Phonon::State);
	void handleFinished();
private:
    Phonon::AudioOutput *p_output;
    Phonon::MediaObject *p_object;
    QMap<QString,Phonon::MediaSource*> mp_sources;
signals:
    void playingFinished();
};

}

#endif // AUDIOPLAYER_H
