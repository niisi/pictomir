#include "desktop/selectuserdialog.h"
#include "core/platform_configuration.h"

using namespace Core;
namespace Desktop {

SelectUserDialog::SelectUserDialog(QWidget *parent)
    : QDialog(parent)
{
    ui = new Ui::SelectUserDialog();
    ui->setupUi(this);
    s_currentUserRoot = "";
//    s_currentUserRoot = QDesktopServices::storageLocation(QDesktopServices::DataLocation)+
//                        "/Users/default/";

//    if (qApp->arguments().contains("-t")) {
//        s_currentUserRoot = PlatformConfiguration::shareRoot();
//    }


    const QString windowImagePath = PlatformConfiguration::resourcesPath()+"system-users.png";
    const QString removeImagePath = PlatformConfiguration::resourcesPath()+"list-remove.png";
//    const QString stylesheetPath = ":/resources/selectuserdialog.css";



    Q_ASSERT ( QFile::exists(windowImagePath) );
    Q_ASSERT ( QFile::exists(removeImagePath) );
//    Q_ASSERT ( QFile::exists(stylesheetPath) );
    setWindowIcon(QIcon(windowImagePath));
    ui->btnRemove->setIcon(QIcon(removeImagePath));
    ui->btnRemove->setIconSize(QSize(22,22));

//    QFile css(stylesheetPath);
//    if (css.open(QIODevice::ReadOnly|QIODevice::Text)) {
//        QTextStream ts(&css);
//        ts.setAutoDetectUnicode(true);
//        QString cssData = ts.readAll().simplified();
//        css.close();
//        if (!cssData.isEmpty())
//            setStyleSheet(cssData);
//    }

    connect (ui->edit, SIGNAL(textEdited(QString)),
             this, SLOT(handleNameEdited(QString)));

    connect (ui->listUsers, SIGNAL(currentTextChanged(QString)),
             this, SLOT(handleItemSelected(QString)));

    connect (ui->btnRemove, SIGNAL(clicked()),
             this, SLOT(handleRemoveUserPressed()));

    connect (ui->btnGo, SIGNAL(clicked()),
             this, SLOT(handleGoPressed()));

    setWindowModality(Qt::ApplicationModal);

    init();

}

void SelectUserDialog::init()
{
    ui->listUsers->clear();
    ui->listUsers->addItems(scanUsers());
    handleNameEdited("");
}

QStringList SelectUserDialog::scanUsers()
{
    map_userDirs.clear();
    const QString usersRoot =
            QDesktopServices::storageLocation(QDesktopServices::DataLocation)+
            "/Users/";
    QStringList entries = QDir(usersRoot).entryList(QDir::Dirs);
    QStringList users;
    foreach (const QString &dirName, entries) {
        QFile f(usersRoot+"/"+dirName+"/username.utf8.txt");
        if (f.open(QIODevice::ReadOnly|QIODevice::Text)) {
            QTextStream ts(&f);
            ts.setAutoDetectUnicode(true);
            QString userName = ts.readAll().simplified();
            if (!userName.isEmpty()) {
                users << userName;
                map_userDirs[userName] = dirName;
            }
            f.close();
        }
    }
    return users;
}

void SelectUserDialog::handleGoPressed()
{
    QDir usersRoot(QDesktopServices::storageLocation(QDesktopServices::DataLocation)+
                   "/Users/");
    const QString userName = ui->edit->text().simplified();
    if (userName.isEmpty()) {
        s_currentUserRoot = "";
    }
    else {
        if (map_userDirs.contains(userName)) {
            s_currentUserRoot = usersRoot.absolutePath()+"/"+map_userDirs[userName];
        }
        else {
            QCryptographicHash md5(QCryptographicHash::Md5);
            md5.addData(userName.toUtf8());
            const QString dirName = QString::fromAscii(md5.result().toHex());
            usersRoot.mkpath(dirName);
            QFile f(QDesktopServices::storageLocation(QDesktopServices::DataLocation)+
                    "/Users/"+dirName+"/username.utf8.txt");
            if (f.open(QIODevice::WriteOnly|QIODevice::Text)) {
                QTextStream ts(&f);
                ts.setCodec("UTF-8");
                ts.setGenerateByteOrderMark(true);
                ts << userName;
                f.close();
            }
            s_currentUserRoot = usersRoot.absolutePath()+"/"+dirName;
        }
    }
    accept();
}

void SelectUserDialog::handleItemSelected(const QString &name)
{
    if (!name.isNull() && !name.isEmpty()) {
        ui->edit->setText(name);
        handleNameEdited(name);
        ui->btnRemove->setEnabled(true);
    }
    else {
        handleNameEdited(ui->edit->text());
        ui->btnRemove->setEnabled(false);
    }
}

void SelectUserDialog::handleRemoveUserPressed()
{
    const QString userName = ui->listUsers->currentItem()->text();
    QMessageBox::StandardButton btn;
    btn = QMessageBox::question(this, tr("Remove user"),
                                tr("Are you sure to delete user %1?").arg(userName),
                                QMessageBox::Yes|QMessageBox::No);
    if (btn==QMessageBox::Yes) {
        QDir usersRoot(QDesktopServices::storageLocation(QDesktopServices::DataLocation)+
                       "/Users/");
        QString dirName = map_userDirs[userName];
        QDir userDir(usersRoot.absoluteFilePath(dirName));
        foreach (const QString &entry, userDir.entryList()) {
            if (entry=="." || entry=="..")
                continue;
            userDir.remove(entry);
        }
        usersRoot.rmdir(dirName);
        map_userDirs.remove(userName);
        QListWidgetItem *item = ui->listUsers->takeItem(ui->listUsers->currentRow());
        delete item;
        ui->edit->setText("");
        handleItemSelected("");
    }
}

void SelectUserDialog::handleNameEdited(const QString &name)
{
    ui->btnGo->setText(tr("Log in as %1").arg(name.simplified().isEmpty()? tr("Guest") : name.simplified()));
}



const QString& SelectUserDialog::currentUserRoot() const
{
    return s_currentUserRoot;
}

}
