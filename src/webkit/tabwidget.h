#ifndef TABWIDGET_H
#define TABWIDGET_H

#include <QTabWidget>

namespace Webkit {

class TabWidget : public QTabWidget
{
    Q_OBJECT
public:
    explicit TabWidget(QWidget *parent = 0);

signals:
    void newTabRequest();
public slots:

protected:
    void tabInserted(int index);
    void tabRemoved(int index);

};

} // namespace Webkit

#endif // TABWIDGET_H
