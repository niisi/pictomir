#include "util/contentprovider.h"
#include "httpservice/miniserver.h"
#include "3rd-party/cookiejar/cookiejar.h"

namespace Util {


class NetworkAccessManager
        : public QNetworkAccessManager
{
    friend class ContentProvider;
    friend class ContentProviderPrivate;
public:
    class ContentProviderPrivate * d;
    class ContentProvider * q;
protected:
    QNetworkReply * createRequest(Operation op, const QNetworkRequest &request, QIODevice *outgoingData);
    CookieJar * m_cookieJar;
};


class ContentProviderPrivate
        : public QThread
{
public:
    ContentProvider * q;
    HTTPService::MiniServer * miniServer;
    static ContentProvider * instance;
    QUrl remoteUrl;
    NetworkAccessManager * networkAccessManager;
    QByteArray data;
    QString mimeType;
    QString error;

    void run();
};

ContentProvider * ContentProviderPrivate::instance = NULL;

QNetworkReply * NetworkAccessManager::createRequest(Operation op, const QNetworkRequest &request, QIODevice *outgoingData)
{
    if (request.url().scheme()=="local") {
        return d->miniServer->serveRequest(this, op, request, outgoingData);
    }
    else {
        return QNetworkAccessManager::createRequest(op, request, outgoingData);
    }
}


void ContentProviderPrivate::run()
{
    data.clear();
    error.clear();
    mimeType.clear();
    QNetworkRequest request(remoteUrl);
    QNetworkReply * reply = networkAccessManager->get(request);
    while (reply->isRunning()) {
        msleep(250);
    }
    if (reply->error()==QNetworkReply::NoError) {
        data = reply->readAll();
        mimeType = reply->header(QNetworkRequest::ContentTypeHeader).toString();
    }
    else {
        error = reply->errorString();
    }
}

ContentProvider::ContentProvider(QObject *parent) :
    QObject(parent)
{
    d = new ContentProviderPrivate;
    d->setParent(this);
    d->q = this;
    d->networkAccessManager = new NetworkAccessManager;
    d->networkAccessManager->d = d;
    d->miniServer = new HTTPService::MiniServer;
    d->networkAccessManager->q = this;
    d->networkAccessManager->m_cookieJar = new CookieJar(d->networkAccessManager);
    d->networkAccessManager->setCookieJar(d->networkAccessManager->m_cookieJar);
}

ResponseData ContentProvider::fetchUrl(const QUrl &url)
{
    d->remoteUrl = url;
    d->start();
    d->wait();
    ResponseData result;
    result.content = d->data;
    result.error = d->error;
    result.mimeType = d->mimeType;
    return result;
}

QNetworkAccessManager * ContentProvider::sharedNetworkAccessManager()
{
    return d->networkAccessManager;
}

ContentProvider::~ContentProvider()
{
    d->miniServer->deleteLater();
    d->networkAccessManager->deleteLater();
    d->deleteLater();
}

ContentProvider * ContentProvider::instance()
{
    if (ContentProviderPrivate::instance==NULL)
        ContentProviderPrivate::instance = new ContentProvider;
    return ContentProviderPrivate::instance;
}

} // namespace Pictomir

