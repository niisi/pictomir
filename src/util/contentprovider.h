#ifndef CONTENTPROVIDER_H
#define CONTENTPROVIDER_H

#include <QtCore>
#include <QtNetwork>

namespace Util {

struct ResponseData {
    QString error;
    QString mimeType;
    QByteArray content;
};


class ContentProvider : public QObject
{
    Q_OBJECT
public:
    explicit ContentProvider(QObject *parent = 0);
    ResponseData fetchUrl(const QUrl& url);
    QNetworkAccessManager * sharedNetworkAccessManager();
    static ContentProvider * instance();
    ~ContentProvider();
private:
    class ContentProviderPrivate * d;
};

} // namespace Pictomir

#endif // CONTENTPROVIDER_H
