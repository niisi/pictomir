#ifndef PICTOMIR_COMMON_H
#define PICTOMIR_COMMON_H
#include <QtCore>
#include <QtGui>
#include <QtXml>
#ifdef QT_WEBKIT_LIB
    #include <QtWebKit>
#endif
#include "enums.h"

#ifndef Q_OS_WINCE
    class LoadGameDialog;
#endif

#ifdef WITH_PHONON
    class AudioPlayer;
#endif

namespace Runtime {
    class ExecutorKRT;
}

namespace Robot25D {
    class Plugin;
    class RobotView;
}


#include "schema/sch_game.h"

namespace Core {

class PictomirPluginInterface;
class MainWindowInterface;
class PictomirProgram;
class GameProvider;
class ActionManager;
class PluginManager;



class PictomirCommon
    : public QObject
{
    Q_OBJECT;
public:
    PictomirCommon(QObject *parent = 0);
    QString loadGameFromFile(const QString &fileName);
    static PictomirCommon* instance();
    inline void setUiType(PictomirUiType uiType) { e_uiType = uiType; }
    MainWindowInterface *mainWindow() const;
    PictomirUiType userInterfaceType() const;
    ActionManager * actionManager();
    PluginManager * pluginManager();
    PictomirProgram * program();
    void createUserInterface(PictomirUiType uiType,
                             const QSize& size, bool fs);
    inline bool isPrevTaskAvailable() const { return b_prevTaskAvailable; }
    inline bool isNextTaskAvailable() const { return b_nextTaskAvailable; }
    QString currentTaskTitle() const;
    QString currentTaskEnvironment() const;
    ~PictomirCommon();
signals:
    void selectUser();
    void stateChanged(Core::ProgramState state);
    void information(const QString &msg);
    void taskHintChanged(const QString &mimetype, const QByteArray &data);
public slots:
    void prepareLoadGame();
    void loadUserState(const QString &currentUserRoot);
    void saveGame();
    void resetProgramToInitial();
    void saveProgramInTask();
    void saveEnvironment();
    void executeProgram(bool disableAnimation = false);
    void executionFinished(const QString &message);
    void pauseProgram();
    void debugProgram();
    void stepProgram();
    void resetProgramAndModule();
    void resetProgram();
    void runToEnd();
    void prepareQuit();
    void stopProgram();
    void executionError(const QString &msg);
    void loadTask(int index);

    void handleStepFinished(const QString &message);
    void handleStepStarted(const QString &message);
    void loadPreviousTask();
    void loadNextTask();

public:
    QString resolveGameFileName(const QString &baseName, bool isLocal) const;
public:
    void createUiActions();
    QAction *a_resetProgramToInitial;
    QAction *a_selectUser;
    QAction *a_nextTask;
    QAction *a_previousTask;
    QAction *a_exit;
    QAction *a_start;
    QAction *a_pause;
    QAction *a_runToEnd;
    QAction *a_step;
    QAction *a_debug;
    QAction *a_stop;
    QAction *a_reset;
    QAction *a_resetProgramAndModule;

    QAction *a_showHelp;
    QAction *a_showAbout;
    bool b_gameLoaded;

    Schema::Game sch_current;
    Schema::Game sch_initial;

    QString s_gameFileName;
    QString s_gameLocalFileName;

    Robot25D::Plugin *m_currentModule;
    Robot25D::RobotView *m_currentModuleWidget;
    GameProvider *m_gameProvider;

    QString s_currentUserRoot;

    ProgramState e_state;
    PictomirProgram *m_program;

    bool b_nextTaskAvailable;
    bool b_prevTaskAvailable;
    QString s_errorText;
    PictomirUiType e_uiType;

    ActionManager * m_actionManager;
    Runtime::ExecutorKRT * m_executor;

signals:
    void taskLoaded();
    void highlightRequest(int funcNo, int cellNo);
    void conditionChecked(bool value);
    void executionError(const QList<int>&, const QList<int>&);
    void programStructureChanged();
    void taskSuccessed();

};

}
#endif // PICTOMIR_COMMON_H
