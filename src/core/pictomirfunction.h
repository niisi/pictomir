#ifndef PICTOMIRFUNCTION_H
#define PICTOMIRFUNCTION_H

#include <QtCore>
#include <QtGui>
#include "enums.h"

namespace Core {

class ActionModel;
class FunctionWidgetInterface;
class ActionManager;

class PictomirFunction : public QObject
{
    Q_OBJECT
public:
    explicit PictomirFunction(const QSize& size
                              , FunctionModifiers modifiers
                              , const QString &title
                              , const QStringList &hints
                              , ActionManager * actionManager
                              , QObject *parent = 0);

    inline bool isModified() const { return b_modified; }
    inline void setModified(bool v) { b_modified = v; }
    ActionModel *action(int x, int y) const;
    ActionModel *action(int index) const;
    ActionModel *repeater() const;
    ActionModel *condition() const;
    ActionManager * actionManager() const;


    QPair<const QList<int>, const QList<const ActionModel*> > nonEmptyActions() const;
    inline FunctionModifiers modifiers() const { return e_modifiers; }

    void modifyAlgorhitm(const QSize &size, FunctionModifiers modifiers);

//    int actionIndex(const ActionModel *action) const;
    inline QSize size() const { return sz_size; }
    inline QString title() const { return s_title; }

    void setTitle(const QString &title);

    ~PictomirFunction();


public slots:
    void setRepeater(const ActionModel* action);
    void setCondition(const ActionModel* action);
    void setAction(int x, int y, const ActionModel *action);
    void setAction(int index, const ActionModel *action);
    void clearProgram();
    void alterChange();

signals:
    void changeRequest();
    void removeRequest();
    void programChanged();

private:
    FunctionModifiers e_modifiers;
    QString s_title;
    QStringList sl_hints;
    QVector< QVector< ActionModel* > > vv_data;
    ActionModel *m_repeater;
    ActionModel *m_condition;
    QSize sz_size;
    QList< QPointer <FunctionWidgetInterface> > l_views;
    bool b_modified;
    ActionManager * m_actionManager;

};

}

#endif // PICTOMIRFUNCTION_H
