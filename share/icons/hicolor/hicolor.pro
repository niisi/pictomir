TEMPLATE=subdirs
SUBDIRS=

png16.files=16x16/apps/pictomir.png 16x16/apps/pictomir-creator.png 16x16/apps/pictomir-navigator.png
png16.path=/share/icons/hicolor/16x16/apps/

png32.files=32x32/apps/pictomir.png 32x32/apps/pictomir-creator.png 32x32/apps/pictomir-navigator.png
png32.path=/share/icons/hicolor/32x32/apps/

png48.files=48x48/apps/pictomir.png 48x48/apps/pictomir-creator.png 48x48/apps/pictomir-navigator.png
png48.path=/share/icons/hicolor/48x48/apps/

png64.files=64x64/apps/pictomir.png 64x64/apps/pictomir-creator.png 64x64/apps/pictomir-navigator.png
png64.path=/share/icons/hicolor/64x64/apps/

png128.files=128x128/apps/pictomir.png 128x128/apps/pictomir-creator.png 128x128/apps/pictomir-navigator.png
png128.path=/share/icons/hicolor/128x128/apps/

svg.files=scalable/apps/pictomir.svg scalable/apps/pictomir-creator.svg scalable/apps/pictomir-navigator.png
svg.path=/share/icons/hicolor/scalable/apps/

INSTALLS += png16 png32 png48 png64 png128 svg

