#include "core/actionmanager.h"
#include "core/actionview.h"
#include "core/actionmodel.h"
#include "robot25d/default_plugin.h"
#include "util/svgrenderfarm.h"
#include "schema/sch_command.h"

namespace Core {

class ActionManagerPrivate {
public:
    ActionManager * q;

    bool locked;
    ActionView * activeView;
    QList<ActionModel*> models;
};

ActionManager::ActionManager(QObject *parent) :
    QObject(parent)
{
    d = new ActionManagerPrivate;
    d->q = this;
    d->locked = false;
    d->activeView = NULL;
}

ActionManager::~ActionManager()
{
    foreach (ActionModel *m, d->models) {
        delete m;
    }
    delete d;
}

void ActionManager::setLocked(bool v)
{
    if (v) {
        setActiveView(NULL, true);
    }
    d->locked = v;
}

bool ActionManager::isLocked() const
{
    return d->locked;
}

void ActionManager::setActiveView(ActionView *view, bool notifyView)
{
    if (d->activeView) {
        d->activeView->setActive(false);
    }
    d->activeView = view;
    if (d->activeView && notifyView) {
        d->activeView->setActive(true);
    }
}

ActionModel * ActionManager::activeModel() const
{
    if (d->activeView) {
        int id = d->activeView->modelId();
        return modelById(id);
    }
    else {
        return NULL;
    }
}

ActionView * ActionManager::activeView()
{
    return d->activeView;
}

ActionModel * ActionManager::modelById(int id) const
{
    Q_ASSERT(id>=0);
    Q_ASSERT(id<d->models.size());
    ActionModel *mod = d->models.at(id);
    Q_CHECK_PTR(mod);
    return mod;
}

ActionModel* ActionManager::modelByShortcut(const QString &shortcut) const
{
    Schema::Command toSearch;
    if (Schema::parceAGKXML(shortcut, toSearch)) {
        for (int i=0; i<d->models.size(); i++) {
            ActionModel *mod = d->models.at(i);
            Q_CHECK_PTR(mod);
            Schema::Command cmd = mod->standartCommand();
            if (cmd==toSearch)
                return mod;
        }
    }
    return NULL;
}

ActionModel * ActionManager::modelByStandartCommand(Schema::Command cmd) const
{
    for (int i=0; i<d->models.size(); i++) {
        ActionModel * mod = d->models.at(i);
        Q_CHECK_PTR(mod);
        if (mod->standartCommand()==cmd)
            return mod;
    }
    return NULL;
}

void ActionManager::add(ActionModel *model)
{
    model->i_modelId = model->i_actionId;
    d->models << model;
}

void ActionManager::createActions(const QList<Robot25D::Plugin*> modules)
{
    // Cleaning
    for (int i=0; i<d->models.size(); i++) {
        if (d->models[i]!=NULL)
            d->models[i]->deleteLater();
    }
    d->models.clear();

    QList<ActionModel*> models;

    // Repeaters
    models << new ActionModel("forever",
                              -1,
                              "repeat/forewer",
                              tr("Forever"),
                              Schema::RepForever,
                              NULL,
                              this,
                              ActionRepeat,
                              Base);
    models.last()->i_modelId = models.size()-1;
    models << new ActionModel("2",
                              -1,
                              "repeat/2",
                              tr("Twice"),
                              Schema::Rep2,
                              NULL,
                              this,
                              ActionRepeat,
                              Base);
    models.last()->i_modelId = models.size()-1;
    models << new ActionModel("3",
                              -1,
                              "repeat/3",
                              tr("Three times"),
                              Schema::Rep3,
                              NULL,
                              this,
                              ActionRepeat,
                              Base);
    models.last()->i_modelId = models.size()-1;
    models << new ActionModel("4",
                              -1,
                              "repeat/4",
                              tr("Four times"),
                              Schema::Rep4,
                              NULL,
                              this,
                              ActionRepeat,
                              Base);
    models.last()->i_modelId = models.size()-1;
    models << new ActionModel("5",
                              -1,
                              "repeat/5",
                              tr("Five times"),
                              Schema::Rep5,
                              NULL,
                              this,
                              ActionRepeat,
                              Base);
    models.last()->i_modelId = models.size()-1;
    models << new ActionModel("6",
                              -1,
                              "repeat/6",
                              tr("Six times"),
                              Schema::Rep6,
                              NULL,
                              this,
                              ActionRepeat,
                              Base);
    models.last()->i_modelId = models.size()-1;

    // Module
    for (int k=0; k<modules.size(); k++) {
        Robot25D::Plugin *plugin = modules[k];
        QList<Robot25D::Command> conditions;
        QList<Robot25D::Command> actions;
        conditions = plugin->conditions();
        actions = plugin->actions();
        QLocale::Language lang = QLocale::C;
        QString lng = QCoreApplication::instance()->property("locale").toString();
        QLocale locale(lng);
        lang = locale.language();

        for (int i=0; i<conditions.size(); i++) {
            Robot25D::Command cmd = conditions.at(i);
            QString name = cmd.name;
            if (cmd.name_i18n.contains(lang))
                name = cmd.name_i18n[lang];
            QString text = cmd.tooltip;
            if (cmd.tooltip_i18n.contains(lang))
                text = cmd.tooltip_i18n[lang];
            Util::SvgRenderFarm::instance()->createSharedRendered(name, cmd.svgImageData);
            models << new ActionModel(name, cmd.id, "",
                                      text, cmd.standartCommand, plugin, this,
                                      ActionCondition, Module);
            models.last()->i_modelId = models.size()-1;
        }

        for (int i=0; i<conditions.size(); i++) {
            Robot25D::Command cmd = actions.at(i);
            QString name = cmd.name;
            if (cmd.name_i18n.contains(lang))
                name = cmd.name_i18n[lang];
            QString text = cmd.tooltip;
            if (cmd.tooltip_i18n.contains(lang))
                text = cmd.tooltip_i18n[lang];
            Util::SvgRenderFarm::instance()->createSharedRendered(name, cmd.svgImageData);
            models << new ActionModel(name, cmd.id, "",
                                      text, cmd.standartCommand, plugin, this,
                                      ActionPlain, Module);
            models.last()->i_modelId = models.size()-1;
        }
    }

    // Function calls
    const QString abc = trUtf8(" АБВГДЕЖЗИКЛМНОПРСТУФ");
    for (int i=0; i<6; i++) {
        models << new ActionModel(QString("f%1").arg(i+1),
                                  -1,
                                  QString("function/%1").arg(abc[i+1]),
                                  tr("Call command %1").arg(abc[i+1]),
//                                  abc[i],
                                  Schema::Command( int(Schema::CmdCall1)+i ),
                                  NULL,
                                  this,
                                  ActionPlain,
                                  Functions);
        models.last()->i_modelId = models.size()-1;
    }
    d->models = models;
}

QList<ActionModel*> ActionManager::models(bool onlyEnabled, ActionType at, ActionCategory cat)
{
    QList<ActionModel*> l;
    QList<ActionModel*> all_models = d->models;
    for (int i=0; i<all_models.size(); i++) {
        ActionModel *m = all_models[i];
        if (onlyEnabled && !m->isEnabled()) {
            continue;
        }
        if (at!=ActionNone && m->actionType()!=at) {
            continue;
        }
        if (cat!=Any && m->actionCategory()!=cat) {
            continue;
        }
        l << m;
    }
    return l;
}




void ActionManager::setEnabledActionsList(Robot25D::Plugin *selectedModule,
                                        bool plain, bool cond, bool rep, int funcCnt)
{
    d->activeView = NULL;

    for (int i=0; i<d->models.size(); i++) {
        ActionModel *m = d->models[i];
        if (m->actionType()==ActionPlain)
            m->setEnabled(plain);
        if (m->actionType()==ActionCondition)
            m->setEnabled(cond);
        if (m->actionType()==ActionRepeat)
            m->setEnabled(rep);
    }

    int funcs = 0;

    for (int i=0; i<d->models.size(); i++) {
        ActionModel *m = d->models[i];
        if (m->actionCategory()==Module) {
            m->setEnabled(m->isEnabled() && m->module()==selectedModule);
        }
        if (m->actionCategory()==Functions) {
            m->setEnabled(m->isEnabled() && funcs < funcCnt);
            funcs ++;
        }
    }
}

} // namespace Core
