#ifndef SCH_GAME_H
#define SCH_GAME_H

#include "schema/sch_task.h"
#include "util/fp.h"

namespace Schema {

struct Game {
    /* title */
    QString title;
    /* legal */
    QStringList authors;
    QString copyright;
    QString license;
    QString homepage;
    /* content */
    QList<Task> tasks;
    /* entry point */
    int index;
};


extern bool parceJSON(const QScriptValue &value, Game &game);
extern bool parceAGKXML(const QDomNode &root, Game &game, bool userCurrentProgram);
extern QString generateJSON(const Game &game);
extern QString generateAGKXML(const Game & game);
extern QString generateKumirCourse(const Game & game, const QString & resourcesDirName);

extern bool isEqual(const Game &a, const Game &b);

} // namespace Schema

#endif // SCH_GAME_H
