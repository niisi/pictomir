#include "mobile/m_functionwidget.h"
#include "core/pictomirfunction.h"
#include <QtGui>

#include "core/actiontype.h"
#include "core/actionmodel.h"
#include "core/actionview.h"
#include "desktop/actionprogramview.h"
#include "core/actionmanager.h"

using namespace Core;

namespace Mobile {

FWTCell::FWTCell(FunctionWidget *fw
                 , PictomirFunction *f
                 , ActionType actionType
                 , int index
                 , QGraphicsItem *parent)
    : QGraphicsObject(parent)
    , f_ptr(f)
    , fw_ptr(fw)
    , e_actionType(actionType)
    , i_index(index)
    , b_mousePressed(false)
    , b_hovered(false)
    , b_hasItemInside(false)
{
    setAcceptDrops(true);
    setAcceptHoverEvents(true);
    QColor start;
    if (actionType==ActionPlain) {
        start = QColor(64,128,64);
    }
    if (actionType==ActionCondition) {
        start = QColor(128,64,64);
    }
    if (actionType==ActionRepeat) {
        start = QColor(64,64,128);
    }
    setColor(start);

    createAnimationPath();
    createHighlighterObjects();
    setHighlighterState(0.0);

    an_highlighter = new QPropertyAnimation(this, "highlighterState", this);
    r_highlighterState = 0.0;
    an_highlighter->setLoopCount(-1);
    an_highlighter->setStartValue(0.0);
    an_highlighter->setEndValue(1.0);
    an_highlighter->setDuration(1000);

    an_unhighlighter = new QPropertyAnimation(this, "highlighterOpacity", this);
    an_unhighlighter->setStartValue(1.0);
    an_unhighlighter->setEndValue(0.0);
    an_unhighlighter->setDuration(400);
    connect(an_unhighlighter, SIGNAL(finished()), this, SLOT(handleHighlightAnimationFinished()));

    an_changeColor = new QPropertyAnimation(this, "highlighterColor", this);
    an_changeColor->setDuration(500);
}

void FWTCell::changeHighlightColor(const QColor &color)
{
    an_changeColor->stop();
    an_changeColor->setStartValue(highlighterColor());
    an_changeColor->setEndValue(color);
    an_changeColor->start();
}

void FWTCell::highlight()
{
    setHighlighterState(0.0);
    for (int i=0; i<v_highlighterObjects.size(); i++) {
        v_highlighterObjects[i]->setOpacity(1.0);
        v_highlighterObjects[i]->setVisible(true);
    }
    an_unhighlighter->stop();
    an_highlighter->start();
}

void FWTCell::unhighlight()
{
    an_unhighlighter->start();
}

void FWTCell::createHighlighterObjects()
{
    v_highlighterObjects.resize(4);
    QGraphicsEllipseItem *item;
    m_highlighterGradient = QRadialGradient();
//    m_highlighterGradient.setColorAt(0.0, QColor("yellow"));
    m_highlighterGradient.setColorAt(1.0, QColor(0,0,0,0));
    m_highlighterGradient.setSpread(QGradient::PadSpread);
    m_highlighterGradient.setCoordinateMode(QGradient::ObjectBoundingMode);
    m_highlighterGradient.setRadius(0.5);
    m_highlighterGradient.setFocalPoint(0.5, 0.5);
    m_highlighterGradient.setCenter(0.5, 0.5);

    item = new QGraphicsEllipseItem(QRectF(0, 0, 16, 16), this);
    item->setPen(Qt::NoPen);
    item->setBrush(m_highlighterGradient);
    item->setZValue(0.5);
    item->setVisible(false);
    v_highlighterObjects[0] = item;

    item = new QGraphicsEllipseItem(QRectF(0, 0, 14, 14), this);
    item->setPen(Qt::NoPen);
    item->setBrush(m_highlighterGradient);
    item->setZValue(0.5);
    item->setVisible(false);
    v_highlighterObjects[1] = item;

    item = new QGraphicsEllipseItem(QRectF(0, 0, 12, 12), this);
    item->setPen(Qt::NoPen);
    item->setBrush(m_highlighterGradient);
    item->setZValue(0.5);
    item->setVisible(false);
    v_highlighterObjects[2] = item;

    item = new QGraphicsEllipseItem(QRectF(0, 0, 10, 10), this);
    item->setPen(Qt::NoPen);
    item->setBrush(m_highlighterGradient);
    item->setZValue(0.5);
    item->setVisible(false);
    v_highlighterObjects[3] = item;

    setHighlighterColor(QColor("yellow"));
}

void FWTCell::setHighlighterColor(const QColor &color)
{
    cl_highlighterColor = color;
    m_highlighterGradient.setColorAt(0.0, color);
    for (int i=0; i<v_highlighterObjects.size(); i++) {
        v_highlighterObjects[i]->setBrush(m_highlighterGradient);
    }
}

void FWTCell::setHighlighterOpacity(qreal value)
{
    r_highlighterOpacity = value;
    for (int i=0; i<v_highlighterObjects.size(); i++) {
        QGraphicsItem *item = v_highlighterObjects[i];
        item->setOpacity(value);
    }
}

void FWTCell::setHighlighterState(qreal v)
{
    r_highlighterState = v;
    const qreal distance = 0.05;

    for (int i=0; i<v_highlighterObjects.size(); i++) {
        QGraphicsItem *item = v_highlighterObjects[i];
        QPointF offset = QPointF(-item->boundingRect().width()/2, -item->boundingRect().height()/2);
        qreal delay = i * distance;
        qreal perc = r_highlighterState - delay;
        if (perc>1.0)
            perc -= 1.0;
        if (perc<0.0)
            perc += 1.0;
        Q_ASSERT (perc >= 0.0);
        Q_ASSERT (perc <= 1.0);
        QPointF base = p_animationPath.pointAtPercent(perc);
        item->setPos(base+offset);
    }

}

void FWTCell::createAnimationPath()
{
    if (e_actionType==ActionPlain) {
        p_animationPath.addRoundedRect(QRectF(6, 6, 28, 28), 10, 10);
    }
    else if (e_actionType==ActionCondition) {
        QVector<QPoint> points(4);
        points[3] = QPoint(20, 6);
        points[2] = QPoint(6,20);
        points[1] = QPoint(20,34);
        points[0] = QPoint(34,20);
        p_animationPath.addPolygon(QPolygonF(points));
        p_animationPath.closeSubpath();
    }
    else if (e_actionType==ActionRepeat) {
        p_animationPath.addEllipse(6, 6, 28, 28);
    }
    else {
        qFatal("Unknown action type!");
    }
}

void FWTCell::handleHighlightAnimationFinished()
{
    an_highlighter->stop();
    for (int i=0; i<v_highlighterObjects.size(); i++) {
        v_highlighterObjects[i]->setVisible(false);
    }
    setHighlighterColor(QColor("yellow"));
}

FWTCell::~FWTCell()
{

}


void FWTCell::paint(QPainter *p,
                    const QStyleOptionGraphicsItem *,
                    QWidget *)
{
    p->save();
    p->setBrush(cl_color);
    p->setPen(Qt::SolidLine);
    p->setRenderHint(QPainter::Antialiasing, true);
    if (e_actionType==ActionPlain)
        p->drawRoundedRect(2,2,36,36,10,10);
    else if (e_actionType==ActionRepeat)
        p->drawEllipse(2,2,36,36);
    else if (e_actionType==ActionCondition) {
        QVector<QPoint> points(4);
        points[0] = QPoint(20,0);
        points[1] = QPoint(0,20);
        points[2] = QPoint(20,40);
        points[3] = QPoint(40,20);
        p->drawPolygon(points);
    }
//    p->setPen(Qt::SolidLine);
//    p->drawPath(p_animationPath);
//    p->drawText(QPoint(0,0),QString::number(i_index));
    p->restore();
}

bool FWTCell::canInsertItem(const ActionModel *model) const
{
    if (!model)
        return false;
    ActionType outerType = model->actionType();
    ActionType myType = e_actionType;
    bool typeMatch = outerType==myType;
    bool locked = f_ptr->actionManager()->isLocked();
    return typeMatch && !b_hasItemInside && !locked;
}

bool FWTCell::canInsertActiveItem() const
{
    return canInsertItem(f_ptr->actionManager()->activeModel());
}


void FWTCell::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    b_mousePressed = true;
    event->accept();
}

void FWTCell::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    if (b_mousePressed && canInsertActiveItem()) {
        b_mousePressed = false;
        insertActiveItem();
        event->accept();
    }
    else if (b_hasItemInside) {
        b_mousePressed = false;
        event->accept();
        emit imitateActionClicked(m_viewInside, event->button()==Qt::RightButton);
    }
    else {
        b_mousePressed = false;
        event->ignore();
    }
}

void FWTCell::insertActiveItem()
{
    ActionModel *model = f_ptr->actionManager()->activeModel();
    if (i_index==-2)
        f_ptr->setRepeater(model);
    if (i_index==-1)
        f_ptr->setCondition(model);
    if (i_index>=0)
        f_ptr->setAction(i_index, model);

    insertItem(model);
}

void FWTCell::insertItem(ActionModel *model)
{
    Desktop::ActionProgramView * view = new Desktop::ActionProgramView(model, this);
    qreal x_offset = (40 - view->graphicsItem()->boundingRect().width())/2;
    qreal y_offset = (40 - view->graphicsItem()->boundingRect().height())/2;
    view->graphicsItem()->setPos(x_offset, y_offset);
    connect(view->graphicsItem(), SIGNAL(clicked(bool)), fw_ptr, SLOT(handleActionClicked(bool)));
    f_ptr->actionManager()->setActiveView(NULL, true);
    b_hasItemInside = true;
    m_viewInside = view;
}

void FWTCell::unsetItemInside()
{
    b_hasItemInside = false;
    m_viewInside = NULL;
}


FunctionWidget::FunctionWidget(PictomirFunction *f, QGraphicsItem *parent) :
        FunctionWidgetInterface(parent)
{
    m_highlightedCell = 0;
    m_function = f;
    if (f->modifiers()!=PlainFunction) {
        if (f->modifiers() & RepeatFunction) {
            FWTCell *item = new FWTCell(this, f,ActionRepeat,-2,NULL);
//            scene()->addItem(item);
            l_cells << item;
            connect (item, SIGNAL(imitateActionClicked(ActionProgramView*,bool)),
                     this, SLOT(handleActionClicked(ActionProgramView*,bool)));
        }
        if (f->modifiers() & ConditionFunction) {
            FWTCell *item = new FWTCell(this, f, ActionCondition, -1, NULL);
//            scene()->addItem(item);
            l_cells << item;
            connect (item, SIGNAL(imitateActionClicked(ActionProgramView*,bool)),
                     this, SLOT(handleActionClicked(ActionProgramView*,bool)));
        }
    }
    int index = 0;
    for (int row=0; row<f->size().height(); row++) {
        for (int col=0; col<f->size().width(); col++) {
            FWTCell *item = new FWTCell(this, f, ActionPlain, index, NULL);
            index += 1;
//            scene()->addItem(item);
            l_cells << item;
            connect (item, SIGNAL(imitateActionClicked(ActionProgramView*,bool)),
                     this, SLOT(handleActionClicked(ActionProgramView*,bool)));
        }
    }

    e_currentOrientation = Qt::Horizontal;
    relayout();
    loadProgramFromModel();

}

PictomirFunction* FunctionWidget::model()
{
    return m_function;
}


void FunctionWidget::setOrientation(Qt::Orientation orientation)
{
    e_currentOrientation = orientation;
    relayout();
}

void FunctionWidget::handleActionClicked(Desktop::ActionProgramView *view, bool rightClick)
{
    Q_CHECK_PTR(view);
    if (!rightClick) {
        if (m_function->actionManager()->activeView()==view) {
            m_function->actionManager()->setActiveView(NULL, true);
        }
        else {
            m_function->actionManager()->setActiveView(view, true);
        }
    }
    else {
        FWTCell *cell = qobject_cast<FWTCell*>(view->parentObject());
        Q_CHECK_PTR(cell);
        if (cell->index()==-2) {
            m_function->setRepeater(NULL);
        }
        else if (cell->index()==-1) {
            m_function->setCondition(NULL);
        }
        else {
            m_function->setAction(cell->index(), NULL);
        }
        if (m_function->actionManager()->activeView()==view) {
            m_function->actionManager()->setActiveView(NULL, true);
        }
        cell->unsetItemInside();
        view->dispose();
    }
    emit activationChanged();
}

void FunctionWidget::handleActionClicked(bool rightClick)
{
    Desktop::ActionProgramView* view = qobject_cast<Desktop::ActionProgramView*>(sender());
    handleActionClicked(view, rightClick);
}

void FunctionWidget::connectSignalActivationChanged(QObject *obj, const char *method)
{
    connect (this, SIGNAL(activationChanged()), obj, method, Qt::DirectConnection);
}

void FunctionWidget::loadProgramFromModel()
{
    for (int i=0; i<l_cells.size(); i++) {
        FWTCell *cell = l_cells[i];
        int index = cell->index();
        ActionModel *model = NULL;
        if (index==-2)
            model = m_function->repeater();
        else if (index==-1)
            model = m_function->condition();
        else
            model = m_function->action(index);
        if (model)
            cell->insertItem(model);
    }
}

void FunctionWidget::unlock()
{

}

void FunctionWidget::lock()
{
    if (m_highlightedCell) {
        m_highlightedCell->unhighlight();
    }
    m_highlightedCell = NULL;
}

void FunctionWidget::relayout()
{
//    int W = 0;
//    int H = 0;
//    int fW = m_function->size().width();
//    int fH = m_function->size().height();
//    if (e_currentOrientation==Qt::Horizontal) {
//        W = qMax(fW, fH);
//        H = qMin(fW, fH);
//    }
//    else {
//        W = qMin(fW, fH);
//        H = qMax(fW, fH);
//    }
//    Q_ASSERT (W*H == fW*fH);
//    qreal x_base = 5;
//    qreal y_base = 5;
//    int widgetWidth = width();
//    int widgetHeight = height();
//    qreal actualWidth = W * 40 + (W-1)*5;
//    qreal actualHeight = H * 40 + (H-1)*5;
//    if (actualWidth<widgetWidth) {
//        x_base = (widgetWidth-actualWidth) / 2;
//    }
//    if (actualHeight<widgetHeight) {
//        y_base = (widgetHeight-actualHeight) / 2;
//    }
//    qreal x = x_base;
//    qreal y = y_base;
//    int index = 0;
//    for (int row=0; row<H; row++) {
//        x = x_base;
//        for (int col=0; col<W; col++) {
//            l_cells[index]->setPos(x, y);
//            index += 1;
//            x += 45;
//        }
//        y += 45;
//    }

//    setSceneRect(0,0,widgetWidth,widgetHeight);
}

void FunctionWidget::highlightPlainCell(int index)
{
    if (m_highlightedCell) {
        m_highlightedCell->unhighlight();
    }
    FWTCell *cell = NULL;
    for (int i=0; i<l_cells.size(); i++) {
        if (l_cells[i]->index()==index) {
            cell = l_cells[i];
            break;
        }
    }
    Q_CHECK_PTR(cell);
    m_highlightedCell = cell;
    cell->highlight();
}

void FunctionWidget::highlightErrorCell(int index)
{
    FWTCell *cell = NULL;
    for (int i=0; i<l_cells.size(); i++) {
        if (l_cells[i]->index()==index) {
            cell = l_cells[i];
            break;
        }
    }
    if (m_highlightedCell!=cell) {
        m_highlightedCell->unhighlight();
        m_highlightedCell = cell;
        m_highlightedCell->setHighlighterColor(QColor("red"));
        m_highlightedCell->highlight();
    }
    else {
        m_highlightedCell->changeHighlightColor(QColor("red"));
    }
}

void FunctionWidget::highlightConditionalValue(bool value)
{
    Q_CHECK_PTR(m_highlightedCell);
    m_highlightedCell->changeHighlightColor(value? QColor("green") : QColor("red"));
}

void FunctionWidget::unhighlight()
{
    if (m_highlightedCell) {
        m_highlightedCell->unhighlight();
        m_highlightedCell = NULL;
    }

}

void FunctionWidget::setFunctionTitle(const QString &title)
{
    Q_UNUSED(title);
}
}
