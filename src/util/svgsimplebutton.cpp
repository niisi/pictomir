#include "svgsimplebutton.h"

namespace Util {

SvgSimpleButton::SvgSimpleButton(const QString &svgPath, QGraphicsItem *parent) :
    QGraphicsSvgItem(svgPath, parent)
{
    b_mousePressed = false;
    setAcceptsHoverEvents(true);
}

void SvgSimpleButton::setEnabled(bool v)
{
    b_mousePressed = false;
    QGraphicsSvgItem::setEnabled(v);
}

void SvgSimpleButton::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if (isEnabled()) {
        event->accept();
        b_mousePressed = true;
    }
    else {
        event->ignore();
    }
}

void SvgSimpleButton::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    if (isEnabled() && b_mousePressed) {
        event->accept();
        b_mousePressed = false;
        emit clicked();
    }
    else {
        event->ignore();
    }
}

void SvgSimpleButton::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
    b_mousePressed = false;
    event->accept();
}

} // namespace Util
