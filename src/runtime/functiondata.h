#ifndef FUNCTIONDATA_H
#define FUNCTIONDATA_H

#include <QtCore>

struct FunctionData
{
    QStringList instructions;
    QVector<QVariant> locals;
    QVariant registers[26];
    QString backtraceInfo;
    quint32 ip;
};

#endif // FUNCTIONDATA_H
