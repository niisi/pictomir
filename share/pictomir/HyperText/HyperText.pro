TEMPLATE=subdirs
SUBDIRS=

about.files=about/*.html about/*.png about/*.css about/*.js about/*.svg
help.files=help/*.html help/*.png help/*.css help/*.js help/*.svg

about.path=/share/pictomir/HyperText/about/
help.path=/share/pictomir/HyperText/help/

INSTALLS += about help
