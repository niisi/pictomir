#ifndef DS_BOTTOMDOCKITEM_H
#define DS_BOTTOMDOCKITEM_H

#include <QtCore>
#include <QtGui>

namespace Desktop {

class HelperItem : public QObject, public QGraphicsRectItem
{
    Q_OBJECT;
public:
    HelperItem(QGraphicsItem *parent = 0);
    ~HelperItem();
    QSize fullSize() const;
public slots:
    bool isEnabled() const;
    void setEnabled(bool v);
    void setBorder(const QSize &blockSize);
protected:
    void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
    void hoverLeaveEvent(QGraphicsSceneHoverEvent *event);
    bool b_enabled;
    QGraphicsPathItem * g_border;

};

} // namespace Desktop

#endif // DS_BOTTOMDOCKITEM_H
