#include "core/actionmodel.h"
#include "util/svgrenderfarm.h"
#include "robot25d/default_plugin.h"
#include "core/actionview.h"
#include "core/platform_configuration.h"
#include "core/actionmanager.h"


namespace Core {

ActionModel::ActionModel(const QString &actionName
                         , int actionId
                         , const QString &iconFileName
                         , const QString &actionText
                         , Schema::Command standartCommand
                         , Robot25D::Plugin *module
                         , ActionManager * manager
                         , ActionType actionType
                         , ActionCategory category)
{
    sch_command = standartCommand;
    s_actionName = actionName;
    s_iconFileName = iconFileName;
    i_actionId = actionId;
    e_type = actionType;
    e_category = category;
    s_text = actionText;
    ptr_module = module;
    ptr_manager = manager;
    b_enabled = false;
    if (iconFileName.isEmpty())
        m_renderer = Util::SvgRenderFarm::instance()->getSharedRendered(actionName);
    else if (iconFileName.startsWith("function/")) {
        const QString letter = iconFileName.mid(9);
        m_renderer = Util::SvgRenderFarm::instance()->createSharedRenderedForFunction(iconFileName);
    }
    else
    {
        const QString fileName = PlatformConfiguration::resourcesPath()+
                "icons/"+
                iconFileName + ".svg";
        QByteArray data;

        // Qt release-build bug!!! We can't use QFile here :-(
        FILE *f = fopen(QDir::toNativeSeparators(fileName).toLocal8Bit().data(), "rb");
        Q_ASSERT (f);
        char *buffer;
        buffer = (char*)calloc(1024, sizeof(char));
        size_t bytesRead = 0;
        do {
            bytesRead = fread(buffer, sizeof(char), 1024, f);
            data.append(buffer, bytesRead);
        }
        while (bytesRead>0);
        fclose(f);
        free(buffer);

        m_renderer = Util::SvgRenderFarm::instance()->createSharedRendered(actionName, data);
        Q_ASSERT( m_renderer->isValid() );
    }
}

int ActionModel::modelId() const
{
    return i_modelId;
}


QString ActionModel::svgData() const
{
    return QString::fromAscii(Util::SvgRenderFarm::instance()->getSvgData(s_actionName));
}

QImage ActionModel::image() const
{
    Q_CHECK_PTR(m_renderer);
    QImage image(m_renderer->defaultSize(), QImage::Format_ARGB32);
    image.fill(0);
    QPainter painter;
    painter.begin(&image);
    m_renderer->render(&painter);
    painter.end();
    return image;
}

QString ActionModel::imageData() const
{
    QByteArray ba;
    QBuffer b(&ba);
    b.open(QIODevice::WriteOnly);
    image().save(&b,"PNG");
    b.close();
    QString result = "<img src='data:image/png;base64,"+ba.toBase64()+"'>\n";
    return result;
}

QString ActionModel::text() const
{
    return s_text;
}



ActionManager * ActionModel::manager() const
{
    return ptr_manager;
}

}

