#ifndef CR_HINTEDITOR_H
#define CR_HINTEDITOR_H

#include <QtGui>
#include <QtSvg>


namespace Schema {
    struct Game;
}

namespace Creator {

namespace Ui {
    class HintEditor;
}



class HintEditor : public QWidget
{
    Q_OBJECT

public:
    explicit HintEditor(QWidget *parent = 0);
    inline Schema::Game * game() const { return m_game; }
    void setGame(Schema::Game * game);
    inline void setTaskIndex(int index) { i_taskIndex = index; }
    inline int taskIndex() const { return i_taskIndex; }
    void setHint(Schema::Game * game, int index);
    inline QAction * actionClear() { return a_clear; }
    inline QAction * actionSetText() { return a_setText; }
    inline QAction * actionSetPicture() { return a_setPicture; }
    ~HintEditor();

public slots:
    void updateWindowTitle();
    inline void checkTaskIndex(const QMap<int,int> map) { if (map.contains(i_taskIndex)) i_taskIndex = map[i_taskIndex]; }
    void sync();

private slots:
    void clearHint();
    void setImage();
    void setText();

private:
    void closeEvent(QCloseEvent *event);
    QAction *a_clear;
    QAction *a_setText;
    QAction *a_setPicture;
    Ui::HintEditor *ui;
    Schema::Game * m_game;
    int i_taskIndex;
    QString s_mimetype;
    QByteArray m_data;
};


} // namespace Creator
#endif // CR_HINTEDITOR_H
