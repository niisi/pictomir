#ifndef JSONREADER_H
#define JSONREADER_H

#include <QtCore>
#include "abstractreader.h"
#include "structures.h"

namespace Core {

    class JsonReaderPrivate;

    class JsonReader
            : public QObject
            , public AbstractReader
    {
    public:
        JsonReader(QObject * parent = 0);
        QString parseData(const QByteArray &data, const QUrl &baseUrl, Game &game) const;
        void setContentProvider(Util::ContentProvider *provider);
        Util::ContentProvider * contentProvider();
        ~JsonReader();
    private:
        JsonReaderPrivate * d;
    };

}

#endif
