#ifndef COLORS_H
#define COLORS_H

#define CELL_BG QColor(0,128,0,96)
#define CELL_CONDITION_BG QColor(128,0,0,64)
#define CELL_REPEAT_BG QColor(0,0,128,64)
#define BG_COLOR "darkgray"
#define DOCK_COLOR "black"

#endif // COLORS_H
