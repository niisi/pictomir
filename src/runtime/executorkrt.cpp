#include <QtGui>
#include "runtime/executorkrt.h"
#include "robot25d/default_plugin.h"
#include "runtime/krt.h"

namespace Runtime {


ExecutorKRT::ExecutorKRT(QObject * parent)
    : QThread(parent)
{
    m_instance = this;
    b_stopping = false;
    b_initialized = false;
    b_animationEnabled = true;
    b_scheduledToDelete = false;
    mutex_sync = new QMutex;
    mutex_stopping = new QMutex;
    mutex_pausing = new QMutex;
    mutex_initialized = new QMutex;
    mutex_complexInstructionFinished = new QMutex;
    mutex_animationEnabled = new QMutex;
    mutex_delete = new QMutex;
    m_krt = new KRT();
    m_krt->connectSignalINTCallback(this, SLOT(INT_handler(quint16,QStack<QVariant>*)) );
}

ExecutorKRT::~ExecutorKRT()
{
    if (m_krt)
        delete m_krt;
    delete mutex_stopping;
    delete mutex_pausing;
    delete mutex_initialized;
    delete mutex_complexInstructionFinished;
    delete mutex_animationEnabled;
    delete mutex_delete;
    delete mutex_sync;

}

void ExecutorKRT::INT_handler(quint16 int_no, QStack<QVariant> *data_stack)
{
//    qDebug() << "ExecutorKRT: " << "received INT " << int_no;
    if (int_no==255) {
        // Подстветка простой инструкции
        mutex_animationEnabled->lock();
        bool processStep = b_animationEnabled;
        mutex_animationEnabled->unlock();
        if (!processStep)
            return;
        Q_ASSERT ( data_stack->size()>=3 );
        QVariant cell = data_stack->pop();
        QVariant func = data_stack->pop();
        QVariant paus = data_stack->pop();
        Q_ASSERT ( cell.type()==QVariant::Int );
        Q_ASSERT ( func.type()==QVariant::Int );
        int funcNo = func.toInt();
        int cellNo = cell.toInt();
        int pauseDuration = paus.toInt();
        Q_ASSERT ( funcNo >= 0 );
        Q_ASSERT ( cellNo >= -2);
        Q_ASSERT ( pauseDuration >= 0 );
        highlight(funcNo, cellNo, pauseDuration);
    }
    else if (int_no==254) {
        // Подстветка условия
        mutex_animationEnabled->lock();
        bool processStep = b_animationEnabled;
        mutex_animationEnabled->unlock();
        if (!processStep)
            return;
        Q_ASSERT ( data_stack->size()>=3 );
        QVariant func = data_stack->pop();
        QVariant cell = data_stack->pop();
        QVariant state = data_stack->pop();
        int funcNo = func.toInt();
        int cellNo = cell.toInt();
        bool value = state.toBool();
        QString color = value? "green" : "red";
        highlightCondition(funcNo, cellNo, color);
    }
    else if (int_no==253) {
        // Подстветка повторителя
        mutex_animationEnabled->lock();
        bool processStep = b_animationEnabled;
        mutex_animationEnabled->unlock();
        if (!processStep)
            return;
        Q_ASSERT ( data_stack->size()>=2 );
        QVariant func = data_stack->pop();
        QVariant val = data_stack->pop();
        int funcNo = func.toInt();
        int value = val.toInt();
        highlightRepeat(funcNo, value);
    }
    else if (int_no==252) {
        // Завершение одного шага
        mutex_complexInstructionFinished->lock();
        b_complexInstructionFinished = true;
        mutex_complexInstructionFinished->unlock();
    }
    else if (int_no==251) {
        // Установка текста о том, какая команда выполняется
        Q_ASSERT ( data_stack->size()>=1 );
        QVariant txt = data_stack->pop();
        s_currentCommandName = txt.toString();
    }
    else {
        Q_ASSERT ( data_stack->size()>=2 );
        QVariant requireReturnValue = data_stack->pop();
        QVariant argumentsCount = data_stack->pop();
        Q_ASSERT ( data_stack->size()>=argumentsCount.toInt() );
        QList<QVariant> arguments;
        for (int i=0; i<argumentsCount.toInt(); i++ ) {
            arguments << data_stack->pop();
        }
        int result = DO(int_no, arguments);
        if (requireReturnValue.toBool()) {
            QVariant retValue;
            retValue = result>0;
            data_stack->push(retValue);
        }
    }
}

void ExecutorKRT::init(
        Robot25D::Plugin *robot,
        const QStringList &program
        )
{
    QMutexLocker l1(mutex_stopping);
    QMutexLocker l2(mutex_pausing);
    QMutexLocker l3(mutex_initialized);
    QMutexLocker l4(mutex_complexInstructionFinished);
    QMutexLocker l5(mutex_animationEnabled);


    b_runtimeError = false;
    b_stepMode = false;
    b_stopping = false;
    b_pausing = false;
    b_firstStep = true;

    b_complexInstructionFinished = false;

//    qDebug() << program;
    if (program.isEmpty())
        return;
    m_robot = robot;

    Q_CHECK_PTR(m_robot);
    m_robot->reset();
    m_krt->init(program);
    b_initialized = true;
}

void ExecutorKRT::highlight(int funcNo, int cellNo, int pauseLength)
{
    emit highlightRequest(funcNo, cellNo);
    msleep(pauseLength*4);
}

void ExecutorKRT::highlightCondition(int funcNo, int cellNo,
                                     const QString &color)
{
    emit highlightConditionRequest(funcNo, cellNo, color);
    msleep(400);
}

void ExecutorKRT::highlightRepeat(int funcNo, int value)
{
    emit highlightRepeatRequest(funcNo, value);
    msleep(50);
}

int ExecutorKRT::DO(int id, const QList<QVariant> &arguments)
{
    Q_UNUSED(arguments);
    QString errorText;
    b_runtimeError = false;
    QLocale::Language lang = QLocale::C;
    QString lng = QCoreApplication::instance()->property("locale").toString();
    QLocale locale(lng);
    lang = locale.language();
    bool result = false;
    mutex_sync->lock();
    b_execFinished = false;
    mutex_sync->unlock();
    if (b_animationEnabled) {
        m_robot->evaluateCommandAnimated(id);
    }
    else {
        m_robot->evaluateCommandHidden(id);
    }

    errorText = m_robot->lastError(lang);
    qDebug() << "Last error: " << errorText;

    mutex_stopping->lock();
//    b_stopping = true;
//    b_runtimeError = true;
    mutex_stopping->unlock();

    if (b_animationEnabled && id >= 4) {
        waitForSync();
    }
    else {
        mutex_sync->lock();
        b_execFinished = true;
        mutex_sync->unlock();
    }

    mutex_delete->lock();
    bool del = b_scheduledToDelete;
    mutex_delete->unlock();
    if (del)
        return 0;

    if (isAnimationEnabled())
        msleep(200);
    if (!errorText.isEmpty()) {
        error(errorText);
        return 0;
    }
    else {
        result = m_robot->lastResult().toBool();
    }
    if (result)
        return 1;
    else
        return 0;
}

void ExecutorKRT::run()
{    
    Q_CHECK_PTR(m_robot);

    if (!b_initialized)
        return;
    if (!b_stepMode) {
        msleep(1000); // wait until ui changes
        forever {
            mutex_stopping->lock();
            mutex_pausing->lock();
            bool executeNext = !b_stopping && !b_pausing && m_krt->hasMoreInstructions() &&!b_scheduledToDelete;
            mutex_stopping->unlock();
            mutex_pausing->unlock();
            if (executeNext)
                m_krt->executeNextInstruction();
            else
                break;
        }
    }
    else {
        forever {
            mutex_complexInstructionFinished->lock();
            mutex_stopping->lock();
            mutex_delete->lock();
            bool executeNext = m_krt->hasMoreInstructions() &&
                               !b_complexInstructionFinished &&
                               !b_stopping &&
                               !b_scheduledToDelete;
            mutex_stopping->unlock();
            mutex_complexInstructionFinished->unlock();
            mutex_delete->unlock();
            if (executeNext)
                m_krt->executeNextInstruction();
            else
                break;
        }
    }
    mutex_delete->lock();
    bool del = b_scheduledToDelete;
    mutex_delete->unlock();
    if (del) {
        return;
    }
    const QString executionTerminatedStr = QApplication::translate("Runtime::ExecutorKRT", "Execution terminated");
    const QString executionFinishedStr = QApplication::translate("Runtime::ExecutorKRT", "Execution finished");
    const QString executionStartedStr = QApplication::translate("Runtime::ExecutorKRT", "Execution started. Next is: %1").arg(s_currentCommandName);
    const QString stepFinishedStr = QApplication::translate("Runtime::ExecutorKRT", "Step finished. Next is: '%1'").arg(s_currentCommandName);
    mutex_stopping->lock();
    if (b_stopping || !m_krt->hasMoreInstructions()) {
        m_krt->clear();
        b_initialized = false;
        emit executionFinished(b_stopping ? executionTerminatedStr : executionFinishedStr);
    }
    else if (b_stepMode && !b_stopping) {
        emit stepFinished(b_firstStep ? executionStartedStr : stepFinishedStr);
        b_firstStep = false;
    }
    mutex_stopping->unlock();
}

void ExecutorKRT::step()
{
    b_stepMode = true;
    QMutexLocker l1(mutex_complexInstructionFinished);
    b_complexInstructionFinished = false;
    QMutexLocker l2(mutex_stopping);
    b_stopping = false;
    start();
}

void ExecutorKRT::pause()
{
    QMutexLocker l(mutex_pausing);
    b_pausing = true;
    b_firstStep = false;
}

void ExecutorKRT::stop()
{
    QMutexLocker l1(mutex_stopping);
    QMutexLocker l2(mutex_pausing);
    b_stopping = true;
    if (b_pausing)
        emit executionFinished(QApplication::translate("Runtime::ExecutorKRT", "Execution paused"));
    if (!isRunning()) {
        m_krt->clear();
        b_initialized = false;
        emit executionFinished(QApplication::translate("Runtime::ExecutorKRT", "Execution terminated"));
    }
}

void ExecutorKRT::dispose()
{
    mutex_delete->lock();
    b_scheduledToDelete = true;
    mutex_delete->unlock();
    mutex_stopping->lock();
    b_stopping = true;
    mutex_stopping->unlock();
    mutex_sync->lock();
    b_execFinished = true;
    mutex_sync->unlock();
}

void ExecutorKRT::waitForStopped()
{
    while (isRunning()) {
        msleep(20);
    }
}

void ExecutorKRT::setAnimationEnabled(bool v)
{
    QMutexLocker l(mutex_animationEnabled);
    b_animationEnabled = v;
}

void ExecutorKRT::continuousRun()
{
    QMutexLocker l1(mutex_stopping);
    QMutexLocker l2(mutex_pausing);
    QMutexLocker l3(mutex_complexInstructionFinished);
    b_pausing = false;
    b_stepMode = false;
    b_stopping = false;
    start();
}

void ExecutorKRT::waitForSync()
{
	bool value;
#ifdef Q_OS_WIN32
	const unsigned int TIMEOUT=1;
#endif
#ifdef Q_OS_UNIX
	const unsigned int TIMEOUT=1;
#endif
#ifdef Q_OS_WINCE
	const unsigned int TIMEOUT=1;
#endif
    forever {
        mutex_sync->lock();
        value = b_execFinished;
        mutex_sync->unlock();
        if (value) {
            break;
        }
        else {
            msleep(TIMEOUT);
        }
    }
}

void ExecutorKRT::commandFinished()
{
    qDebug() << __FUNCTION__;
    mutex_sync->lock();
    b_execFinished = true;
    mutex_sync->unlock();
}


void ExecutorKRT::error(const QString &message)
{
//    qDebug() << "Calling error with message: " << message;
    QMutexLocker l1(mutex_stopping);
    b_stopping = true;
    b_runtimeError = true;
    b_initialized = false;
    t_backtrace = m_krt->backtraceInfo();
    emit executionError(message);
}

QStack<QPair<int,int> > ExecutorKRT::backtrace() const
{
    QStack<QString> bt = t_backtrace;
    QStack< QPair<int, int> > result;
    for (int i=0; i<bt.size(); i++) {
        QStringList pair = bt[i].split(",");
        if (pair.size()==2) {
            int funcNo = pair[0].toInt();
            int cellNo = pair[1].toInt();
            result.push( QPair<int,int> (funcNo, cellNo) );
        }
    }
    return result;
}

}
