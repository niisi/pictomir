#ifndef LOADGAMEDIALOG_H
#define LOADGAMEDIALOG_H

#include <QtGui>
#include "ui_loadgamedialog.h"


namespace Core {
    class GameProvider;
}

namespace Desktop {

class RenameGameDialog;

class LoadGameDialog:
        public QDialog,
        private Ui_LoadGameDialog
{
    Q_OBJECT;
public:
    explicit LoadGameDialog(Core::GameProvider *provider, QWidget *parent);
    void init(const QStringList &items, int selectedIndex);
    int selectedIndex() const;
private slots:
    void handleGameItemSelected(QListWidgetItem* item);
    void handleCreateNewClicked();
    void handleDeleteClicked();
    void handleRenameClicked();
    void handleCloneClicked();
private:
    Core::GameProvider *m_gameProvider;
    RenameGameDialog *m_renameGameDialog;


};

}

#endif // LOADGAMEDIALOG_H
