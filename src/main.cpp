#include <QtCore>
#include <QtGui>
#include "core/pictomir.h"
#include "core/platform_configuration.h"
#include "webkit/w_mainwindow.h"
#include "desktop/mainwindow.h"
#include "mobile/m_mainwindow.h"
#include "creator/cr_mainwindow.h"
#include "version.def"
#include "util/svgrenderfarm.h"

using namespace Core;
using namespace Desktop;



bool isContentWritable(const QString &path) {
    QDir dir(path);
    if (!QFileInfo(path).isWritable()) {
        return false;
    }
    QStringList entries = dir.entryList();
    foreach (const QString &entry, entries) {
        if (entry.startsWith("."))
            continue;
        bool writable;
        if (QFileInfo(dir.absoluteFilePath(entry)).isDir())
            writable = isContentWritable(dir.absoluteFilePath(entry));
        else
            writable = QFileInfo(dir.absoluteFilePath(entry)).isWritable();
        if (!writable)
            return false;
    }
    return true;
}

bool tasksAndEnvironmentsWritable() {
    return isContentWritable(PlatformConfiguration::tasksPath())
            && isContentWritable(PlatformConfiguration::envPath());
}

Core::PictomirUiType detectUiType() {
#if defined(Q_OS_WINCE) || defined(Q_OS_SYMBIAN)
    return Core::MobileUi;
#else
    Core::PictomirUiType result = Core::DesktopUi;
    foreach (const QString &arg, qApp->arguments()) {
        if (arg.startsWith("--ui=")) {
            const QString uiType = arg.mid(5).toLower();
            if (uiType=="mobile") {
                result = Core::MobileUi;
            }
            else if (uiType=="webkit" || uiType=="navigator") {
                result = Core::WebKitUi;
            }
            else if (uiType=="creator") {
                result = Core::CreatorUi;
            }
        }
    }
    return result;
#endif
}

QPair<QString,QString> detectLanguage() {
    QLocale loc = QLocale::system();
//    QString language = QLocale::languageToString(loc.language()).toLower();
    QString language = "russian";
    QString localeName = loc.name();
    foreach (const QString arg, qApp->arguments()) {
        if (arg.startsWith("--language=")) {
            language = arg.mid(11);
        }
    }

    if (language=="russian")
        localeName = "ru_RU";
    qApp->setProperty("language", language);

    return QPair<QString,QString>(language, localeName);
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QCoreApplication::setOrganizationName(QString::fromUtf8("НИИСИ РАН"));
    QCoreApplication::setApplicationName(QString::fromUtf8("ПиктоМир"));
    QCoreApplication::setApplicationVersion(VERSION);


    Core::PictomirUiType uiType = detectUiType();
    QString splashFileName = PlatformConfiguration::resourcesPath()+"/splashscreen";
    if (uiType==Core::CreatorUi) {
        splashFileName = PlatformConfiguration::resourcesPath()+"/splashscreen-creator";
        a.setWindowIcon(QIcon(PlatformConfiguration::imagesPath()+"/creator_icon.png"));
    }
    else if (uiType==Core::WebKitUi) {
        splashFileName = PlatformConfiguration::resourcesPath()+"/splashscreen-navigator";
        a.setWindowIcon(QIcon(PlatformConfiguration::imagesPath()+"/navigator_icon.png"));
    }
    else {
        a.setWindowIcon(QIcon(PlatformConfiguration::imagesPath()+"/pictomir_icon.png"));
    }


    a.setProperty("pluginsPath", PlatformConfiguration::modulesPath());


    QTranslator *translator = new QTranslator(&a);
    const QString translatorsPath = PlatformConfiguration::translatorsPath();
    const QPair<QString, QString> lang = detectLanguage();
    if (translator->load(lang.first, translatorsPath)) {
        a.installTranslator(translator);
    }
    else {
        delete translator;
    }
    a.setProperty("language", lang.first);
    a.setProperty("locale", lang.second);


    const QSize defaultSize = uiType==Core::MobileUi? QSize(240, 320) : QSize(1000, 600);
//    bool fullscreen = uiType==Core::MobileUi;
    QSize uiSize;
    bool useCustomSize = false;
    foreach (const QString &arg, qApp->arguments()) {
        if (arg.startsWith("--size")) {
            QStringList pair = arg.mid(7).split(",");
            bool ok1;
            bool ok2;
            int w = pair.at(0).toInt(&ok1);
            int h = pair.at(1).toInt(&ok2);
            if (ok1 && ok2) {
                uiSize = QSize(w,h);
                useCustomSize = true;
            }
        }
        else if (arg=="--fullscreen") {
//            fullscreen = true;
        }
    }

    if (!useCustomSize) {
        uiSize = defaultSize;
    }

    QSplashScreen *splash = 0;
    if (uiType==MobileUi) {
        QSize screenSize = QApplication::desktop()->screenGeometry().size();
        QImage image(screenSize, QImage::Format_ARGB32);
        image.fill(QColor("black").rgba());
        QRect rect = QRect(0,0,image.width(), image.height());
        QImage logo(":/resources/pictomir_icon.png");
        QPainter p(&image);
        qDebug() << rect;
        p.drawImage(rect.center() + QPoint(-logo.width()/2, -logo.height()/2),logo);
        p.setPen(QColor("white"));
        QFont fnt = p.font();
        fnt.setPixelSize(32);
        fnt.setBold(true);
        p.setFont(fnt);
        //        int textX = centerX - QFontMetrics(p.font()).width(a.applicationName());
        int textWidth = QFontMetrics(fnt).width(a.applicationName());
        int textHeight = QFontMetrics(fnt).height();
        //        int textY = centerY + logo.height()/2 + 16;
        p.drawText(rect.center() + QPoint(-textWidth/2, logo.height()/2 + 10 + textHeight),
                   a.applicationName());
        p.end();
        splash = new QSplashScreen(QPixmap::fromImage(image));


    }
    else {
        splashFileName+="_"+lang.first+".png";
        splash = new QSplashScreen(splashFileName);
    }
    if (uiType==MobileUi)
        splash->showFullScreen();
    else
        splash->show();

    QWidget *mw = NULL;

    PictomirCommon * pictomir = new PictomirCommon(&a);

    if (uiType==Core::DesktopUi) {
        mw = new Desktop::MainWindow(pictomir);
    }
    else if (uiType==Core::CreatorUi) {
        mw = new Creator::MainWindow(pictomir);
    }
    else if (uiType==Core::MobileUi) {
//        mw = new Mobile::MainWindow(pictomir);
        qFatal("Mobile not implemented yet");
    }
    else {
        mw = new Webkit::MainWindow(pictomir);
    }
    QObject::connect (qApp, SIGNAL(lastWindowClosed()), qApp, SLOT(quit()));
    splash->finish(mw);
    mw->show();
    int result = a.exec();
    delete mw;
    delete splash;
    delete pictomir;
    Util::SvgRenderFarm::clear();
    return result;
}
