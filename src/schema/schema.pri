
HEADERS += \
    schema/sch_environment.h \
    schema/sch_algorhitm.h \
    schema/sch_command.h \
    schema/sch_program.h \
    schema/sch_task.h \
    schema/sch_game.h

SOURCES += \
    schema/sch_environment.cpp \
    schema/sch_algorhitm.cpp \
    schema/sch_command.cpp \
    schema/sch_program.cpp \
    schema/sch_task.cpp \
    schema/sch_game.cpp
