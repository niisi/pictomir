#ifndef CR_MAINWINDOW_H
#define CR_MAINWINDOW_H

#include <QtCore>
#include <QtGui>

namespace Core {
    class PictomirCommon;
}

namespace Schema {
    struct Game;
}

namespace Util {
    class AboutDialog;
}

namespace Creator {

class TaskListWindow;

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(Core::PictomirCommon * pictomir, QWidget *parent = 0);
    ~MainWindow();

public slots:
    void newGame();
    void loadGame();
    void loadRecentGame();
    void showEnvEditor(bool);
    void showProgramEditor(bool);
    void showHintEditor(bool);

private slots:
    void saveState();
    void restoreState();
    void saveGame();
    void saveGameAs();

    void prepareGameMenu();
    void prepareTaskMenu();
    void prepareEnvironmentMenu();
    void prepareProgramMenu();
    void prepareHintMenu();
    void showHelp();

private:
    void closeEvent(QCloseEvent *event);
    Schema::Game loadGameFromFile(const QString &fileName, QString &error);
    QPair<Schema::Game*, int> resolveTask() const;
    TaskListWindow * resolveTaskListWindow(const Schema::Game * gamePtr);

    QString resolveGameFileName(const QString &baseName) const;
    Ui::MainWindow *ui;
    Util::AboutDialog * m_about;
    Core::PictomirCommon * m_pictomir;
    quint16 i_newGameIndex;
};


} // namespace Creator
#endif // CR_MAINWINDOW_H
