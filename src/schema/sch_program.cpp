#include "sch_program.h"
#include "util/fp.h"

namespace Schema {

extern bool parceJSON(const QScriptValue &value, Program &program)
{
    if (value.isArray()) {
        int len = value.property("length").toInteger();
        for (int i=0 ; i<len; i++) {
            QScriptValue jsAlg = value.property(i);
            Algorhitm alg;
            if (parceJSON(jsAlg, alg)) {
                program << alg;
            }
        }

    }
    return true;
}

extern bool parceAGKXML(const QList<QDomNode> & algSpecs, const QDomNode & programNode, Program &program)
{
    QList<QDomElement> algs;
    for (int i=0; i<programNode.childNodes().count(); i++) {
        if (programNode.childNodes().at(i).isElement() &&
            programNode.childNodes().at(i).nodeName()=="alg")
            algs << programNode.childNodes().at(i).toElement();
    }
    for (int i=0; i<algSpecs.size(); i++) {
        Algorhitm alg;
        QDomNode spec = algSpecs[i];
        QDomNode content;
        if (i<algs.size()) {
            content = algs[i];
        }
        if (parceAGKXML(spec, content, alg)) {
            program << alg;
        }
    }
    return true;
}

extern QString addLeadingTab(const QString &t);

extern QString generateJSON(const Program &program)
{
    QStringList r;
    for (int i=0; i<program.size(); i++) {
        QStringList lines = generateJSON(program[i]).split("\n");
        for (int j=0; j<lines.size(); j++) {
            lines[j] = ""+lines[j];
        }
        r << lines.join("\n").trimmed();
    }
    QString result = "[ ";
    result += r.join(", ");
    result += " ]";
    return result;
}

extern QString generateKumProgram(const Program &program)
{
    QString result = QString::fromUtf8(
                "использовать Вертун\n"
                "\n"
                );
    static const QString Letters = QString::fromUtf8(" АБВГДЕЖЗИКЛМНОПРСТУФХЦЧШЩЭЮЯ");
    for (int i=0; i<program.size(); i++) {
        if (i > 0) {
            result += "\n\n";
        }
        const QString algName = i == 0
                ? QString::fromUtf8("Главный")
                : QString::fromUtf8("Команда %1").arg(Letters.at(i));
        result += QString::fromUtf8("алг %1\n").arg(algName);
        result += QString::fromUtf8("нач\n");
        const Algorhitm & algorithm = program.at(i);
        result += generateKumBody(algorithm);
        result += "\n";
        result += QString::fromUtf8("кон\n\n");
    }
    result += QString::fromUtf8(""
                                "алг лит @просклонять(цел число)|@hidden\n"
                                "нач|@hidden\n"
                                "цел m|@hidden\n"
                                "m:=mod(число, 10)|@hidden\n"
                                "выбор|@hidden\n"
                                "при m=1 и число<>11:|@hidden\n"
                                "знач:=\"незакрашенная клетка\"|@hidden\n"
                                "при (m=2 или m=3 или m=4) и m<>12 и m<>13 и m<>14:|@hidden\n"
                                "знач:=\"незакрашенные клетки\"|@hidden\n"
                                "иначе|@hidden\n"
                                "знач:=\"незакрашенных клеток\"|@hidden\n"
                                "все|@hidden\n"
                                "кон|@hidden\n"
                                "|@hidden\n"
                                "алг цел @тестирование|@hidden\n"
                                "нач|@hidden\n"
                                "Главный|@hidden\n"
                                "лог все_ точки закрашены|@hidden\n"
                                "все_ точки закрашены:=да|@hidden\n"
                                "цел x, y, w, h|@hidden\n"
                                "вещ количество всех точек, количество незакрашенных точек|@hidden\n"
                                "вещ минимум если_ программа выполнена, максимум если_ не_ все_ закрашено, вещ_ оценка|@hidden\n"
                                "минимум если_ программа выполнена:=2|@hidden\n"
                                "максимум если_ не_ все_ закрашено:=6|@hidden\n"
                                "количество всех точек:=0|@hidden\n"
                                "количество незакрашенных точек:=0|@hidden\n"
                                "h:=@@размер поля y|@hidden\n"
                                "w:=@@размер поля x|@hidden\n"
                                "нц для y от 1 до h|@hidden\n"
                                "нц для x от 1 до w|@hidden\n"
                                "если @@есть точка(x, y) то|@hidden\n"
                                "количество всех точек := количество всех точек + 1|@hidden\n"
                                "если @@клетка не закрашена(x, y) то|@hidden\n"
                                "все_ точки закрашены:=нет|@hidden\n"
                                "количество незакрашенных точек:=количество незакрашенных точек + 1|@hidden\n"
                                "все|@hidden\n"
                                "все|@hidden\n"
                                "кц|@hidden\n"
                                "кц|@hidden\n"
                                "если все_ точки закрашены |@hidden\n"
                                "то |@hidden\n"
                                "вывод \"Задание выполнено\", нс|@hidden\n"
                                "знач:=10|@hidden\n"
                                "иначе|@hidden\n"
                                "вывод \"Задание не выполнено: осталось\"|@hidden\n"
                                "вывод \" \", int(количество незакрашенных точек), \" \"|@hidden\n"
                                "вывод @просклонять(int(количество незакрашенных точек))|@hidden\n"
                                "вещ_ оценка:=(количество всех точек - количество незакрашенных точек) / количество всех точек * (максимум если_ не_ все_ закрашено - минимум если_ программа выполнена)|@hidden\n"
                                "знач:=int(минимум если_ программа выполнена + вещ_ оценка)|@hidden\n"
                                "все|@hidden\n"
                                "кон|@hidden\n"
                                "");
    return result;
}

extern bool isEqual(const Program &a, const Program &b)
{
    if (a.size()==b.size()) {
        bool difference = false;
        for (int i=0; i<a.size(); i++) {
            difference |= ! isEqual(a[i], b[i]);
        }
        return !difference;
    }
    else {
        return false;
    }
}

} // namespace Schema
