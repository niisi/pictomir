#include "httpservice/miniserver.h"
#include "core/platform_configuration.h"

#include <QtScript>

namespace HTTPService {


struct MiniServerResponse
{
    QString uri;
    QString mimeType;
    QByteArray data;
    int code;
    QString errorText;
};

class MiniServerNetworkReply
        : public QNetworkReply

{
public:
    explicit MiniServerNetworkReply(const MiniServerResponse &response, const QNetworkRequest &request, QObject * parent = 0);
    ~MiniServerNetworkReply();
    qint64 bytesAvailable() const;
    inline bool isSequential() const { return true; }
    inline void abort() { }
protected:
    qint64 readData(char *data, qint64 maxlen);
    qint64 position;
private:
    QByteArray data;
    QString contentType;
};


qint64 MiniServerNetworkReply::bytesAvailable() const
{
    return data.size();
}

qint64 MiniServerNetworkReply::readData(char *buffer, qint64 maxlen)
{    
    qint64 cnt = qMin<qint64>(maxlen, data.size()-position);
    for (int i=0; i<cnt; i++) {
        const char ch = data.at(position+i);
        buffer[i] = ch;
    }
    position += cnt;
    return cnt;
}



MiniServerNetworkReply::MiniServerNetworkReply(const MiniServerResponse &response, const QNetworkRequest &request, QObject * parent)
    : QNetworkReply(parent)
{
    Q_UNUSED(request);
    setReadBufferSize(1024*1024*10);
    QNetworkReply::open(QIODevice::ReadOnly);
    position = 0;
    contentType = response.mimeType;
    if (response.code==200) {
        // HTTP 200 is OK
        data = response.data;
    }
    else {
        QString error = QString("<html><head><title>Error %1</title></head><body><h1>Error %1</h1><p>%2</p></body></html>")
                .arg(response.code)
                .arg(response.errorText);
        data = error.toUtf8();
    }
    QUrl qualifedUrl;
    qualifedUrl.setScheme("local");
    qualifedUrl.setPath(response.uri);
    setUrl(qualifedUrl);
    setHeader(QNetworkRequest::LastModifiedHeader, QDateTime::currentDateTime());
    setHeader(QNetworkRequest::ContentLengthHeader, data.size());
    setHeader(QNetworkRequest::ContentTypeHeader, response.mimeType);
    setHeader(QNetworkRequest::LocationHeader, qualifedUrl);
    QMetaObject::invokeMethod(this, "metaDataChanged", Qt::QueuedConnection);
    QMetaObject::invokeMethod(this, "downloadProgress", Qt::QueuedConnection,
                              Q_ARG(qint64, data.size()), Q_ARG(qint64, data.size()));
    QMetaObject::invokeMethod(this, "readyRead", Qt::QueuedConnection);
    QMetaObject::invokeMethod(this, "finished", Qt::QueuedConnection);
}

MiniServerNetworkReply::~MiniServerNetworkReply()
{

}

class MiniServerPrivate
{
public:
    MiniServer * q;
    QString localRoot;
    QScriptEngine * scriptEngine;

    void reloadConfiguration();
    QString preprocess(QString &t, const QString &fileName);
    MiniServerResponse GET(const QUrl &url);
    MiniServerResponse POST(const QUrl &url, const QByteArray &postData);
};

MiniServerResponse MiniServerPrivate::GET(const QUrl &url) {
    const QString path = "/"+url.host()+"/"+url.path();
    MiniServerResponse resp;
    QString fileName = localRoot+"/"+path;
    while (fileName.contains("//")) {
        fileName.replace("//","/");
    }
    if (fileName.endsWith("/")) {
        if (QFile::exists(fileName+"index.pms.html"))
            fileName = fileName + "index.pms.html";
        else
            fileName += "index.html";
    }
    if (QFile::exists(fileName)) {
        QFile f(fileName);
        if (f.open(QIODevice::ReadOnly)) {
            if (fileName.endsWith(".pms.html")) {
                QString d = QString::fromUtf8(f.readAll());
                resp.errorText = preprocess(d, fileName);
                resp.data = d.toUtf8();
            }
            else {
                resp.data = f.readAll();
//                qDebug() << resp.data;
            }
            resp.code = resp.errorText.isEmpty() ? 200 : 500;
            f.close();
        }
        else {
            resp.code = 503;
            resp.errorText = "File access denied: "+fileName;
        }
    }
    else {
        resp.code = 404;
        resp.errorText = "File not found: "+fileName;
    }
    if (fileName.endsWith(".js")) {
        resp.mimeType = "text/javascript;charset=utf-8";
    }
    else if (fileName.endsWith(".json")) {
        resp.mimeType = "text/json;charset=utf-8";
    }
    else if (fileName.endsWith(".svg")) {
        resp.mimeType = "image/svg+xml";
    }
    else if (fileName.endsWith(".css")) {
        resp.mimeType = "text/css;charset=utf8";
    }
    else if (fileName.endsWith(".png")) {
        resp.mimeType = "image/png";
    }
    else if (fileName.endsWith(".jpg")||fileName.endsWith(".jpeg")) {
        resp.mimeType = "image/jpeg";
    }
    else if (fileName.endsWith(".gif")) {
        resp.mimeType = "image/gif";
    }
    else {
        resp.mimeType = "text/html;charset=utf-8";
    }
    resp.uri = fileName.mid(localRoot.size());
    return resp;
}

QString MiniServerPrivate::preprocess(QString &s, const QString &filePath)
{
    if (!(filePath.endsWith(".pms.html") || filePath.endsWith(".pms.json") || filePath.endsWith(".pms.js"))) {
        return "";
    }
    QRegExp rxInclude("\\{%\\s*include\\s+(\\S+)(\\s+\\S+)*\\s*%\\}");
    rxInclude.setMinimal(true);
    QRegExp rxEvaluate("\\{%\\s*evaluate\\s+(.+)\\s*%\\}");
    rxEvaluate.setMinimal(true);
    int pos = 0;
    QString basePath = filePath;
    if (!basePath.endsWith("/")) {
        pos = basePath.lastIndexOf("/");
        basePath = basePath.left(pos)+"/";
    }
    forever {
        pos = rxInclude.indexIn(s);
        if (pos!=-1) {
            const QString fileName = rxInclude.cap(1);
            const QString fullName = basePath + fileName.trimmed();
            QFile f(fullName);
            if (f.open(QIODevice::ReadOnly)) {
                QString includeData = QString::fromUtf8(f.readAll());
                f.close();
                if (rxInclude.capturedTexts().size()>2) {
                    const QStringList params = rxInclude.cap(2).split(",");
                    for (int i=0; i<params.size(); i++) {
                        const QString patt = QString("{{%1}}").arg(i+1);
                        const QString repl = params[i].trimmed();
                        includeData.replace(patt, repl);
                    }
                }
                QString localError = preprocess(includeData, fullName);

                if (!localError.isEmpty()) {
                    return localError;
                }

                int len = rxInclude.matchedLength();
                s.replace(pos, len, includeData);
            }
            else {
                return "Can't open file: "+fullName;
            }
        }
        else {
            break;
        }
    }
    forever {
        pos = rxEvaluate.indexIn(s);
        if (pos!=-1) {
            const QString program = rxEvaluate.cap(1);
            QScriptValue value = scriptEngine->evaluate(program);
            if (value.isError()) {
                return value.toString();
            }
            int len = rxEvaluate.matchedLength();
            s.replace(pos, len, value.toString());
        }
        else {
            break;
        }
    }
    return "";
}

void MiniServerPrivate::reloadConfiguration()
{
    const QString configFile = localRoot+"/miniserver.config.json";
    QFile f(configFile);
    if (f.open(QIODevice::ReadOnly)) {
        const QString data = QString::fromLocal8Bit(f.readAll());
        f.close();
        QScriptValue root = scriptEngine->evaluate(data);
        Q_UNUSED(root);
    }
}

QNetworkReply * MiniServer::serveRequest(QNetworkAccessManager * manager, QNetworkAccessManager::Operation op, const QNetworkRequest &request, QIODevice *outgoingData)
{
    MiniServerResponse resp;
    if (op==QNetworkAccessManager::PostOperation) {
        QByteArray postData = outgoingData->readAll();
//        resp = d->POST(request.url(), postData);
        Q_UNUSED(postData); resp = d->GET(request.url()); // TODO implement POST method
    }
    else {
        resp = d->GET(request.url());
    }
    MiniServerNetworkReply * reply = new MiniServerNetworkReply(resp, request, manager);
    return reply;
}

MiniServer::MiniServer(QObject *parent) :
    QObject(parent)
{
    d = new MiniServerPrivate;
    d->scriptEngine = new QScriptEngine;
    setLocalRoot(Core::PlatformConfiguration::shareRoot()+"/server_data/");
}

void MiniServer::setLocalRoot(const QString &path)
{
    d->localRoot = path;
    d->reloadConfiguration();
}

QString MiniServer::localRoot() const
{
    return d->localRoot;
}

MiniServer::~MiniServer()
{
    d->scriptEngine->deleteLater();
    delete d;
}

} // namespace HTTPService


