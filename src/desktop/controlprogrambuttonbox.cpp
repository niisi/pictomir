#include "desktop/controlprogrambuttonbox.h"
#include "core/platform_configuration.h"
#include "util/svgbutton.h"

using namespace Core;
using namespace Util;

namespace Desktop {


ControlProgramButtonBox::ControlProgramButtonBox(QGraphicsItem *parent)
    : QObject(0)
    , QGraphicsRectItem(parent)

{
    setPen(Qt::NoPen);
    g_start = new SvgButton("ui_desktop/program_control_buttons.svg", "start");
    g_stop = new SvgButton("ui_desktop/program_control_buttons.svg", "stop");
    g_pause = new SvgButton("ui_desktop/program_control_buttons.svg", "pause");
    g_reset = new SvgButton("ui_desktop/program_control_buttons.svg", "reset");
//    g_step = new SvgButton("ui_desktop/program_control_buttons.svg", "step");
    g_debug = new SvgButton("ui_desktop/program_control_buttons.svg", "debug");
    g_fastRun = new SvgButton("ui_desktop/program_control_buttons.svg", "fastrun");


    g_start->setParentItem(this);
    g_stop->setParentItem(this);
    g_pause->setParentItem(this);
    g_reset->setParentItem(this);
    g_debug->setParentItem(this);
    g_fastRun->setParentItem(this);

    connect(g_start, SIGNAL(clicked()), this, SIGNAL(startClicked()));
    connect(g_stop, SIGNAL(clicked()), this, SIGNAL(stopClicked()));
    connect(g_pause, SIGNAL(clicked()), this, SIGNAL(pauseClicked()));
    connect(g_reset, SIGNAL(clicked()), this, SIGNAL(resetClicked()));
//    connect(g_step, SIGNAL(clicked()), this, SIGNAL(stepClicked()));
    connect(g_debug, SIGNAL(clicked()), this, SIGNAL(debugClicked()));
    connect(g_fastRun, SIGNAL(clicked()), this, SIGNAL(fastRunClicked()));

    setRect(0, 0, 180, 130);
    e_currentState = EditingProgram;
    setProgramState(e_currentState);

}

void ControlProgramButtonBox::setProgramState(ProgramState state)
{
    if (state==EditingProgram) {
        g_pause->disable(QPointF(0,0), QSizeF(0,0));
        g_stop->disable(QPointF(90, 130), QSizeF(0,0));
        g_reset->disable(QPointF(90, 0), QSizeF(0,0));
//        g_step->disable(QPointF(90,130), QSizeF(0,0));

        g_start->enable(QPointF(56, 50), QSizeF(64, 64));
        g_debug->enable(QPointF(16, 12), QSizeF(40, 40));
        g_fastRun->enable(QPointF(120, 12), QSizeF(40, 40));
        g_reset->enable(QPointF(68, 0), QSizeF(40,40));
    }
    else if (state==RunningProgram) {
//        if (e_currentState!=PauseProgram) {
//            g_start->disable(QPointF(90,0), QSizeF(0,0));
//            g_reset->disable(QPointF(96, 0), QSizeF(0,0));
//            g_debug->disable(QPointF(0,130), QSizeF(0,0));

//            g_stop->enable(QPointF(16, 4), QSizeF(40, 40));
//            g_pause->enable(QPointF(59, 20), QSizeF(58, 58));
//            g_fastRun->enable(QPointF(120, 4), QSizeF(40, 40));
//        }
//        else {
//            g_pause->disable(QPointF(59, 20), QSizeF(0, 0));
//            g_start->disable(QPointF(68, 63), QSizeF(40, 40));

//        }
        if (e_currentState!=PauseProgram) {
            g_start->disable(QPointF(90,0), QSizeF(0,0));
            g_reset->disable(QPointF(96, 0), QSizeF(0,0));
            g_debug->disable(QPointF(0,130), QSizeF(0,0));

            g_pause->enable(QPointF(16, 4), QSizeF(40, 40));
            g_stop->enable(QPointF(59, 20), QSizeF(58, 58));
            g_fastRun->enable(QPointF(120, 4), QSizeF(40, 40));
        }
        else {
            g_pause->disable(QPointF(59, 20), QSizeF(0, 0));
            g_start->disable(QPointF(68, 63), QSizeF(40, 40));

        }
    }
    else if (state==PauseProgram) {
        g_pause->disable(QPointF(59, 20), QSizeF(0, 0));
        g_reset->disable(QPointF(90, 0), QSizeF(0,0));

        g_debug->enable(QPointF(59,0), QSizeF(58, 58));
        g_fastRun->enable(QPointF(120, 4), QSizeF(40, 40));
        g_stop->enable(QPointF(16, 4), QSizeF(40, 40));
        g_start->enable(QPointF(68, 63), QSizeF(40, 40));
//        g_step->enable(QPointF(0,0), QSizeF(40,40));
    }
    else if (state==AnalisysProgram) {
        g_stop->disable(QPointF(90, 130), QSizeF(0,0));
        g_pause->disable(QPointF(0,0), QSizeF(0, 0));
//        g_step->disable(QPointF(90,130), QSizeF(0,0));

        g_reset->enable(QPointF(60, 12), QSizeF(56, 56));
        g_start->enable(QPointF(68,80), QSizeF(40,40));
        g_debug->enable(QPointF(20, 80), QSizeF(40, 40));
        g_fastRun->enable(QPointF(116, 80), QSizeF(40, 40));
    }
    else {
        qFatal("Wrong program state!");
    }
    e_currentState = state;
}
}
