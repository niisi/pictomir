HEADERS += desktop/mainwindow.h \
    desktop/functionwidget.h \
    desktop/selectuserdialog.h \
    desktop/loadgamedialog.h \
    desktop/renamegamedialog.h \
    desktop/dockwidget.h \
    desktop/navigatetaskbuttonbox.h \
    desktop/controlprogrambuttonbox.h \
    desktop/actionprogramview.h \
    desktop/actiondockview.h \
    desktop/helperitem.h \
    desktop/hintitem.h
SOURCES += desktop/mainwindow.cpp \
    desktop/functionwidget.cpp \
    desktop/selectuserdialog.cpp \
    desktop/loadgamedialog.cpp \
    desktop/renamegamedialog.cpp \
    desktop/dockwidget.cpp \
    desktop/navigatetaskbuttonbox.cpp \
    desktop/controlprogrambuttonbox.cpp \
    desktop/actionprogramview.cpp \
    desktop/actiondockview.cpp \
    desktop/helperitem.cpp \
    desktop/hintitem.cpp
FORMS += desktop/selectuser.ui \
    desktop/loadgamedialog.ui \
    desktop/renamegamedialog.ui \
    desktop/mainwindow.ui \
    desktop/savestatedialog.ui
