#ifndef WEBPAGE_H
#define WEBPAGE_H

#include <QtWebKit>

namespace Webkit {

class PageGlobalObject;

class WebPage
        : public QWebPage
{
    Q_OBJECT;
public:
    explicit WebPage(QObject * parent = 0);

public slots:
    void setJavaScriptObjects();
signals:
    void newPageCreated(QWebPage * page);
protected:
    QObject * createPlugin(const QString &classid, const QUrl &url, const QStringList &paramNames, const QStringList &paramValues);
    QObject * createRobot(const QUrl &url, const QStringList &paramNames, const QStringList &paramValues);
    QObject * createShelf(const QUrl &url, const QStringList &paramNames, const QStringList &paramValues);
    QObject * createAlgorhitm(const QUrl &url, const QStringList &paramNames, const QStringList &paramValues);
    QObject * createForm(const QUrl &url, const QStringList &paramNames, const QStringList &paramValues);
    QWebPage * createWindow(WebWindowType type);
    PageGlobalObject * obj;


};

}

#endif // WEBPAGE_H
