#include "renamegamedialog.h"
#include "ui_renamegamedialog.h"


namespace Desktop {
RenameGameDialog::RenameGameDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RenameGameDialog)
{
    ui->setupUi(this);
    connect (ui->name, SIGNAL(textEdited(QString)),
             this, SLOT(handleNameEdited(QString)));
}

RenameGameDialog::~RenameGameDialog()
{
    delete ui;
}

void RenameGameDialog::init(const QStringList &names, const QString &currentName)
{
    m_names = names;
    ui->name->setText(currentName);
    ui->status->setText("");
    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(true);
}

void RenameGameDialog::handleNameEdited(const QString &txt)
{
    const QString normName = txt.simplified();
    if (normName.isEmpty()) {
        ui->status->setText(tr("Game name is empty"));
        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
    }
    else if (m_names.contains(normName)) {
        ui->status->setText(tr("This name is already in use"));
        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
    }
    else {
        ui->status->setText("");
        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(true);
    }
}

QString RenameGameDialog::newName() const
{
    return ui->name->text().simplified();
}
}
