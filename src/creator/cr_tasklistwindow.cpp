#include "cr_tasklistwindow.h"
#include "ui_cr_tasklistwindow.h"
#include "ui_cr_renametaskdialog.h"
#include "core/actionmanager.h"
#include "schema/sch_game.h"
#include "schema/sch_task.h"
#include "schema/sch_environment.h"
#include "core/platform_configuration.h"

namespace Creator {

TaskListWindow::TaskListWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TaskListWindow)
{

    a_clone = new QAction(tr("Clone"), this);
    a_remove = new QAction(tr("Remove"), this);
    a_rename = new QAction(tr("Rename"), this);
    a_up = new QAction(("Move up"), this);
    a_down = new QAction(tr("Move down"), this);


    connect(a_clone, SIGNAL(triggered()), this,  SLOT(cloneTask()));
    connect(a_remove, SIGNAL(triggered()), this, SLOT(removeTask()));
    connect(a_rename, SIGNAL(triggered()), this, SLOT(renameTask()));
    connect(a_up, SIGNAL(triggered()), this, SLOT(moveUp()));
    connect(a_down, SIGNAL(triggered()), this, SLOT(moveDown()));

    ui->setupUi(this);

    const QSize iconSize(24, 24);

    ui->btnAdd->setIcon(QIcon(Core::PlatformConfiguration::resourcesPath()+"/ui_creator/list-add.png"));
    ui->btnAdd->setIconSize(iconSize);
    ui->btnRemove->setIcon(QIcon(Core::PlatformConfiguration::resourcesPath()+"/ui_creator/list-remove.png"));
    ui->btnRemove->setIconSize(iconSize);
    ui->btnRename->setIcon(QIcon(Core::PlatformConfiguration::resourcesPath()+"/ui_creator/edit-rename.png"));
    ui->btnRename->setIconSize(iconSize);
    ui->btnUp->setIcon(QIcon(Core::PlatformConfiguration::resourcesPath()+"/ui_creator/go-up.png"));
    ui->btnUp->setIconSize(iconSize);
    ui->btnDown->setIcon(QIcon(Core::PlatformConfiguration::resourcesPath()+"/ui_creator/go-down.png"));
    ui->btnDown->setIconSize(iconSize);

    connect(ui->btnAdd, SIGNAL(clicked()), a_clone, SLOT(trigger()));
    connect(ui->btnRemove, SIGNAL(clicked()), a_remove, SLOT(trigger()));
    connect(ui->btnRename, SIGNAL(clicked()), a_rename, SLOT(trigger()));
    connect(ui->btnUp, SIGNAL(clicked()), a_up, SLOT(trigger()));
    connect(ui->btnDown, SIGNAL(clicked()), a_down, SLOT(trigger()));




    connect(ui->listWidget, SIGNAL(currentRowChanged(int)), this, SLOT(selectTask(int)));
    connect(ui->listWidget, SIGNAL(itemDoubleClicked(QListWidgetItem*)), this, SLOT(handleTaskDoubleClicked(QListWidgetItem*)));
    connect(ui->listWidget, SIGNAL(itemChanged(QListWidgetItem*)), this, SLOT(handleTaskChanged(QListWidgetItem*)));

    connect(ui->gameTitle, SIGNAL(editingFinished()), this, SLOT(handleGameTitleEdited()));

    renameDialog = new QDialog(this);
    ui_renameDialog = new Ui::RenameTaskDialog;
    ui_renameDialog->setupUi(renameDialog);

    ui->BY->setIcon(QIcon(Core::PlatformConfiguration::resourcesPath()+"/cc_licensees/cc-by.png"));
    ui->BY_NC->setIcon(QIcon(Core::PlatformConfiguration::resourcesPath()+"/cc_licensees/cc-by-nc.png"));
    ui->BY_NC_SA->setIcon(QIcon(Core::PlatformConfiguration::resourcesPath()+"/cc_licensees/cc-by-nc-sa.png"));
    ui->BY_ND->setIcon(QIcon(Core::PlatformConfiguration::resourcesPath()+"/cc_licensees/cc-by-nd.png"));
    ui->BY_SA->setIcon(QIcon(Core::PlatformConfiguration::resourcesPath()+"/cc_licensees/cc-by-sa.png"));
    ui->BY_NC_ND->setIcon(QIcon(Core::PlatformConfiguration::resourcesPath()+"/cc_licensees/cc-by-nc-nd.png"));

#if QT_VERSION>=0x040700
    ui->gameTitle->setPlaceholderText(tr("Game title should not be empty!"));
    ui->authors->setPlaceholderText(tr("Your name here"));
    ui->copyright->setPlaceholderText(tr("[Usually] your organization name"));
    ui->homepage->setPlaceholderText(tr("Hot to contact you"));
#endif

    ui->btnShowEnv->setIcon(QIcon(Core::PlatformConfiguration::resourcesPath()+"/ui_creator/applications-engineering.png"));
    connect(ui->btnShowEnv, SIGNAL(clicked()), this, SLOT(handleShowEnv()));

    ui->btnShowHint->setIcon(QIcon(Core::PlatformConfiguration::resourcesPath()+"/ui_creator/system-help.png"));
    connect(ui->btnShowHint, SIGNAL(clicked()), this, SLOT(handleShowHint()));

    ui->btnShowProgram->setIcon(QIcon(Core::PlatformConfiguration::resourcesPath()+"/ui_creator/applications-education-school.png"));
    connect(ui->btnShowProgram, SIGNAL(clicked()), this, SLOT(handelShowPr()));


}


void TaskListWindow::selectTask(int index)
{
    a_up->setEnabled(index>0);
    ui->btnUp->setEnabled(index>0);
    a_down->setEnabled(index < ui->listWidget->count()-1);
    ui->btnDown->setEnabled(index < ui->listWidget->count()-1);
}

void TaskListWindow::cloneTask()
{
    int index = ui->listWidget->currentRow();
    Q_ASSERT(index>=0);
    Q_ASSERT(index<current_game.tasks.size());
    int oldsize = current_game.tasks.size();
    Schema::Task t = current_game.tasks[index];
    current_game.tasks.insert(index+1, t);
    current_game.tasks[index+1].title.prepend(tr("Copy of "));
    QListWidgetItem * item = new QListWidgetItem(current_game.tasks[index+1].title);
    item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsEnabled);
    ui->listWidget->insertItem(index+1, item);
    ui->listWidget->setCurrentRow(index+1);
    selectTask(index+1);
    QMap<int,int> movedList;
    for (int i=index+1; i<oldsize; i++) {
        movedList[i]=i+1;
    }
    emit taskMoved(movedList);
    a_remove->setEnabled(true);
    ui->btnRemove->setEnabled(true);
}

void TaskListWindow::removeTask()
{
    QMessageBox::StandardButton r = QMessageBox::question(0,
                                                          tr("Remove task"),
                                                          tr("Are you sure to REMOVE this task and its contents?"),
                                                          QMessageBox::Yes | QMessageBox::No,
                                                          QMessageBox::No);
    if (r==QMessageBox::Yes) {
        int index = ui->listWidget->currentRow();
        Q_ASSERT(index>=0);
        Q_ASSERT(index<current_game.tasks.size());
        int oldsize = current_game.tasks.size();
        ui->listWidget->setCurrentRow(index>0? index-1 : index+1);
        selectTask(index>0? index-1 : index+1);
        QListWidgetItem * item = ui->listWidget->takeItem(index);
        delete item;
        current_game.tasks.removeAt(index);
        QMap<int,int> movedList;
        for (int i=index+1; i<oldsize; i++) {
            movedList[i]=i-1;
        }
        emit taskMoved(movedList);
    }
    a_remove->setEnabled(current_game.tasks.size()>1);
    ui->btnRemove->setEnabled(current_game.tasks.size()>1);
}

int TaskListWindow::taskIndex() const
{
    return ui->listWidget->currentRow();
}

void TaskListWindow::detectCCLicense(const QString &lic)
{
    QString l = lic.toLower();
    l.replace("_", " ");
    l.replace("-", " ");
    l = l.simplified();
    l.replace("creative ", "c");
    l.replace("creative", "c");
    l.replace("commons","c");
    enum CCLicense { None, BY, BY_SA, BY_NC, BY_NC_SA, BY_ND, BY_NC_ND } ccLicense = None;
    if (l.startsWith("cc by") || l.startsWith("cc cc by")) {
        bool sa = l.contains("sa");
        bool nc = l.contains("nc");
        bool nd = l.contains("nd");
        if (!sa && !nc && !nd)
            ccLicense = BY;
        else if (sa && !nc && !nd)
            ccLicense = BY_SA;
        else if (!sa && nc && !nd)
            ccLicense = BY_NC;
        else if (sa && nc && !nd)
            ccLicense = BY_NC_SA;
        else if (!sa && !nc && nd)
            ccLicense = BY_ND;
        else if (!sa && nc && nd)
            ccLicense = BY_NC_ND;
        else
            ccLicense = None;
    }
    switch (ccLicense) {
    case BY:
        ui->license->setCurrentIndex(0);
        ui->stackedWidget->setCurrentIndex(0);
        ui->BY->setChecked(true);
        break;
    case BY_SA:
        ui->license->setCurrentIndex(0);
        ui->stackedWidget->setCurrentIndex(0);
        ui->BY_SA->setChecked(true);
        break;
    case BY_NC:
        ui->license->setCurrentIndex(0);
        ui->stackedWidget->setCurrentIndex(0);
        ui->BY_NC->setChecked(true);
        break;
    case BY_NC_SA:
        ui->license->setCurrentIndex(0);
        ui->stackedWidget->setCurrentIndex(0);
        ui->BY_NC_SA->setChecked(true);
        break;
    case BY_ND:
        ui->license->setCurrentIndex(0);
        ui->stackedWidget->setCurrentIndex(0);
        ui->BY_ND->setChecked(true);
        break;
    case BY_NC_ND:
        ui->license->setCurrentIndex(0);
        ui->stackedWidget->setCurrentIndex(0);
        ui->BY_NC_ND->setChecked(true);
        break;
    default:
        ui->license->setCurrentIndex(1);
        ui->stackedWidget->setCurrentIndex(1);
        ui->plainTextEdit->setPlainText(lic);
    }
}

QString TaskListWindow::license() const
{
    QString result;
    if (ui->license->currentIndex()==0) {
        result = "Creative Commons ";
        if (ui->BY->isChecked())
            result += "BY";
        if (ui->BY_SA->isChecked())
            result += "BY-SA";
        if (ui->BY_NC->isChecked())
            result += "BY-NC";
        if (ui->BY_NC_SA->isChecked())
            result += "BY-NC-SA";
        if (ui->BY_ND->isChecked())
            result += "BY-ND";
        if (ui->BY_NC_ND)
            result += "BY-NC-ND";
    }
    else {
        result = ui->plainTextEdit->toPlainText();
    }
    return result;
}

void TaskListWindow::handleGameTitleEdited()
{
    QString txt = ui->gameTitle->text();
    ui->gameTitle->setText(txt.simplified());
    if (txt.simplified().isEmpty()) {
        setWindowTitle(s_originalGameName);
        current_game.title = s_originalGameName;
    }
    else {
        setWindowTitle(txt.simplified());
        current_game.title = txt.simplified();
    }
    emit taskTitleChanged();
}

void TaskListWindow::setGame(const Schema::Game & game, const QString &fileName)
{
    current_game = game;
    original_game = game;
    s_fileName = fileName;
    s_originalGameName = game.title;
    setWindowTitle(game.title);
    ui->gameTitle->setText(game.title);
    ui->authors->setText(game.authors.join(", "));
    ui->copyright->setText(game.copyright);
    ui->homepage->setText(game.homepage);
    detectCCLicense(game.license);
    ui->listWidget->clear();
    for (int i=0; i<game.tasks.size(); i++) {
        const QString title = game.tasks[i].title;
        QListWidgetItem * item = new QListWidgetItem(title);
        item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsEnabled);

        ui->listWidget->addItem(item);


    }
    if (ui->listWidget->count()>0) {
        ui->listWidget->setCurrentRow(0);
    }
    a_remove->setEnabled(ui->listWidget->count()>1);
    ui->btnRemove->setEnabled(current_game.tasks.size()>1);
}

void TaskListWindow::handleTaskDoubleClicked(QListWidgetItem *item)
{
    ui->listWidget->editItem(item);
}

void TaskListWindow::handleTaskChanged(QListWidgetItem *item)
{
    int index = ui->listWidget->row(item);
    current_game.tasks[index].title = item->text().simplified();
    emit taskTitleChanged();
}

void TaskListWindow::renameTask()
{
    int index = ui->listWidget->currentRow();
    Q_ASSERT(index>=0);
    Q_ASSERT(index<current_game.tasks.size());
    ui_renameDialog->lineEdit->setText(current_game.tasks[index].title);
    if (renameDialog->exec()==QDialog::Accepted) {
        const QString newTitle = ui_renameDialog->lineEdit->text().simplified();
        current_game.tasks[index].title = newTitle;
        ui->listWidget->item(index)->setText(newTitle);
        emit taskTitleChanged();
    }
}

bool TaskListWindow::isChanged() const
{
    return !Schema::isEqual(current_game, original_game);
}

void TaskListWindow::closeEvent(QCloseEvent *event)
{
    emit syncRequest();
    if (isChanged()) {
        QMessageBox::StandardButton answer = QMessageBox::question(this, tr("Save changes"),
                                                                   tr("This game have unsaved changes. Save it?"),
                                                                   QMessageBox::Yes|QMessageBox::No|QMessageBox::Cancel);
        if (answer==QMessageBox::Yes) {
            if (!saveGame()) {
                event->ignore();
                return;
            }
        }
        else if (answer==QMessageBox::Cancel) {
            event->ignore();
            return;
        }
    }
    emit closeRequest();
    event->accept();
}

bool TaskListWindow::saveGame()
{
    if (s_fileName.isEmpty()) {
        return saveGameAs();
    }
    else {
        return saveToFile(s_fileName);
    }
}

QString TaskListWindow::gameTitle() const
{
    return current_game.title;
}

bool TaskListWindow::saveGameAs()
{
    QSettings settings;
    QString lastDir = settings.value("Creator/Game/LastDir", Core::PlatformConfiguration::tasksPath()).toString();
    QString selectedFilter;
    const QString filter = QString::fromUtf8("Игры ПиктоМир версии >= 0.12.x (*.pm.json);;"
                              "Игры ПиктоМир версии < 0.12.x (*.pm.xml);;"
                              "Практикумы Кумир (*.kurs.xml)");
    QString fileName = QFileDialog::getSaveFileName(this, tr("Load game..."), lastDir, filter, &selectedFilter);
    static QRegExp rxSuffix("\\((.+)\\)");
    rxSuffix.indexIn(selectedFilter);
    const QString suffix = rxSuffix.cap(1).mid(1);
    if (!fileName.isEmpty()) {
        if (!fileName.endsWith(suffix))
            fileName += suffix;

        settings.setValue("Creator/Game/LastDir", QFileInfo(fileName).dir().absolutePath());
        s_fileName = fileName;
        return saveToFile(fileName);
    }
    else {
        return false;
    }
}

bool TaskListWindow::saveToFile(const QString &fileName)
{
    QFile f(fileName);
    if (f.open(QIODevice::WriteOnly|QIODevice::Text)) {
        QTextStream ts(&f);
        ts.setCodec("UTF-8");
        ts.setGenerateByteOrderMark(true);
        if (fileName.endsWith(".json")) {
            const QString data = Schema::generateJSON(current_game);
            ts << data;
            f.close();
            original_game = current_game;
            s_fileName = fileName;
            return true;
        }
        else if (fileName.endsWith(".kurs.xml")) {
            const QString resourcesDirName = QFileInfo(fileName).absoluteFilePath() + ".resources";
            QString data = Schema::generateKumirCourse(current_game, resourcesDirName);
            ts << data;
            f.close();
            return true;
        }
        else {
            QString data = Schema::generateAGKXML(current_game);
            const QString fileBaseName =
                    QFileInfo(f).fileName();
            data.replace("$$$$$", fileBaseName);
            ts << data;
            f.close();
            original_game = current_game;
            s_fileName = fileName;
            QDir levelUp = QDir(QFileInfo(f).absoluteFilePath());
            levelUp.cdUp();
            levelUp.cdUp();
            levelUp.mkpath("Environments/"+fileBaseName);
            for (int i=0; i<current_game.tasks.size(); i++) {
                Schema::Environment environment = current_game.tasks[i].environment;
                QString envData = Schema::generateKumFil(environment);
                QFile envF(levelUp.absolutePath()+"/Environments/"+fileBaseName+"/"+QString::number(i+1)+".fil");
                if (!envF.open(QIODevice::WriteOnly|QIODevice::Text)) {
                    QMessageBox::critical(this, tr("Can't save task"),
                                          tr("Can't write environment one of environments for this task")
                                          );
                    return false;
                }
                else {
                    QTextStream ets(&envF);
                    ets.setCodec("UTF-8");
                    ets << envData;
                    envF.close();
                }
            }
            return true;
        }
    }
    else {
        QMessageBox::critical(this, tr("Can't save task"),
                              tr("Destination directory is not writable"));
        return false;
    }
}

void TaskListWindow::swapTasksPositions(int oldIndex, int newIndex)
{
    QListWidgetItem * curItem = ui->listWidget->takeItem(oldIndex);
    ui->listWidget->insertItem(newIndex, curItem);
    current_game.tasks.swap(oldIndex, newIndex);
    QMap<int,int> movedList;
    movedList[oldIndex] = newIndex;
    movedList[newIndex] = oldIndex;
    emit taskMoved(movedList);
}

void TaskListWindow::moveUp()
{
    int oldIndex = ui->listWidget->currentRow();
    int newIndex = oldIndex-1;
    swapTasksPositions(oldIndex, newIndex);
}

void TaskListWindow::moveDown()
{
    int oldIndex = ui->listWidget->currentRow();
    int newIndex = oldIndex+1;
    swapTasksPositions(oldIndex, newIndex);
}


TaskListWindow::~TaskListWindow()
{
    delete ui;
}

} // namespace Creator
