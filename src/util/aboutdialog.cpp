#include "aboutdialog.h"
#include "ui_aboutdialog.h"

#include <QtCore>

namespace Util {

AboutDialog::AboutDialog(const QString &pixmapPath, const QString &licensePath, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AboutDialog)
{
    ui->setupUi(this);
    QPixmap px(pixmapPath);
    ui->splash->setPixmap(px);
    QFile f(licensePath);
    if (f.open(QIODevice::ReadOnly|QIODevice::Text)) {
        QTextStream ts(&f);
        ts.setCodec("UTF-8");
        ts.setAutoDetectUnicode(true);
        const QString licData = ts.readAll();
        f.close();
        if (licensePath.endsWith(".html")) {
            ui->license->setHtml(licData);
        }
        else {
            ui->license->setPlainText(licData);
        }
    }
    else {
        ui->license->setPlainText(tr("No text yet..."));
    }
    ui->version->setText(ui->version->text().arg(qApp->applicationVersion()));
}

AboutDialog::~AboutDialog()
{
    delete ui;
}

} // namespace Util
