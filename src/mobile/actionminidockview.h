#ifndef ACTIONMINIDOCKVIEW_H
#define ACTIONMINIDOCKVIEW_H

#include <QtCore>
#include <QtGui>
#include <QtSvg>

#include "core/actionview.h"

namespace Core {
class ActionModel;
}

namespace Mobile {


class ActionMiniDockView
    : public QGraphicsSvgItem
    , public Core::ActionView
{
    Q_OBJECT;
    Q_PROPERTY(qreal pulse READ pulse WRITE setPulse);
public:
    explicit ActionMiniDockView(Core::ActionModel *model, QGraphicsItem *parent = 0);
    inline QGraphicsSvgItem* graphicsItem() { return this; }
    inline Core::ActionModel* model() { return m_model; }
    int modelId() const;
    bool isEnabled() const;
    QSize originalSize() const;
    QSize currentSize() const;
    void setActive(bool v);
    inline qreal pulse() const { return r_pulse; }
    void setPulse(qreal value);
private:
    Core::ActionModel *m_model;
    bool b_active;
    QPropertyAnimation *an_active;
    QPropertyAnimation *an_stopActive;
    qreal r_pulse;
};

}

#endif // ACTIONMINIDOCKVIEW_H
