#include "hintitem.h"
#include "util/svgsimplebutton.h"
#include "core/platform_configuration.h"

namespace Desktop {

HintItem::HintItem(QGraphicsItem * parent)
    : QObject(0)
    , QGraphicsRectItem(parent)
{
    setPen(Qt::NoPen);
    setBrush(Qt::NoBrush);

    g_background = new QGraphicsPathItem(this);
    g_background->setPen(QPen(QColor("white"),2));
    QLinearGradient gr(0,1,0,0);
    gr.setCoordinateMode(QGradient::ObjectBoundingMode);
    gr.setColorAt(0, QColor(160, 160, 160, 245));
    gr.setColorAt(1, QColor(240, 240, 240, 245));
    g_background->setBrush(gr);

    g_closeButton = new Util::SvgSimpleButton(Core::PlatformConfiguration::resourcesPath()+"/ui_desktop/close_hint.svg", this);
    connect(g_closeButton, SIGNAL(clicked()), this, SLOT(closeHintWindow()));


    g_text = new QGraphicsTextItem(this);
    QFont font;
#ifdef Q_OS_WIN32
#endif
#ifdef Q_WS_X11
    font.setFamily("serif");
#endif
    font.setPixelSize(24);
    font.setBold(true);
    font.setItalic(true);
    g_text->setFont(font);
    g_text->setDefaultTextColor(QColor("black"));

    g_image = new QGraphicsPixmapItem(this);

    m_renderer = new QSvgRenderer(this);

}

void HintItem::closeHintWindow()
{
    setVisible(false);
    emit closed();
}

void HintItem::setRect(qreal x, qreal y, qreal w, qreal h)
{
    setRect(QRectF(x,y,w,h));
}

void HintItem::setRect(const QRectF &rect)
{
    m_rect = rect;
    setZValue(11000001);
    setPos(rect.topLeft());
    prepareGeometryChange();
    updateBackground(rect.size());
    if (e_hintType==Image)
        updateImageSize(rect.size());
    if (e_hintType==Drawing)
        updateDrawingSize(rect.size());
    if (e_hintType==Text)
        updateTextSize(rect.size());
    g_closeButton->setPos(rect.width()-10-g_closeButton->boundingRect().width(), 10);
}

void HintItem::updateImageSize(const QSizeF &size)
{
    const QSizeF maxImageSize = size - QSizeF(20, 70);
    const QSizeF actualSize = g_image->pixmap().size();
    qreal scaleX = 1.0;
    qreal scaleY = 1.0;
    if (maxImageSize.width()<actualSize.width())
        scaleX = maxImageSize.width() / actualSize.width();
    if (maxImageSize.height()<actualSize.height())
        scaleY = maxImageSize.height() / actualSize.height();
    qreal scale = qMin(scaleX, scaleY);
    g_image->setScale(scale);
    qreal x = 10 + (size.width() - g_image->boundingRect().width())/2.0;
    qreal y = 30 + (size.height() - g_image->boundingRect().height())/2.0;
    g_image->setPos(x,y);
}

void HintItem::updateDrawingSize(const QSizeF &size)
{
    const QSizeF maxImageSize = size - QSizeF(20, 70);
    const QSizeF actualSize = m_renderer->defaultSize();
    qreal scaleX = 1.0;
    qreal scaleY = 1.0;
    if (maxImageSize.width()<actualSize.width())
        scaleX = maxImageSize.width() / actualSize.width();
    if (maxImageSize.height()<actualSize.height())
        scaleY = maxImageSize.height() / actualSize.height();
    qreal scale = qMin(scaleX, scaleY);
    QSizeF realSize(actualSize.width()*scale, actualSize.height()*scale);
    QImage img(realSize.toSize(), QImage::Format_ARGB32);
    img.fill(0);
    QPainter painter(&img);
    m_renderer->render(&painter, QRectF(QPointF(0,0), realSize));
    QPixmap px = QPixmap::fromImage(img);
    g_image->setPixmap(px);
    qreal x = 10 + (size.width() - g_image->boundingRect().width())/2.0;
    qreal y = 30 + (size.height() - g_image->boundingRect().height())/2.0;
    g_image->setPos(x,y);
}

void HintItem::updateTextSize(const QSizeF &size)
{
    QFontMetricsF fm(g_text->font());
    qreal maxWidth = qMin(size.width()-20, fm.width(g_text->toPlainText()));
    g_text->setTextWidth(maxWidth);
    qreal x = 10 + (size.width() - g_text->boundingRect().width())/2.0;
    qreal y = 30 + (size.height() - g_text->boundingRect().height())/2.0;
    g_text->setPos(x,y);
}

void HintItem::setHint(const QString &mimeType, const QByteArray &data)
{
    g_image->setVisible(false);
    g_text->setVisible(false);
    e_hintType = None;
    if (mimeType=="text/plain") {
        g_text->setPlainText(QString::fromLocal8Bit(data));
        g_text->setVisible(true);
        updateTextSize(m_rect.size());
        e_hintType = Text;
    }
    else if (mimeType=="image/png") {
        QImage img;
        bool ok = img.loadFromData(data);
        if (ok) {
            QPixmap px = QPixmap::fromImage(img);
            g_image->setPixmap(px);
            g_image->setVisible(true);
            updateImageSize(m_rect.size());
            e_hintType = Image;
        }
    }
    else if (mimeType.startsWith("image/svg")) {
        bool ok = m_renderer->load(data);
        if (ok) {
            g_image->setVisible(true);
            updateDrawingSize(m_rect.size());
            e_hintType = Drawing;
        }
    }
}

HintItem::~HintItem()
{
    delete g_background;
    delete g_text;
    delete g_image;
    delete g_closeButton;
    delete m_renderer;
}

void HintItem::updateBackground(const QSizeF &size)
{
    QPainterPath path;
    path.moveTo(0, 12);
    path.arcTo(0,0,24,24,180,-90);
    path.lineTo(size.width()-12, 0);
    path.arcTo(size.width()-24,0,24,24,90,-90);
    path.lineTo(size.width(), size.height()-12);
    path.arcTo(size.width()-24,size.height()-24,24,24,0,-90);
    path.lineTo(12, size.height());
    path.arcTo(0,size.height()-24,24,24,-90,-90);
    path.lineTo(0, 12);
    path.closeSubpath();

    g_background->setPath(path);
}



} // namespace Desktop
