#ifndef ACTIONTYPE_H
#define ACTIONTYPE_H

namespace Core {

    enum ActionType { ActionPlain, ActionCondition, ActionRepeat, ActionNone };
    enum ActionCategory { Base, Module, Functions, Any };

}

#endif // ACTIONTYPE_H
