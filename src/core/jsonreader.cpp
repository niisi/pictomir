#include <QtScript>
#include "jsonreader.h"
#include "contentprovider.h"


namespace Core {

class JsonReaderPrivate {
public:
    QScriptEngine * scriptEngine;
    JsonReader * q;
    Util::ContentProvider * contentProvider;

    Game parseGame(const QScriptValue &root, const QUrl &baseUrl, QString &error) const;
    Task parseTask(const QScriptValue &root, const QUrl &baseUrl, QString &error) const;
    Algorhitm parseAlgorhitm(const QScriptValue &root, QString &error) const;
};


Util::ContentProvider * JsonReader::contentProvider()
{
    return d->contentProvider;
}

void JsonReader::setContentProvider(Util::ContentProvider *provider)
{
    d->contentProvider = provider;
}

JsonReader::JsonReader(QObject *parent)
    : QObject(parent)
{
    d = new JsonReaderPrivate;
    d->q = this;
    d->contentProvider = NULL;
    d->scriptEngine = new QScriptEngine;
}

JsonReader::~JsonReader()
{
    d->scriptEngine->deleteLater();
    delete d;
}

Game JsonReaderPrivate::parseGame(const QScriptValue &root, const QUrl &baseUrl, QString &error) const
{
    Game g;
    g.title = root.property("title").toString();
    g.author = root.property("author").toString();
    g.copyright = root.property("copyright").toString();
    QScriptValue tasks = root.property("tasks");
    if (tasks.isArray()) {
        int size = tasks.property("length").toInteger();
        for (int i=0; i<size; i++) {
            QScriptValue jTask = tasks.property(i);
            QString localError;
            Task task = parseTask(jTask, baseUrl, localError);
            if (!localError.isEmpty())
                error += localError + "\n";
            g.tasks << task;
        }
    }
    else {
        error = "No tasks\n";
    }
    return g;
}

Task JsonReaderPrivate::parseTask(const QScriptValue &root, const QUrl &baseUrl, QString &error) const
{
    Task t;
    t.title = root.property("title").toString();
    QString env = root.property("env").toString();
    QUrl envUrl = baseUrl.resolved(QUrl(env));
    Util::ResponseData data = contentProvider->fetchUrl(envUrl);
    if (data.error.isEmpty()) {
        t.environment = data.content;
        if (data.mimeType=="text/json" || data.mimeType=="text/javascript") {
            t.environmentFormat = FileJsonFormat;
        }
        else if (data.mimeType=="text/xml") {
            t.environmentFormat = FileXmlFormat;
        }
        else {
            t.environmentFormat = FileTextFormat;
        }
    }
    else {
        error += "Can't fetch environment: "+data.error + "\n";
    }
    t.environment = data.content;
    QScriptValue jProgram = root.property("program");
    if (jProgram.isArray()) {
        int size = jProgram.property("length").toInteger();
        for (int i=0; i<size; i++) {
            QScriptValue jAlgorhitm = jProgram.property(i);
            QString localError;
            Algorhitm algorhitm = parseAlgorhitm(jAlgorhitm, localError);
            if (!localError.isEmpty())
                error += localError + "\n";
            t.program << algorhitm;
        }
    }
    return t;
}

Algorhitm JsonReaderPrivate::parseAlgorhitm(const QScriptValue &root, QString &error) const
{
    Algorhitm alg;
    alg.width = root.property("width").toInteger();
    alg.height = root.property("height").toInteger();
    alg.data = QVector<QString>(alg.height * alg.width);
    alg.condition = root.property("condition").toString();
    alg.repeater = root.property("repeater").toString();
    QScriptValue jHints = root.property("hints");
    error = "";
    if (jHints.isArray()) {
        int size = jHints.property("length").toInteger();
        for (int i=0; i<size; i++) {
            alg.hints << jHints.property(i).toString();
        }
    }
    int index = 0;
    QScriptValue jRows = root.property("data");
    if (jRows.isArray()) {
        int rowsCount = jRows.property("length").toInteger();
        int effectiveRowsCount = qMin(alg.height, rowsCount);
        for (int y=0; y<effectiveRowsCount; y++) {
            QScriptValue jRow = jRows.property(y);
            if (jRow.isArray()) {
                int columnsCount = jRow.property("length").toInteger();
                int effectiveColumnsCount = qMin(alg.width, columnsCount);
                int freeColumns = qMax(0, alg.width-effectiveColumnsCount);
                for (int x=0; x<effectiveColumnsCount; x++) {
                    QScriptValue value = jRow.property(x);
                    alg.data[index] = value.toString();
                    index++;
                }
                index += freeColumns;
            }
        }
    }
    return alg;
}

QString JsonReader::parseData(const QByteArray &data, const QUrl &baseUrl, Game &game) const
{
    QScriptValue root = d->scriptEngine->evaluate(QString::fromUtf8(data));
    if (root.isError()) {
        return QString("Syntax error while parsing JSON: %1").arg(root.toString());
    }
    QString error;
    game = d->parseGame(root, baseUrl, error);
    return error;
}


}
