#include "desktop/navigatetaskbuttonbox.h"
#include "util/svgbutton.h"

using namespace Util;
namespace Desktop {

NavigateTaskButtonBox::NavigateTaskButtonBox(QGraphicsItem *parent)
    : QObject(0)
    , QGraphicsRectItem(parent)
{

    setPen(Qt::NoPen);

    g_prev = new SvgButton("ui_desktop/navigation_buttons.svg","previous");
    g_next = new SvgButton("ui_desktop/navigation_buttons.svg","next");
    connect (g_prev, SIGNAL(clicked()), this, SLOT(handleButtonClicked()));
    connect (g_next, SIGNAL(clicked()), this, SLOT(handleButtonClicked()));

    g_prev->setParentItem(this);
    g_next->setParentItem(this);


//    if (qApp->arguments().contains("-t")) {
//        g_addPrev = new SvgButton("ui_desktop/teacher_mode_icons.svg", "add-previous");
//        g_removeCurrent = new SvgButton("ui_desktop/teacher_mode_icons.svg","remove-current");
//        g_addNext = new SvgButton("ui_desktop/teacher_mode_icons.svg", "add-next");
//        connect (g_addNext, SIGNAL(clicked()), this, SLOT(handleButtonClicked()));
//        connect (g_removeCurrent, SIGNAL(clicked()), this, SLOT(handleButtonClicked()));
//        connect (g_addPrev, SIGNAL(clicked()), this, SLOT(handleButtonClicked()));

//        scene()->addItem(g_addPrev);
//        scene()->addItem(g_removeCurrent);
//        scene()->addItem(g_addNext);

//        g_addPrev->enable(QPointF(128, 16), QSizeF(40, 40));
//        g_removeCurrent->enable(QPointF(178, 16), QSizeF(40, 40));
//        g_addNext->enable(QPointF(228, 16), QSizeF(40, 40));
//        setFixedSize(288, 64);
//        setSceneRect(0,0,288,64);
//    }
//    else {
        g_addNext = g_addPrev = g_removeCurrent = 0;
        setRect(0,0,128,64);
//    }


    enablePrev();
    enableNext();

}

void NavigateTaskButtonBox::enablePrev()
{
    g_prev->enable(QPointF(16, 16), QSizeF(40, 40));
}

void NavigateTaskButtonBox::enableNext()
{
    g_next->enable(QPointF(64, 16), QSizeF(40, 40));
}

void NavigateTaskButtonBox::disablePrev()
{
    g_prev->disable(QPointF(16, 64), QSizeF(0, 0));
}

void NavigateTaskButtonBox::disableNext()
{
    g_next->disable(QPointF(64, 64), QSizeF(0, 0));
}

void NavigateTaskButtonBox::alertNext()
{
    g_next->setAttended(true);
}

void NavigateTaskButtonBox::unalertNext()
{
    g_next->setAttended(false);
}

void NavigateTaskButtonBox::handleButtonClicked()
{
    if (sender()==g_prev)
        emit prevTaskClicked();
    if (sender()==g_next)
        emit nextTaskClicked();
    if (sender()==g_addNext)
        emit addNextTaskClicked();
    if (sender()==g_addPrev)
        emit addPrevTaskClicked();
    if (sender()==g_removeCurrent)
        emit removeCurrentTaskClicked();
}
}
