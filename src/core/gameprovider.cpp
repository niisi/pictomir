#include "gameprovider.h"
#include <QtXml>
#include "platform_configuration.h"
#include "schema/sch_game.h"

namespace Core {

GameProvider::GameProvider(QObject *parent)
    : QObject(parent)
{

}

void GameProvider::init(const QString &lastGameName)
{
    s_lastGameName = lastGameName;
    l_titles.clear();
    l_baseNames.clear();
    QDir tasksDir = QDir(PlatformConfiguration::tasksPath());
    tasksDir.setFilter(QDir::Files|QDir::Readable|QDir::NoDotAndDotDot);
    tasksDir.setNameFilters(QStringList() << "*.pm.json");
    int selectNo = -1;
    foreach (QString entryName, tasksDir.entryList()) {
        if (!entryName.startsWith("_")) {
            QString fileName = tasksDir.absoluteFilePath(entryName);
            QPair<QString,QString> pair = loadPmDocument(fileName);
            QString fn = QFileInfo(pair.second).baseName();
            QString title = pair.first;
            if (!title.isEmpty() && !l_baseNames.contains(fn)) {
                l_baseNames << fn;
                l_titles << title;
                if (lastGameName==title) {
                    selectNo = l_titles.size()-1;
                }
            }

        }
    }
    i_selectedIndex = selectNo;
}

QPair<QString, QString>  GameProvider::loadPmDocument(const QString &fileName)
{
    if (fileName.endsWith(".pm.xml")) {
        return loadPmXmlDocument(fileName);
    }
    else if (fileName.endsWith(".pm.json") || fileName.endsWith(".pm.json")) {
        return loadPmJsonDocument(fileName);
    }
    else {
        return QPair<QString,QString>(QString(), QString());
    }
}

QPair<QString, QString>  GameProvider::loadPmXmlDocument(const QString &fileName)
{
    bool gameDocument = false;
    QDomDocument doc;
    QString fn = fileName;
    while (!gameDocument) {
        if (!QFile::exists(fn)) {
            qDebug() << QString::fromUtf8("Не найден файл: ")+fn;
            return QPair<QString,QString>(QString(), QString());
        }
        QFile f(fn);
        QString xmlError;
        int xmlLine, xmlCol;
        if (!doc.setContent(&f,&xmlError,&xmlLine,&xmlCol)) {
            qDebug() << QString("XML error in %4 at line %1, col. %2:\n%3").
                        arg(xmlLine).arg(xmlCol).arg(xmlError).arg(fn);
            return QPair<QString,QString>(QString(), QString());
        }
        if (doc.documentElement().nodeName()=="xlink") {
            QString href = doc.documentElement().attribute("href","");
            if (href.isEmpty()) {
                qDebug() << "Empty xlink href in " << fn;
                return QPair<QString,QString>(QString(), QString());
            }
            fn = QFileInfo(f).absoluteDir().absoluteFilePath(href);
        }
        else if (doc.documentElement().nodeName()=="game") {
            gameDocument = true;
            QString title = doc.documentElement().attribute("title");
            return QPair<QString,QString>(title, QFileInfo(fn).fileName());
        }
        else {
            qDebug() << "Not PictoMir XML!" << fn;
            return QPair<QString,QString>(QString(),
                                          QFileInfo(fn).fileName()
                                          );
            break;
        }
    }
    return QPair<QString,QString>(QString(), QString());
}

QPair<QString, QString>  GameProvider::loadPmJsonDocument(const QString &fileName)
{
    bool gameDocument = false;
    QScriptEngine engine;
    QScriptValue value;
    QString fn = fileName;
    while (!gameDocument) {
        if (!QFile::exists(fn)) {
            qDebug() << QString::fromUtf8("Не найден файл: ")+fn;
            return QPair<QString,QString>(QString(), QString());
        }
        QFile f(fn);
        QString jsData;
        if (f.open(QIODevice::ReadOnly|QIODevice::Text)) {
            QTextStream ts(&f);
            ts.setCodec("UTF-8");
            ts.setAutoDetectUnicode(true);
            jsData = "var game = "+ts.readAll()+";";
            f.close();
        }
        engine.evaluate(jsData);
        if (engine.hasUncaughtException()) {
             int line = engine.uncaughtExceptionLineNumber();
             QStringList bt = engine.uncaughtExceptionBacktrace();
             qDebug() << "uncaught exception at line" << line << ":" << value.toString();
             for (int i=0; i<bt.size(); i++) {
                 qDebug() << bt[i];
             }
        }
        value = engine.evaluate("game");
        if (engine.hasUncaughtException()) {
             int line = engine.uncaughtExceptionLineNumber();
             qDebug() << "uncaught exception at line" << line << ":" << value.toString();
        }
        else if (value.isObject()) {
            QScriptValue tval = value.property("title");
            if (tval.isError()) {
                qDebug() << tval.toString();
                return QPair<QString,QString>(QString(), QString());
            }
            else if (tval.isString()) {
                QString title = tval.toString();
                return QPair<QString,QString>(title, QFileInfo(fn).fileName());
            }
            else {
                qDebug() << "Not pictomir script!";
                return QPair<QString,QString>(QString(), QString());
            }
        }
        else if (value.isString()) {
            QString href = value.toString().trimmed();
            if (href.isEmpty()) {
                qDebug() << "Empty xlink href in " << fn;
                return QPair<QString,QString>(QString(), QString());
            }
            fn = QFileInfo(f).absoluteDir().absoluteFilePath(href);
        }
    }
    return QPair<QString,QString>(QString(), QString());
}



QString GameProvider::fileNameByIndex(int index) const
{
    if (index<0) {
        return "";
    }
    else {
        return l_baseNames[index]+".pm.json";
    }
}

int GameProvider::lastIndex() const
{
    return i_selectedIndex;
}
}
