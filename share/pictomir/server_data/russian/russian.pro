TEMPLATE = subdirs
SUBDIRS = 

common_images.files = common_images/*.png
common_images.path = /share/pictomir/server_data/russian/common_images/

icons.files = icons/*.png
icons.path = /share/pictomir/server_data/russian/icons/

introduction.files = introduction/*.fil 
introduction.path = /share/pictomir/server_data/russian/introduction/

help.files = help/*
help.path = /share/pictomir/server_data/russian/help/

creator.files = creator/*
creator.path = /share/pictomir/server_data/russian/creator/

root.files = *.js *.css *.html *.json
root.path = /share/pictomir/server_data/russian/

INSTALLS += common_images icons introduction root help creator
