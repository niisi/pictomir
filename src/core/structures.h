#ifndef STRUCTURES_H
#define STRUCTURES_H

#include <QtCore>
#include "fileformat.h"


namespace Core {

    struct Algorhitm {
        QStringList hints;
        QString repeater;
        QString condition;
        int width;
        int height;
        QVector<QString> data;
    };

    typedef QList<Algorhitm> Program;

    struct Task
    {
        QString title;
        QByteArray environment;
        FileFormat environmentFormat;
        Program program;
    };

    struct Game {
        QString title;
        QString author;
        QString copyright;
        QList<Task> tasks;
    };


}

#endif
