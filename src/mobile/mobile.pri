HEADERS += mobile/m_mainwindow.h \
    mobile/m_dockwidget.h \
    mobile/m_functionwidget.h \
    mobile/smartphonebuttonbox.h \
    mobile/smartphonemenudialog.h \
    mobile/actionminidockview.h
SOURCES += mobile/m_mainwindow.cpp \
    mobile/m_dockwidget.cpp \
    mobile/m_functionwidget.cpp \
    mobile/smartphonebuttonbox.cpp \
    mobile/smartphonemenudialog.cpp \
    mobile/actionminidockview.cpp
FORMS += mobile/smartphonemenudialog.ui
