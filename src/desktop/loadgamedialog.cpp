#include "desktop/loadgamedialog.h"
#include "core/platform_configuration.h"

#include <QtCore>
#include <QtGui>
#include <QtXml>

#include "core/gameprovider.h"
#include "desktop/renamegamedialog.h"

using namespace Core;

namespace Desktop {

LoadGameDialog::LoadGameDialog(GameProvider *provider, QWidget *parent):
        QDialog(parent),
        Ui_LoadGameDialog()
        , m_gameProvider(provider)
{
    setupUi(this);

    connect ( listGames, SIGNAL(currentItemChanged(QListWidgetItem*,QListWidgetItem*)),
              this, SLOT(handleGameItemSelected(QListWidgetItem*)) );

    if (!qApp->arguments().contains("-t")) {
        btnNew->setVisible(false);
        btnRename->setVisible(false);
        btnDelete->setVisible(false);
        btnClone->setVisible(false);
    }
    else {
        m_renameGameDialog = new RenameGameDialog();

        connect (btnNew, SIGNAL(clicked()), this, SLOT(handleCreateNewClicked()));
        connect (btnDelete, SIGNAL(clicked()), this, SLOT(handleDeleteClicked()));
        connect (btnRename, SIGNAL(clicked()), this, SLOT(handleRenameClicked()));
        connect (btnClone, SIGNAL(clicked()), this, SLOT(handleCloneClicked()));
    }


}



void LoadGameDialog::handleGameItemSelected(QListWidgetItem *item)
{
    if (item!=NULL) {
        if (qApp->arguments().contains("-t")) {
            btnClone->setEnabled(true);
            btnRename->setEnabled(true);
            btnDelete->setEnabled(true);
        }
        btnOK->setEnabled(true);
    }
    else {
        if (qApp->arguments().contains("-t")) {
            btnClone->setEnabled(false);
            btnRename->setEnabled(false);
            btnDelete->setEnabled(false);
        }
        btnOK->setEnabled(false);
    }
}

int LoadGameDialog::selectedIndex() const
{
    return listGames->currentRow();
}

void LoadGameDialog::init(const QStringList &items, int selectedIndex)
{
    listGames->clear();
    listGames->addItems(items);
    if (selectedIndex>=0) {
        btnOK->setEnabled(true);
        listGames->setCurrentRow(selectedIndex);
    }
    else {
        btnOK->setEnabled(false);
    }
}

//QString LoadGameDialog::selectedFileName() const
//{
//    int gameIndex = listGames->currentRow();

//    QString baseName = l_baseNames[gameIndex];
//    return baseName+".pm.xml";

//}

void LoadGameDialog::handleCreateNewClicked()
{

}

void LoadGameDialog::handleDeleteClicked()
{

}

void LoadGameDialog::handleRenameClicked()
{

}

void LoadGameDialog::handleCloneClicked()
{

}
}
