#ifndef DockWidget_Touchscreen_H
#define DockWidget_Touchscreen_H

#include <QtCore>
#include <QtGui>
#include <QtSvg>

#include "interfaces/dockwidget_interface.h"
#include "core/enums.h"


namespace Core {
    class ActionModel;
    class ActionView;
    class ActionDockView;
    class ActionManager;
}

namespace Mobile {

class DockWidgetTouchscreen :
        public QGraphicsView,
        public Core::DockWidgetInterface
{
    Q_OBJECT;
public:
    DockWidgetTouchscreen(const QSize &iconSize, QWidget *parent = 0);
    void connectSignalTouched(QObject *obj, const char *method);
    void connectSignalActivationChanged(QObject *obj, const char *method);
    Core::ActionManager * actionManager();
    void setActionManager(Core::ActionManager *manager);
    QWidget* thisWidget();
public slots:
    void updateVisiblity();
    void setOrientation(Qt::Orientation);
protected:
    void addSpacer();
    void resizeEvent(QResizeEvent *event);
    void relayoutDock();
    void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);

protected slots:
    void handleItemClicked();
private:
    QList<Core::ActionModel*> l_actions;
    QList<Core::ActionView*> l_items;
    int i_dockWidth;
    QList<int> l_spacers;

    QSize sz_iconSize;
    Qt::Orientation e_currentOrientation;
    QLinearGradient gr_background;
    qreal r_offset;
    bool b_pressed;
    QPointF pnt_pressed;
    QGraphicsItem* g_pressed;

    Core::ActionManager * m_actionManager;


signals:
    void touched();
    void activationChanged();

};

}

#endif
