#include "svgrenderfarm.h"
#include "core/platform_configuration.h"

namespace Util {


SvgRenderFarm::SvgRenderFarm()
        : QObject(NULL)
{
}

SvgRenderFarm* SvgRenderFarm::m_instance = NULL;

SvgRenderFarm* SvgRenderFarm::instance()
{
    if (m_instance==NULL) {
        m_instance = new SvgRenderFarm();
    }
    return m_instance;
}

void SvgRenderFarm::clear()
{
    if (m_instance) {
        delete m_instance;
    }
}

SvgRenderFarm::~SvgRenderFarm()
{
    foreach (const QString &key, m_map.keys()) {
        delete m_map[key];
    }
    m_map.clear();
    m_data.clear();
}

QSvgRenderer* SvgRenderFarm::createSharedRendered(const QString &actionName, const QByteArray &data)
{
    m_map[actionName] = new QSvgRenderer(data);
    m_data[actionName] = QByteArray(data);
    QSvgRenderer* result = getSharedRendered(actionName);
    Q_CHECK_PTR(result);
    return result;
}

QSvgRenderer* SvgRenderFarm::createSharedRendered(const QString &actionName, const QString &fileName)
{
    m_map[actionName] = new QSvgRenderer(fileName);
    QFile f(fileName);
    if ( f.open(QIODevice::ReadOnly) ) {
        m_data[actionName] = f.readAll();
        f.close();
    }
    else {
        m_data[actionName] = QByteArray();
    }
    QSvgRenderer* result = getSharedRendered(actionName);
    Q_CHECK_PTR(result);
    return result;
}

QByteArray SvgRenderFarm::getSvgData(const QString &actionName) const
{
    return m_data[actionName];
}

QSvgRenderer* SvgRenderFarm::createSharedRenderedForFunction(const QString &actionName)
{
    const QString fileName = Core::PlatformConfiguration::imagesPath()+"/function_icon.svg";
    const QString letter = actionName.mid(9);
    QByteArray data;

    // Qt release-build bug!!! We can't use QFile here :-(
    FILE *f = fopen(QDir::toNativeSeparators(fileName).toLocal8Bit().data(), "rb");
    Q_ASSERT (f);
    char *buffer;
    buffer = (char*)calloc(1024, sizeof(char));
    size_t bytesRead = 0;
    do {
        bytesRead = fread(buffer, sizeof(char), 1024, f);
        data.append(buffer, bytesRead);
    }
    while (bytesRead>0);
    fclose(f);
    free(buffer);
    QString d = QString::fromLocal8Bit(data);
//    qDebug() << d;
    d.replace("@", letter);
    QByteArray data2 = d.toUtf8();
    m_data[actionName] = data2;
    m_map[actionName] = new QSvgRenderer(m_data[actionName]);
    QSvgRenderer* result = getSharedRendered(actionName);
    Q_CHECK_PTR(result);
    return result;
}

QSvgRenderer* SvgRenderFarm::getSharedRendered(const QString &actionName)
{
    if (m_map.contains(actionName))
        return m_map[actionName];
    else
        return NULL;
}

}
