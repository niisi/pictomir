var p = pictomir;

function enableButtons(ids) {
    for (var i=0; i<ids.length; i++) {
        var btn = document.getElementById(ids[i]);
        btn.className = "button";
    }
}

function disableButtons(ids) {
    for (var i=0; i<ids.length; i++) {
        var btn = document.getElementById(ids[i]);
        btn.className = "button_disabled";
    }
}

onRobotCommandFinished = function(groupName) {
    if (groupName=="first_task_just_move") {
        enableButtons(["b11", "b12", "b13", "b14"]);
        var unpaintedCells = pictomir.underdoneCellsLeft("first_task_just_move");
        var text = "";
        if (unpaintedCells==1) {
            text = "1&nbsp;клетку";
        }
        else if (unpaintedCells>1 && unpaintedCells<5) {
            text = unpaintedCells+"&nbsp;клетки";
        }
        else {
            text = unpaintedCells+"&nbsp;клеток";
        }
        var span = document.getElementById("t1_counter");
        span.innerHTML = text;
    }
}

onRobotCommandFailed = function(groupName) {
    if (groupName=="first_task_just_move") {
        var t1_crash = document.getElementById("t1_crash");
        var t1_cells = document.getElementById("t1_cells");
        t1_crash.className = "visible";
        t1_cells.className = "hidden";
    }
}

function t1_left() {
    if (document.getElementById("b11").className=="button") {
        disableButtons(["b11", "b12", "b13", "b14"]);
        pictomir.evaluateCommand("first_task_just_move", "turn left");
    }
}

function t1_right() {
    if (document.getElementById("b13").className=="button") {
        disableButtons(["b11", "b12", "b13", "b14"]);
        pictomir.evaluateCommand("first_task_just_move", "turn right");
    }
}

function t1_forward() {
    if (document.getElementById("b12").className=="button") {
        disableButtons(["b11", "b12", "b13", "b14"]);
        pictomir.evaluateCommand("first_task_just_move", "go forward");
    }
}

function t1_paint() {
    if (document.getElementById("b14").className=="button") {
        disableButtons(["b11", "b12", "b13", "b14"]);
        pictomir.evaluateCommand("first_task_just_move", "paint");
    }
}

function t1_reset() {
    enableButtons(["b11", "b12", "b13", "b14"]);
    var t1_crash = document.getElementById("t1_crash");
    var t1_cells = document.getElementById("t1_cells");
    t1_crash.className = "hidden";
    t1_cells.className = "visible";
    pictomir.resetActor("first_task_just_move");
}

function t2_start() {
    if (document.getElementById("t2_start_link").className!="") {
        document.getElementById("t2_start_link").className = "";
        pictomir.start("first_task_just_run");
        document.getElementById("t2_crash").className = "hidden";
    }
}

function t3_start() {
    if (document.getElementById("t3_start_link").className!="") {
        document.getElementById("t3_start_link").className = "";
        document.getElementById("t3_crash").className = "hidden";
        document.getElementById("t3_cells").className = "hidden";
        pictomir.start("first_task_fix_and_run");
    }
}

function t4_start() {
    if (document.getElementById("t4_start_link").className!="") {
        document.getElementById("t4_start_link").className = "";
        document.getElementById("t4_crash").className = "hidden";
        document.getElementById("t4_cells").className = "hidden";
        pictomir.start("first_task_compose");
    }
}

function t4_reset() {
    pictomir.resetActor("first_task_compose");
}

onEvaluationFinished = function(group, status) {
    if (group=="first_task_just_run") {
        document.getElementById("t2_start_link").className = "link_button";
        if (status=="crash") {
            document.getElementById("t2_crash").className = "";
        }
    }
    if (group=="first_task_fix_and_run") {
        document.getElementById("t3_start_link").className = "link_button";
        if (status=="crash") {
            document.getElementById("t3_crash").className = "";
        }
        else if (status!=0) {
            document.getElementById("t3_cells").className = "";
        }
        else {
            document.getElementById("t3_done").className = "";
        }
    }
    if (group=="first_task_compose") {
        document.getElementById("t4_start_link").className = "link_button";
        if (status=="crash") {
            document.getElementById("t4_crash").className = "";
        }
        else if (status!=0) {
            document.getElementById("t4_cells").className = "";
        }
        else {
            document.getElementById("t4_done").className = "";
        }
    }
}