#ifndef SAVESTATEDIALOG_H
#define SAVESTATEDIALOG_H

#include <QtGui>
#include "ui_savestatedialog.h"

namespace Desktop {

class SaveStateDialog:
        public QDialog,
        private Ui_SaveStateDialog
{
    Q_OBJECT;
public:
    explicit SaveStateDialog(QWidget *parent);

    void init(const QString &reason,
              const QString &currentGameName,
              const QString &defaultFileName,
              const QString &baseFileName);

    QPair<QString,QString> nameAndPath() const;
    inline QMessageBox::StandardButton result() const { return f_result; }

private slots:
    void handleEditTextChanged(const QString &text);
    void handleBtnSavePressed();
    void handleBtnDiscardPressed();
    void handleBtnCancelPressed();

private:
    QMap<QString,QString> m_filenames;
    QString s_baseFileName;
    QMessageBox::StandardButton f_result;


};

}

#endif // SAVESTATEDIALOG_H
