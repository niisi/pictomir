<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
<context>
    <name>ActionManager</name>
    <message>
        <source>Forever</source>
        <translation type="obsolete">Бесконечно</translation>
    </message>
    <message>
        <source>Twice</source>
        <translation type="obsolete">Два раза</translation>
    </message>
    <message>
        <source>Three times</source>
        <translation type="obsolete">Три раза</translation>
    </message>
    <message>
        <source>Four times</source>
        <translation type="obsolete">Четыре раза</translation>
    </message>
    <message>
        <source>Five times</source>
        <translation type="obsolete">Пять раз</translation>
    </message>
    <message>
        <source>Six times</source>
        <translation type="obsolete">Шесть раз</translation>
    </message>
    <message>
        <source> ABCDEFGHIJKLMNOPQRSTUVWXYZ</source>
        <translation type="obsolete"> АБВГЕДЖЗИКЛМНОПРСТУФХЦЧШЩЫЭЮЯ</translation>
    </message>
    <message>
        <source>Call command %1</source>
        <translation type="obsolete">Выполнить команду %1</translation>
    </message>
</context>
<context>
    <name>AddAlgorhitmDialog</name>
    <message>
        <source>Algorhitm properties</source>
        <translation>Свойства алгоритма</translation>
    </message>
    <message>
        <source>Algorhitm size</source>
        <translation>Число клеток</translation>
    </message>
    <message>
        <source>Width:</source>
        <translation>Ширина:</translation>
    </message>
    <message>
        <source>Height:</source>
        <translation>Высота:</translation>
    </message>
    <message>
        <source>Has repeater</source>
        <translation>Повторитель</translation>
    </message>
    <message>
        <source>Has condition</source>
        <translation>Условие</translation>
    </message>
</context>
<context>
    <name>ClearButton</name>
    <message>
        <source>Clear</source>
        <translation>Очистить</translation>
    </message>
</context>
<context>
    <name>CookieExceptionsModel</name>
    <message>
        <source>Website</source>
        <translation>Сайт</translation>
    </message>
    <message>
        <source>Rule</source>
        <translation>Правило</translation>
    </message>
    <message>
        <source>Allow</source>
        <translation>Разрешить</translation>
    </message>
    <message>
        <source>Block</source>
        <translation>Запретить</translation>
    </message>
    <message>
        <source>Allow For Session</source>
        <translation>Разрешить до перезапуска</translation>
    </message>
</context>
<context>
    <name>CookieModel</name>
    <message>
        <source>Website</source>
        <translation>Сайт</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <source>Path</source>
        <translation>Путь</translation>
    </message>
    <message>
        <source>Secure</source>
        <translation>Безопасно</translation>
    </message>
    <message>
        <source>Expires</source>
        <translation>Заканчивается</translation>
    </message>
    <message>
        <source>Contents</source>
        <translation>Содержимое</translation>
    </message>
    <message>
        <source>Session cookie</source>
        <translation>Сеансовые cookie</translation>
    </message>
    <message>
        <source>true</source>
        <translation></translation>
    </message>
    <message>
        <source>false</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>CookiesDialog</name>
    <message>
        <source>Cookies</source>
        <translation>Cookies</translation>
    </message>
    <message>
        <source>&amp;Remove</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <source>Remove &amp;All Cookies</source>
        <translation>Удалить все</translation>
    </message>
    <message>
        <source>Add &amp;Rule</source>
        <translation>Добавить правило</translation>
    </message>
</context>
<context>
    <name>CookiesExceptionsDialog</name>
    <message>
        <source>Cookie Exceptions</source>
        <translation>Исключение для Cookie</translation>
    </message>
    <message>
        <source>New Exception</source>
        <translation>Новое исключение</translation>
    </message>
    <message>
        <source>Domain:</source>
        <translation>Домен:</translation>
    </message>
    <message>
        <source>Block</source>
        <translation>Запретить</translation>
    </message>
    <message>
        <source>Allow For Session</source>
        <translation>Разрешить до перезапуска</translation>
    </message>
    <message>
        <source>Allow</source>
        <translation>Разрешить</translation>
    </message>
    <message>
        <source>Exceptions</source>
        <translation>Исключения</translation>
    </message>
    <message>
        <source>&amp;Remove</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <source>Remove &amp;All</source>
        <translation>Удалить все</translation>
    </message>
</context>
<context>
    <name>Core::ActionManager</name>
    <message>
        <source>Forever</source>
        <translation>Бесконечно</translation>
    </message>
    <message>
        <source>Twice</source>
        <translation>Два раза</translation>
    </message>
    <message>
        <source>Three times</source>
        <translation>Три раза</translation>
    </message>
    <message>
        <source>Four times</source>
        <translation>Четыре раза</translation>
    </message>
    <message>
        <source>Five times</source>
        <translation>Пять раз</translation>
    </message>
    <message>
        <source>Six times</source>
        <translation>Шесть раз</translation>
    </message>
    <message>
        <source> ABCDEFGHIJKLMNOPQRSTUVWXYZ</source>
        <translation type="obsolete"> АБВГЕДЖЗИКЛМНОПРСТУФХЦЧШЩЫЭЮЯ</translation>
    </message>
    <message>
        <source>Call command %1</source>
        <translation>Выполнить команду %1</translation>
    </message>
    <message utf8="true">
        <source> АБВГДЕЖЗИКЛМНОПРСТУФ</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Core::GameProvider</name>
    <message>
        <source>New game</source>
        <translation type="obsolete">Новая игра</translation>
    </message>
    <message>
        <source>Type here to set task title</source>
        <translation type="obsolete">Введите заголовок задания</translation>
    </message>
    <message>
        <source>Copy of </source>
        <translation type="obsolete">Копия </translation>
    </message>
    <message>
        <source>Copy %1 of </source>
        <translation type="obsolete">%1 копия </translation>
    </message>
</context>
<context>
    <name>Core::PictomirCommon</name>
    <message>
        <source>Cant load environment</source>
        <translation type="obsolete">Не могу загрузить обстановку</translation>
    </message>
    <message>
        <source>File not exists: %1</source>
        <translation type="obsolete">Файл %1 не найден</translation>
    </message>
    <message>
        <source>Permission denied: %1</source>
        <translation type="obsolete">Нет доступа к %1</translation>
    </message>
    <message>
        <source>No embedded environment for this task</source>
        <translation type="obsolete">Для этого задания нет встроенной обстановки</translation>
    </message>
    <message>
        <source>Embedded environment is empty for this task</source>
        <translation type="obsolete">Встроенная обстановка этого задания пустая</translation>
    </message>
    <message>
        <source>Environment data damaged in reference: %1</source>
        <translation type="obsolete">В %1 повреждена обстановка</translation>
    </message>
    <message>
        <source>Environment data damaged</source>
        <translation>Повреждена обстановка</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <source>Running step...</source>
        <translation>Выполняю шаг...</translation>
    </message>
    <message>
        <source>Begin per-step running...</source>
        <translation>Начало пошагового выполнения...</translation>
    </message>
    <message>
        <source>Unpainted cells left: %1</source>
        <comment>1</comment>
        <translation>Осталась %1 незакрашенная клетка</translation>
    </message>
    <message>
        <source>Unpainted cells left: %1</source>
        <comment>2-4</comment>
        <translation>Остались %1 незакрашенные клетки</translation>
    </message>
    <message>
        <source>Unpainted cells left: %1</source>
        <comment>other</comment>
        <translation>Осталось %1 незакрашенных клеток</translation>
    </message>
    <message>
        <source>Game finished!</source>
        <translation>Игра пройдена!</translation>
    </message>
    <message>
        <source>Task finished!</source>
        <translation>Задание пройдено!</translation>
    </message>
    <message>
        <source>Copy of </source>
        <translation type="obsolete">Копия </translation>
    </message>
</context>
<context>
    <name>Core::PictomirProgram</name>
    <message>
        <source> ABCDEFGHIJKLMNOPQRSTUVWXYZ</source>
        <comment>letters</comment>
        <translation type="obsolete"> АБВГЕДЖЗИКЛМНОПРСТУФХЦЧШЩЫЭЮЯ</translation>
    </message>
    <message>
        <source>Main</source>
        <translation type="obsolete">Главный</translation>
    </message>
    <message>
        <source>Algorhitm %1</source>
        <translation type="obsolete">Алгоритм %1</translation>
    </message>
    <message>
        <source>Alg %1</source>
        <translation type="obsolete">Алг. %1</translation>
    </message>
    <message utf8="true">
        <source> АБВГДЕЖЗИКЛМНОПРСТУФ</source>
        <comment>letters</comment>
        <translation></translation>
    </message>
    <message utf8="true">
        <source>Главный</source>
        <translation></translation>
    </message>
    <message utf8="true">
        <source>Алгоритм %1</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Creator::EnvironmentEditor</name>
    <message>
        <source>New...</source>
        <translation>Очистить...</translation>
    </message>
    <message>
        <source>Load...</source>
        <translation>Загрузить...</translation>
    </message>
    <message>
        <source>Import from Kumir...</source>
        <translation>Импортировать Кумир-обстановку...</translation>
    </message>
    <message>
        <source>Export to Kumir...</source>
        <translation>Экспорт в Кумир...</translation>
    </message>
    <message>
        <source>Kumir robot environment files (*.fil)</source>
        <translation>Обстановки Робота из Кумир (*.fil)</translation>
    </message>
    <message>
        <source>Can&apos;t import environment from Kumir</source>
        <translation>Не могу импортировать обстановку</translation>
    </message>
    <message>
        <source>Load environment...</source>
        <translation>Загрузить обстановку...</translation>
    </message>
    <message>
        <source>Can&apos;t export environment to Kumir</source>
        <translation>Ну могу сохранить Кумир-обстановку</translation>
    </message>
    <message>
        <source>Save environment...</source>
        <translation>Сохранить обстановку...</translation>
    </message>
    <message>
        <source>File is not writable</source>
        <translation>Нет прав для записи</translation>
    </message>
    <message>
        <source>Load game...</source>
        <translation type="obsolete">Загрузить игру...</translation>
    </message>
    <message>
        <source>File is not readable</source>
        <translation>Файл не прочитался</translation>
    </message>
    <message>
        <source>File is not robot environment or file damaged</source>
        <translation>Этот файл не похож на обстановку робота или поврежден</translation>
    </message>
    <message>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <source>H</source>
        <translation></translation>
    </message>
    <message>
        <source>R</source>
        <translation></translation>
    </message>
    <message>
        <source>W</source>
        <translation></translation>
    </message>
    <message>
        <source>P</source>
        <translation></translation>
    </message>
    <message>
        <source>O</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Creator::HintEditor</name>
    <message>
        <source>No hint</source>
        <translation>Нет подсказки</translation>
    </message>
    <message>
        <source>Set text</source>
        <translation>Задать текст</translation>
    </message>
    <message>
        <source>Set picture...</source>
        <translation>Задать картинку...</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation>Очистить</translation>
    </message>
    <message>
        <source>Image</source>
        <translation>Изображение</translation>
    </message>
    <message>
        <source>Clear hint</source>
        <translation>Убрать подсказку</translation>
    </message>
    <message>
        <source>Set plain text as hint</source>
        <translation>Задать текст подсказки</translation>
    </message>
    <message>
        <source>Load picture as hint...</source>
        <translation>Задать картинку подсказки...</translation>
    </message>
    <message>
        <source>Supported pictures (*.png *.svg);;Raster images (*.png);;Vector drawings (*.svg)</source>
        <translation>Поддерживаемые картинки (*.png *.svg);;Растровые изображения (*.png);;Векторные рисунки (*.svg)</translation>
    </message>
    <message>
        <source>Load picture...</source>
        <translation>Загрузить картинку...</translation>
    </message>
    <message>
        <source>Can&apos;t load image</source>
        <translation>Но могу загрузить изображение</translation>
    </message>
    <message>
        <source>File not readable</source>
        <translation>Файл не читается</translation>
    </message>
    <message>
        <source>File is not PNG iamge</source>
        <translation>Это не PNG-изображение</translation>
    </message>
    <message>
        <source>Can&apos;t load drawing</source>
        <translation>Не могу прочитать рисунок</translation>
    </message>
    <message>
        <source>File is not SVG drawing</source>
        <translation>Это не SVG-рисунок</translation>
    </message>
    <message>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Creator::MainWindow</name>
    <message>
        <source>PictoMir Creator</source>
        <translation>ПиктоМир-Конструктор</translation>
    </message>
    <message>
        <source>Game</source>
        <translation>Игра</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Справка</translation>
    </message>
    <message>
        <source>Task</source>
        <translation>Задание</translation>
    </message>
    <message>
        <source>Envrironment</source>
        <translation>Обстановка</translation>
    </message>
    <message>
        <source>Program</source>
        <translation>Программа</translation>
    </message>
    <message>
        <source>Hint</source>
        <translation>Подсказка</translation>
    </message>
    <message>
        <source>Load...</source>
        <translation>Загрузить...</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <source>Save as...</source>
        <translation>Сохранить как...</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <source>New</source>
        <translation>Создать</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <source>Rename...</source>
        <translation>Удалить...</translation>
    </message>
    <message>
        <source>Clone</source>
        <translation>Скопировать</translation>
    </message>
    <message>
        <source>Move up</source>
        <translation>Поднять вверх</translation>
    </message>
    <message>
        <source>Move down</source>
        <translation>Опустить вниз</translation>
    </message>
    <message>
        <source>Show editor</source>
        <translation>Показать редактор</translation>
    </message>
    <message>
        <source>Create new...</source>
        <translation>Создать...</translation>
    </message>
    <message>
        <source>Import from Kumir...</source>
        <translation>Импортировать из Кумир...</translation>
    </message>
    <message>
        <source>Add algorhitm...</source>
        <translation>Добавить алгоритм...</translation>
    </message>
    <message>
        <source>Change algorhitm</source>
        <translation>Изменть алгоритм</translation>
    </message>
    <message>
        <source>Remove algorhitm</source>
        <translation>Удалить алгоритм</translation>
    </message>
    <message>
        <source>Clear hint</source>
        <translation>Убрать подсказку</translation>
    </message>
    <message>
        <source>Set plain text as hint</source>
        <translation>Задать текст подсказки</translation>
    </message>
    <message>
        <source>Load picture as hint...</source>
        <translation>Задать картинку подсказки...</translation>
    </message>
    <message>
        <source>Recent games</source>
        <translation>Последние файлы</translation>
    </message>
    <message>
        <source>Usage</source>
        <translation>Руководство по использованию</translation>
    </message>
    <message>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <source>Export to Kumir...</source>
        <translation>Экспорт в Кумир...</translation>
    </message>
    <message>
        <source>Untitled task %1</source>
        <translation>Новое задание %1</translation>
    </message>
    <message>
        <source>New game %1</source>
        <translation>Новая игра %1</translation>
    </message>
    <message>
        <source>The following games has unsaved changes:
%1
Save them?
</source>
        <translation>Есть несохраненные игры:
%1
Сохранить их?</translation>
    </message>
    <message>
        <source>Save changes</source>
        <translation>Сохранить изменения</translation>
    </message>
    <message>
        <source>Pictomir tasks, version &gt;= 0.12.x (*.pm.json);;Pictomir tasks, version &lt; 0.12.x (*.pm.xml)</source>
        <translation>Игры ПиктоМира версии от 0.12 и выше (*.pm.json);;Игры ПиктоМира версии до 0.12 (*.pm.xml)</translation>
    </message>
    <message>
        <source>Load game...</source>
        <translation>Загрузить игру...</translation>
    </message>
    <message>
        <source>Can&apos;t load game</source>
        <translation>Не могу загрузить игру</translation>
    </message>
</context>
<context>
    <name>Creator::NewEnvironmentDialog</name>
    <message>
        <source>New environment...</source>
        <translation>Новая обстановка</translation>
    </message>
    <message>
        <source>Size from west to east:</source>
        <translation>Число клеток с запада на восток:</translation>
    </message>
    <message>
        <source>Size from north to south:</source>
        <translation>Число клеток с севера на юг:</translation>
    </message>
</context>
<context>
    <name>Creator::ProgramEditor</name>
    <message>
        <source>Add algorhitm</source>
        <translation>Добавить алгоримт</translation>
    </message>
    <message>
        <source>Change algorhitm</source>
        <translation>Изменить алгоритм</translation>
    </message>
    <message>
        <source>Remove algorhitm</source>
        <translation>Убрать алгоритм</translation>
    </message>
    <message>
        <source>Add algorhitm...</source>
        <translation>Добавить алгоритм...</translation>
    </message>
    <message>
        <source>Are you sure to REMOVE this algorhitm and its contents?</source>
        <translation>Действительно УДАЛИТЬ алгоритм вместе с его содержимым?</translation>
    </message>
    <message>
        <source> ABCDEFGHIJKLMNOPQRSTUVWXYZ</source>
        <comment>letters</comment>
        <translation type="obsolete"> АБВГЕДЖЗИКЛМНОПРСТУФХЦЧШЩЫЭЮЯ</translation>
    </message>
    <message>
        <source>Main</source>
        <translation type="obsolete">Главный</translation>
    </message>
    <message>
        <source>Algorhitm %1</source>
        <translation type="obsolete">Алгоритм %1</translation>
    </message>
    <message>
        <source>Alg %1</source>
        <translation type="obsolete">Алг. %1</translation>
    </message>
    <message>
        <source>Change &quot;%1&quot;</source>
        <translation>Изменить &quot;%1&quot;</translation>
    </message>
    <message>
        <source>Remove &quot;%1&quot;</source>
        <translation>Убрать &quot;%1&quot;</translation>
    </message>
    <message utf8="true">
        <source> АБВГДЕЖЗИКЛМНОПРСТУФ</source>
        <comment>letters</comment>
        <translation></translation>
    </message>
    <message utf8="true">
        <source>Главный</source>
        <translation></translation>
    </message>
    <message utf8="true">
        <source>Алгоритм %1</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Creator::RenameTaskDialog</name>
    <message>
        <source>Dialog</source>
        <translation>Имя задания</translation>
    </message>
    <message>
        <source>Task title:</source>
        <translation>Заголовок задания:</translation>
    </message>
</context>
<context>
    <name>Creator::TaskListWindow</name>
    <message>
        <source>Game title:</source>
        <translation>Имя игры:</translation>
    </message>
    <message>
        <source>Information</source>
        <translation>Общая информация</translation>
    </message>
    <message>
        <source>Author(s):</source>
        <translation>Автор(ы):</translation>
    </message>
    <message>
        <source>Copyright:</source>
        <translation>Авторские права:</translation>
    </message>
    <message>
        <source>Home page:</source>
        <translation>Web-ссылка:</translation>
    </message>
    <message>
        <source>License</source>
        <translation>Лицензионное соглашение</translation>
    </message>
    <message>
        <source>Other</source>
        <translation>Другое</translation>
    </message>
    <message>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <source>Clone</source>
        <translation>Копировать</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation>Переименовать</translation>
    </message>
    <message>
        <source>Move down</source>
        <translation>Опустить вниз</translation>
    </message>
    <message>
        <source>Game title should not be empty!</source>
        <translation>Имя игры не должно быть пустым!</translation>
    </message>
    <message>
        <source>Your name here</source>
        <translation>Ваше имя</translation>
    </message>
    <message>
        <source>[Usually] your organization name</source>
        <translation>(Обычно) имя организации</translation>
    </message>
    <message>
        <source>Hot to contact you</source>
        <translation>Как связаться с Вами</translation>
    </message>
    <message>
        <source>Copy of </source>
        <translation>Копия </translation>
    </message>
    <message>
        <source>Remove task</source>
        <translation>Удалить задание</translation>
    </message>
    <message>
        <source>Are you sure to REMOVE this task and its contents?</source>
        <translation>Действително УДАЛИТЬ это задание со всем его содержимым?</translation>
    </message>
    <message>
        <source>Save changes</source>
        <translation>Сохранить изменения</translation>
    </message>
    <message>
        <source>This game have unsaved changes. Save it?</source>
        <translation>Эта игра не сохранена. Сохранить?</translation>
    </message>
    <message>
        <source>Pictomir tasks, version &gt;= 0.12.x (*.pm.json);;Pictomir tasks, version &lt; 0.12.x (*.pm.xml)</source>
        <translation>Игры ПиктоМира версии от 0.12 и выше (*.pm.json);;Игры ПиктоМира версии до 0.12 (*.pm.xml)</translation>
    </message>
    <message>
        <source>Load game...</source>
        <translation>Загрузить игру...</translation>
    </message>
    <message>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <source>E</source>
        <translation></translation>
    </message>
    <message>
        <source>P</source>
        <translation></translation>
    </message>
    <message>
        <source>H</source>
        <translation></translation>
    </message>
    <message>
        <source>Creative Commons </source>
        <translation></translation>
    </message>
    <message>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <source>Can&apos;t save task</source>
        <translation></translation>
    </message>
    <message>
        <source>Can&apos;t write environment one of environments for this task</source>
        <translation></translation>
    </message>
    <message>
        <source>Destination directory is not writable</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Desktop::EnvEditorToolBar</name>
    <message>
        <source>No edit</source>
        <translation type="obsolete">Без редактирования</translation>
    </message>
</context>
<context>
    <name>Desktop::MainWindow</name>
    <message>
        <source>MainWindow</source>
        <translation>ПиктоМир</translation>
    </message>
    <message>
        <source>&amp;Game</source>
        <translation>Игра</translation>
    </message>
    <message>
        <source>&amp;View</source>
        <translation>Вид</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>Справка</translation>
    </message>
    <message>
        <source>&amp;Action</source>
        <translation>Действие</translation>
    </message>
    <message>
        <source>&amp;Program</source>
        <translation>Программа</translation>
    </message>
    <message>
        <source>Select user...</source>
        <translation>Выбрать пользователя...</translation>
    </message>
    <message>
        <source>Load game...</source>
        <translation>Загрузить игру...</translation>
    </message>
    <message>
        <source>Next task</source>
        <translation>Следующее задание</translation>
    </message>
    <message>
        <source>Previous task</source>
        <translation>Предыдущее задание</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <source>Low quality, but faster</source>
        <translation>Пониженное качество изображения</translation>
    </message>
    <message>
        <source>Full screen</source>
        <translation>На весь экран</translation>
    </message>
    <message>
        <source>Content</source>
        <translation>Содержание</translation>
    </message>
    <message>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <source>Start</source>
        <translation>Марш</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation>Стоп</translation>
    </message>
    <message>
        <source>Run to end</source>
        <translation>Выполнить до конца</translation>
    </message>
    <message>
        <source>Step</source>
        <translation>Шаг</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation>На старт</translation>
    </message>
    <message>
        <source>Reset program to initial</source>
        <translation>Вернуть программу к исходной</translation>
    </message>
    <message>
        <source>&amp;Copy to clipboard</source>
        <translation>Скопировать в буфер обмена</translation>
    </message>
    <message>
        <source>Pictomir</source>
        <translation>ПиктоМир</translation>
    </message>
    <message>
        <source>F2</source>
        <translation></translation>
    </message>
    <message>
        <source>F3</source>
        <translation></translation>
    </message>
    <message>
        <source>PgDown</source>
        <translation></translation>
    </message>
    <message>
        <source>PgUp</source>
        <translation></translation>
    </message>
    <message>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <source>Ctrl+Shift+Y</source>
        <translation></translation>
    </message>
    <message>
        <source>F11</source>
        <translation></translation>
    </message>
    <message>
        <source>F1</source>
        <translation></translation>
    </message>
    <message>
        <source>F9</source>
        <translation></translation>
    </message>
    <message>
        <source>Esc</source>
        <translation></translation>
    </message>
    <message>
        <source>Shift+F9</source>
        <translation></translation>
    </message>
    <message>
        <source>F8</source>
        <translation></translation>
    </message>
    <message>
        <source>F10</source>
        <translation></translation>
    </message>
    <message>
        <source>Ctrl+N</source>
        <translation></translation>
    </message>
    <message>
        <source>Ctrl+C</source>
        <translation></translation>
    </message>
    <message>
        <source>Save as Kumir program...</source>
        <translation>Сохранить как программу Кумир...</translation>
    </message>
    <message>
        <source>Save as Kumir program</source>
        <translation>Сохранить как программу Кумир</translation>
    </message>
    <message>
        <source>Can&apos;t save program</source>
        <translation>Не могу сохранить программу</translation>
    </message>
    <message>
        <source>Can&apos;t save kumir program to file
%1
Access denied</source>
        <translation>Ошибка записи в файл
%1</translation>
    </message>
</context>
<context>
    <name>Desktop::RenameGameDialog</name>
    <message>
        <source>Game name is empty</source>
        <translation>Имя игры не указано</translation>
    </message>
    <message>
        <source>This name is already in use</source>
        <translation>Это имя уже используется</translation>
    </message>
</context>
<context>
    <name>Desktop::SelectUserDialog</name>
    <message>
        <source>Remove user</source>
        <translation>Удалить пользователя</translation>
    </message>
    <message>
        <source>Are you sure to delete user %1?</source>
        <translation>Вы уверены в том, что хотите удалить пользователя %1?</translation>
    </message>
    <message>
        <source>Log in as %1</source>
        <translation>Войти как %1</translation>
    </message>
    <message>
        <source>Guest</source>
        <translation>Гость</translation>
    </message>
</context>
<context>
    <name>EnvironmentFileControl</name>
    <message>
        <source>New...</source>
        <translation type="obsolete">Очистить...</translation>
    </message>
</context>
<context>
    <name>LoadGameDialog</name>
    <message>
        <source>Select game - PictoMir</source>
        <translation>Выбрать игру - ПиктоМир</translation>
    </message>
    <message>
        <source>Create new game</source>
        <translation>Создать новую игру</translation>
    </message>
    <message>
        <source>Rename selected game</source>
        <translation>Переименовать выбранную игру</translation>
    </message>
    <message>
        <source>Delete selected game</source>
        <translation>Удалить выбранную игру</translation>
    </message>
    <message>
        <source>Clone selected game</source>
        <translation>Дублировать выбранную игру</translation>
    </message>
    <message>
        <source>Select</source>
        <translation>Выбрать</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <source>Return</source>
        <translation></translation>
    </message>
    <message>
        <source>Esc</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Pictomir</source>
        <translation type="obsolete">ПиктоМир</translation>
    </message>
    <message>
        <source>PictoMir</source>
        <translation type="obsolete">ПиктоМир</translation>
    </message>
    <message>
        <source>Add new tab</source>
        <translation type="obsolete">Новая вкладка</translation>
    </message>
    <message>
        <source>Toggle menu visiblity</source>
        <translation type="obsolete">Показать/скрыть меню</translation>
    </message>
    <message>
        <source>New Tab</source>
        <translation type="obsolete">Новая вкладка</translation>
    </message>
    <message>
        <source>%1 - PictoMir</source>
        <translation type="obsolete">%1 - ПиктоМир</translation>
    </message>
    <message>
        <source>%1 - PictoMir Navigator</source>
        <translation type="obsolete">%1 - ПиктоМир Навигатор</translation>
    </message>
</context>
<context>
    <name>PictomirCommon</name>
    <message>
        <source>Select user...</source>
        <translation type="obsolete">Сменить пользователя...</translation>
    </message>
    <message>
        <source>Reset program to initial</source>
        <translation type="obsolete">Вернуть программу к исходной</translation>
    </message>
    <message>
        <source>Start</source>
        <translation type="obsolete">Марш</translation>
    </message>
    <message>
        <source>Run to end</source>
        <translation type="obsolete">Выполнить до конца</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation type="obsolete">Стоп</translation>
    </message>
    <message>
        <source>Step</source>
        <translation type="obsolete">Шаг</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation type="obsolete">На старт</translation>
    </message>
    <message>
        <source>Copy of </source>
        <translation type="obsolete">Копия </translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Ошибка</translation>
    </message>
</context>
<context>
    <name>PictomirProgram</name>
    <message>
        <source> ABCDEFGHIJKLMNOPQRSTUVWXYZ</source>
        <comment>letters</comment>
        <translation type="obsolete"> АБВГЕДЖЗИКЛМНОПРСТУФХЦЧШЩЫЭЮЯ</translation>
    </message>
    <message>
        <source>Main</source>
        <translation type="obsolete">Главный</translation>
    </message>
    <message>
        <source>Algorhitm %1</source>
        <translation type="obsolete">Алгоритм %1</translation>
    </message>
    <message>
        <source>Alg %1</source>
        <translation type="obsolete">Алг. %1</translation>
    </message>
</context>
<context>
    <name>RenameGameDialog</name>
    <message>
        <source>Game name is empty</source>
        <translation>Имя игры не указано</translation>
    </message>
    <message>
        <source>Rename game</source>
        <translation>Переименовать игру</translation>
    </message>
    <message>
        <source>Game name:</source>
        <translation>Имя игры:</translation>
    </message>
</context>
<context>
    <name>Robot25D::Plugin</name>
    <message>
        <source>Planet</source>
        <translation>Сцена</translation>
    </message>
    <message>
        <source>Load environment...</source>
        <translation>Загрузить обстановку...</translation>
    </message>
    <message>
        <source>Load default environment</source>
        <translation>Загрузить стартовую обстановку</translation>
    </message>
    <message>
        <source>Load environment</source>
        <translation>Загрузить обстановку</translation>
    </message>
    <message>
        <source>Environments (*.fil)</source>
        <translation>Файлы обстановок (*.fil)</translation>
    </message>
</context>
<context>
    <name>Runtime::ExecutorKRT</name>
    <message>
        <source>Execution terminated</source>
        <translation>Выполнение прервано</translation>
    </message>
    <message>
        <source>Execution finished</source>
        <translation>Выполнение завершено</translation>
    </message>
    <message>
        <source>Execution started. Next is: %1</source>
        <translation>Начало пошагового выполнения. Следующий шаг: «%1»</translation>
    </message>
    <message>
        <source>Step finished. Next is: &apos;%1&apos;</source>
        <translation>Шаг выполнен. Следующий: «%1»</translation>
    </message>
    <message>
        <source>Execution paused</source>
        <translation>Пауза</translation>
    </message>
</context>
<context>
    <name>SaveStateDialog</name>
    <message>
        <source>Dialog</source>
        <translation>Имя задания</translation>
    </message>
    <message>
        <source>Save game?</source>
        <translation></translation>
    </message>
    <message>
        <source>Save as:</source>
        <translation></translation>
    </message>
    <message>
        <source>Return</source>
        <translation></translation>
    </message>
    <message>
        <source>Do not save</source>
        <translation></translation>
    </message>
    <message>
        <source>Shift+Return</source>
        <translation></translation>
    </message>
    <message>
        <source>Cancel %1</source>
        <translation></translation>
    </message>
    <message>
        <source>Esc</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SearchLineEdit</name>
    <message>
        <source>Search</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SelectUserDialog</name>
    <message>
        <source>Select user - PictoMir</source>
        <translation>Смена пользователя ПиктоМир</translation>
    </message>
    <message>
        <source>Please select user or create one:</source>
        <translation>Выберете существующего пользователя или введите имя нового</translation>
    </message>
    <message>
        <source>Remove selected user</source>
        <translation>Удалить выбранного</translation>
    </message>
    <message>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <source>Log in as %1!</source>
        <translation>Войти как %1!</translation>
    </message>
    <message>
        <source>Remove user</source>
        <translation type="obsolete">Удалить пользователя</translation>
    </message>
    <message>
        <source>Are you sure to delete user %1?</source>
        <translation type="obsolete">Вы уверены в том, что хотите удалить пользователя %1?</translation>
    </message>
    <message>
        <source>Log in as %1</source>
        <translation type="obsolete">Войти как %1</translation>
    </message>
    <message>
        <source>Guest</source>
        <translation type="obsolete">Гость</translation>
    </message>
</context>
<context>
    <name>Util::AboutDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="obsolete">Имя задания</translation>
    </message>
    <message>
        <source>About...</source>
        <translation>О программе...</translation>
    </message>
    <message>
        <source>Version %1</source>
        <translation>Версия %1</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <source>No text yet...</source>
        <translation>Нет текста для данного языка...</translation>
    </message>
    <message>
        <source>Splashscreen</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Webkit::MainWindow</name>
    <message>
        <source>MainWindow</source>
        <translation>ПиктоМир</translation>
    </message>
    <message>
        <source>&amp;Window</source>
        <translation>&amp;Окно</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Справка</translation>
    </message>
    <message>
        <source>&amp;View</source>
        <translation>&amp;Вид</translation>
    </message>
    <message>
        <source>New tab</source>
        <translation>Новая вкладка</translation>
    </message>
    <message>
        <source>Close tab</source>
        <translation>Закрыть текущую вкладку</translation>
    </message>
    <message>
        <source>Go start page</source>
        <translation>К начальной странице</translation>
    </message>
    <message>
        <source>Start page</source>
        <translation>Начальная страница</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>Назад</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation>Вперед</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <source>Usage</source>
        <translation>Руководство по использованию</translation>
    </message>
    <message>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <source>Show menubar</source>
        <translation>Показывать меню</translation>
    </message>
    <message>
        <source>Fullscreen</source>
        <translation>На весь экран</translation>
    </message>
    <message>
        <source>PictoMir</source>
        <translation>ПиктоМир</translation>
    </message>
    <message>
        <source>Add new tab</source>
        <translation>Новая вкладка</translation>
    </message>
    <message>
        <source>Toggle menu visiblity</source>
        <translation>Показать/скрыть меню</translation>
    </message>
    <message>
        <source>New Tab</source>
        <translation>Новая вкладка</translation>
    </message>
    <message>
        <source>%1 - PictoMir</source>
        <translation>%1 - ПиктоМир</translation>
    </message>
    <message>
        <source>%1 - PictoMir Navigator</source>
        <translation>%1 - ПиктоМир Навигатор</translation>
    </message>
    <message>
        <source>Ctrl+T</source>
        <translation></translation>
    </message>
    <message>
        <source>Ctrl+W</source>
        <translation></translation>
    </message>
    <message>
        <source>Alt+Home</source>
        <translation></translation>
    </message>
    <message>
        <source>Alt+Right</source>
        <translation></translation>
    </message>
    <message>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <source>F1</source>
        <translation></translation>
    </message>
    <message>
        <source>Ctrl+M</source>
        <translation></translation>
    </message>
    <message>
        <source>F11, Alt+Return</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Webkit::Tab</name>
    <message>
        <source>MainWindow</source>
        <translation>ПиктоМир</translation>
    </message>
    <message>
        <source>Go back one page</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;</source>
        <translation></translation>
    </message>
    <message>
        <source>Alt+Left</source>
        <translation></translation>
    </message>
    <message>
        <source>Go forward one page</source>
        <translation></translation>
    </message>
    <message>
        <source>&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>Alt+Right</source>
        <translation></translation>
    </message>
    <message>
        <source>Reload current page</source>
        <translation></translation>
    </message>
    <message>
        <source>R</source>
        <translation></translation>
    </message>
    <message>
        <source>F5</source>
        <translation></translation>
    </message>
    <message>
        <source>Stop loading page</source>
        <translation></translation>
    </message>
    <message>
        <source>X</source>
        <translation></translation>
    </message>
    <message>
        <source>Esc</source>
        <translation></translation>
    </message>
    <message>
        <source>Go to entered address</source>
        <translation></translation>
    </message>
    <message>
        <source>Go</source>
        <translation></translation>
    </message>
    <message>
        <source>about:blank</source>
        <translation></translation>
    </message>
</context>
</TS>
