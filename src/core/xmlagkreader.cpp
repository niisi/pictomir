#include "xmlagkreader.h"
#include <QtXml>

QString Core::XmlAgkReader::parseData(const QByteArray &data, const QUrl &base, Game &game) const
{
    QDomDocument doc;
    int errLine; int errCol; QString err;
    doc.setContent(data, false, &err, &errLine, &errCol);
    if (!err.isEmpty()) {
        return QString("Error in Xml at (%1,%2) : 3").arg(errLine).arg(errCol).arg(err);
    }
    game.title = doc.documentElement().attribute("title");
    QDomNodeList tasks = doc.elementsByTagName("task");
    for (int i=0; i<tasks.size(); i++) {
        Task task;
        QDomElement xTask = tasks.at(i).toElement();
        task.title = xTask.attribute("title");
        QString env = xTask.attribute("env").trimmed();
        if (env=="#" && xTask.elementsByTagName("env").count()==1) {
            QDomElement xEnv  = xTask.elementsByTagName("env").at(0).toElement();
            QString r;
            for (int j=0; j<xEnv.childNodes().size(); j++) {
                QDomText text = xEnv.childNodes().at(j).toText();
                r += text.data();
            }
            r = r.trimmed();
            task.environment = r.toLocal8Bit().data();
            task.environmentFormat = Core::FileTextFormat;
        }
        else if (!env.isEmpty()) {
            const QDir envDir = QDir(base.toLocalFile());
            const QString fullPath = envDir.absoluteFilePath(env);
            QFile f(fullPath);
            if (f.open(QIODevice::ReadOnly)) {
                task.environment = f.readAll();
                f.close();
            }
            if (env.endsWith(".json") || env.endsWith(".js")) {
                task.environmentFormat = Core::FileJsonFormat;
            }
            else if (env.endsWith(".xml")) {
                task.environmentFormat = Core::FileXmlFormat;
            }
            else {
                task.environmentFormat = Core::FileTextFormat;
            }
        }
        QDomNodeList programs = xTask.elementsByTagName("program");

    }
    return "";
}
