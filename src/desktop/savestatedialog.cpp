#include "savestatedialog.h"
#include <QtXml>



namespace Desktop {

SaveStateDialog::SaveStateDialog(QWidget *parent) :
        QDialog(parent),
        Ui::SaveStateDialog()
{
    setupUi(this);

//    QString css;
//#ifndef Q_OS_MAC
//    QString cssPath = QApplication::applicationDirPath()+
//                      "/../share/pictomir/Stylesheets/savestatedialog.css";
//#endif
//#ifdef Q_OS_MAC
//    QString cssPath = QApplication::applicationDirPath()+
//                      "/../Resources/Stylesheets/savestatedialog.css";
//#endif
//    QFile f(cssPath);
//    if (f.open(QIODevice::ReadOnly|QIODevice::Text)) {
//        QTextStream ts(&f);
//        ts.setCodec("UTF-8");
//        css = ts.readAll();
//        f.close();
//        setStyleSheet(css);
//    }

    connect (btnCancel, SIGNAL(clicked()), this, SLOT(handleBtnCancelPressed()));
    connect (btnSave, SIGNAL(clicked()), this, SLOT(handleBtnSavePressed()));
    connect (btnDiscard, SIGNAL(clicked()), this, SLOT(handleBtnDiscardPressed()));

    connect (comboBox, SIGNAL(editTextChanged(QString)),
             this, SLOT(handleEditTextChanged(QString)));    
    setWindowTitle(QApplication::applicationName());

}

void SaveStateDialog::init(const QString &reason,
                           const QString &currentGameName,
                           const QString &defaultFileName,
                           const QString &baseFileName)
{
    comboBox->setFocus(Qt::TabFocusReason);
    m_filenames.clear();
    if (!reason.isEmpty()) {
        btnCancel->setVisible(true);
        btnCancel->setText(btnCancel->text().replace("%1",reason));
    }
    else {
        btnCancel->setVisible(false);
    }

    f_result = QMessageBox::Cancel;

    const QString localRoot = QDesktopServices::storageLocation(
            QDesktopServices::DataLocation)+"/Tasks/";
    QDir local(localRoot);
    QStringList files = local.entryList();
    int defaultIndex = -1;
    for (int i=0; i<files.count(); i++) {
        if (!files[i].endsWith(".pm.xml")) {
            continue;
        }
        QFile f(local.absoluteFilePath(files[i]));
        QDomDocument doc;
        doc.setContent(&f);
        if (doc.documentElement().nodeName()=="game") {
            QString savedName = doc.documentElement().attribute("saved_name");
            QString title = doc.documentElement().attribute("title");
            if (!savedName.isEmpty() && title==currentGameName) {
                comboBox->addItem(savedName);
                m_filenames[savedName] = files[i];
                if (defaultFileName==files[i])
                    defaultIndex = comboBox->count()-1;
            }
        }
    }
    if (defaultFileName.isEmpty()) {
        comboBox->addItem(tr("Saved game %1").arg(QDateTime::currentDateTime().toString("dd MMM, hh:mm")));
        defaultIndex = comboBox->count()-1;
    }
    if (defaultIndex!=-1) {
        comboBox->setCurrentIndex(defaultIndex);
    }
    s_baseFileName = baseFileName;
}

QPair<QString,QString> SaveStateDialog::nameAndPath() const
{
    const QString localRoot = QDesktopServices::storageLocation(
            QDesktopServices::DataLocation)+"/Tasks/";
    if (!QDir(localRoot).exists()) {
        QDir::root().mkpath(localRoot);
    }
    QDir local(localRoot);
    QString title = comboBox->currentText();
    QString fileName;
    if (m_filenames.contains(title.trimmed())) {
        fileName = m_filenames[title.trimmed()];
    }
    else {
        QStringList files = local.entryList(QStringList() << ".save??.pm.xml");
        int index = 0;
        QString suffix;
        do {
            index++;
            suffix = QString::number(index);
            if (suffix.length()==1)
                suffix = "0"+suffix;
            suffix = ".save" + suffix + ".pm.xml";
        } while ( QFile::exists(localRoot+s_baseFileName+suffix));
        fileName = s_baseFileName+suffix;
    }
    return QPair<QString,QString>(title, localRoot+fileName);
}

void SaveStateDialog::handleBtnCancelPressed()
{
    f_result = QMessageBox::Cancel;
    close();
}

void SaveStateDialog::handleBtnDiscardPressed()
{
    f_result = QMessageBox::Discard;
    close();
}

void SaveStateDialog::handleBtnSavePressed()
{
    f_result = QMessageBox::Save;
    close();
}

void SaveStateDialog::handleEditTextChanged(const QString &text)
{
    btnSave->setEnabled(!text.trimmed().isEmpty());
}
}
