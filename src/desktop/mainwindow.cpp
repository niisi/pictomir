
#include "desktop/mainwindow.h"
#include "desktop/loadgamedialog.h"
#include "desktop/functionwidget.h"
#include "runtime/executorkrt.h"
#include "util/pictomirinfo.h"
#include "desktop/savestatedialog.h"
#include "desktop/selectuserdialog.h"
#include "core/pictomir.h"
#include "interfaces/dockwidget_interface.h"
#include "interfaces/functionwidget_interface.h"
#include "desktop/dockwidget.h"
#include "core/pictomirprogram.h"
#include "core/platform_configuration.h"
#include "desktop/controlprogrambuttonbox.h"
#include "desktop/navigatetaskbuttonbox.h"
#include "core/actionmodel.h"
#include "desktop/loadgamedialog.h"
#include "core/gameprovider.h"
#include "core/pictomirfunction.h"
#include "core/actionmanager.h"
#include "desktop/selectuserdialog.h"
#include "core/pictomirprogram.h"
#include "robot25d/default_plugin.h"
#include "robot25d/robotview.h"
#include "ui_mainwindow.h"
#include "desktop/helperitem.h"
#include "util/svgsimplebutton.h"
#include "robot25d/robotitem.h"
#include "desktop/hintitem.h"
#include "util/aboutdialog.h"

#define MAX_FUNC_WIDTH 300

using namespace Core;

namespace Desktop {

MainWindow::MainWindow(PictomirCommon * pictomir, QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    statusBar()->setVisible(false);
    ui->setupUi(this);
    setStatusBar(0);
    b_started = false;
    b_sizeChanged = true;
    e_currentState = EditingProgram;
    m_pictomir = pictomir;
    init();
}

void MainWindow::copyProgramToClipboard()
{
    const QString txt = m_pictomir->program()->kumirProgram();
    QClipboard * clipboard = QApplication::clipboard();
    clipboard->setText(txt);
}

void MainWindow::saveAsKumirProgram()
{
    const QString programText = m_pictomir->program()->kumirProgram();
    QSettings sett;
    QString lastDir = sett.value("LastDirs/KumirProgram",
                                 QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation)).toString();
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save as Kumir program"),lastDir,"Kumir programs (*.kum)");
    if (!fileName.isEmpty()) {
        sett.setValue("LastDir/KumirProgram", fileName);
        QFile f(fileName);
        if (f.open(QIODevice::WriteOnly|QIODevice::Text)) {
            QTextStream ts(&f);
            ts.setCodec("UTF-16");
            ts.setGenerateByteOrderMark(true);
            ts << programText;
            f.close();
        }
        else {
            QMessageBox::critical(this, tr("Can't save program"),
                                  tr("Can't save kumir program to file\n%1\nAccess denied").arg(fileName));
        }
    }
}

void MainWindow::init()
{
    static const char * css = ""
            "QMenuBar { "
            "   background-color: black;"
            "   border-bottom: 1px solid lightgray;"
            "   font-weight: bold;"
            "   spacing: 16px;"
            "}"
            "QMenuBar::item {"
            "   background: transparent;"
            "   color: lightgray;"
            "   border: 1px solid transparent;"
            "}"
            "QMenuBar::item:hover {"
            "   background: transparent;"
            "   color: green;"
            "   border: 1px solid green;"
            "}"
            "QMenuBar::item:pressed {"
            "   background-color: green;"
            "   color: white;"
            "   border: 1px solid green;"
            "}"
            "QGraphicsView {"
            "   border: 0;"
            "}"
            ;


    setMinimumSize(800, 600);
    menuBar()->setMouseTracking(true);
    setStyleSheet(css);



    createHelpViewer();
    m_selectUserDialog = new SelectUserDialog(this);
    m_loadGameDialog = new LoadGameDialog(m_pictomir->m_gameProvider, this);


    scene = new QGraphicsScene();
    view = new QGraphicsView(scene, this);
    view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setCentralWidget(view);

    QLinearGradient gr(0,1,0,0);
    gr.setCoordinateMode(QGradient::ObjectBoundingMode);

    QColor c1 = QColor(QColor(255,255,255,64));
    QColor c2 = QColor(QColor(255,255,255,0));
    gr.setColorAt(0.0, c1);
    gr.setColorAt(0.5, c1);
    gr.setColorAt(1.0, c2);

    g_background = new QGraphicsRectItem;
    g_background->setPen(Qt::NoPen);
    g_background->setBrush(gr);
//    g_background->setBrush(QColor("red"));
    scene->addItem(g_background);

    QLinearGradient gr2(0,1,0,0);
    gr2.setCoordinateMode(QGradient::ObjectBoundingMode);
    gr2.setColorAt(1.0, QColor(255,255,255,128));
    gr2.setColorAt(0.1, QColor(255,255,255,32));

    g_table = new QGraphicsRectItem;
    g_table->setPen(Qt::NoPen);
    g_table->setBrush(gr2);
    scene->addItem(g_table);


    debugMarkerBlue = new QGraphicsEllipseItem(-3,-3,6,6,0);
    debugMarkerBlue->setBrush(QColor("blue"));
    debugMarkerBlue->setZValue(1000);
//    scene->addItem(debugMarkerBlue);

    debugMarkerRed = new QGraphicsEllipseItem();
    debugMarkerRed->setBrush(QColor("red"));
    debugMarkerRed->setZValue(1000);
//    scene->addItem(debugMarkerRed);

    debugMarkerGreen = new QGraphicsEllipseItem();
    debugMarkerGreen->setBrush(QColor("green"));
    debugMarkerGreen->setZValue(1000);
//    scene->addItem(debugMarkerGreen);


    view->setBackgroundBrush(QBrush(QImage(PlatformConfiguration::resourcesPath()+"/background.png")));
    Q_ASSERT(QFile::exists(PlatformConfiguration::resourcesPath()+"/background.png"));
    robot = m_pictomir->m_currentModuleWidget;
    dock = new DockWidget(0);
    dock->setActionManager(m_pictomir->actionManager());

    programButtonBox = new ControlProgramButtonBox;
    scene->addItem(programButtonBox);
    programButtonBox->setZValue(1000000);

    connect(programButtonBox, SIGNAL(startClicked()), m_pictomir->a_start, SLOT(trigger()));
    connect(programButtonBox, SIGNAL(stopClicked()), m_pictomir->a_stop, SLOT(trigger()));
    connect(programButtonBox, SIGNAL(pauseClicked()), m_pictomir->a_pause, SLOT(trigger()));
    connect(programButtonBox, SIGNAL(debugClicked()), m_pictomir->a_step, SLOT(trigger()));
    connect(programButtonBox, SIGNAL(resetClicked()), m_pictomir->a_resetProgramAndModule, SLOT(trigger()));
    connect(programButtonBox, SIGNAL(fastRunClicked()), m_pictomir->a_runToEnd, SLOT(trigger()));


    connect(ui->actionReset_program_to_initial, SIGNAL(triggered()), m_pictomir->a_resetProgramToInitial, SLOT(trigger()));
    connect(ui->actionStart, SIGNAL(triggered()), m_pictomir->a_start, SLOT(trigger()));
    connect(ui->actionStop, SIGNAL(triggered()), m_pictomir->a_stop, SLOT(trigger()));
    connect(ui->actionRun_to_end, SIGNAL(triggered()), m_pictomir->a_runToEnd, SLOT(trigger()));
    connect(ui->actionStep, SIGNAL(triggered()), m_pictomir->a_step, SLOT(trigger()));
    connect(ui->actionReset, SIGNAL(triggered()), m_pictomir->a_resetProgramAndModule, SLOT(trigger()));
    connect(ui->actionPrevious_task, SIGNAL(triggered()), m_pictomir, SLOT(loadPreviousTask()));
    connect(ui->actionNext_task, SIGNAL(triggered()), m_pictomir, SLOT(loadNextTask()));
    connect(ui->actionCopy_to_clipboard, SIGNAL(triggered()), this, SLOT(copyProgramToClipboard()));
    connect(ui->actionSave_as_Kumir_program, SIGNAL(triggered()), this, SLOT(saveAsKumirProgram()));

    connect(ui->actionAbout, SIGNAL(triggered()), this, SLOT(showAbout()));
    connect(ui->actionContent, SIGNAL(triggered()), this, SLOT(showHelp()));

    navButtonBox = new NavigateTaskButtonBox;

    connect(navButtonBox, SIGNAL(prevTaskClicked()), m_pictomir->a_previousTask, SLOT(trigger()));
    connect(navButtonBox, SIGNAL(nextTaskClicked()), m_pictomir->a_nextTask, SLOT(trigger()));

    connect(m_pictomir, SIGNAL(programStructureChanged()), dock, SLOT(updateVisiblity()));
    connect(m_pictomir, SIGNAL(programStructureChanged()), this, SLOT(updateAlgorhitms()));
    connect(m_pictomir, SIGNAL(highlightRequest(int,int)), this, SLOT(handleHighlightPlainFunctionRequest(int,int)));
    connect(m_pictomir, SIGNAL(taskHintChanged(QString,QByteArray)), this, SLOT(setTaskHint(QString,QByteArray)));
    connect(m_pictomir, SIGNAL(executionError(QList<int>,QList<int>)), this, SLOT(handleExecutionError(QList<int>,QList<int>)));

    scene->addItem(dock);

    scene->addItem(robot);
    scene->addItem(navButtonBox);
    navButtonBox->setZValue(1000000);

    statusLine = new QGraphicsTextItem;
    QFont fnt;
#ifdef Q_OS_WIN32
    fnt.setFamily("Times");
#else
    fnt.setFamily("serif");
#endif
    fnt.setPixelSize(20);
    fnt.setItalic(true);
    statusLine->setFont(fnt);
    statusLine->setDefaultTextColor(QColor("white"));
    statusLine->setZValue(1000001);
    scene->addItem(statusLine);

    an_statusLine = new QPropertyAnimation(statusLine, "opacity");
    an_statusLine->setDuration(1000);

    connect(m_pictomir, SIGNAL(stateChanged(Core::ProgramState)), this, SLOT(setInteractiveMode(Core::ProgramState)));
    connect(m_pictomir, SIGNAL(taskLoaded()), this, SLOT(handleTaskLoaded()));
    connect(m_pictomir, SIGNAL(taskSuccessed()), navButtonBox, SLOT(alertNext()));
    connect(m_pictomir, SIGNAL(information(QString)), this, SLOT(setStatusText(QString)));

    connect(qApp, SIGNAL(aboutToQuit()), this, SLOT(saveSettings()));
    connect(ui->actionQuit, SIGNAL(triggered()), qApp, SLOT(quit()));
    connect(ui->actionSelect_user, SIGNAL(triggered()), this, SLOT(showSelectUserDialog()));
    connect(ui->actionLoad_game, SIGNAL(triggered()), this, SLOT(showLoadGameDialog()));
    connect(ui->actionLow_quality_but_faster,SIGNAL(triggered(bool)), this, SLOT(setLowQualityView(bool)));
    connect(ui->actionFull_screen,SIGNAL(triggered(bool)),this,SLOT(setFullscreenMode(bool)));


    createHelperItem();
}

void MainWindow::createHelperItem()
{
    g_robotDirectionHint = new HelperItem();
    scene->addItem(g_robotDirectionHint);
    g_robotDirectionHint->setVisible(true);

    g_btnTurnLeft = new Util::SvgSimpleButton(
                Core::PlatformConfiguration::resourcesPath()+"/icons/actions/turn_left.svg"
                , g_robotDirectionHint);
    g_btnTurnRight = new Util::SvgSimpleButton(
                Core::PlatformConfiguration::resourcesPath()+"/icons/actions/turn_right.svg"
                , g_robotDirectionHint);
    g_btnTurnLeft->setPos(5,15);
    g_btnTurnRight->setPos(90,15);

    g_robotItem = new QGraphicsPixmapItem(g_robotDirectionHint);
    for (int i=0; i<16; i++) {
        arr_robotPixmaps[i] = QPixmap(Core::PlatformConfiguration::resourcesPath()+QString("/robot_frames/%1.png").arg(i+1));
    };
    setRobotPixmap(0);
    g_robotItem->setPos(43, 3);
    connect(g_btnTurnLeft, SIGNAL(clicked()), this, SLOT(turnRobotLeft()));
    connect(g_btnTurnRight, SIGNAL(clicked()), this, SLOT(turnRobotRight()));
    an_robotHint = new QPropertyAnimation(this, "robotHintState", this);
    an_robotHint->setDuration(300);
    g_robotDirectionHint->setPos(10, 100);
    g_robotDirectionHint->setBorder(QSize(125,60));


    g_taskHint = new HelperItem();
    scene->addItem(g_taskHint);
    g_taskHint->setVisible(true);

    g_btnShowHint = new Util::SvgSimpleButton(Core::PlatformConfiguration::resourcesPath()+"/ui_desktop/hint.svg", g_taskHint);
    connect(g_btnShowHint, SIGNAL(clicked()), this, SLOT(showTaskHint()));
    g_btnShowHint->setPos(40, 6);
    g_taskHint->setPos(10, 170);
    g_taskHint->setBorder(QSize(125,60));

    g_hintWindow = new HintItem;
    scene->addItem(g_hintWindow);
    g_hintWindow->setVisible(false);


}

void MainWindow::showTaskHint()
{
    g_hintWindow->setVisible(true);
}

void MainWindow::setRobotHintState(int v)
{
    setRobotPixmap(v);
}

int MainWindow::robotHintState() const
{
    return i_robotPixmapIndex;
}

void MainWindow::setRobotPixmap(int v)
{
    if (v<0)
        v += 16;
    v = v % 16;
    g_robotItem->setPixmap(arr_robotPixmaps[v]);
    i_robotPixmapIndex = v;
//    qDebug() << "setRobotPixmap("<<v<<")";
}

void MainWindow::turnRobotLeft()
{
    an_robotHint->stop();
    setRobotPixmap(i_robotPixmapIndex/ 4 * 4);
    an_robotHint->setStartValue(robotHintState());
    an_robotHint->setEndValue(robotHintState()+4);
    an_robotHint->start();
}

void MainWindow::turnRobotRight()
{
    an_robotHint->stop();
    setRobotPixmap(i_robotPixmapIndex/ 4 * 4);
    an_robotHint->setStartValue(robotHintState());
    an_robotHint->setEndValue(robotHintState()-4);
    an_robotHint->start();
}


void MainWindow::updateSizes()
{
    view->setSceneRect(0,0,view->width()-20,view->height()-20);
    dock->setRect(QRectF(0,0,view->width()-20,90));
    qreal funcmaxwidth = 0;
    for (int i=0; i<wl_functions.size(); i++) {
        funcmaxwidth = qMax(funcmaxwidth, wl_functions[i]->boundingRect().size().width());
    }
    QSizeF robotSize = view->sceneRect().size() - QSizeF(funcmaxwidth+20,0) - QSizeF(0, 90+30);
    QSizeF robotActualSize = robot->childrenBoundingRect().size();
    qreal xx = (robotSize.width()-robotActualSize.width())/2;
    qreal yy = (robotSize.height()-robotActualSize.height())/2;
//    qreal x = robot->childrenBoundingRect().center().x();
//    qreal y = robot->childrenBoundingRect().center().y();
    qreal x = robot->pos().x();
    qreal y = robot->pos().y();
    debugMarkerRed->setRect(robot->pos().x()-3, robot->pos().y()-3, 6, 6);
    qreal dx = xx-x;
    qreal dy = yy-y;
    robot->moveBy(dx, dy);
    debugMarkerGreen->setRect(robot->pos().x()-3, robot->pos().y()-3, 6, 6);
    qreal fy = 100;
    for (int i=0; i<wl_functions.size(); i++) {
        int dW = funcmaxwidth - wl_functions[i]->boundingRect().width();
        wl_functions[i]->setPos(view->width()-55-funcmaxwidth+(dW), fy);
        fy += wl_functions[i]->boundingRect().height() + 20;
    }
    programButtonBox->setPos(view->width()-250-funcmaxwidth, 100);
    programButtonBox->setOpacity(0.8);
    navButtonBox->setPos(30,view->height()-100);
    updateTextRect();
    g_background->setRect(view->width()-funcmaxwidth-65,
                          100, funcmaxwidth+20,
                          view->height()-120);
    g_table->setRect(0, view->height()-100, view->width()-funcmaxwidth-70, 4);
    g_hintWindow->setRect(5, 95, view->width()-75-funcmaxwidth, view->height()-200);
}

void MainWindow::updateTextRect()
{
    qreal funcmaxwidth = 0;
    for (int i=0; i<wl_functions.size(); i++) {
        funcmaxwidth = qMax(funcmaxwidth, wl_functions[i]->boundingRect().size().width());
    }
    QRectF r(140,view->height()-90,view->width()-funcmaxwidth-210,50);
//    scene->addItem(debugMarkerRed);
//    scene->addItem(debugMarkerBlue);
//    scene->addItem(new QGraphicsRectItem(r));
//    debugMarkerRed->setPos(r.topLeft());
//    debugMarkerRed->setVisible(true);
//    debugMarkerBlue->setPos(r.bottomRight());
//    debugMarkerBlue->setVisible(true);
    statusLine->setPos(r.topLeft());
    statusLine->setTextWidth(r.width());
    qreal leftSpace = (r.width()-QFontMetricsF(statusLine->font()).width(statusLine->toPlainText()))/2.0;
    if (leftSpace>0) {
        statusLine->moveBy(leftSpace, 0);
    }
    if (leftSpace<0) {
        statusLine->moveBy(0, -20);
    }
}

void MainWindow::showSelectUserDialog()
{
    if (m_selectUserDialog->exec()==QDialog::Accepted) {
        m_pictomir->loadUserState(m_selectUserDialog->currentUserRoot());
    }
}

void MainWindow::start()
{
    loadSettings();
    b_started = true;
//    if (!qApp->arguments().contains("-t") && !qApp->arguments().contains("--teacher")) {
//        m_selectUserDialog->exec();
//    }
    m_pictomir->loadUserState(m_selectUserDialog->currentUserRoot());
    updateSizes();
}

void MainWindow::updateAlgorhitms()
{
    for (int i=0; i<wl_functions.size(); i++) {
        scene->removeItem(wl_functions[i]);
        wl_functions[i]->deleteLater();
    }
    wl_functions.clear();
    Core::PictomirProgram *p = m_pictomir->program();
    for (int i=0; i<p->functionsCount(); i++) {
        Core::FunctionWidgetInterface * f = new Desktop::FunctionWidget(p->function(i));
        f->connectSignalActivationChanged(m_pictomir->a_reset, SIGNAL(triggered()));
        scene->addItem(f);
        wl_functions << f;
    }
    updateSizes();
}



void MainWindow::showEvent(QShowEvent *event)
{
    Q_UNUSED ( event );
    if (!b_started)
        start();

}

void MainWindow::clearStatusLine()
{
//    m_statusLine->setText("");
}

void MainWindow::loadSettings()
{
    QSettings s;
    QRect geom = s.value("Window/Geometry",QRect(0,0,700,700)).toRect();
    resize(geom.size());
    move(geom.topLeft());
    setFullscreenMode(s.value("Window/FullScreen",false).toBool());
    setLowQualityView(s.value("Window/LowQuality",false).toBool());
    qApp->setProperty("disableSound", s.value("Application/DisableSound",false));
}

void MainWindow::saveSettings()
{
    QSettings s;
    s.setValue("Window/Geometry",QRect(pos(), size()));
    s.setValue("Application/DisableSound",qApp->property("disableSound"));
    s.setValue("Window/FullScreen", isFullScreen());
    s.setValue("Window/LowQuality", !view->renderHints().testFlag(QPainter::Antialiasing));
}







MainWindow::~MainWindow()
{
    delete ui;
    delete m_loadGameDialog;
    delete m_selectUserDialog;

    delete dock;
    delete programButtonBox;
    delete navButtonBox;
    delete robot;

    foreach (Core::FunctionWidgetInterface * wi, wl_functions) {
        delete wi;
    }
    scene->clear();

    delete scene;
    delete view;
    delete an_statusLine;

}


void MainWindow::setFullscreenMode(bool v)
{
    if (sender()!=ui->actionFull_screen)
        ui->actionFull_screen->setChecked(v);
    if (v)
        showFullScreen();
    else
        showNormal();

}

void MainWindow::setLowQualityView(bool v)
{
    if (sender()!=ui->actionLow_quality_but_faster)
        ui->actionLow_quality_but_faster->setChecked(v);
    view->setRenderHint(QPainter::Antialiasing, !v);
    view->setRenderHint(QPainter::TextAntialiasing, !v);
    view->update();
}

void MainWindow::showAbout()
{
    about->exec();
}


void MainWindow::showHelp()
{
    QProcess::startDetached(QCoreApplication::applicationFilePath(),
                            QStringList() << "--ui=navigator" << QString("local://%1/manual.pms.html").arg(qApp->property("language").toString()));
}


void MainWindow::createHelpViewer()
{
    const QString lang = qApp->property("language").toString();
    about = new Util::AboutDialog(Core::PlatformConfiguration::resourcesPath()+"/pictomir_icon.png",
                                  Core::PlatformConfiguration::resourcesPath()+"/about_pictomir_"+lang+".html", this);
}




void MainWindow::resizeEvent(QResizeEvent *event)
{
    Q_UNUSED(event);
    QMainWindow::resizeEvent(event);
    view->setSceneRect(0,0,view->size().width(),view->size().height());
    updateSizes();

}


void MainWindow::setTaskTitle(const QString &txt)
{
    setWindowTitle(txt + " - "+tr("Pictomir"));
}

void MainWindow::setStatusText(const QString &txt)
{
    if (txt.isEmpty()) {
        an_statusLine->setEndValue(0.0);
        an_statusLine->start();
    }
    else {
        statusLine->setPlainText(txt);
        updateTextRect();
        an_statusLine->setStartValue(0.0);
        an_statusLine->setEndValue(1.0);
        an_statusLine->start();
    }
}



void MainWindow::setDefaultFocus()
{
//    w_dock->setFocus();
}

void MainWindow::setInteractiveMode(ProgramState state)
{
    ui->actionReset_program_to_initial->setEnabled(state==EditingProgram||state==AnalisysProgram);
    ui->actionSelect_user->setEnabled(state==EditingProgram||state==AnalisysProgram);
    ui->actionLoad_game->setEnabled(state==EditingProgram||state==AnalisysProgram);
    ui->actionRun_to_end->setEnabled(true);
    ui->actionStart->setEnabled(state!=RunningProgram);
    ui->actionStop->setEnabled(state==RunningProgram||state==PauseProgram);
    ui->actionStep->setEnabled(state==EditingProgram||state==AnalisysProgram||state==PauseProgram);
    ui->actionReset->setEnabled(state==AnalisysProgram);
    programButtonBox->setProgramState(state);
    if (state==EditingProgram) {
        setStatusText("");
    }
    if (e_currentState==EditingProgram || e_currentState==AnalisysProgram) {
        if (state==RunningProgram) {
            for (int i=0; i<wl_functions.count(); i++) {
                wl_functions[i]->lock();
            }
            m_pictomir->actionManager()->lock();
            navButtonBox->setOpacity(0.2);
            navButtonBox->setEnabled(false);
        }
    }
    if ( (e_currentState==RunningProgram||e_currentState==PauseProgram) && state==AnalisysProgram) {

        for (int i=0; i<wl_functions.count(); i++) {
            FunctionWidgetInterface *wi = wl_functions[i];
            wi->unlock();
            if (m_pictomir->s_errorText.isEmpty()) {
                wi->unhighlight();
            }
        }
        m_pictomir->actionManager()->unlock();
        navButtonBox->setOpacity(1.0);
        navButtonBox->setEnabled(true);
    }

    if (e_currentState==AnalisysProgram) {
        for (int i=0; i<wl_functions.count(); i++) {
            FunctionWidgetInterface *wi = wl_functions[i];
            wi->unhighlight();
        }
        navButtonBox->unalertNext();
    }
    e_currentState = state;
}



void MainWindow::handleTaskLoaded()
{
    setInteractiveMode(EditingProgram);
    navButtonBox->unalertNext();
    ui->actionPrevious_task->setEnabled(m_pictomir->isPrevTaskAvailable());
    ui->actionNext_task->setEnabled(m_pictomir->isNextTaskAvailable());
    if (m_pictomir->isPrevTaskAvailable()) {
        navButtonBox->enablePrev();
    }
    else {
        navButtonBox->disablePrev();
    }
    if (m_pictomir->isNextTaskAvailable()) {
        navButtonBox->enableNext();
    }
    else {
        navButtonBox->disableNext();
    }
    setTaskTitle(m_pictomir->currentTaskTitle());
    setStatusText("");

//    if (qApp->arguments().contains("-t")) {
//        const QString reference = m_pictomir->currentTaskEnvironment();
//        m_envControl->setSource(reference);
//        if (reference.trimmed()=="#" || reference.trimmed().isEmpty()) {
//            m_editorWidget->setEnabled(true);
//        }
//        else {
//            m_editorWidget->setEnabled(false);
//        }
//    }
//    setControlsVisible(m_pictomir->program()->functionsCount()>0);
//    setDefaultFocus();
}

void MainWindow::setTaskHint(const QString &mimetype, const QByteArray &data)
{
    Q_UNUSED(data);
    g_hintWindow->setHint(mimetype, data);
    g_taskHint->setVisible(!mimetype.isEmpty());
}



void MainWindow::startPauseProgram()
{
    if (e_currentState==RunningProgram)
        m_pictomir->a_pause->trigger();
    else
        m_pictomir->a_start->trigger();
}

void MainWindow::handleUserProgramTouch()
{
    if (e_currentState==AnalisysProgram)
        m_pictomir->a_reset->trigger();
}

void MainWindow::handleHighlightPlainFunctionRequest(int funcNo, int cellNo)
{
    Q_ASSERT ( funcNo >= 0 );
    Q_ASSERT ( funcNo < wl_functions.size() );
    FunctionWidgetInterface *wi = wl_functions[funcNo];
    wi->highlightPlainCell(cellNo);
}

void MainWindow::handleExecutionError(const QList<int> &fns, const QList<int> &cis)
{
    Q_ASSERT(fns.size()==cis.size());
    for (int i=0; i<fns.size(); i++) {
        int funcNo = fns[i];
        int cellIndex = cis[i];
        Q_ASSERT ( funcNo >= 0 );
        Q_ASSERT ( funcNo < wl_functions.size() );
        Q_CHECK_PTR( wl_functions.at(funcNo) );
        FunctionWidgetInterface *wi = wl_functions[funcNo];
        wi->highlightErrorCell(cellIndex);
    }
}

void MainWindow::showLoadGameDialog()
{
    m_pictomir->prepareLoadGame();
    m_loadGameDialog->init(m_pictomir->m_gameProvider->gamesList(),
                           m_pictomir->m_gameProvider->lastIndex());
    LoadGameDialog *dialog = m_loadGameDialog;
    if (dialog->exec()==QDialog::Accepted) {
        int selectedIndex = dialog->selectedIndex();
        const QString fileName = m_pictomir->m_gameProvider->fileNameByIndex(selectedIndex);
        m_pictomir->loadGameFromFile(fileName);
    }
}



void MainWindow::handleTaskSuccessed()
{
//    m_navButtonBox->alertNext();
}

}
