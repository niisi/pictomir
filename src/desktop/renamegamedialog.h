#ifndef RENAMEGAMEDIALOG_H
#define RENAMEGAMEDIALOG_H

#include <QtGui>

namespace Ui {
    class RenameGameDialog;
}
namespace Desktop {


class RenameGameDialog : public QDialog
{
    Q_OBJECT

public:
    explicit RenameGameDialog(QWidget *parent = 0);
    void init(const QStringList &names, const QString &currentName);
    ~RenameGameDialog();
    QString newName() const;

private slots:
    void handleNameEdited(const QString &txt);

private:
    Ui::RenameGameDialog *ui;
    QStringList m_names;
};

}
#endif // RENAMEGAMEDIALOG_H
