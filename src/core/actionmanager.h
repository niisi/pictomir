#ifndef ACTIONMANAGER_H
#define ACTIONMANAGER_H

#include <QtCore>
#include "core/actiontype.h"
#include "schema/sch_command.h"


namespace Robot25D
{
    class Plugin;
}

namespace Core {

class ActionModel;
class ActionView;

class ActionManager : public QObject
{
    Q_OBJECT;
    Q_PROPERTY(bool locked READ isLocked WRITE setLocked);
public:
    explicit ActionManager(QObject *parent = 0);
    bool isLocked() const;

    ActionModel * activeModel() const;
    ActionView * activeView();
    ActionModel * modelById(int id) const;
    ActionModel * modelByShortcut(const QString &shortcut) const;
    ActionModel * modelByStandartCommand(Schema::Command) const;
    QList<ActionModel*> models(bool onlyEnabled = true,
                               ActionType at=ActionNone,
                               ActionCategory=Any);
    void createActions(const QList<Robot25D::Plugin*> modules);
    void setEnabledActionsList(Robot25D::Plugin *selectedModule,
                                      bool plain, bool cond, bool rep, int funcCnt);

    void setActiveView(ActionView *view, bool notifyView);

    ~ActionManager();

public slots:
    void add(ActionModel * model);
    inline void lock() { setLocked(true); }
    inline void unlock() { setLocked(false); }
    void setLocked(bool v);

private:
    class ActionManagerPrivate * d;

};

} // namespace Core

#endif // ACTIONMANAGER_H
