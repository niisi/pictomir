#ifndef SVGRENDERFARM_H
#define SVGRENDERFARM_H

#include <QtCore>
#include <QtSvg>

namespace Util {

class SvgRenderFarm : public QObject
{
    public:
        SvgRenderFarm();
        static SvgRenderFarm *instance();
        QSvgRenderer *createSharedRendered(const QString &actionName, const QByteArray &data);
        QSvgRenderer *createSharedRendered(const QString &actionName, const QString &fileName);
        QSvgRenderer *createSharedRenderedForFunction(const QString &actionName);
        QSvgRenderer *getSharedRendered(const QString &actionName);
        QByteArray getSvgData(const QString &actionName) const;
        static void clear();
        ~SvgRenderFarm();
    private:
        static SvgRenderFarm *m_instance;
        QMap<QString,QSvgRenderer*> m_map;
        QMap<QString,QByteArray> m_data;
};

}

#endif // SVGRENDERFARM_H
