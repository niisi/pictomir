#ifndef ENUMS_H
#define ENUMS_H

#include <QtCore>

namespace Core {

enum PictomirUiType {
    DesktopUi,
    MobileUi,
    WebKitUi,
    CreatorUi
};

enum ProgramState {
    EditingProgram,
    RunningProgram,
    PauseProgram,
    AnalisysProgram
};

enum FunctionModifier {
    PlainFunction,
    RepeatFunction,
    ConditionFunction
};


Q_DECLARE_FLAGS(FunctionModifiers, FunctionModifier);
};

Q_DECLARE_OPERATORS_FOR_FLAGS(Core::FunctionModifiers);
Q_DECLARE_METATYPE(Core::ProgramState);

#endif // ENUMS_H

