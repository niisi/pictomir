#include "sch_task.h"
#include "core/platform_configuration.h"
#include "util/fp.h"


namespace Schema {

extern bool parceJSON(const QScriptValue &value, Task &task)
{
    if (value.property("title").isString())
        task.title = value.property("title").toString();
    QScriptValue env = value.property("environment");
    bool envOk = false;
    if (env.isValid()) {
        envOk = parceJSON(env, task.environment);
    }
    QScriptValue hint = value.property("hint");
    if (hint.isObject()) {
        bool compressed = false;
        QString encoding;
        QString mimetype;
        QString strData;
        if (hint.property("compressed").isBool()) {
            compressed = hint.property("compressed").toBool();
        }
        if (hint.property("encoding").isString()) {
            encoding = hint.property("encoding").toString().toLower().trimmed();
        }
        if (hint.property("mimetype").isString()) {
            mimetype = hint.property("mimetype").toString().toLower().trimmed();
        }
        if (hint.property("data").isString()) {
            strData = hint.property("data").toString();
        }
        if (!strData.isEmpty() && !mimetype.isEmpty()) {
            QByteArray decoded;
            if (encoding=="base64") {
                decoded = QByteArray::fromBase64(strData.toAscii());
            }
            else if (encoding=="hex") {
                decoded = QByteArray::fromHex(strData.toAscii());
            }
            else {
                decoded = strData.toLocal8Bit();
                compressed = false;
            }
            if (compressed) {
                task.hintData = qUncompress(decoded);
            }
            else {
                task.hintData = decoded;
            }
            QCryptographicHash hash(QCryptographicHash::Md5);
            hash.addData(task.hintData);
            qDebug() << "Load binary data of size " << task.hintData.size() << " with MD5: " << hash.result().toHex();
            task.hintMimeType = mimetype;
        }
    }

    parceJSON(value.property("program"), task.program);
    return envOk;
}

extern bool parceAGKXML(const QDomNode & taskNode, Task &task, bool userCurrentProgram)
{
    const QString envRoot = Core::PlatformConfiguration::envPath();
    QString envPath = taskNode.toElement().attribute("env");
    QString envData;
    task.title = taskNode.toElement().attribute("title").simplified();

    if (envPath.trimmed()!="#") {
        QFile f(envRoot+"/"+envPath);
        if (!f.exists()) {
            return false;
        }
        if (!f.open(QIODevice::ReadOnly)) {
            return false;
        }
        envData = QString::fromLocal8Bit(f.readAll());
        f.close();
    }
    else {
        QDomNodeList envs = taskNode.toElement().elementsByTagName("env");
        if (envs.isEmpty()) {
            return false;
        }
        QDomNode env = envs.at(0);
        if (env.childNodes().count()==0) {
            return false;
        }
        QDomText txt = env.firstChild().toText();
        envData = txt.data();
    }
    if (!parceKumirFil(envData, task.environment)) {
        return false;
    }
    QList<QDomNode> algSpecs;
    QDomNode initialContent;
    QDomNode currentContent;
    Schema::Program initialProgram;
    Schema::Program currentProgram;
    bool hasCurrent = false;
    for (int i=0; i<taskNode.childNodes().count(); i++) {
        QDomNode child = taskNode.childNodes().at(i);
        if (child.isElement() && child.nodeName()=="alg") {
            algSpecs << child;
        }
        if (child.isElement() && child.nodeName()=="program") {
            const QString type = child.toElement().attribute("type");
            if (type.toLower().trimmed()=="initial") {
                initialContent = child;
            }
            else if (type.toLower().trimmed()=="saved") {
                currentContent = child;
                hasCurrent = true;
            }
        }
    }
    Schema::parceAGKXML(algSpecs, initialContent, initialProgram);
    if (hasCurrent) {
        hasCurrent = Schema::parceAGKXML(algSpecs, currentContent, currentProgram);
    }
    task.program = (userCurrentProgram && hasCurrent) ? currentProgram : initialProgram;
    return true;
}

extern QString addLeadingTab(const QString &t)
{
    return "\t"+t;
}

QString screenString(const QByteArray &data)
{
    QString s = QString::fromLocal8Bit(data);
    s.replace("\\", "\\\\");
    s.replace("\n", "\\n");
    s.replace("\"", "\\\"");
    return s;
}

QString encodeData(const QByteArray &data)
{
    QCryptographicHash hash(QCryptographicHash::Md5);
    hash.addData(data);
    qDebug() << "Store binary data of size " << data.size() << " with MD5: " << hash.result().toHex();
    QByteArray stage1 = qCompress(data, 9);
    QByteArray stage2 = stage1.toBase64();
    return QString::fromAscii(stage2);
}

extern QString generateAGKXML(const Task &task)
{
    QString result = "<task title=\""+task.title+"\" "+
            "env=\"$$$$$/%%%%%.fil\">\n";
    for (int i=0; i<task.program.size(); i++) {
        result += QString::fromAscii("\t<alg width=\"%1\" height=\"%2\" repeat=\"%3\" condition=\"%4\"/>\n")
                .arg(task.program[i].size.width())
                .arg(task.program[i].size.height())
                .arg(task.program[i].repeater? "true" : "false")
                .arg(task.program[i].condition? "true" : "false");
    }
    result += "\t<program type=\"initial\">\n";
    for (int i=0; i<task.program.size(); i++) {
        QStringList alg = generateAGKXML(task.program[i]).split("\n");
        for (int l=0; l<alg.size(); l++) {
            result += "\t" + alg[l]+"\n";
        }
    }
    result += "\t</program>\n";
    result += "</task>\n";
    return result;
}

extern QString generateJSON(const Task &task)
{
    const QString template0 = "{\n\t\"title\": %1,\n\t\"hint\": %2,\n\t\"environment\": %3,\n\t\"program\": %4 \n}";
    const QString title = "\""+task.title+"\"";
    QString hint = "null";
    if (!task.hintData.isEmpty() && !task.hintMimeType.isEmpty()) {
        QString mime = task.hintMimeType;
        QString d = mime.startsWith("text/")? screenString(task.hintData) : encodeData(task.hintData);
        bool compressed = !mime.startsWith("text/");
        const QString encoding = mime.startsWith("text/")? "notencoded" : "base64";
        hint = QString::fromAscii("{ \"mimetype\": \"%1\", \"encoding\": \"%4\", \"compressed\": %2, \"data\": \"%3\" }")
                .arg(mime)
                .arg(compressed? "true" : "false")
                .arg(d)
                .arg(encoding);

    }
    QStringList envLines = generateJSON(task.environment).split("\n");
    for (int j=0; j<envLines.size(); j++) {
        envLines[j] = "\t"+envLines[j];
    }
    const QString env = envLines.join("\n").trimmed();
    QStringList programLines = generateJSON(task.program).split("\n");
    for (int j=0; j<programLines.size(); j++) {
        programLines[j] = "\t"+programLines[j];
    }
    const QString program = programLines.join("\n").trimmed();
    const QString data = template0
            .arg(title)
            .arg(hint)
            .arg(env)
            .arg(program);
    return data;
}


static void generateDescriptionHtml(quint16 index, const Task &task, const QString &resourcesDirName);

extern QString generateKumirCourse(quint16 index, const Task &task, const QString &resourcesDirName)
{
    const QString template0 = QString::fromUtf8(""
                                                "<T xml:id=\"%4\" xmlns:xml=\"http://www.w3.org/XML/1998/namespace\" xml:name=\"%2\">\n"
                                                "<TITLE>%2</TITLE>\n"
                                                "<DESC>%3/%1.html</DESC>\n"
                                                "<CS>Кумир</CS>\n"
                                                "<ISP ispname=\"Вертун\">\n"
                                                "    <ENV>%3/%1.env.json</ENV>\n"
                                                "</ISP>\n"
                                                "<PROGRAM>%3/%1.kum</PROGRAM>\n"
                                                "<READY>false</READY>\n"
                                                "<MARK>0</MARK>\n"
                                                "</T>\n"
                                                "");

    const int slashPos = resourcesDirName.lastIndexOf("/");
    const QString relativeResourcesDirName = resourcesDirName.mid(slashPos + 1);
    const QString data = template0.arg(index).arg(task.title).arg(relativeResourcesDirName).arg(index + 1u);

    if (!QDir::root().exists(resourcesDirName))
        QDir::root().mkpath(resourcesDirName);

    const QString kumirData = generateKumProgram(task.program);
    const QString envData = generateJSON(task.environment);

    QFile kumirFile(QString("%1/%2.kum").arg(resourcesDirName).arg(index));
    kumirFile.open(QIODevice::WriteOnly);
    QTextStream kumirStream(&kumirFile);
    kumirStream.setCodec("UTF-8");
    kumirStream.setGenerateByteOrderMark(true);
    kumirStream << kumirData;
    kumirFile.close();

    QFile envFile(QString("%1/%2.env.json").arg(resourcesDirName).arg(index));
    envFile.open(QIODevice::WriteOnly|QIODevice::Text);
    QTextStream envStream(&envFile);
    envStream.setCodec("UTF-8");
    envStream << envData;
    envFile.close();

    generateDescriptionHtml(index, task, resourcesDirName);

    return data;
}

static void generateDescriptionHtml(quint16 index, const Task &task, const QString &resourcesDirName)
{
    QFile f(QString("%1/%2.html").arg(resourcesDirName).arg(index));
    f.open(QIODevice::WriteOnly|QIODevice::Text);
    QTextStream ts(&f);
    ts.setCodec("UTF-8");
    ts << QString::fromUtf8(
              "<html>\n"
              "    <head><title>Задание&nbsp;%1</title></head>\n"
              "    <body>\n"
              "        <p>%2</p>\n"
              ).arg(index + 1u).arg(task.title);
    if (task.hintData.length() > 0) {
        ts << QString::fromUtf8(
                  "<p><span style=\"font-weight: bold;\">Подсказка: </span>\n"
                  );
        if (task.hintMimeType.startsWith("text/")) {
            ts << QString::fromUtf8(task.hintData) + "\n";
        }
        else {
            ts << QString("<br><img src=\"%1.png\">\n").arg(index);
            QFile pngFile(QString("%1/%2.png").arg(resourcesDirName).arg(index));
            pngFile.open(QIODevice::WriteOnly);
            pngFile.write(task.hintData);
            pngFile.close();
        }
        ts << "        </p>\n";
    }
    ts << "    </body>\n"
          "</html>\n";
    f.close();
}

extern bool isEqual(const Task &a, const Task &b)
{
    if (a.title.simplified()!=b.title.simplified())
        return false;
    if (a.hintMimeType.simplified()!=b.hintMimeType.simplified())
        return false;
    if (a.hintData.size()!=b.hintData.size())
        return false;
    if (!isEqual(a.environment, b.environment))
        return false;
    if (!isEqual(a.program, b.program))
        return false;
    for (int i=0; i<a.hintData.size(); i++) {
        char aa = a.hintData[i];
        char bb = b.hintData[i];
        if (aa!=bb)
            return false;
    }
    return true;
}

} // namespace Schema
