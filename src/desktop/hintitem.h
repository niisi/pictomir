#ifndef HINTITEM_H
#define HINTITEM_H

#include <QtGui>
#include <QtSvg>

namespace Util {
    class SvgSimpleButton;
}

namespace Desktop {

class HintItem : public QObject, public QGraphicsRectItem
{
    Q_OBJECT;
public:
    HintItem(QGraphicsItem * parent = 0);
    void setRect(qreal x, qreal y, qreal w, qreal h);
    void setRect(const QRectF &rect);
    void setHint(const QString &mimeType, const QByteArray &data);
    ~HintItem();
public slots:
    void closeHintWindow();
signals:
    void closed();
private:
    enum HintType { None, Text, Image, Drawing } e_hintType;
    void updateBackground(const QSizeF &size);
    void updateImageSize(const QSizeF &size);
    void updateDrawingSize(const QSizeF &size);
    void updateTextSize(const QSizeF &size);
    QGraphicsTextItem * g_text;
    QGraphicsPixmapItem * g_image;
    QGraphicsPathItem * g_background;
    Util::SvgSimpleButton * g_closeButton;
    QSvgRenderer * m_renderer;
    QRectF m_rect;
};

} // namespace Desktop

#endif // HINTITEM_H
