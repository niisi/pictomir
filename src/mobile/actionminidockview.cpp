#include "mobile/actionminidockview.h"
#include "core/actionmodel.h"

using namespace Core;

namespace Mobile {

ActionMiniDockView::ActionMiniDockView(ActionModel *model, QGraphicsItem *parent)
    : QGraphicsSvgItem(parent)
    , m_model(model)
{
    an_active = new QPropertyAnimation(this, "pulse", this);
    an_active->setStartValue(0.0);
    an_active->setKeyValueAt(0.5, 1.0);
    an_active->setEndValue(0.0);
    an_active->setDuration(800);
    an_active->setLoopCount(-1);

    an_stopActive = new QPropertyAnimation(this, "pulse", this);
    an_stopActive->setEndValue(0.0);
    an_stopActive->setDuration(300);
    r_pulse = 0.0;

    setSharedRenderer(m_model->m_renderer);
}

void ActionMiniDockView::setPulse(qreal value)
{
    r_pulse = value;
    qreal scaleFactor = 1.0 - 0.3 * r_pulse;
    setTransformOriginPoint(boundingRect().center());
    setScale(scaleFactor);
//    qreal angle = (1.0-cos(r_pulse*3.14159)) * 10;
//    setRotation(angle);
    update();
}


int ActionMiniDockView::modelId() const
{
    return m_model->modelId();
}

bool ActionMiniDockView::isEnabled() const
{
    return m_model->isEnabled();
}

QSize ActionMiniDockView::originalSize() const
{
    return m_model->m_renderer->defaultSize();
}

QSize ActionMiniDockView::currentSize() const
{
    return boundingRect().size().toSize();
}

void ActionMiniDockView::setActive(bool v)
{
    if (b_active && !v) {
        an_active->stop();
        an_stopActive->setStartValue(an_active->currentValue());
        an_stopActive->start();
    }
    b_active = v;
    if (b_active) {
        an_active->start();
    }
}
}
