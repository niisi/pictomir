#include "audioplayer.h"

AudioPlayer::AudioPlayer(QObject *parent):
        QObject(parent)
{
    p_output = new Phonon::AudioOutput(Phonon::NotificationCategory, this);
    p_object = new Phonon::MediaObject(this);
    Phonon::createPath(p_object, p_output);
    
	connect (p_object, SIGNAL(stateChanged(Phonon::State,Phonon::State)),
             this, SLOT(handleStateChanged(Phonon::State,Phonon::State)));

	connect (p_object, SIGNAL(finished()), 
		this, SLOT(handleFinished()) );

    QString suffix;
#ifndef Q_OS_LINUX
    suffix = ".mp3";
#else
    suffix = ".ogg";
#endif
    QString soundsPath = QCoreApplication::applicationDirPath()+
                         "/Pictomir/Sounds/";
    QDir snds(soundsPath);
    QStringList entries = snds.entryList(QStringList() << "*"+suffix);
    for (int i=0; i<entries.count(); i++) {
        QString entry = entries[i];
        QString path = snds.absoluteFilePath(entry);
        Phonon::MediaSource *source = new Phonon::MediaSource(path);
        QString name = entry.left(entry.length()-4);
        mp_sources[name] = source;
    }
}

void AudioPlayer::play(const QString &soundName)
{
    if (mp_sources.contains(soundName)) {
        p_object->setCurrentSource(*(mp_sources[soundName]));
        p_object->play();
    }
    else {
        qDebug() << "No sound: " << soundName;
        emit playingFinished();
    }
}

void AudioPlayer::handleStateChanged(Phonon::State state, Phonon::State)
{
	Q_UNUSED(state);
//	qDebug() << "Phonon state changed;";
//    if (state==Phonon::StoppedState) {
//        qDebug() << "Playing finished.";
//    }
//    if (state==Phonon::ErrorState) {
//        qDebug() << "Phonon error";
//    }
}

void AudioPlayer::handleFinished()
{
	qDebug() << "Phonon finished";
	emit playingFinished();
}
