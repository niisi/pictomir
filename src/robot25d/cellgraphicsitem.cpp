#include "cellgraphicsitem.h"
#include "robotview.h"
#include "robotcell.h"
#include "robotitem.h"
#include "util.h"

#define NO_WALL_OPACITY 0.5
#define WALL_OPACITY 0.8

namespace Robot25D {

CellGraphicsItem::CellGraphicsItem(const QPolygonF &poly,
                                   const QPolygonF &polySouth,
                                   const QPolygonF &polyEast,
                                   const QPolygonF &polyNorth,
                                   const QPolygonF &polyWest,
                                   bool editable,
                                   int cellX,
                                   int cellY,
                                   RobotView *view,
                                   QGraphicsItem *parent)
                                       : QGraphicsPolygonItem(poly, parent)
                                       , p_south(polySouth)
                                       , p_east(polyEast)
                                       , p_north(polyNorth)
                                       , p_west(polyWest)
                                       , b_editable(editable)
                                       , i_cellX(cellX)
                                       , i_cellY(cellY)
                                       , m_view(view)
                                       , b_hoveredCell(false)
                                       , b_pressed(false)
{
    if (i_cellX<0 || i_cellY<0)
        b_editable = false;
    if (i_cellX>=m_view->m_field[0].size())
        b_editable = false;
    if (i_cellY>=m_view->m_field.size())
        b_editable = false;
    setAcceptHoverEvents(b_editable);

}

void CellGraphicsItem::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
    RobotView::EditMode mode = m_view->editMode();
    b_hoveredCell = false;

    if (mode==RobotView::Paint || mode==RobotView::Points || mode==RobotView::Robot) {
        b_hoveredCell = true;
        scene()->update(scene()->sceneRect());
    }
    else if (mode==RobotView::Walls) {
        RobotCell cell = m_view->m_field[i_cellY][i_cellX];

        if (cell.wallDownItem) {
            cell.wallDownItem->setOpacity(1.0);
            cell.wallDownItem->setVisible(cell.wallDown);
        }
        if (cell.wallLeftItem) {
            cell.wallLeftItem->setOpacity(1.0);
            cell.wallLeftItem->setVisible(cell.wallLeft);
        }
        if (cell.wallRightItem) {
            cell.wallRightItem->setOpacity(1.0);
            cell.wallRightItem->setVisible(cell.wallRight);
        }
        if (cell.wallUpItem) {
            cell.wallUpItem->setOpacity(1.0);
            cell.wallUpItem->setVisible(cell.wallUp);
        }

        int w = whichWall(event->pos());
        if (w==UP_WALL) {
            if (i_cellY>0) {
                cell.wallUpItem->setVisible(true);
                cell.wallUpItem->setOpacity(cell.wallUp? WALL_OPACITY : NO_WALL_OPACITY);
            }
        }
        if (w==DOWN_WALL) {
            if (i_cellY<m_view->m_field.size()-1) {
                cell.wallDownItem->setVisible(true);
                cell.wallDownItem->setOpacity(cell.wallDown? WALL_OPACITY : NO_WALL_OPACITY);
            }
        }
        if (w==LEFT_WALL) {
            if (i_cellX>0) {
                cell.wallLeftItem->setVisible(true);
                cell.wallLeftItem->setOpacity(cell.wallLeft? WALL_OPACITY : NO_WALL_OPACITY);
            }
        }
        if (w==RIGHT_WALL) {
            if (i_cellX<m_view->m_field[0].size()-1) {
                cell.wallRightItem->setVisible(true);
                cell.wallRightItem->setOpacity(cell.wallRight? WALL_OPACITY : NO_WALL_OPACITY);
            }
        }
    }
}

void CellGraphicsItem::hoverLeaveEvent(QGraphicsSceneHoverEvent *)
{
    b_hoveredCell = false;

    RobotCell cell = m_view->m_field[i_cellY][i_cellX];
    if (cell.wallDownItem) {
        cell.wallDownItem->setOpacity(1.0);
        cell.wallDownItem->setVisible(cell.wallDown);
    }
    if (cell.wallLeftItem) {
        cell.wallLeftItem->setOpacity(1.0);
        cell.wallLeftItem->setVisible(cell.wallLeft);
    }
    if (cell.wallRightItem) {
        cell.wallRightItem->setOpacity(1.0);
        cell.wallRightItem->setVisible(cell.wallRight);
    }
    if (cell.wallUpItem) {
        cell.wallUpItem->setOpacity(1.0);
        cell.wallUpItem->setVisible(cell.wallUp);
    }

    scene()->update(scene()->sceneRect());
    b_pressed = false;
}

void CellGraphicsItem::hoverMoveEvent(QGraphicsSceneHoverEvent *event)
{
    RobotView::EditMode mode = m_view->editMode();
    if (mode==RobotView::Walls) {
        RobotCell cell = m_view->m_field[i_cellY][i_cellX];
        if (cell.wallDownItem) {
            cell.wallDownItem->setOpacity(1.0);
            cell.wallDownItem->setVisible(cell.wallDown);
        }
        if (cell.wallLeftItem) {
            cell.wallLeftItem->setOpacity(1.0);
            cell.wallLeftItem->setVisible(cell.wallLeft);
        }
        if (cell.wallRightItem) {
            cell.wallRightItem->setOpacity(1.0);
            cell.wallRightItem->setVisible(cell.wallRight);
        }
        if (cell.wallUpItem) {
            cell.wallUpItem->setOpacity(1.0);
            cell.wallUpItem->setVisible(cell.wallUp);
        }

        int w = whichWall(event->pos());
        if (w==UP_WALL) {
            if (i_cellY>0) {
                cell.wallUpItem->setVisible(true);
                cell.wallUpItem->setOpacity(cell.wallUp? WALL_OPACITY : NO_WALL_OPACITY);
            }
        }
        if (w==DOWN_WALL) {
            if (i_cellY<m_view->m_field.size()-1) {
                cell.wallDownItem->setVisible(true);
                cell.wallDownItem->setOpacity(cell.wallDown? WALL_OPACITY : NO_WALL_OPACITY);
            }
        }
        if (w==LEFT_WALL) {
            if (i_cellX>0) {
                cell.wallLeftItem->setVisible(true);
                cell.wallLeftItem->setOpacity(cell.wallLeft? WALL_OPACITY : NO_WALL_OPACITY);
            }
        }
        if (w==RIGHT_WALL) {
            if (i_cellX<m_view->m_field[0].size()-1) {
                cell.wallRightItem->setVisible(true);
                cell.wallRightItem->setOpacity(cell.wallRight? WALL_OPACITY : NO_WALL_OPACITY);
            }
        }

    }
}

void CellGraphicsItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QGraphicsPolygonItem::paint(painter, option, widget);
    if (b_hoveredCell) {
        painter->save();
        painter->setBrush(QColor(0,0,0,128));
        painter->drawPolygon(polygon());
        painter->restore();
    }
//    if (i_hoveredWall) {
//        painter->save();
//        painter->setBrush(QColor(0,0,0,128));
//        if (i_hoveredWall==DOWN_WALL) {
//            painter->drawPolygon(p_south);
//        }
//        else if (i_hoveredWall==RIGHT_WALL) {
//            painter->drawPolygon(p_east);
//        }
//        else if (i_hoveredWall==UP_WALL) {
//            painter->drawPolygon(p_north);
//        }
//        else if (i_hoveredWall==LEFT_WALL) {
//            painter->drawPolygon(p_west);
//        }
//        painter->restore();
//    }
}

int CellGraphicsItem::whichWall(const QPointF &p) const
{
    qDebug() << "Which wall at " << p << " ?";
    Qt::FillRule rule = Qt::WindingFill;
    int result = 0;
    if (p_south.containsPoint(p, rule)) {
        result = DOWN_WALL;
    }
    else if (p_east.containsPoint(p, rule)) {
        result = RIGHT_WALL;
    }
    else if (p_north.containsPoint(p, rule)) {
        result = UP_WALL;
    }
    else if (p_west.containsPoint(p, rule)) {
        result = LEFT_WALL;
    }
    qDebug() << "Result is : " << result;
    return result;
}

void CellGraphicsItem::mousePressEvent(QGraphicsSceneMouseEvent *)
{
    b_pressed = true;
}

void CellGraphicsItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    if (b_pressed && b_editable) {
        RobotView::EditMode mode = m_view->editMode();
        if (mode==RobotView::Paint) {
            bool p = !m_view->m_field[i_cellY][i_cellX].painted;
            m_view->updateCell(i_cellX, i_cellY, p);
            m_view->m_originalField[i_cellY][i_cellX].painted = p;
        }
        else if (mode==RobotView::Points) {
            bool p = !m_view->m_field[i_cellY][i_cellX].pointed;
            m_view->m_field[i_cellY][i_cellX].pointed = p;
            qDebug() << "Set point value " << p;
            m_view->m_field[i_cellY][i_cellX].pointItem->setVisible(p);
            m_view->m_originalField[i_cellY][i_cellX].pointed = p;
        }
        else if (mode==RobotView::Walls) {
            int w = whichWall(event->pos());
            if (w==UP_WALL && i_cellY>0) {
                bool v = !m_view->m_field[i_cellY][i_cellX].wallUp;
                m_view->m_field[i_cellY][i_cellX].wallUp = v;
                m_view->m_field[i_cellY-1][i_cellX].wallDown = v;
                m_view->m_originalField[i_cellY][i_cellX].wallUp = v;
                m_view->m_originalField[i_cellY-1][i_cellX].wallDown = v;
                m_view->m_field[i_cellY][i_cellX].wallUpItem->setVisible(v);
            }
            if (w==DOWN_WALL && i_cellY<m_view->m_field.size()-1) {
                bool v = !m_view->m_field[i_cellY][i_cellX].wallDown;
                m_view->m_field[i_cellY][i_cellX].wallDown = v;
                m_view->m_field[i_cellY+1][i_cellX].wallUp = v;
                m_view->m_originalField[i_cellY][i_cellX].wallDown = v;
                m_view->m_originalField[i_cellY+1][i_cellX].wallUp = v;
                m_view->m_field[i_cellY][i_cellX].wallDownItem->setVisible(v);
            }
            if (w==LEFT_WALL && i_cellX>0) {
                bool v = !m_view->m_field[i_cellY][i_cellX].wallLeft;
                m_view->m_field[i_cellY][i_cellX].wallLeft = v;
                m_view->m_field[i_cellY][i_cellX-1].wallRight = v;
                m_view->m_originalField[i_cellY][i_cellX].wallLeft = v;
                m_view->m_originalField[i_cellY][i_cellX-1].wallRight = v;
                m_view->m_field[i_cellY][i_cellX].wallLeftItem->setVisible(v);
            }
            if (w==RIGHT_WALL && i_cellX<m_view->m_field[0].size()-1) {
                bool v = !m_view->m_field[i_cellY][i_cellX].wallRight;
                m_view->m_field[i_cellY][i_cellX].wallRight = v;
                m_view->m_field[i_cellY][i_cellX+1].wallLeft = v;
                m_view->m_originalField[i_cellY][i_cellX].wallRight = v;
                m_view->m_originalField[i_cellY][i_cellX+1].wallLeft = v;
                m_view->m_field[i_cellY][i_cellX].wallRightItem->setVisible(v);
            }
        }
        else if (mode==RobotView::Robot) {
            Point2Di cp = m_view->m_robotItem->scenePosition();
            if (cp.x==i_cellX && cp.y==i_cellY) {
                RobotItem::Direction dir = m_view->m_robotItem->direction();
                RobotItem::Direction newDir = dir;
                if (event->button()==Qt::LeftButton) {
                    // ccw rotate
                    if (dir==RobotItem::South)
                        newDir = RobotItem::East;
                    if (dir==RobotItem::East)
                        newDir = RobotItem::North;
                    if (dir==RobotItem::North)
                        newDir = RobotItem::West;
                    if (dir==RobotItem::West)
                        newDir = RobotItem::South;
                }
                else {
                    // cw rotate
                    if (dir==RobotItem::South)
                        newDir = RobotItem::West;
                    if (dir==RobotItem::East)
                        newDir = RobotItem::South;
                    if (dir==RobotItem::North)
                        newDir = RobotItem::East;
                    if (dir==RobotItem::West)
                        newDir = RobotItem::North;
                }
                m_view->m_robotItem->setDirection(newDir);
            }
            else {
                Point2Di np;
                np.x = i_cellX;
                np.y = i_cellY;
                m_view->m_robotItem->setScenePosition(np);
            }
            m_view->m_originalRobotPosition = m_view->m_robotItem->scenePosition();
            m_view->m_originalRobotDirection = m_view->m_robotItem->direction();
        }
    }
}
}
