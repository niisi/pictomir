#ifndef CR_GAMEWINDOW_H
#define CR_GAMEWINDOW_H

#include <QtCore>
#include <QtGui>

#include "schema/sch_game.h"

namespace Schema {
    struct Game;
    struct Task;
}

namespace Creator {

namespace Ui {
    class TaskListWindow;
    class RenameTaskDialog;
}

class TaskListWindow : public QWidget
{
    Q_OBJECT

public:
    explicit TaskListWindow(QWidget *parent = 0);
    ~TaskListWindow();
    inline QAction * actionClone() { return a_clone; }
    inline QAction * actionRemove() { return a_remove; }
    inline QAction * actionRename() { return a_rename; }
    inline QAction * actionUp() { return a_up; }
    inline QAction * actionDown() { return a_down; }

signals:
    void requestEnvironmentEditor(bool v);
    void requestProgramEditor(bool v);
    void requestHintEditor(bool v);

    void taskMoved(const QMap<int,int>);
    void taskTitleChanged();
    void syncRequest();
    void closeRequest();

public slots:
    void setGame(const Schema::Game & game, const QString &fileName);
    QString license() const;
    inline Schema::Game * gamePtr() { return &current_game; }
    QString gameTitle() const;
    int taskIndex() const;
    inline void setTaskIndex(int v) { selectTask(v); }
    void selectTask(int index);
    void handleTaskDoubleClicked(QListWidgetItem * item);
    void handleTaskChanged(QListWidgetItem * item);

    bool isChanged() const;

    void cloneTask();
    void removeTask();
    void renameTask();
    void moveUp();
    void moveDown();

    bool saveGame();
    bool saveGameAs();

private slots:
    void handleGameTitleEdited();
    inline void handleShowEnv() { emit requestEnvironmentEditor(true); }
    inline void handelShowPr() { emit requestProgramEditor(true); }
    inline void handleShowHint() { emit requestHintEditor(true); }


private:
    void swapTasksPositions(int oldIndex, int newIndex);
    bool saveToFile(const QString &fileName);
    void closeEvent(QCloseEvent *event);
    void detectCCLicense(const QString &lic);
    QString s_originalGameName;
    QString s_fileName;
    QAction * a_clone;
    QAction * a_remove;
    QAction * a_rename;
    QAction * a_up;
    QAction * a_down;
    Ui::TaskListWindow *ui;
    Schema::Game current_game;
    Schema::Game original_game;
    QDialog * renameDialog;
    Ui::RenameTaskDialog * ui_renameDialog;
};


} // namespace Creator
#endif // CR_GAMEWINDOW_H
