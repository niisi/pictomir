
HEADERS += \
    creator/cr_mainwindow.h \
    creator/cr_environmenteditor.h \
    creator/cr_programeditor.h \
    creator/addalgorhitmdialog.h \
    creator/cr_tasklistwindow.h \
    creator/newenvironmentdialog.h \
    creator/cr_hinteditor.h

SOURCES += \
    creator/cr_mainwindow.cpp \
    creator/cr_environmenteditor.cpp \
    creator/cr_programeditor.cpp \
    creator/addalgorhitmdialog.cpp \
    creator/cr_tasklistwindow.cpp \
    creator/newenvironmentdialog.cpp \
    creator/cr_hinteditor.cpp

FORMS += \
    creator/cr_mainwindow.ui \
    creator/cr_environmenteditor.ui \
    creator/addalgorhitmdialog.ui \
    creator/cr_tasklistwindow.ui \
    creator/newenvironmentdialog.ui \
    creator/cr_renametaskdialog.ui \
    creator/cr_hinteditor.ui
