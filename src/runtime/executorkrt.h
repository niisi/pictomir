#ifndef EXECUTORKRT_H
#define EXECUTORKRT_H

#include <QtCore>

namespace Robot25D {

    class Plugin;
}

namespace Runtime {

class KRT;

class ExecutorKRT : public QThread
{
    Q_OBJECT
public:
    explicit ExecutorKRT(QObject *parent);
    inline bool isInitialized() const { QMutexLocker l(mutex_initialized); return b_initialized; }
    inline bool isAnimationEnabled() const { QMutexLocker l(mutex_animationEnabled); return b_animationEnabled; }
    void init(Robot25D::Plugin *robot,
              const QStringList &program
              );
    inline bool isErrorTerminated() const { return b_runtimeError; }
    QStack< QPair<int,int> > backtrace() const;
    void waitForStopped();
    ~ExecutorKRT();

public slots:
    void dispose();
    void stop();
    void step();
    void pause();
    void continuousRun();
    void commandFinished();
    void setAnimationEnabled(bool v);
    int DO(int id, const QList<QVariant> &arguments);
    void highlight(int funcNo, int cellNo, int pauseLength);
    void highlightCondition(int funcNo, int cellNo, const QString &color);
    void highlightRepeat(int funcNo, int value);
protected:
    virtual void run();
protected slots:
    void error(const QString &message);
    void waitForSync();
    void INT_handler(quint16 intNo, QStack<QVariant> *dataStack);
private:
    ExecutorKRT *m_instance;
    bool b_stopping;
    QMutex *mutex_stopping;
    QMutex *mutex_pausing;
    QMutex *mutex_initialized;
    QMutex *mutex_complexInstructionFinished;
    QMutex *mutex_animationEnabled;
    QMutex *mutex_delete;
    Robot25D::Plugin *m_robot;
    bool b_initialized;
    bool b_runtimeError;
    QMutex *mutex_sync;
    bool b_execFinished;
    bool b_pausing;
    bool b_stepMode;
    bool b_complexInstructionFinished;
    bool b_firstStep;
    bool b_animationEnabled;
    bool b_scheduledToDelete;
    QString s_currentCommandName;

    QStack<QString> t_backtrace;

    KRT *m_krt;
signals:
    void executionError(const QString &message);
    void executionFinished(const QString &message);
    void highlightRequest(int,int);
    void highlightConditionRequest(int,int,const QString&);
    void highlightRepeatRequest(int,int);
    void stepFinished(const QString &message);
    void stepStarted(const QString &message);


};

}

#endif
