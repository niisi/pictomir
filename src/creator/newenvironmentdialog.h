#ifndef NEWENVIRONMENTDIALOG_H
#define NEWENVIRONMENTDIALOG_H

#include <QtCore>
#include <QtGui>


namespace Creator {

namespace Ui {
    class NewEnvironmentDialog;
}

class NewEnvironmentDialog : public QDialog
{
    Q_OBJECT

public:
    explicit NewEnvironmentDialog(QWidget *parent = 0);
    ~NewEnvironmentDialog();
    QSize environemntSize() const;

private:
    Ui::NewEnvironmentDialog *ui;
};

}

#endif // NEWENVIRONMENTDIALOG_H
