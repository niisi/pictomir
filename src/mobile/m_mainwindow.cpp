#include "core/pictomir.h"
#include "mobile/m_mainwindow.h"
#include "mobile/m_dockwidget.h"
#include "mobile/smartphonebuttonbox.h"
#include "core/pluginmanager.h"
#include "interfaces/functionwidget_interface.h"
#include "mobile/m_functionwidget.h"
#include "core/pictomirprogram.h"
#include "mobile/smartphonemenudialog.h"
#include "core/gameprovider.h"


using namespace Core;

namespace Mobile {

MainWindow::MainWindow(PictomirCommon * pictomir, QWidget *parent)
    : QMainWindow(parent)
{

    e_currentState = EditingProgram;
    setStyleSheet("background-color:black;color:white;");
    p_pictomir = pictomir;
    p_pictomir->pluginManager()->setUiType(MobileUi);
    p_pictomir->pluginManager()->loadPlugins();
    QSize screenSize = qApp->desktop()->screenGeometry().size();
    if (screenSize.width()>screenSize.height())
        e_currentOrientation = Qt::Horizontal;
    else
        e_currentOrientation = Qt::Vertical;


    //    w_dockContainer = new QDockWidget(this);
    w_dock = new DockWidgetTouchscreen(QSize(30,30), this);
    //    w_dockContainer->setFeatures(QDockWidget::NoDockWidgetFeatures);
    //    w_dockContainer->setWidget(w_dock);
    //    w_dockContainer->setVisible(false);


    //    w_dockContainer->setTitleBarWidget(0);
    //    addDockWidget(Qt::BottomDockWidgetArea, w_dockContainer);

    //    w_selectorContainer = new QDockWidget(this);

    QStringList selectorButtons;
    selectorButtons << "ui_smartphone_and_mid/btn_show_menu.svg";
    selectorButtons << "ui_smartphone_and_mid/btn_show_robot.svg";
    selectorButtons << "ui_smartphone_and_mid/btn_show_alg_main.svg";
    selectorButtons << "ui_smartphone_and_mid/btn_show_alg_1.svg";
    selectorButtons << "ui_smartphone_and_mid/btn_show_alg_2.svg";
    selectorButtons << "ui_smartphone_and_mid/btn_show_alg_3.svg";
    selectorButtons << "ui_smartphone_and_mid/btn_show_alg_4.svg";
    selectorButtons << "ui_smartphone_and_mid/btn_show_alg_5.svg";
    selectorButtons << "ui_smartphone_and_mid/btn_show_alg_6.svg";

    w_selector = new SmartphoneButtonBox(selectorButtons, this);
    connect(w_selector, SIGNAL(iconPressed(int)), this, SLOT(handleSelectorButtonClick(int)));

    //    w_selectorContainer->setFeatures(QDockWidget::NoDockWidgetFeatures);
    //    w_selectorContainer->setTitleBarWidget(0);
    //    w_selectorContainer->setWidget(w_selector);
    //    addDockWidget(Qt::LeftDockWidgetArea, w_selectorContainer);


    w_dock->setOrientation(e_currentOrientation);



    w_leftBorder = new QStackedWidget(this);
    //    w_leftBorder->setStyleSheet("background-color:red;");
    w_leftBorder->setFixedWidth(40);
    w_rightBorder = new QStackedWidget(this);
    //    w_rightBorder->setStyleSheet("background-color:blue;");
    w_rightBorder->setFixedWidth(40);
    w_bottomBorder = new QStackedWidget(this);
    //    w_bottomBorder->setStyleSheet("background-color:green;");
    w_bottomBorder->setFixedHeight(40);
    w_centralWidget = new QStackedWidget(this);
    w_centralWidget->setStyleSheet("background-color:black;");
    QWidget *w = new QWidget(this);
    QGridLayout *l = new QGridLayout;
    l->setContentsMargins(0,0,0,0);
    w->setLayout(l);
    l->addWidget(w_leftBorder, 0, 0);
    l->addWidget(w_centralWidget, 0, 1);
    l->addWidget(w_rightBorder, 0, 2);
    l->addWidget(w_bottomBorder, 1, 1);
    setCentralWidget(w);

    w_leftBorder->addWidget(w_dock);
    w_bottomBorder->addWidget(w_dock);
    w_bottomBorder->addWidget(w_selector);
    w_rightBorder->addWidget(w_selector);

    connect (p_pictomir, SIGNAL(taskLoaded()), this, SLOT(handleTaskLoaded()));
    createView();


    sl_buttonsEdit << "ui_smartphone_and_mid/go-previous.svg";
    sl_buttonsEdit << "ui_smartphone_and_mid/go-next.svg";
    sl_buttonsEdit << "";
    sl_buttonsEdit << "ui_smartphone_and_mid/btn_debug.svg";
    sl_buttonsEdit << "ui_smartphone_and_mid/btn_start.svg";
    sl_buttonsEdit << "ui_smartphone_and_mid/btn_fast_run.svg";

    sl_buttonsAnalisys << "ui_smartphone_and_mid/go-previous.svg";
    sl_buttonsAnalisys << "ui_smartphone_and_mid/go-next.svg";
    sl_buttonsAnalisys << "";
    sl_buttonsAnalisys << "ui_smartphone_and_mid/btn_reset.svg";
    sl_buttonsAnalisys << "ui_smartphone_and_mid/btn_debug.svg";
    sl_buttonsAnalisys << "ui_smartphone_and_mid/btn_start.svg";
    sl_buttonsAnalisys << "ui_smartphone_and_mid/btn_fast_run.svg";

    sl_buttonsPause << "ui_smartphone_and_mid/btn_stop.svg";
    sl_buttonsPause << "ui_smartphone_and_mid/btn_step.svg";
    sl_buttonsPause << "ui_smartphone_and_mid/btn_start.svg";
    sl_buttonsPause << "ui_smartphone_and_mid/btn_fast_run.svg";

    sl_buttonsRun << "ui_smartphone_and_mid/btn_stop.svg";
    sl_buttonsRun << "ui_smartphone_and_mid/btn_pause.svg";
    sl_buttonsRun << "ui_smartphone_and_mid/btn_fast_run.svg";



    w_runControl = new SmartphoneButtonBox(sl_buttonsRun, this);
    connect (w_runControl, SIGNAL(iconPressed(int)), this, SLOT(handleRunControlButtonClick(int)));
    w_editControl = new SmartphoneButtonBox(sl_buttonsEdit, this);
    connect (w_editControl, SIGNAL(iconPressed(int)), this, SLOT(handleEditControlButtonClick(int)));
    w_pauseControl = new SmartphoneButtonBox(sl_buttonsPause, this);
    connect (w_pauseControl, SIGNAL(iconPressed(int)), this, SLOT(handlePauseButtonClick(int)));
    w_analisysControl = new SmartphoneButtonBox(sl_buttonsAnalisys, this);
    connect (w_analisysControl, SIGNAL(iconPressed(int)), this, SLOT(handleAnalisysButtonClick(int)));

    w_controlWidget = new QStackedWidget(this);
    w_controlWidget->addWidget(w_runControl);
    w_controlWidget->addWidget(w_pauseControl);
    w_controlWidget->addWidget(w_editControl);
    w_controlWidget->addWidget(w_analisysControl);
    w_controlWidget->setCurrentWidget(w_editControl);

    b_showModule = true;

    createMenus();

    relayoutWindow();
}


bool MainWindow::controlsVisible() const
{
    return true;
}

void MainWindow::setTaskTitle(const QString &)
{

}

void MainWindow::setStatusText(const QString &)
{

}

void MainWindow::setInteractiveMode(ProgramState ps)
{
    if (ps==AnalisysProgram) {
        w_controlWidget->setCurrentWidget(w_analisysControl);
    }
    else if (ps==EditingProgram) {
        w_controlWidget->setCurrentWidget(w_editControl);
    }
    else if (ps==RunningProgram) {
        w_controlWidget->setCurrentWidget(w_runControl);
    }
    else if (ps==PauseProgram) {
        w_controlWidget->setCurrentWidget(w_pauseControl);
    }
    e_currentState = ps;
}

void MainWindow::setControlsVisible(bool)
{

}

void MainWindow::resizeEvent(QResizeEvent *event)
{
    qDebug() << "Resize event: " << event->size();
    QSize sz = event->size();
    if (sz.width()>sz.height())
        e_currentOrientation = Qt::Horizontal;
    else
        e_currentOrientation = Qt::Vertical;
    relayoutWindow();
    event->accept();
}

void MainWindow::tabletEvent(QTabletEvent *event)
{
    qDebug() << "Tablet event!";
    qreal rot = event->rotation();
    qDebug() << "Rotation: " << rot;
    relayoutWindow();
    event->accept();
}

void MainWindow::relayoutWindow()
{

    //    removeDockWidget(w_dockContainer);
    //    removeDockWidget(w_selectorContainer);
    //    bool dockVisible = w_dock->isVisible();
    w_dock->setOrientation(e_currentOrientation);
    if (e_currentOrientation==Qt::Horizontal) {
        qDebug() << "Horizontal";
        //        w_dock->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        w_dock->setFixedHeight(40);
        w_dock->setFixedWidth(QWIDGETSIZE_MAX);
        //        w_selector->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
        w_selector->setFixedWidth(40);
        w_selector->setFixedHeight(QWIDGETSIZE_MAX);
        w_selector->setDockPosition(Qt::RightDockWidgetArea);
        w_controlWidget->setFixedHeight(40);
        w_analisysControl->setFixedHeight(40);
        w_editControl->setFixedHeight(40);
        w_runControl->setFixedHeight(40);
        w_pauseControl->setFixedHeight(40);
        w_controlWidget->setFixedWidth(QWIDGETSIZE_MAX);
        w_analisysControl->setFixedWidth(QWIDGETSIZE_MAX);
        w_editControl->setFixedWidth(QWIDGETSIZE_MAX);
        w_runControl->setFixedWidth(QWIDGETSIZE_MAX);
        w_pauseControl->setFixedWidth(QWIDGETSIZE_MAX);
        w_analisysControl->setDockPosition(Qt::BottomDockWidgetArea);
        w_editControl->setDockPosition(Qt::BottomDockWidgetArea);
        w_runControl->setDockPosition(Qt::BottomDockWidgetArea);
        w_pauseControl->setDockPosition(Qt::BottomDockWidgetArea);
        w_leftBorder->setVisible(false);
        w_rightBorder->setVisible(true);
        if (b_showModule) {
            w_bottomBorder->addWidget(w_controlWidget);
            w_bottomBorder->setCurrentWidget(w_controlWidget);
        }
        else {
            w_bottomBorder->addWidget(w_dock);
            w_bottomBorder->setCurrentWidget(w_dock);
        }
        w_rightBorder->addWidget(w_selector);
        w_rightBorder->setCurrentWidget(w_selector);
        //        addDockWidget(Qt::BottomDockWidgetArea, w_dockContainer);
        //        addDockWidget(Qt::RightDockWidgetArea, w_selectorContainer);
    }
    else {
        qDebug() << "Vertical";
        //        w_dock->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
        w_dock->setFixedWidth(40);
        w_dock->setFixedHeight(QWIDGETSIZE_MAX);
        //        w_selector->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        w_selector->setFixedHeight(40);
        w_selector->setFixedWidth(QWIDGETSIZE_MAX);
        w_selector->setDockPosition(Qt::BottomDockWidgetArea);
        if (b_showModule) {
            w_leftBorder->addWidget(w_controlWidget);
            w_leftBorder->setCurrentWidget(w_controlWidget);
        }
        else {
            w_leftBorder->addWidget(w_dock);
            w_leftBorder->setCurrentWidget(w_dock);
        }
        w_controlWidget->setFixedHeight(QWIDGETSIZE_MAX);
        w_analisysControl->setFixedHeight(QWIDGETSIZE_MAX);
        w_editControl->setFixedHeight(QWIDGETSIZE_MAX);
        w_runControl->setFixedHeight(QWIDGETSIZE_MAX);
        w_pauseControl->setFixedHeight(QWIDGETSIZE_MAX);

        w_controlWidget->setFixedWidth(40);
        w_analisysControl->setFixedWidth(40);
        w_editControl->setFixedWidth(40);
        w_runControl->setFixedWidth(40);
        w_pauseControl->setFixedWidth(40);

        w_analisysControl->setDockPosition(Qt::LeftDockWidgetArea);
        w_editControl->setDockPosition(Qt::LeftDockWidgetArea);
        w_runControl->setDockPosition(Qt::LeftDockWidgetArea);
        w_pauseControl->setDockPosition(Qt::LeftDockWidgetArea);
        w_leftBorder->setVisible(true);
        w_bottomBorder->addWidget(w_selector);
        w_bottomBorder->setCurrentWidget(w_selector);
        w_rightBorder->setVisible(false);
        //        addDockWidget(Qt::LeftDockWidgetArea, w_dockContainer);
        //        addDockWidget(Qt::BottomDockWidgetArea, w_selectorContainer);
    }
    //    if (dockVisible)
    //        w_dockContainer->show();
    //    w_selectorContainer->show();
    for (int i=0; i<wl_functions.size(); i++) {
        FunctionWidget *f = wl_functions[i];
        Q_CHECK_PTR(f);
        f->setOrientation(e_currentOrientation);
    }

}

void MainWindow::handleTaskLoaded()
{
    p_pictomir->m_currentModuleWidget->setVisible(true);
    for (int i=0; i<wl_functions.size(); i++) {
        //        wl_functions[i]->thisWidget()->deleteLater();
    }
    wl_functions.clear();
    PictomirProgram *program = p_pictomir->m_program;
    QList<bool> enabledSelectorButtons;
    enabledSelectorButtons << true;
    enabledSelectorButtons << true;
    for (int i=0; i<program->functionsCount(); i++) {
        QGraphicsView * f_view = new QGraphicsView(this);
        f_view->setScene(new QGraphicsScene);
        FunctionWidget *t = new FunctionWidget(program->function(i));
        f_view->scene()->addItem(t);
        w_centralWidget->addWidget(f_view);
        t->setOrientation(e_currentOrientation);
        enabledSelectorButtons << true;
        wl_functions << t;
    }
    for (int i=program->functionsCount(); i<7; i++)
        enabledSelectorButtons << false;
    w_selector->setButtonsEnabled(enabledSelectorButtons);
    w_dock->updateVisiblity();
}

void MainWindow::createView()
{
    QStringList modules = p_pictomir->pluginManager()->modules();
    for (int i=0; i<modules.count(); i++) {
        const QString moduleName = modules[i];
        PictomirPluginInterface *module;
        QWidget *moduleWidget;
        module = p_pictomir->pluginManager()->module(moduleName);
        moduleWidget = p_pictomir->pluginManager()->widget(moduleName);
        Q_CHECK_PTR(module);
        if (moduleWidget) {
            Q_CHECK_PTR(moduleWidget);
            Q_CHECK_PTR(w_centralWidget);
            w_centralWidget->addWidget(moduleWidget);
        }
    }
}

void MainWindow::handleSelectorButtonClick(int index)
{
    QWidget *widget = 0;
    if (index==0) {
        showMenuWidget();
    }
    else if (index==1) {
        widget= p_pictomir->m_currentModuleWidget;
        //        w_dockContainer->setVisible(false);
        w_centralWidget->setCurrentWidget(widget);
        if (e_currentOrientation==Qt::Horizontal) {
            w_bottomBorder->addWidget(w_controlWidget);
            w_bottomBorder->setCurrentWidget(w_controlWidget);
        }
        else {
            w_leftBorder->setCurrentWidget(w_controlWidget);
            w_leftBorder->setCurrentWidget(w_controlWidget);
        }
        b_showModule = true;
    }
    else {
        Q_ASSERT(index-2<wl_functions.size());
        //        widget = wl_functions[index-2]->thisWidget();
        w_centralWidget->setCurrentWidget(widget);
        //        w_dockContainer->setVisible(true);
        FunctionWidget *ts = qobject_cast<FunctionWidget*>(widget);
        if (e_currentOrientation==Qt::Horizontal) {
            w_bottomBorder->addWidget(w_dock);
            w_bottomBorder->setCurrentWidget(w_dock);
        }
        else {
            w_leftBorder->addWidget(w_dock);
            w_leftBorder->setCurrentWidget(w_dock);
        }
        ts->relayout();
        b_showModule = false;
    }
}

void MainWindow::handleAnalisysButtonClick(int index)
{
    if (index==0) {
        p_pictomir->a_previousTask->trigger();
    }
    else if (index==1) {
        p_pictomir->a_nextTask->trigger();
    }
    else if (index==2) {
        p_pictomir->a_reset->trigger();
    }
    else if (index==3) {
        p_pictomir->a_debug->trigger();
    }
    else if (index==4) {
        p_pictomir->a_start->trigger();
    }
    else if (index==5) {
        p_pictomir->a_runToEnd->trigger();
    }
    else {
        qFatal("Oopps!...");
    }
}

void MainWindow::handleEditControlButtonClick(int index)
{
    if (index==0) {
        p_pictomir->a_previousTask->trigger();
    }
    else if (index==1) {
        p_pictomir->a_nextTask->trigger();
    }
    else if (index==2) {
        p_pictomir->a_debug->trigger();
    }
    else if (index==3) {
        p_pictomir->a_start->trigger();
    }
    else if (index==4) {
        p_pictomir->a_runToEnd->trigger();
    }
    else {
        qFatal("Oopps!...");
    }
}

void MainWindow::handlePauseButtonClick(int index)
{
    if (index==0) {
        p_pictomir->a_stop->trigger();
    }
    else if (index==1) {
        p_pictomir->a_step->trigger();
    }
    else if (index==2) {
        p_pictomir->a_start->trigger();
    }
    else if (index==3) {
        p_pictomir->a_runToEnd->trigger();
    }
    else {
        qFatal("Oopps!...");
    }
}

void MainWindow::handleRunControlButtonClick(int index)
{
    if (index==0) {
        p_pictomir->a_stop->trigger();
    }
    else if (index==1) {
        p_pictomir->a_pause->trigger();
    }
    else if (index==2) {
        p_pictomir->a_runToEnd->trigger();
    }
    else {
        qFatal("Oopps!...");
    }
}


void MainWindow::createMenus()
{
    w_mainMenu = new SmartphoneMenuDialog(this);
    w_mainMenu->init(QStringList() << tr("Select game...") << tr("Exit"), 0);
    w_taskDialog = new SmartphoneMenuDialog(this);
}

void MainWindow::showMenuWidget()
{
    qDebug() << "Show menu widget";
    w_mainMenu->showFullScreen();
    if (w_mainMenu->exec()==QDialog::Accepted) {
        if (w_mainMenu->selectedItemIndex()==0) {
            qDebug() << "Select game";
            p_pictomir->prepareLoadGame();
            w_taskDialog->showFullScreen();
            w_taskDialog->init(p_pictomir->m_gameProvider->gamesList(),
                               p_pictomir->m_gameProvider->lastIndex());
            if (w_taskDialog->exec()==QDialog::Accepted) {
                int index = w_taskDialog->selectedItemIndex();
                const QString game = p_pictomir->m_gameProvider->fileNameByIndex(index);
                p_pictomir->loadGameFromFile(game);
            }
        }
        else if (w_mainMenu->selectedItemIndex()==1) {
            qApp->quit();
        }
    }
}


}
