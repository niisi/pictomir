#include "core/pictomirfunction.h"
#include "core/actionmodel.h"
#include "interfaces/functionwidget_interface.h"

namespace Core {

PictomirFunction::PictomirFunction(const QSize& size
                                   , FunctionModifiers modifiers
                                   , const QString &title
                                   , const QStringList &hints
                                   , ActionManager * actionManager
                                   , QObject *parent)
    : QObject(parent)
    , e_modifiers(modifiers)
    , s_title(title)
    , sl_hints(hints)
    , sz_size(size)
    , b_modified(false)
    , m_actionManager(actionManager)
{

    vv_data = QVector< QVector<ActionModel*> > (size.height());
    for (int i=0; i<size.height(); i++) {
        QVector<ActionModel*> row = QVector<ActionModel*>(size.width(), NULL);
        vv_data[i] = row;
    }
    m_condition = NULL;
    m_repeater = NULL;
}

ActionManager * PictomirFunction::actionManager() const
{
    return m_actionManager;
}

ActionModel* PictomirFunction::repeater() const
{
    return m_repeater;
}

ActionModel* PictomirFunction::condition() const
{
    return m_condition;
}

ActionModel* PictomirFunction::action(int index) const
{
    Q_ASSERT( index >= 0);
    int row = index / sz_size.width();
    int col = index % sz_size.width();
    return action(col, row);
}

void PictomirFunction::setAction(int index, const ActionModel *action)
{
    Q_ASSERT( index >= 0);
    int row = index / sz_size.width();
    int col = index % sz_size.width();
    setAction(col, row, action);
}

ActionModel* PictomirFunction::action(int x, int y) const
{
    Q_ASSERT( x >= 0);
    Q_ASSERT( y >= 0);
    Q_ASSERT( y < sz_size.height() );
    Q_ASSERT( x < sz_size.width() );
    return vv_data[y][x];
}

//int PictomirFunction::actionIndex(const ActionModel *action) const
//{
//    if (action==m_repeater)
//        return -1;
//    if (action==m_condition)
//        return -2;
//    int row;
//    int col;
//    bool found = false;
//    for (row=0; row<sz_size.height(); row++) {
//        for (col=0; col<sz_size.width(); col++) {
//            if (vv_data[row][col]==action) {
//                found = true;
//                break;
//            }
//        }
//        if (found)
//            break;
//    }
//    Q_ASSERT(found);
//    int index = row * sz_size.width() + col;
//    qDebug() << "Found action index: " << index;
//    return index;
//}

QPair<const QList<int>, const QList<const ActionModel*> > PictomirFunction::nonEmptyActions() const
{
    QList<const ActionModel*> actions;
    int index = 0;
    QList<int> indeces;
    for (int row=0; row<sz_size.height(); row++) {
        for (int col=0; col<sz_size.width(); col++) {
            if (vv_data[row][col]!=NULL) {
                actions << vv_data[row][col];
                indeces << index;
            }
            index += 1;
        }
    }
    const QPair<const QList<int>, const QList<const ActionModel*> > result(indeces, actions);

    return result;
}

void PictomirFunction::alterChange()
{
    emit programChanged();
}

void PictomirFunction::clearProgram()
{
    for (int row=0; row<sz_size.height(); row++) {
        for (int col=0; col<sz_size.width(); col++) {
            vv_data[row][col] = NULL;
        }
    }
    m_repeater = NULL;
    m_condition = NULL;
}

void PictomirFunction::setRepeater(const ActionModel *action)
{
    m_repeater = const_cast<ActionModel*>(action);
}

void PictomirFunction::setCondition(const ActionModel *action)
{
    m_condition = const_cast<ActionModel*>(action);
}

void PictomirFunction::setAction(int x, int y, const ActionModel *action)
{
    Q_ASSERT( x>=0 );
    Q_ASSERT( y>=0 );
    Q_ASSERT( x<sz_size.width() );
    Q_ASSERT( y<sz_size.height() );
    vv_data[y][x] = const_cast<ActionModel*>(action);
}


PictomirFunction::~PictomirFunction()
{
    for (int i=0; i<l_views.size(); i++) {
        if (!l_views[i].isNull())
            l_views[i]->deleteLater();
    }
}

void PictomirFunction::setTitle(const QString &title)
{
    s_title = title;
    for (int i=0; i<l_views.size(); i++) {
        FunctionWidgetInterface *fw = dynamic_cast<FunctionWidgetInterface*>(l_views[0].data());
        if (fw)
            fw->setFunctionTitle(title);
    }
}

void PictomirFunction::modifyAlgorhitm(const QSize &size, FunctionModifiers modifiers)
{
    QList< ActionModel *> toSave;
    for (int y=0; y<vv_data.size(); y++) {
        for (int x=0; x<vv_data[y].size(); x++) {
            ActionModel* m = vv_data[y][x];
            if (m) {
                toSave << m;
            }
        }
    }
    sz_size = size;
    vv_data = QVector< QVector<ActionModel*> > (size.height());
    for (int i=0; i<size.height(); i++) {
        QVector<ActionModel*> row = QVector<ActionModel*>(size.width(), NULL);
        vv_data[i] = row;
    }
    int index = 0;
    for (int y=0; y<vv_data.size(); y++) {
        for (int x=0; x<vv_data[y].size(); x++) {
            if (index<toSave.size()) {
                vv_data[y][x] = toSave[index];
                index += 1;
            }
        }
    }
    if (! (modifiers & RepeatFunction) ) {
        m_repeater = NULL;
    }
    if (! (modifiers & ConditionFunction) ) {
        m_condition = NULL;
    }
    e_modifiers = modifiers;
    emit changeRequest();
}
}
