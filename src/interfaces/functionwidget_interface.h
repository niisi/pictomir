#ifndef FUNCTIONWIDGET_INTERFACE_H
#define FUNCTIONWIDGET_INTERFACE_H

#include <QtCore>
#include <QtGui>

namespace Core {

class PictomirFunction;

class FunctionWidgetInterface
    : public QObject
    , public QGraphicsRectItem
{
public:
    inline FunctionWidgetInterface(QGraphicsItem *parent = 0) : QObject(0), QGraphicsRectItem(parent) {}
    virtual PictomirFunction* model() = 0;
    virtual void connectSignalActivationChanged(QObject *obj, const char *method) = 0;
    virtual void lock() = 0;
    virtual void unlock() = 0;
    virtual void highlightPlainCell(int index) = 0;
    virtual void highlightErrorCell(int index) = 0;
    virtual void highlightConditionalValue(bool value) = 0;
    virtual void unhighlight() = 0;
    virtual void setFunctionTitle(const QString &title) = 0;

};

}

#endif // FUNCTIONWIDGET_INTERFACE_H
