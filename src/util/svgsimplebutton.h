#ifndef SVGSIMPLEBUTTON_H
#define SVGSIMPLEBUTTON_H

#include <QtCore>
#include <QtGui>
#include <QtSvg>

namespace Util {

class SvgSimpleButton : public QGraphicsSvgItem
{
    Q_OBJECT
public:
    explicit SvgSimpleButton(const QString &svgPath, QGraphicsItem *parent = 0);
signals:
    void clicked();
public slots:
    void setEnabled(bool v);
protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void hoverLeaveEvent(QGraphicsSceneHoverEvent *event);
private:
    bool b_mousePressed;
};

} // namespace Util

#endif // SVGSIMPLEBUTTON_H
