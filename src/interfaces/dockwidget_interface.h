#ifndef DOCKWIDGET_INTERFACE_H
#define DOCKWIDGET_INTERFACE_H

#include <QtCore>
#include <QtGui>

#include "core/enums.h"

namespace Core {

class ActionManager;

class DockWidgetInterface
{
public:
    virtual void setActionManager(ActionManager *) = 0;
    virtual ActionManager * actionManager() = 0;
    virtual void connectSignalTouched(QObject *obj, const char *method) = 0;
    virtual void connectSignalActivationChanged(QObject *obj, const char *method) = 0;
    virtual void updateVisiblity() = 0;
};

}

#endif // DOCKWIDGET_INTERFACE_H
