#ifndef SELECTUSERDIALOG_H
#define SELECTUSERDIALOG_H

#include <QtCore>
#include <QtGui>

#include "ui_selectuser.h"

namespace Desktop {

class SelectUserDialog
    : public QDialog
{
    Q_OBJECT;
public:
    explicit SelectUserDialog(QWidget *parent);
    void init();
    SelectUserDialog* instance();
    const QString &currentUserRoot() const;
protected slots:
    QStringList scanUsers();
    void handleRemoveUserPressed();
    void handleGoPressed();
    void handleNameEdited(const QString &name);
    void handleItemSelected(const QString &name);
private:
    Ui_SelectUserDialog *ui;
    QMap<QString, QString> map_userDirs;
    QString s_currentUserRoot;

};

}

#endif // SELECTUSERDIALOG_H
