#ifndef MainWindowDesktop_H
#define MainWindowDesktop_H

#include <QtCore>
#include <QtGui>

#include "core/enums.h"
class QWebView;

namespace Core {
class Function;
class DockWidgetInterface;
class FunctionWidgetInterface;
class PictomirCommon;
}

namespace Util {
class SvgSimpleButton;
class AboutDialog;
}

namespace Robot25D {
class RobotView;
class RobotItem;

}

namespace Desktop {
class LoadTaskDialog;
class LoadGameDialog;
class ControlProgramButtonBox;
class NavigateTaskButtonBox;
class LoadGameDialog;
class EnvironmentFileControl;
class AddAlgorhitmDialog;
class SelectUserDialog;
class DockWidget;
class Ui_MainWindow;
class HelperItem;
class HintItem;



class MainWindow :
        public QMainWindow
{
    Q_OBJECT;
    Q_PROPERTY(int robotHintState READ robotHintState WRITE setRobotHintState);

public:
    MainWindow(Core::PictomirCommon * pictomir, QWidget *parent = 0);
    ~MainWindow();
    void start();


public slots:
    void setTaskTitle(const QString &txt);
    void setStatusText(const QString &txt);
    void setDefaultFocus();
    void setInteractiveMode(Core::ProgramState state);
    void setFullscreenMode(bool v);
    void setLowQualityView(bool v);
    void clearStatusLine();
    void loadSettings();
    void saveSettings();
    void showAbout();
    void showHelp();
    void showLoadGameDialog();
    void showSelectUserDialog();
    void copyProgramToClipboard();
    void saveAsKumirProgram();


protected:

    virtual void resizeEvent(QResizeEvent *event);
    virtual void showEvent(QShowEvent *event);


protected slots:
    int robotHintState() const ;
    void setRobotHintState(int v);
    void handleTaskLoaded();
    void startPauseProgram();
    void handleUserProgramTouch();
    void handleHighlightPlainFunctionRequest(int funcNo, int cellNo);
    void handleExecutionError(const QList<int> &fns, const QList<int> &cis);
    void handleTaskSuccessed();

    void updateSizes();
    void updateTextRect();
    void updateAlgorhitms();

    void setRobotPixmap(int v);
    void turnRobotLeft();
    void turnRobotRight();

    void showTaskHint();

    void setTaskHint(const QString &mimetype, const QByteArray &data);


private:

    Ui_MainWindow * ui;

    bool b_started;
    bool b_sizeChanged;

    void createMenuBar();
    void init();
    void createHelperItem();
    void createHelpViewer();

    Core::PictomirCommon *m_pictomir;
    QList< Core::FunctionWidgetInterface* > wl_functions;
    QGraphicsRectItem *m_currentActor;
    QList< HelperItem* > l_taskHints;
    HelperItem * g_robotDirectionHint;
    Util::SvgSimpleButton * g_btnTurnLeft;
    Util::SvgSimpleButton * g_btnTurnRight;
    QGraphicsPixmapItem * g_robotItem;
    QPixmap arr_robotPixmaps[16];
    int i_robotPixmapIndex;
    QPropertyAnimation *an_robotHint;

    HelperItem * g_taskHint;
    Util::SvgSimpleButton * g_btnShowHint;

    HintItem * g_hintWindow;

    LoadGameDialog *m_loadGameDialog;

    int i_goNextTaskState;


    SelectUserDialog * m_selectUserDialog;

    QGraphicsScene * scene;
    QGraphicsView * view;
    DockWidget * dock;
    ControlProgramButtonBox * programButtonBox;
    NavigateTaskButtonBox * navButtonBox;
    QGraphicsEllipseItem * debugMarkerRed;
    QGraphicsEllipseItem * debugMarkerBlue;
    QGraphicsEllipseItem * debugMarkerGreen;
    QGraphicsTextItem * statusLine;
    QPropertyAnimation *an_statusLine;

    Core::ProgramState e_currentState;

    QGraphicsRectItem * g_background;
    QGraphicsRectItem * g_table;

    Robot25D::RobotView * robot;

    Util::AboutDialog * about;

};

}

#endif // MainWindowDesktop_H
