#ifndef GAMEPROVIDER_H
#define GAMEPROVIDER_H

#include <QtCore>

namespace Core {

class GameProvider
    : public QObject
{
    Q_OBJECT
public:
    GameProvider(QObject *parent = 0);
    void init(const QString &lastGameName);
    QString fileNameByIndex(int index) const;
    int lastIndex() const;
    inline QStringList gamesList() const { return l_titles; }

private:
    QPair<QString,QString> loadPmXmlDocument(const QString &fileName);
    QPair<QString,QString> loadPmJsonDocument(const QString &fileName);
    QPair<QString,QString> loadPmDocument(const QString &fileName);

    QList<QString> l_baseNames;
    QList<QString> l_titles;
    QString s_lastGameName;

    int i_selectedIndex;
};

}

#endif // GAMEPROVIDER_H
