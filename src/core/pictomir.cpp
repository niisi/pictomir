#include "core/pictomir.h"
#include "desktop/selectuserdialog.h"
#include "desktop/loadgamedialog.h"
#include "robot25d/default_plugin.h"
#include "robot25d/robotview.h"

#include "runtime/executorkrt.h"
#ifdef WITH_PHONON
    #include "desktop/audioplayer.h"
#endif

#include "core/actionmanager.h"

#include "core/pictomirprogram.h"
#include "core/actionmodel.h"
#include "core/platform_configuration.h"
#include "core/gameprovider.h"

using namespace Runtime;
using namespace Desktop;

namespace Core {

PictomirCommon::PictomirCommon(QObject *parent)
    : QObject(parent)
{
//    b_initialized = false;
    e_uiType = DesktopUi;
    b_prevTaskAvailable = false;
    b_nextTaskAvailable = false;
    b_gameLoaded = false;
    m_program = NULL;
//    startTimer(100);
    m_gameProvider = new GameProvider;

#ifdef WITH_PHONON
    m_audioPlayer = new AudioPlayer(this);
#endif
    m_currentModule = new Robot25D::Plugin();

    m_currentModuleWidget = m_currentModule->mainWidget();
    m_actionManager = new ActionManager(this);
    m_actionManager->createActions(QList<Robot25D::Plugin*>() << m_currentModule);

    m_executor = new ExecutorKRT(this);

    createUiActions();
    connect (m_currentModuleWidget, SIGNAL(sync()), m_executor, SLOT(commandFinished()));
    connect ( m_executor, SIGNAL(executionFinished(QString)),
              this, SLOT(executionFinished(QString)));
    connect ( m_executor, SIGNAL(executionError(QString)),
              this, SLOT(executionError(QString)));

    connect ( m_executor, SIGNAL(stepFinished(QString)),
              this, SLOT(handleStepFinished(QString)));
    connect ( m_executor, SIGNAL(stepStarted(QString)),
              this, SLOT(handleStepStarted(QString)) );

    connect ( m_executor, SIGNAL(highlightRequest(int,int)),
              this, SIGNAL(highlightRequest(int,int)));




    connect (qApp, SIGNAL(aboutToQuit()), this, SLOT(prepareQuit()));
}

void PictomirCommon::loadUserState(const QString &currentUserRoot)
{
    s_currentUserRoot = currentUserRoot;
//    s_currentUserRoot = QDesktopServices::storageLocation(QDesktopServices::DataLocation)+
//                        "/Users/default/";
    loadGameFromFile("_default.pm.json");
}


void PictomirCommon::resetProgramToInitial()
{
    resetProgramAndModule();
    int index = sch_current.index;
    Schema::Program pr = sch_initial.tasks[index].program;
    m_program->loadProgram(pr);
}


void PictomirCommon::createUiActions()
{
    a_selectUser = new QAction("Select user...", this);
    connect(a_selectUser, SIGNAL(triggered()), this, SIGNAL(selectUser()));
    a_previousTask = new QAction("Load previous task", this);
    connect(a_previousTask, SIGNAL(triggered()), this, SLOT(loadPreviousTask()));
    a_nextTask = new QAction("Load next task", this);
    connect(a_nextTask, SIGNAL(triggered()), this, SLOT(loadNextTask()));
    a_resetProgramToInitial = new QAction("Reset program to initial",this);
    connect(a_resetProgramToInitial, SIGNAL(triggered()), this, SLOT(resetProgramToInitial()));
    a_start = new QAction("Start", this);
    connect (a_start, SIGNAL(triggered()), this, SLOT(executeProgram()));
    a_pause = new QAction("Pause", this);
    connect (a_pause, SIGNAL(triggered()), this, SLOT(pauseProgram()));
    a_runToEnd = new QAction("Run to end", this);
    connect(a_runToEnd, SIGNAL(triggered()), this, SLOT(runToEnd()));
    a_stop = new QAction("Stop",this);
    connect(a_stop, SIGNAL(triggered()), this, SLOT(stopProgram()));
    a_stop->setEnabled(false);
    a_step = new QAction("Step",this);
    connect(a_step, SIGNAL(triggered()), this, SLOT(stepProgram()));
    a_debug = new QAction("Debug", this);
    connect(a_debug, SIGNAL(triggered()), this, SLOT(debugProgram()));
    a_reset = new QAction("Reset",this);
    a_reset->setEnabled(false);
    connect(a_reset, SIGNAL(triggered()), this, SLOT(resetProgram()));
    a_resetProgramAndModule = new QAction("Reset program and module", this);
    a_resetProgramAndModule->setEnabled(true);
    connect(a_resetProgramAndModule, SIGNAL(triggered()), this, SLOT(resetProgramAndModule()));
}


void PictomirCommon::loadTask(int index)
{
    saveProgramInTask();
    saveEnvironment();
    if (index<0 || index>=sch_current.tasks.size())
        return;
    sch_current.index = index;
    const Schema::Task task = sch_current.tasks[index];
    const Schema::Environment env = task.environment;
    const Schema::Program pr = task.program;

    if (!m_currentModule->loadData(env)) {
        QMessageBox::critical(0, tr("Error"), QApplication::translate("Core::PictomirCommon", "Environment data damaged"));
        return;
    }

    bool hasConditions = false;
    bool hasRepeaters = false;
    for (int i=0; i<pr.size(); i++) {
        if (pr.at(i).condition)
            hasConditions = true;
        if (pr.at(i).repeater)
            hasRepeaters = true;
    }

    if (m_program)
        delete m_program;

    m_program = new PictomirProgram(pr, m_currentModule, m_actionManager, this);
    connect (m_program, SIGNAL(structureChanged()), this, SIGNAL(programStructureChanged()));

    if (index==0) {
        b_prevTaskAvailable = false;
    }
    else {
        b_prevTaskAvailable = true;
    }
    if (index==sch_current.tasks.count()-1) {
        b_nextTaskAvailable = false;
    }
    else {
        b_nextTaskAvailable = true;
    }

    m_actionManager->setEnabledActionsList(m_currentModule,
                                       pr.size()>0,
                                       hasConditions,
                                       hasRepeaters,
                                       pr.size()-1);

    emit programStructureChanged();
    emit taskLoaded();
    emit taskHintChanged(task.hintMimeType, task.hintData);
}

PictomirProgram * PictomirCommon::program()
{
    return m_program;
}

void PictomirCommon::prepareLoadGame()
{
    saveProgramInTask();
    saveEnvironment();
    saveGame();
    m_gameProvider->init(sch_current.title);
}




ActionManager * PictomirCommon::actionManager()
{
    return m_actionManager;
}

void PictomirCommon::saveGame()
{
    if (s_currentUserRoot.isEmpty()) {
        return; // nothing to save for guest account
    }
    const QString jsonData = generateJSON(sch_current);
    QDir().mkpath(s_currentUserRoot);
    QString path = s_currentUserRoot+"/"+s_gameFileName;
    if (path.endsWith(".xml")) {
        path = path.left(path.length()-4)+".json";
    }
    QFile f(path);
    if (f.open(QIODevice::WriteOnly|QIODevice::Text)) {
        QTextStream ts(&f);
        ts.setCodec("UTF-8");
        ts.setGenerateByteOrderMark(true);
        ts << jsonData;
        f.close();
    }
    else {
        qDebug() << "Error opening " << path;
    }
    if (QFile::exists(s_currentUserRoot+"/_default.pm.xml")) {
        QFile().remove(s_currentUserRoot+"/_default.pm.xml");
    }
    QFile f2(s_currentUserRoot+"/_default.pm.json");
    if (f2.open(QIODevice::WriteOnly|QIODevice::Text)) {
        QTextStream ts(&f2);
        ts.setCodec("UTF-8");
        ts.setGenerateByteOrderMark(true);
        QString fileName = s_gameFileName;
        if (fileName.endsWith(".xml")) {
            fileName = fileName.left(fileName.length()-4)+".json";
        }
        ts << "\""+s_gameFileName+"\"";
        f.close();
    }
}


void PictomirCommon::saveProgramInTask()
{
    if (b_gameLoaded)
        sch_current.tasks[sch_current.index].program = m_program->data();
}

void PictomirCommon::saveEnvironment()
{
    if (b_gameLoaded)
        sch_current.tasks[sch_current.index].environment = m_currentModule->saveData2();
}


QString PictomirCommon::loadGameFromFile(const QString &fileName)
{
    if (b_gameLoaded) {
        saveProgramInTask();
        saveEnvironment();
        saveGame();
    }
    b_gameLoaded = false;
    bool gameDocument = false;
    QString fnAbsolute = resolveGameFileName(fileName, true);
    QString fnRelative = fileName;
    while (!gameDocument) {
        if (!QFile::exists(fnAbsolute)) {
            return QString::fromUtf8("Не найден файл: ")+fnAbsolute;
        }
        if (fnAbsolute.endsWith(".xml")) {
            QDomDocument doc;
            QFile f(fnAbsolute);
            QByteArray ba;
            if (f.open(QIODevice::ReadOnly)) {
                ba = f.readAll();
                f.close();
            }
            QString xmlError;
            int xmlLine, xmlCol;
            if (!doc.setContent(ba,&xmlError,&xmlLine,&xmlCol)) {
                return QString("XML error at line %1, col. %2:\n%3").
                        arg(xmlLine).arg(xmlCol).arg(xmlError);
            }
            if (doc.documentElement().nodeName()=="xlink") {
                QString href = doc.documentElement().attribute("href","");
                if (href.isEmpty())
                    return "Empty xlink href in "+fnAbsolute;
                fnAbsolute = resolveGameFileName(href, true);
                fnRelative = href;
            }
            else if (doc.documentElement().nodeName()=="game") {
                QDomNode gameNode = doc.documentElement();
                Schema::Game gama1;
                Schema::Game gama2;
                if (!Schema::parceAGKXML(gameNode, gama1, true)) {
                    return "Error parsing AGKXML!!!";
                }
                else {
                    sch_current = gama1;
                }
                if (!Schema::parceAGKXML(gameNode, gama2, false)) {
                    return "Error parsing AGKXML!!!";
                }
                else {
                    sch_initial = gama2;
                }
                gameDocument = true;
            }
            else {
                return "Not PictoMir XML!";
            }
        }
        else if (fnAbsolute.endsWith(".js") || fnAbsolute.endsWith(".json")) {
            QString jsData;
            QFile f(fnAbsolute);
            if (f.open(QIODevice::ReadOnly|QIODevice::Text)) {
                QTextStream ts(&f);
                ts.setCodec("UTF-8");
                ts.setAutoDetectUnicode(true);
                jsData = "currentGame = "+ts.readAll()+";";
                f.close();
            }
            QScriptEngine engine;
            engine.evaluate("var initialGame = null; var currentGame = null;");
            QScriptValue value = engine.evaluate(jsData);
            if (engine.hasUncaughtException()) {
                 int line = engine.uncaughtExceptionLineNumber();
                 qDebug() << "uncaught exception at line" << line << ":" << value.toString();
            }
            if (value.isError()) {
                return value.toString();
            }
            else if (value.isString()) {
                const QString link = value.toString();
                fnAbsolute = resolveGameFileName(link, true);
                fnRelative = link;
            }
            else if (value.isObject()) {
                Schema::Game gama1;
                Schema::Game gama2;
                bool readUserGameSuccess = false;
                if (Schema::parceJSON(value, gama1)) {
                    sch_current = gama1;
                    readUserGameSuccess = true;
                }

                bool readOriginGameSuccess = false;
                QString fnOrigin = resolveGameFileName(fnRelative, false);
                QFile f2(fnOrigin);
                if (f2.open(QIODevice::ReadOnly|QIODevice::Text)) {
                    QString jsData2;
                    QTextStream ts(&f2);
                    ts.setCodec("UTF-8");
                    ts.setAutoDetectUnicode(true);
                    jsData2 = "initialGame = "+ts.readAll()+";";
                    f2.close();
                    value = engine.evaluate(jsData2);
                    if (engine.hasUncaughtException()) {
                         int line = engine.uncaughtExceptionLineNumber();
                         qDebug() << "uncaught exception at line" << line << ":" << value.toString();
                    }
                    if (value.isError()) {
                        qDebug() << "JS ERROR : " << value.toString();
                    }
                    else if (value.isObject()) {
                        readOriginGameSuccess = Schema::parceJSON(value, gama2);
                    }
                }
                if (readOriginGameSuccess) {
                    sch_initial = gama2;
                }
                else {
                    sch_initial = gama1;
                }
                if (readUserGameSuccess) {
                    sch_current = gama1;
                }
                else if (readOriginGameSuccess) {
                    sch_current = gama2;
                }
                else {
                    return "Error parsing JSON!!!";
                }
                gameDocument = true;
            }
        }

    }
    int savedIndex = sch_current.index;
    if (m_program)
        m_program->deleteLater();
    m_program = 0;
    loadTask(savedIndex);
    s_gameFileName = fnRelative;
    b_gameLoaded = true;
    return "";
}

QString PictomirCommon::resolveGameFileName(const QString &baseName, bool isLocal) const
{
    const QString commonRoot = PlatformConfiguration::tasksPath();
    const QString localRoot = s_currentUserRoot;

    bool localExists = !s_currentUserRoot.isEmpty() && QFile::exists(localRoot+"/"+baseName);

    QDateTime localVersion = QFileInfo(localRoot+"/"+baseName).lastModified();
    QDateTime commonVersion = QFileInfo(commonRoot+"/"+baseName).lastModified();


    if (isLocal) {
        if (localExists && localVersion >= commonVersion) {
            return localRoot +"/"+ baseName;
        }
        else {
            return commonRoot +"/"+ baseName;
        }
    }
    else {
        return commonRoot +"/"+ baseName;
    }
}



void PictomirCommon::stopProgram()
{
    m_executor->stop();
    s_errorText = "";
}

void PictomirCommon::executeProgram(bool disableAnimation)
{

    s_errorText = "";
    m_executor->setAnimationEnabled(!disableAnimation);
    Robot25D::Plugin *ri = m_currentModule;
    if (ri==NULL) {
        qFatal("Robot widget not loaded!");
    }

    if (!m_executor->isInitialized()) {
        QStringList program = m_program->m68kProgram();
        m_executor->init(ri,program);
    }

    emit stateChanged(RunningProgram);
    m_executor->continuousRun();
}

void PictomirCommon::runToEnd()
{
    executeProgram(true);
}

void PictomirCommon::pauseProgram()
{
    Q_ASSERT ( m_executor->isInitialized() );
    emit stateChanged(PauseProgram);
    m_executor->pause();
}

void PictomirCommon::resetProgramAndModule()
{
    m_currentModule->loadData(sch_initial.tasks[sch_current.index].environment);
    resetProgram();
}

void PictomirCommon::resetProgram()
{
    emit stateChanged(EditingProgram);
}

void PictomirCommon::stepProgram()
{
    if (!m_executor->isInitialized()) {
        debugProgram();
    }
    else {
        m_executor->setAnimationEnabled(true);
        Robot25D::Plugin *ri = m_currentModule;
        Q_ASSERT_X( ri!=NULL,
                    "void PictomirCommon::stepProgram()",
                    "Robot_widget not loaded!");
        Q_ASSERT_X( m_executor->isInitialized(),
                    "void PictomirCommon::stepProgram()",
                    "Trying to step on not running program!");

        emit stateChanged(RunningProgram);
        emit information(QApplication::translate("Core::PictomirCommon", "Running step..."));
        Q_UNUSED(ri);
        m_executor->step();
    }
}

void PictomirCommon::debugProgram()
{
    s_errorText = "";
    m_executor->setAnimationEnabled(true);
    Robot25D::Plugin *ri = m_currentModule;
    Q_ASSERT_X( ri!=NULL,
                "void PictomirCommon::debugProgram()",
                "Robot_widget not loaded!");

    emit stateChanged(RunningProgram);
    QStringList program = m_program->m68kProgram();
    m_executor->init(ri, program);
    emit information(QApplication::translate("Core::PictomirCommon", "Begin per-step running..."));
    m_executor->step();
}

void PictomirCommon::handleStepFinished(const QString &message)
{
    emit stateChanged(PauseProgram);
    emit information(message);
}

void PictomirCommon::handleStepStarted(const QString &message)
{
    emit information(message);
}

void PictomirCommon::executionFinished(const QString &message)
{
    bool goToNextProgram = false;
    emit stateChanged(AnalisysProgram);
    if (!m_executor->isErrorTerminated()) {
        QObject *obj = m_currentModuleWidget;
        Q_CHECK_PTR(obj);
        quint16 unpaintedCells = quint16(obj->property("unpaintedPoints").toUInt());
        if (unpaintedCells) {
            int ends = unpaintedCells % 10;
            QString msg;
            if (ends == 1 && (unpaintedCells < 10 || unpaintedCells > 20) ) {
                msg = QApplication::translate("Core::PictomirCommon", "Unpainted cells left: %1","1").arg(unpaintedCells);
            }
            else if ( (ends>=2 && ends<=4) &&
                      !(ends>=10 && ends<20) &&
                      (unpaintedCells < 10 || unpaintedCells > 20)
                      )
            {
                msg = QApplication::translate("Core::PictomirCommon", "Unpainted cells left: %1","2-4").arg(unpaintedCells);
            }
            else {
                msg = QApplication::translate("Core::PictomirCommon", "Unpainted cells left: %1", "other").arg(unpaintedCells);
            }
            emit information(message+". "+msg);
        }
        else {
            emit taskSuccessed();
            goToNextProgram = true;
        }
    }
    if (goToNextProgram) {
        if (sch_current.index>=sch_current.tasks.size()-1) {
            emit information(QApplication::translate("Core::PictomirCommon", "Game finished!"));
        }
        else {
            emit information(QApplication::translate("Core::PictomirCommon", "Task finished!"));
        }

    }
    m_currentModule->finishEvaluation();
}

void PictomirCommon::executionError(const QString &msg)
{
    emit information(msg);
    s_errorText = msg;
    const QStack< QPair<int,int> > backtrace = m_executor->backtrace();
    QList<int> fn;
    QList<int> ci;
    for (int i=0; i<backtrace.size(); i++) {
        fn << backtrace[i].first;
        ci << backtrace[i].second;
    }
    emit executionError(fn, ci);
}


void PictomirCommon::loadNextTask()
{
    loadTask(sch_current.index+1);
}

void PictomirCommon::loadPreviousTask()
{
    loadTask(sch_current.index-1);
}


QString PictomirCommon::currentTaskTitle() const
{
    return sch_current.tasks[sch_current.index].title;
}


PictomirCommon::~PictomirCommon()
{
    if (m_program)
        delete m_program;
    if (m_executor)
        delete m_executor;
    if (m_currentModule)
        delete m_currentModule;
    if (m_actionManager)
        delete m_actionManager;
    if (m_gameProvider)
        delete m_gameProvider;
}

void PictomirCommon::prepareQuit()
{
    saveProgramInTask();
    saveEnvironment();
    saveGame();
}

}
