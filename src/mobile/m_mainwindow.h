#ifndef MainWindow_Smartphone_H
#define MainWindow_Smartphone_H

#include <QtCore>
#include <QtGui>

#include "core/enums.h"

namespace Core {
class PictomirCommon;
};

namespace Mobile {

class DockWidgetTouchscreen;
class SmartphoneButtonBox;
class FunctionWidget;
class SmartphoneMenuDialog;


class MainWindow:
        public QMainWindow
{
    Q_OBJECT;
public:
    explicit MainWindow(Core::PictomirCommon * pictomir, QWidget *parent = 0);
    inline QWidget *thisWidget() { return this; }
    bool controlsVisible() const;
public slots:
    void setTaskTitle(const QString &);
    void setStatusText(const QString &);
    void setInteractiveMode(Core::ProgramState);
    void setControlsVisible(bool);
    void relayoutWindow();
    void showMenuWidget();
protected:
    void resizeEvent(QResizeEvent *);
    void tabletEvent(QTabletEvent *);
    void createView();
    void createMenus();
protected slots:
    void handleTaskLoaded();
    void handleSelectorButtonClick(int index);
    void handleRunControlButtonClick(int index);
    void handleEditControlButtonClick(int index);
    void handleAnalisysButtonClick(int index);
    void handlePauseButtonClick(int index);
private:
    QList<Mobile::FunctionWidget*> wl_functions;
    Qt::Orientation e_currentOrientation;
    DockWidgetTouchscreen *w_dock;
    QStackedWidget *w_centralWidget;
//    QDockWidget *w_dockContainer;
//    QDockWidget *w_selectorContainer;
    QStackedWidget *w_leftBorder;
    QStackedWidget *w_bottomBorder;
    QStackedWidget *w_rightBorder;
    SmartphoneButtonBox *w_selector;
    SmartphoneButtonBox *w_runControl;
    SmartphoneButtonBox *w_editControl;
    SmartphoneButtonBox *w_analisysControl;
    SmartphoneButtonBox *w_pauseControl;
    SmartphoneMenuDialog *w_mainMenu;
    SmartphoneMenuDialog *w_taskDialog;
    QStackedWidget *w_controlWidget;
    QStringList sl_buttonsRun;
    QStringList sl_buttonsPause;
    QStringList sl_buttonsEdit;
    QStringList sl_buttonsAnalisys;
    QWidget *w_moduleWidget;
    Core::PictomirCommon *p_pictomir;
    bool b_showModule;
    Core::ProgramState e_currentState;
};

}

#endif
