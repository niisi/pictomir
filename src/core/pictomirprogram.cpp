#include "core/pictomirprogram.h"
#include "core/actionmodel.h"
#include "core/pictomirfunction.h"
#include "interfaces/functionwidget_interface.h"
#include "core/actionmanager.h"
#include "robot25d/default_plugin.h"
#include "schema/sch_command.h"

namespace Core {

//PictomirProgram::PictomirProgram( const QList<QSize> &sizes
//                                 , const QList<FunctionModifiers> &props
//                                 , const QList<QStringList> &hints
//                                 , Robot25D::Plugin *module
//                                 , ActionManager * actionManager
//                                 , QObject *parent)
//                                     : QObject(parent)
//{
//    m_module = module;
//    m_actionManager = actionManager;
//    b_modified = false;
//    Q_ASSERT(sizes.size()==props.size());
//    Q_ASSERT(props.size()==hints.size());
//    const QString ABC = QString::fromUtf8(" АБВГДЕЖЗИКЛМНОПРСТУФХЦЧШЩЪЫБЭЮЯ");
//    for (int i=0; i<sizes.count(); i++) {
//        const QString title = (i==0)? tr("Main") : tr("Alg %1").arg(ABC[i]);
//        PictomirFunction *f = new PictomirFunction(sizes[i],
//                                                   props[i],
//                                                   title,
//                                                   hints[i],
//                                                   actionManager,
//                                                   this);
//        connect (f, SIGNAL(removeRequest()), this, SLOT(handleRemoveRequest()));
//        connect (f, SIGNAL(changeRequest()), this, SIGNAL(structureChanged()));
//        l_functions << f;
//    }
//}

PictomirProgram::PictomirProgram(const Schema::Program &pr, Robot25D::Plugin *module, ActionManager *actionManager, QObject *parent)
    : QObject(parent)
    , m_module(module)
    , m_actionManager(actionManager)
{
    const QString ABC = trUtf8(" АБВГДЕЖЗИКЛМНОПРСТУФ", "letters");
    for (int i=0; i<pr.size(); i++) {
        Schema::Algorhitm alg = pr[i];
        FunctionModifiers props = PlainFunction;
        if (alg.condition)
            props |= ConditionFunction;
        if (alg.repeater)
            props |= RepeatFunction;
        PictomirFunction * f = addAlgorhitm(alg.size, props);
        const QString title = (i==0)? trUtf8("Главный") : trUtf8("Команда %1").arg(ABC[i]);
        f->setTitle(title);
        connect (f, SIGNAL(removeRequest()), this, SLOT(handleRemoveRequest()));
        connect (f, SIGNAL(changeRequest()), this, SIGNAL(structureChanged()));
    }
    loadProgram(pr);
}

void PictomirProgram::loadProgram(const Schema::Program &pr)
{
    Q_ASSERT( pr.size() == l_functions.size() );
    for (int i=0; i<pr.size(); i++) {
        Schema::Algorhitm alg = pr[i];
        PictomirFunction * f = l_functions[i];
        Q_CHECK_PTR(f);
        f->clearProgram();
        if (alg.condition) {
            ActionModel * m = m_actionManager->modelByStandartCommand(alg.conditionCommand);
            f->setCondition(m);
        }
        if (alg.repeater) {
            ActionModel * m = m_actionManager->modelByStandartCommand(alg.repeaterCommand);
            f->setRepeater(m);
        }
        for (int j=0; j<alg.commands.size(); j++) {
            ActionModel * m = m_actionManager->modelByStandartCommand(alg.commands[j]);
            f->setAction(j, m);
        }
        f->alterChange();
    }
}

Schema::Program PictomirProgram::data() const
{
    Schema::Program p;
    for (int i=0; i<l_functions.size(); i++) {
        Schema::Algorhitm a;
        PictomirFunction *f = l_functions[i];
        a.size = f->size();
        a.repeater = f->modifiers() & Core::RepeatFunction;
        a.condition  = f->modifiers() & Core::ConditionFunction;
        if (f->repeater()) {
            a.repeaterCommand = f->repeater()->standartCommand();
        }
        if (f->condition()) {
            a.conditionCommand = f->condition()->standartCommand();
        }
        a.commands = QVector<Schema::Command>(f->size().width()*f->size().height(), Schema::CmdNone);
        for (int j=0; j<a.commands.size(); j++) {
            ActionModel * m = f->action(j);
            if (m) {
                a.commands[j] = m->standartCommand();
            }
        }
        p << a;
    }
    return p;
}

QStringList PictomirProgram::m68kProgram() const
{
    QList<QStringList> program;
    for (int j=0; j<l_functions.size(); j++ ) {
        QStringList curF;
        int repCellIndex = -1;
        PictomirFunction *f = l_functions[j];
        ActionModel* repeater = f->repeater();
        ActionModel* condition = f->condition();
        int freeRegisterNo = 0;
        bool hasCondition = false;
        if (repeater) {
            bool isNumber;
            int num = repeater->actionName().toInt(&isNumber);
            if (isNumber) {
                freeRegisterNo++;
                curF << "copy B i"+QString::number(num);
                curF << "add B i1";
            }
            repCellIndex = curF.count();
            if (isNumber) {
                // Уменьшаем счетчик
                curF << "sub B i1";
                // Запоминаем значение в регистре C
                curF << "copy C A";
                // Сравниваем с 0
                curF << "cmp B i0";
                // Запоминаем результат сравнения в регистре D
                curF << "copy D A";
                // Вызываем подстветку повторителя
                // с аргументами: значение счетчика, номер функции
                curF << "push C";
                curF << "push i"+QString::number(j);
                curF << "int i253";
                // Восстанавливаем результат сравнения из регистра D
                curF << "copy A D";
                // Если он равен 0, то завершаем выполнение функции
                curF << "jz i%end%";
            }
            else {
                // Вызываем подсветку повторителя с аргументами:
                // 0, номер функции
                curF << "push i0";
                curF << "push i"+QString::number(j);
                curF << "int i253";
            }

        }
        if (condition) {
            hasCondition = true;
            int id = condition->actionId();
            // Указываем, что за команда
            QString actionText = condition->text();
            curF << "push s"+QString::fromAscii(actionText.toUtf8().toHex());
            curF << "int i251";
            // Указываем количество аргументов
            curF << "push i0";
            // Указываем, что нам требуется получить возвращаемое значение
            curF << "push b1";
            // Вызов функции с нужным id
            curF << "int i"+QString::number(id);
            // Вытаскиваем резултат в регистр C
            curF << "pop C";
            // Подстветка клетки-условия, аргументы:
            // номер функции, номер условия (в текущей реализации - всегда 0),
            // и логическое значение результата
            curF << "push C";
            curF << "push i"+QString::number(-1);
            curF << "push i"+QString::number(j);
            curF << "int i254";
            // Сообщаем, что шаг выполнен
            curF << "int i252";
            // Сравниваем результат (регистр C) с логическим значением "истина"
            curF << "cmp C b1";
            // Если ложь, то завершаем выполнение
            curF << "jnz i%end%";
        }
        int i = 0;
        bool hasActionsBeforeFunctionCall = false;
        const QPair<const QList<int>, const QList<const ActionModel*> > actions_and_indeces = f->nonEmptyActions();
        const QList<int> indeces = actions_and_indeces.first;
        const QList<const ActionModel*> actions = actions_and_indeces.second;
        for (int k=0; k<actions.size(); k++) {
            const ActionModel* c = actions[k];
            QString instr = c->actionName();
            if (instr.startsWith("external")) {
                hasActionsBeforeFunctionCall = true;
            }
            else if (instr.startsWith("f")) {
                break;
            }
        }
        const QString letters = trUtf8(" АБВГДЕЖЗИКЛМНОПРСТУФ","letters");
        if (j>0 && !hasActionsBeforeFunctionCall && !hasCondition) {
            // Выполнение алгоритма без команд робота в начале будем
            // считать отдельным шагом
            QString actionText = letters[i];
            curF << "push s"+QString::fromAscii(actionText.toUtf8().toHex());
            curF << "int i251";
            curF << "int i252";
        }
        for (int i=0; i<actions.size(); i++) {
            const ActionModel *c = actions[i];
            // Указываем, что за команда
            QString actionText = c->text();
            curF << "push s"+QString::fromAscii(actionText.toUtf8().toHex());
            curF << "int i251";
            QString instr = c->actionName();
            int highlightPause = 0;
            if (instr.length()==2 && instr.startsWith("f")) {
                // Если вызов функции, то нужна пауза после
                // подстветки, иначе её будет не видно
                highlightPause = 200;
            }
            curF << "push i"+QString::number(highlightPause);
            curF << "push i"+QString::number(j);
            curF << "push i"+QString::number(indeces[i]);
            curF << "int i255";
            // Запоминаем позицию для отладки
            QString posValue = QString::fromAscii(QString("%1,%2").
                                                  arg(j).arg(indeces[i]).
                                                  toUtf8().toHex());
            curF << "bti s"+posValue;
            if (instr.length()!=2 || !instr.startsWith("f")) {
                // Ожидание нажатия кнопки "Шаг"
                curF << "int i252";
                int id = c->actionId();
                curF << "push i0";
                curF << "push b0";
                curF << "int i"+QString::number(id);
            }
            if (instr.length()==2 && instr.startsWith("f")) {
                curF << "jsr i"+instr.mid(1);
            }
        }
        if (repCellIndex!=-1)
            curF << "jmp i"+QString::number(repCellIndex);
        program << curF;
    }
    QStringList toRet;
    for ( int i=0; i<program.count(); i++ ) {
        int size = program.at(i).size();
        toRet << program.at(i).join("\n").replace("%end%",QString::number(size));
    }
    return toRet;
}

QDomElement PictomirProgram::xmlProgram(QDomDocument & doc,
                                     const QString &gameName,
                                     int taskIndex) const
{
    QDomElement program = doc.toDocument().createElement("program");

    if (!gameName.isEmpty())
        program.setAttribute("game", gameName);
    if (taskIndex>-1)
        program.setAttribute("task", taskIndex);

    for (int i=0; i<l_functions.count(); i++) {
        QDomElement algorhitm = doc.createElement("alg");
        program.appendChild(algorhitm);
        PictomirFunction *f = l_functions[i];
        Q_CHECK_PTR(f);
        if (f->condition()) {
//            algorhitm.setAttribute("condition", f->condition()->shortcut());
        }
        if (f->repeater()) {
//            algorhitm.setAttribute("repeat", f->repeater()->shortcut());
        }

        for (int rowNo=0; rowNo<f->size().height(); rowNo++) {
            QDomElement row = doc.createElement("row");
            QStringList elements;
            for (int colNo=0; colNo<f->size().width(); colNo++) {
//                const ActionModel* c = f->action(colNo, rowNo);
//                elements << (c? c->shortcut() : "-");
            }
            QDomText rowText = doc.createTextNode(elements.join(" "));
            row.appendChild(rowText);
            algorhitm.appendChild(row);
        }
    }
    return program;
}

QList<QDomElement> PictomirProgram::algorhitmsStructure(QDomDocument &doc) const
{
    QList<QDomElement> result;
    for (int i=0; i<l_functions.size(); i++ ) {
        QDomElement alg = doc.createElement("alg");
        alg.setAttribute("width",l_functions[i]->size().width());
        alg.setAttribute("height",l_functions[i]->size().height());
        if (l_functions[i]->modifiers() & RepeatFunction)
            alg.setAttribute("repeat", "true");
        if (l_functions[i]->modifiers() & ConditionFunction)
            alg.setAttribute("condition", "true");
        result << alg;
    }
    return result;
}

QString PictomirProgram::htmlProgram(const QString &gameName, int taskIndex) const
{
    QString result = "<html>\n<head><meta content-type='text/html; charset=utf-8'>\n";
    QFile f(QApplication::applicationDirPath()+"/Pictomir/Stylesheets/program.css");
    if (f.open(QIODevice::ReadOnly|QIODevice::Text)) {
        QByteArray cssData = f.readAll();
        f.close();
        result += "<style type='text/css'>\n"+QString::fromAscii(cssData)+"\n</style>\n";
    }

    result += "</head><body>\n";
    QString functionData;
    QString functionName;
    QStringList fdata;
    for (int i=0; i<l_functions.count(); i++) {
        PictomirFunction *f = l_functions.at(i);
        functionName = "<p class='algName'>" + f->title()+"</p>\n";
        bool empty = true;
        int rows = f->size().height();
        int cols = f->size().width();
        functionData = "<table class='algorhitm'><tr><td class='algHeader'>\n";
        if ( f->repeater() || f->condition() ) {
            functionData += "<table><tr>\n";
            functionData += "<td class='repeatCell'>\n";
            if (f->repeater()) {
                functionData += f->repeater()->imageData();
                empty = false;
            }
            functionData += "</td></tr>\n<tr><td class='conditionCell'>\n";
            if (f->condition()) {
                functionData += f->condition()->imageData();
                empty = false;
            }
            functionData += "</td></tr></table>\n";
        }
        functionData += "</td>\n";
        functionData += "<td class='algorhitmBody'><table>\n";

        for (int row=0; row<rows; row++) {
            functionData += "<tr>\n";
            for (int col=0; col<cols; col++) {
                functionData += "<td class='plainCell'>\n";
                const ActionModel* c = f->action(col, row);
                if (c) {
                    empty = false;
                    functionData += c->imageData();
                }
                functionData += "</td>\n";
            }
            functionData += "</tr>\n";
        }
        functionData += "</table>\n";
        functionData += "</td></tr></table>\n";
        {
            result += functionName;
            result += functionData;

        }
    }
    result += "</body></html>\n";
    result += "<!-- EMBED\n";
    QDomDocument doc("program");
    QDomElement program = xmlProgram(doc, gameName, taskIndex);
    doc.appendChild(program);
    result += doc.toString(4);
    result += "END EMBED -->\n";
    return result;
}


QString PictomirProgram::kumirProgram() const
{
    QString result = QString::fromUtf8(
                "использовать Вертун\n"
                "\n"
                );
    static const QString Letters = QString::fromUtf8(" АБВГДЕЖЗИКЛМНОПРСТУФХЦЧШЩЭЮЯ");
    for (int i=0; i<l_functions.size(); i++) {
        if (i > 0) {
            result += "\n\n";
        }
        const QString algName = i == 0
                ? QString::fromUtf8("Главный")
                : QString::fromUtf8("Команда %1").arg(Letters.at(i));
        result += QString::fromUtf8("алг %1\n").arg(algName);
        result += QString::fromUtf8("нач\n");

        const PictomirFunction * f = l_functions.at(i);

        if (f->repeater()) {
            result += QString::fromUtf8("нц");
            bool isNumber;
            int num = f->repeater()->actionName().toInt(&isNumber);
            if (isNumber) {
                result += QString::fromUtf8(" %1 раз\n").arg(num);
            }
            else {
                result += "\n";
            }
        }
        if (f->condition()) {
            result += QString::fromUtf8("если ");
            const ActionModel *item = f->condition();
            const int id = item->actionId();
            result += functionNameById(id, QLocale::Russian);
            result += QString::fromUtf8(" то\n");
        }

        for (int row = 0; row < f->size().height(); row++) {
            for (int col = 0; col < f->size().width(); col++) {
                const ActionModel *item = f->action(col, row);
                if (item) {
                    if (item->actionName().startsWith("f")) {
                        const int no = item->actionName().mid(1).toInt();
                        result += QString::fromUtf8("Команда %1").arg(Letters.at(no));
                    }
                    else {
                        const int id = item->actionId();
                        const QString call = functionNameById(id, QLocale::Russian);
                        result += call;
                    }
                }
                result += "\n";
            }
        }

        if (f->condition())
            result += QString::fromUtf8("все\n");
        if (f->repeater())
            result += QString::fromUtf8("кц\n");

        result += QString::fromUtf8("кон");
    }
    return result;
}

void PictomirProgram::clearProgram()
{
    for (int i=0; i<l_functions.size(); i++) {
        l_functions[i]->clearProgram();
    }
}

PictomirProgram::~PictomirProgram()
{
    for (int i=0; i<l_functions.size(); i++) {
        delete l_functions[i];
    }
}

QString PictomirProgram::functionNameById(int id, QLocale::Language lang) const
{
    QList<Robot25D::Command> all = m_module->actions() + m_module->conditions();
    Robot25D::Command cmd;
    for (int i=0; i<all.size(); i++) {
        if (id==all.at(i).id) {
            cmd = all.at(i);
            break;
        }
    }

    if (cmd.name_i18n.contains(lang))
        return cmd.name_i18n[lang];
    else
        return cmd.name;
}


int PictomirProgram::functionsCount() const
{
    return l_functions.size();
}

PictomirFunction * PictomirProgram::function(int index) const
{
    Q_ASSERT(index<l_functions.size());
    Q_ASSERT(index>=0);
    return l_functions[index];
}

PictomirFunction* PictomirProgram::addAlgorhitm(const QSize &size, FunctionModifiers prop)
{
    b_modified = true;
    const QString algTable = trUtf8(" АБВГДЕЖЗИКЛМНОПРСТУФ", "letters");
    const QString title = l_functions.isEmpty()?
                          trUtf8("Главный") :
                          trUtf8("Алгоритм %1").arg(algTable[l_functions.size()]);
    PictomirFunction *p = new PictomirFunction(size, prop, title, QStringList(), m_actionManager, this);
    connect (p, SIGNAL(removeRequest()), this, SLOT(handleRemoveRequest()));
    connect (p, SIGNAL(changeRequest()), this, SIGNAL(structureChanged()));
    l_functions << p;
    emit structureChanged();
    return p;
}

void PictomirProgram::removeAlgorhitm(int index)
{
    b_modified = true;
    Q_ASSERT ( index >= 0 );
    Q_ASSERT ( index < l_functions.size() );
    l_functions.at(index)->deleteLater();
    l_functions.removeAt(index);
    const QString algTable = trUtf8(" АБВГДЕЖЗИКЛМНОПРСТУФ", "letters");
    for (int i = 0; i<l_functions.size(); i++ ) {
        const QString title = i==0?
                              trUtf8("Главный") :
                              trUtf8("Алгоритм %1").arg(algTable[i]);
        l_functions[i]->setTitle(title);

    }
    emit structureChanged();
}

void PictomirProgram::handleRemoveRequest()
{
    PictomirFunction* f = qobject_cast<PictomirFunction*>(sender());
    Q_CHECK_PTR(f);
    Q_ASSERT ( l_functions.contains(f) );
    removeAlgorhitm(l_functions.indexOf(f));
}

bool PictomirProgram::isModified() const
{
    if (b_modified)
        return true;
    for (int i=0; i<l_functions.size(); i++) {
        if (l_functions[i]->isModified()) {
            return true;
        }
    }
    return false;
}
}
