#!/bin/sh
cd src
qmake "WITH_PHONON=yes" pictomir.pro || qmake-qt4 "WITH_PHONON=yes" pictomir.pro 
make && echo "Now copy 'bin', 'lib' (or 'lib64') and 'share' directories to your package root"
