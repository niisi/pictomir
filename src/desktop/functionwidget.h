#ifndef FUNCTIONWIDGET_MOUSE_H
#define FUNCTIONWIDGET_MOUSE_H

#include <QtCore>
#include <QtGui>
#include <QtSvg>
#include "interfaces/functionwidget_interface.h"
#include "core/actiontype.h"

namespace Core {

class ActionModel;
class PictomirFunction;

}

namespace Util {

class SvgButton;
}

namespace Desktop {
class CurtainItem;
class FWMCell;
class ActionProgramView;
class TextItem;

class FunctionWidget :
        public Core::FunctionWidgetInterface
{
    Q_OBJECT
public:
    explicit FunctionWidget(Core::PictomirFunction *f, QGraphicsItem *parent = 0);
    Core::PictomirFunction* model();
    void connectSignalActivationChanged(QObject *obj, const char *method);
//    QRectF boundingRect() const;
    ~FunctionWidget();
    inline bool isLocked() const { return b_locked; }

protected:
//    void paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);



public slots:
    void lock();
    void unlock();
    void setFunctionTitle(const QString &title);

signals:
    void activationChanged();
    void deleteRequest();
    void changeRequest(const QSize &size, bool rep, bool cond);


public slots:
    void highlightPlainCell(int index);
    void highlightErrorCell(int index);
    void highlightConditionalValue(bool value);
    void unhighlight();
    void loadProgramFromModel();

protected slots:
    void handleActionClicked(ActionProgramView *view, bool rightClick);
    void handleActionClicked(bool rightClick);
    void handleActionDragged();



private:
    Core::PictomirFunction *m_function;
    CurtainItem* g_curtain;
    QList<FWMCell*> l_cells;
    QGraphicsPathItem * lines;
    FWMCell *m_highlightedCell;
    bool b_locked;

    TextItem *g_heading;
};


class FWMCell:
        public QGraphicsObject
{
    Q_OBJECT;
    Q_PROPERTY(QColor color READ color WRITE setColor);
    Q_PROPERTY(int highlighterPixmap READ highlighterPixmap WRITE setHighlighterPixmap);
    Q_PROPERTY(qreal highlighterOpacity READ highlighterOpacity WRITE setHighlighterOpacity);
    Q_PROPERTY(bool highlighterError READ highlighterError WRITE setHighlighterError);
public:
    explicit FWMCell(FunctionWidget *fw, Core::PictomirFunction*f, Core::ActionType actionType, int index, QGraphicsItem* parent = 0);
    inline QColor color() const { return cl_color; }
    inline void setColor(const QColor &v) { cl_color = v; update(); }
    inline int index() { return i_index; }
    inline QRectF boundingRect() const { return QRectF(0, 0, 40, 40); }
    void unsetItemInside();
    void insertItem(Core::ActionModel *model);
    inline int highlighterPixmap() const { return i_highlightPixmap; }
    inline qreal highlighterOpacity() const { return r_highlighterOpacity; }
    void setHighlighterPixmap(int frameNo);
    void setHighlighterOpacity(qreal value);
    ~FWMCell();
public slots:
    void highlight();
    void unhighlight();
    void setHighlighterError(bool v);
signals:
    void imitateActionClicked(ActionProgramView *view, bool rightClick);
protected:
    void createHighlighterObjects();
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *);
    void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
    void hoverLeaveEvent(QGraphicsSceneHoverEvent *event);
    void dragEnterEvent(QGraphicsSceneDragDropEvent *event);
    void dragLeaveEvent(QGraphicsSceneDragDropEvent *event);
    void dropEvent(QGraphicsSceneDragDropEvent *event);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    inline bool highlighterError() const { return b_errorHighlight; }
    void createAnimationPath();
protected slots:
    void handleHighlightAnimationFinished();
private:
    bool canInsertActiveItem() const;
    bool canInsertItem(const Core::ActionModel *model) const;
    void insertActiveItem();
    Core::PictomirFunction *f_ptr;
    FunctionWidget *fw_ptr;
    Core::ActionType e_actionType;
    int i_index;
    QPropertyAnimation *an_hover;
    QPropertyAnimation *an_unhover;
    QPropertyAnimation *an_highlighter;
    QPropertyAnimation *an_unhighlighter;
    QColor cl_color;
    bool b_mousePressed;
    bool b_hovered;
    bool b_hasItemInside;
    QGraphicsPixmapItem *px_highlighter;
    QVector<QPixmap> v_plainHighlighters;
    QVector<QPixmap> v_errorHighlighters;
    bool b_errorHighlight;
    int i_highlightPixmap;
    ActionProgramView *m_viewInside;
    QPainterPath p_animationPath;
    qreal r_highlighterOpacity;
    QRadialGradient m_highlighterGradient;

};

class CurtainItem:
        public QGraphicsObject
{
    Q_OBJECT;
    Q_PROPERTY(qreal openingState READ openingState WRITE setOpeningState);
public:
    explicit CurtainItem(const QSize &size, QGraphicsItem *parent = 0);
    QRectF boundingRect() const;
    qreal openingState() const { return r_state; }
    void setOpeningState(qreal v);
public slots:
    void closeCurtain();
    void openCurtain();
protected:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *);
protected slots:
    void handleOpenAnimationFinished();
private:
    QPropertyAnimation *an_close;
    QPropertyAnimation *an_open;
    QSize sz_size;
    qreal r_state;
    bool b_closed;

};

class TextItem:
        public QGraphicsTextItem
{
    Q_OBJECT;
public:
    explicit TextItem(const QString &txt, int maxW, QGraphicsItem *parent = NULL);
private:
    int i_maxW;
    QPropertyAnimation *animation;

};

}

#endif // FUNCTIONWIDGET_MOUSE_H
