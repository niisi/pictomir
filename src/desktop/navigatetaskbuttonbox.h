#ifndef NAVIGATETASKBUTTONBOX_H
#define NAVIGATETASKBUTTONBOX_H

#include <QtCore>
#include <QtGui>
#include <QtSvg>

namespace Util {
class SvgButton;

}

namespace Desktop {



class NavigateTaskButtonBox : public QObject, public QGraphicsRectItem
{
    Q_OBJECT
public:
    explicit NavigateTaskButtonBox(QGraphicsItem *parent = 0);
public slots:
    void enablePrev();
    void enableNext();
    void disablePrev();
    void disableNext();
    void alertNext();
    void unalertNext();
signals:
    void nextTaskClicked();
    void prevTaskClicked();
    void addNextTaskClicked();
    void addPrevTaskClicked();
    void removeCurrentTaskClicked();

protected slots:
    void handleButtonClicked();
private:
    Util::SvgButton *g_prev;
    Util::SvgButton *g_next;

    Util::SvgButton *g_addNext;
    Util::SvgButton *g_removeCurrent;
    Util::SvgButton *g_addPrev;

};

}

#endif // NAVIGATETASKBUTTONBOX_H
