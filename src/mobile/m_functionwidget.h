#ifndef FUNCTIONWIDGETTOUCHSCREEN_H
#define FUNCTIONWIDGETTOUCHSCREEN_H

#include <QtCore>
#include <QtGui>
#include <QtSvg>

#include "interfaces/functionwidget_interface.h"
#include "core/actiontype.h"
#include "core/enums.h"

namespace Core {

class PictomirFunction;
class ActionModel;
class ActionManager;

}

namespace Desktop {

class ActionProgramView;
}

namespace Mobile {

class FWTCell;

class FunctionWidget:
        public Core::FunctionWidgetInterface
{
    Q_OBJECT;
public:
    explicit FunctionWidget(Core::PictomirFunction *f, QGraphicsItem *parent = 0);
    Core::PictomirFunction* model();
    void connectSignalActivationChanged(QObject *obj, const char *method);
public slots:
    void lock();
    void unlock();
    void setOrientation(Qt::Orientation);
    void setFunctionTitle(const QString &title);

signals:
    void activationChanged();


public slots:
    void highlightPlainCell(int index);
    void highlightErrorCell(int index);
    void highlightConditionalValue(bool value);
    void unhighlight();
    void relayout();

protected slots:
    void handleActionClicked(Desktop::ActionProgramView *view, bool rightClick);
    void handleActionClicked(bool rightClick);
    void loadProgramFromModel();

private:
    Qt::Orientation e_currentOrientation;
    Core::PictomirFunction *m_function;
    QList<FWTCell*> l_cells;
    FWTCell *m_highlightedCell;
};

class FWTCell:
        public QGraphicsObject
{
    Q_OBJECT;
    Q_PROPERTY(QColor color READ color WRITE setColor);
    Q_PROPERTY(qreal highlighterState READ highlighterState WRITE setHighlighterState);
    Q_PROPERTY(qreal highlighterOpacity READ highlighterOpacity WRITE setHighlighterOpacity);
    Q_PROPERTY(QColor highlighterColor READ highlighterColor WRITE setHighlighterColor);
public:
    explicit FWTCell(FunctionWidget *fw, Core::PictomirFunction*f, Core::ActionType actionType, int index, QGraphicsItem* parent = 0);
    inline QColor color() const { return cl_color; }
    inline void setColor(const QColor &v) { cl_color = v; update(); }
    inline int index() { return i_index; }
    inline QRectF boundingRect() const { return QRectF(0, 0, 40, 40); }
    void unsetItemInside();
    void insertItem(Core::ActionModel *model);
    inline qreal highlighterState() const { return r_highlighterState; }
    inline qreal highlighterOpacity() const { return r_highlighterOpacity; }
    void setHighlighterState(qreal value);
    void setHighlighterOpacity(qreal value);
    ~FWTCell();
public slots:
    void highlight();
    void unhighlight();
    void changeHighlightColor(const QColor &color);
    void setHighlighterColor(const QColor &color);
signals:
    void imitateActionClicked(Desktop::ActionProgramView *view, bool rightClick);
protected:
    void createHighlighterObjects();
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    inline QColor highlighterColor() const { return cl_highlighterColor; }
    void createAnimationPath();
protected slots:
    void handleHighlightAnimationFinished();
private:

    bool canInsertActiveItem() const;
    bool canInsertItem(const Core::ActionModel *model) const;
    void insertActiveItem();
    Core::PictomirFunction *f_ptr;
    FunctionWidget *fw_ptr;
    Core::ActionType e_actionType;
    int i_index;
    QPropertyAnimation *an_highlighter;
    QPropertyAnimation *an_unhighlighter;
    QPropertyAnimation *an_changeColor;
    QColor cl_color;
    bool b_mousePressed;
    bool b_hovered;
    bool b_hasItemInside;
    QVector<QGraphicsEllipseItem*> v_highlighterObjects;
    Desktop::ActionProgramView *m_viewInside;
    QPainterPath p_animationPath;
    qreal r_highlighterState;
    qreal r_highlighterOpacity;
    QColor cl_highlighterColor;
    QRadialGradient m_highlighterGradient;

};

}

#endif // FUNCTIONWIDGETTOUCHSCREEN_H
