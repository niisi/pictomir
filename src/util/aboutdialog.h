#ifndef ABOUTDIALOG_H
#define ABOUTDIALOG_H

#include <QDialog>

namespace Util {

namespace Ui {
    class AboutDialog;
}

class AboutDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AboutDialog(const QString &pixmapPath, const QString &licensePath, QWidget *parent = 0);
    ~AboutDialog();

private:
    Ui::AboutDialog *ui;
};


} // namespace Util
#endif // ABOUTDIALOG_H
