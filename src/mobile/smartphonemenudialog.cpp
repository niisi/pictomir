#include "smartphonemenudialog.h"
#include "ui_smartphonemenudialog.h"

namespace Mobile {

SmartphoneMenuDialog::SmartphoneMenuDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SmartphoneMenuDialog)
{
    ui->setupUi(this);
    connect (ui->listWidget, SIGNAL(itemChanged(QListWidgetItem*)),
             this, SLOT(handleItemClicked(QListWidgetItem*)));
}

void SmartphoneMenuDialog::init(const QStringList &items, int selectedNo)
{
    ui->listWidget->clear();
    ui->listWidget->addItems(items);
    if (selectedNo>-1) {
        ui->listWidget->setCurrentRow(selectedNo);
        ui->btnOK->setEnabled(true);
    }
    else {
        ui->btnOK->setEnabled(false);
    }
}

SmartphoneMenuDialog::~SmartphoneMenuDialog()
{
    delete ui;
}

void SmartphoneMenuDialog::handleItemClicked(QListWidgetItem *item)
{
    ui->btnOK->setEnabled(item!=0);
}

int SmartphoneMenuDialog::selectedItemIndex() const
{
    return ui->listWidget->currentRow();
}

}
