#ifndef PICTOMIRPROGRAM_H
#define PICTOMIRPROGRAM_H

#include <QtCore>
#include <QtGui>
#include <QtXml>

#include "enums.h"
#include "schema/sch_program.h"

namespace Robot25D {
    class Plugin;
}

namespace Core {

class PictomirFunction;
class FunctionWidgetInterface;
class ActionManager;

class PictomirProgram : public QObject
{
    Q_OBJECT
public:
//    explicit PictomirProgram(  const QList<QSize>& sizes
//                             , const QList<FunctionModifiers> &props
//                             , const QList<QStringList> &hints
//                             , Robot25D::Plugin *module
//                             , ActionManager * actionManager
//                             , QObject *parent = 0);

    explicit PictomirProgram( const Schema::Program &pr, Robot25D::Plugin *module, ActionManager * actionManager, QObject * parent = 0);

    Schema::Program data() const;

    bool isModified() const;
    QString kumirProgram() const;
    QStringList m68kProgram() const;
    QString htmlProgram(const QString &gameName, int taskIndex) const;

    QList<QDomElement> algorhitmsStructure(QDomDocument &doc) const;
    PictomirFunction * function(int index) const;
    QDomElement xmlProgram(QDomDocument &doc,
                           const QString &gameName,
                           int taskIndex) const;
    ~PictomirProgram();

    int functionsCount() const;
public slots:
    void loadProgram(const Schema::Program &pr);
    void clearProgram();
    PictomirFunction* addAlgorhitm(const QSize &size, FunctionModifiers prop);
    void removeAlgorhitm(int index);

protected:
    QString functionNameById(int id, QLocale::Language lang) const;

protected slots:
    void handleRemoveRequest();

signals:
    void programLoaded();
    void structureChanged();

private:
    QList<PictomirFunction*> l_functions;
    Robot25D::Plugin *m_module;
    ActionManager *m_actionManager;
    bool b_modified;

};

}

#endif // PICTOMIRPROGRAM_H
