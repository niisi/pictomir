#ifndef ABSTRACTREADER_H
#define ABSTRACTREADER_H

#include <QtCore>
#include "structures.h"


namespace Util {
    class ContentProvider;
}


namespace Core {


    class AbstractReader {
    public:
        virtual QString parseData(const QByteArray &data, const QUrl &baseUrl, Game &game) const = 0;
        virtual void setContentProvider(Util::ContentProvider * provider) = 0 ;
        virtual Util::ContentProvider * contentProvider() = 0;
    };
}

#endif // ABSTRACTREADER_H
