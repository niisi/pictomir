#ifndef DockWidget_Mouse_H
#define DockWidget_Mouse_H

#include <QtCore>
#include <QtGui>
#include <QtSvg>

#include "interfaces/dockwidget_interface.h"
#include "core/enums.h"

namespace Core {

class ActionModel;
class ActionView;
class ActionManager;

}

namespace Desktop {

class ActionDockView;

class DockWidget :
        public QObject,
        public QGraphicsRectItem,
        public Core::DockWidgetInterface
{
    Q_OBJECT;
public:
    DockWidget(QGraphicsItem *parent = 0);
    void connectSignalTouched(QObject *obj, const char *method);
    void connectSignalActivationChanged(QObject *obj, const char *method);
    Core::ActionManager * actionManager();
    void setActionManager(Core::ActionManager *);
    void setDrawBackground(bool v);
    void setTableColor(const QColor &c);
    void setTextColor(const QColor &c);
    int width() const;
    int height() const;
    void setRect(const QRectF &rect);
public slots:
    void updateVisiblity();
protected:
    void addSpacer();
//    void paintEvent(QPaintEvent *event);
//    void resizeEvent(QResizeEvent *event);
    void relayoutDock();
protected slots:
    void handleItemClicked();
    void handleItemText(const QString &text);
private:

    QList<Core::ActionModel*> l_actions;
    QList<Desktop::ActionDockView*> l_items;
    int i_dockWidth;
    QList<int> l_spacers;
    QPropertyAnimation *an_textSliding;
    QGraphicsTextItem *g_overText;
    Core::ActionManager * m_actionManager;
    QGraphicsPolygonItem * g_table;
    QGraphicsRectItem *g_background;
signals:
    void touched();
    void activationChanged();
};

}

#endif
