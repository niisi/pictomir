#include "mobile/smartphonebuttonbox.h"
#include "util/svgbutton.h"
#include "core/platform_configuration.h"

namespace Mobile {

SmartphoneButtonBox::SmartphoneButtonBox(const QStringList &buttons, QWidget *parent)
    : QGraphicsView(parent)
{
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setScene(new QGraphicsScene);
    for (int i=0; i<buttons.size(); i++) {
        if (buttons.at(i).isEmpty()) {
            l_spacers << l_buttons.size();
        }
        else {
//            SvgButton *g;
//            g = new SvgButton(buttons[i]);
//            g->setScale(1.0);
//            scene()->addItem(g);
//            l_buttons << g;
        }
    }
    b_pressed = 0;
    r_offset = 0;
    g_pressed = 0;
    relayout();
}

void SmartphoneButtonBox::setDockPosition(Qt::DockWidgetArea position)
{
    e_currentPosition = position;
    r_offset = 0.0;
    relayout();
}

void SmartphoneButtonBox::relayout()
{
    setSceneRect(0,0,width(),height());
//    qreal maxSize = 30;
//    for (int i=0; i<l_buttons.size(); i++) {
//        qreal scaleX = maxSize / l_buttons[i]->boundingRect().width();
//        qreal scaleY = maxSize / l_buttons[i]->boundingRect().height();
//        qreal scale = qMin(scaleX, scaleY);
//        qDebug() << "Scale: " << scale;
//        l_buttons[i]->scale(scale, scale);
//    }
    qreal start = 5 + r_offset;
    for (int i=0; i<l_buttons.size(); i++) {
        if (l_spacers.contains(i))
            start += 20;
        if (e_currentPosition==Qt::BottomDockWidgetArea || e_currentPosition==Qt::TopDockWidgetArea) {
            l_buttons[i]->setPos(start, 5);
        }
        else {
            l_buttons[i]->setPos(5, start);
        }
        start += 35;
    }
    update();
}

void SmartphoneButtonBox::resizeEvent(QResizeEvent *event)
{
    relayout();
    event->accept();
}

void SmartphoneButtonBox::paintEvent(QPaintEvent *event)
{
    QPainter p(viewport());
    p.save();
    p.setRenderHint(QPainter::Antialiasing, true);
    p.setPen(Qt::NoPen);
    p.setBrush(QColor("lightsteelblue"));
    p.drawRoundedRect(0,0,width(),height(),10,10);
    if (e_currentPosition==Qt::LeftDockWidgetArea) {
        p.drawRect(0,0,10,height());
    }
    else if (e_currentPosition==Qt::RightDockWidgetArea) {
        p.drawRect(width()-10,0,10,height());
    }
    else if (e_currentPosition==Qt::TopDockWidgetArea) {
        p.drawRect(0,0,width(),10);
    }
    else if (e_currentPosition==Qt::BottomDockWidgetArea) {
        p.drawRect(0, height()-10, width(), 10);
    }
    p.restore();
    QGraphicsView::paintEvent(event);
}

void SmartphoneButtonBox::setButtonsEnabled(const QList<bool> &buttons)
{
    Q_ASSERT(buttons.size()==l_buttons.size());
    for (int i=0; i<buttons.size(); i++) {
        l_buttons[i]->setVisible(buttons[i]);
    }
}

void SmartphoneButtonBox::mousePressEvent(QMouseEvent *event)
{
    pnt_pressed = event->pos();
    b_pressed = true;
    g_pressed = scene()->itemAt(event->pos());
    event->accept();
}

void SmartphoneButtonBox::mouseReleaseEvent(QMouseEvent *event)
{
    if (!b_pressed) {
        event->ignore();
        return;
    }
    if (g_pressed && scene()->itemAt(event->pos())==g_pressed) {
        // item click
#ifndef QT_NO_DYNAMIC_CAST
        Util::SvgButton *svg_pressed = dynamic_cast<Util::SvgButton*>(g_pressed);
#else
		Util::SvgButton *svg_pressed = (Util::SvgButton*)(g_pressed);
#endif
        int index = l_buttons.indexOf(svg_pressed);
        emit iconPressed(index);
    }
    else {
        // just stop moving
    }
    event->accept();
}


void SmartphoneButtonBox::mouseMoveEvent(QMouseEvent *event)
{
    if (!b_pressed) {
        event->ignore();
        return;
    }
    int offset_x = event->pos().x()-pnt_pressed.toPoint().x();
    int offset_y = event->pos().y()-pnt_pressed.toPoint().y();
    int offset;
    if (e_currentPosition==Qt::TopDockWidgetArea || e_currentPosition==Qt::BottomDockWidgetArea)
        offset = abs(offset_x);
    else
        offset = abs(offset_y);
    bool slide = offset >= QApplication::startDragDistance();
    if (slide) {
        if (e_currentPosition==Qt::TopDockWidgetArea || e_currentPosition==Qt::BottomDockWidgetArea)
            r_offset += event->pos().x() - pnt_pressed.x();
        else
            r_offset += event->pos().y() - pnt_pressed.y();
        relayout();
        pnt_pressed = event->pos();
    }
    event->accept();
}

void SmartphoneButtonBox::setCurrentIcon(int index)
{
    i_currentIcon = index;
    update();
}

}
