#ifndef XMLAGKREADER_H
#define XMLAGKREADER_H

#include <QtCore>
#include "abstractreader.h"
#include "structures.h"

namespace Core {

    class XmlAgkReader
            : public QObject
            , public AbstractReader
    {
    public:
        QString parseData(const QByteArray &data, const QUrl &base, Game &game) const;
    };

}

#endif // XMLAGKREADER_H
