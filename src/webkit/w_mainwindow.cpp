#include "webkit/w_mainwindow.h"
#include "ui_w_mainwindow.h"
#include "webkit/webtab.h"
#include "core/platform_configuration.h"
#include "core/pictomir.h"
#include "util/aboutdialog.h"

namespace Webkit {

MainWindow::MainWindow(Core::PictomirCommon * pictomir, QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , m_pictomir(pictomir)
{
    ui->setupUi(this);
    setWindowTitle(tr("PictoMir"));
    connect(qApp, SIGNAL(aboutToQuit()), this, SLOT(saveSettings()));
    connect(ui->tabWidget, SIGNAL(currentChanged(int)), this, SLOT(tabChanged(int)));
    connect(ui->tabWidget, SIGNAL(tabCloseRequested(int)), this, SLOT(tabCloseRequest(int)));

    QToolButton * btnNewTab = new QToolButton(this);
    btnNewTab->setObjectName("btnNewTab");
    btnNewTab->setText("+");
    btnNewTab->setAutoRaise(true);
    btnNewTab->setIcon(QIcon(Core::PlatformConfiguration::resourcesPath()+"/ui_webkit/icons/add.png"));
    btnNewTab->setToolTip(tr("Add new tab"));
    connect(btnNewTab, SIGNAL(clicked()), this, SLOT(createNewTab()));
    ui->tabWidget->setCornerWidget(btnNewTab, Qt::TopLeftCorner);

#ifndef Q_OS_MAC
    QToolButton * btnToggleMenu = new QToolButton(this);
    btnToggleMenu->setObjectName("btnToggleMenu");
    btnToggleMenu->setText("M");
    btnToggleMenu->setAutoRaise(true);
    btnToggleMenu->setIcon(QIcon(Core::PlatformConfiguration::resourcesPath()+"/ui_webkit/icons/menu.png"));
    btnToggleMenu->setToolTip(tr("Toggle menu visiblity"));
    connect(btnToggleMenu, SIGNAL(clicked()), this, SLOT(btnShowMenuClicked()));
    ui->tabWidget->setCornerWidget(btnToggleMenu, Qt::TopRightCorner);
#endif

    connect(ui->actionNew_tab, SIGNAL(triggered()), this, SLOT(createNewTab()));
    connect(ui->tabWidget, SIGNAL(newTabRequest()), this, SLOT(createNewTab()));
    connect(ui->actionClose_tab, SIGNAL(triggered()), this, SLOT(closeCurrentTab()));
    connect(ui->actionQuit, SIGNAL(triggered()), qApp, SLOT(quit()));
    connect(ui->actionUsage, SIGNAL(triggered()), this, SLOT(showUsage()));
    connect(ui->actionAbout, SIGNAL(triggered()), this, SLOT(showAbout()));
#ifndef Q_OS_MAC
    connect(ui->actionShow_menubar, SIGNAL(triggered(bool)), this, SLOT(toggleMenubar(bool)));
#endif
#ifdef Q_OS_MAC
    delete ui->actionShow_menubar;
#endif
    connect(ui->actionFullscreen, SIGNAL(triggered(bool)), this, SLOT(toggleFullscreen(bool)));
    restoreSettings();

    addAction(ui->actionNew_tab);
    addAction(ui->actionClose_tab);
    addAction(ui->actionQuit);
#ifndef Q_OS_MAC
    addAction(ui->actionShow_menubar);
#endif
    addAction(ui->actionFullscreen);
    addAction(ui->actionUsage);

    const QString lang = qApp->property("language").toString();
    m_about = new Util::AboutDialog(Core::PlatformConfiguration::resourcesPath()+"/navigator_icon.png",
                                  Core::PlatformConfiguration::resourcesPath()+"/about_navigator_"+lang+".html", this);

}


void MainWindow::createNewTab()
{
    WebTab * tab = addTab(NULL);
    ui->tabWidget->setCurrentWidget(tab);
    tab->goHome();
    tab->setAddressLineFocus();
}

void MainWindow::createNewTab(QWebPage * page)
{
    WebTab * tab = addTab(page);
    ui->tabWidget->setCurrentWidget(tab);
}

void MainWindow::showUsage()
{
    WebTab * tab = addTab(NULL);
    ui->tabWidget->setCurrentWidget(tab);
    QString language = qApp->property("language").toString();
    tab->setUrl(QUrl(QString("local://%1/navigator.pms.html").arg(language)));
}

void MainWindow::showAbout()
{
    m_about->exec();
}

void MainWindow::btnShowMenuClicked()
{
    toggleMenubar(!menuBar()->isVisible());
}

void MainWindow::toggleMenubar(bool v)
{
    if (sender()!=ui->actionShow_menubar)
        ui->actionShow_menubar->setChecked(v);
    menuBar()->setVisible(v);
}

void MainWindow::toggleFullscreen(bool v)
{
    setWindowState(v? Qt::WindowFullScreen : Qt::WindowNoState);
}

void MainWindow::closeCurrentTab()
{
    if (ui->tabWidget->count()>1) {
        ui->actionClose_tab->setEnabled(ui->tabWidget->count()>2);
        delete ui->tabWidget->currentWidget();
    }
}

void MainWindow::restoreSettings()
{
    QSettings settings;
    QRect r = settings.value("WebView/Geometry").toRect();
    if (!r.isValid()) {
        r.setSize(QSize(1000, 700));
    }
    resize(r.size());
    move(r.topLeft());

    QString startUrl;
    for (int i=1; i<qApp->argc(); i++) {
        const QString arg = qApp->arguments()[i];
        if (!arg.startsWith("-")) {
            startUrl = arg;
        }
    }

    toggleMenubar(settings.value("WebView/MenuBarVisible", false).toBool());
    toggleFullscreen(settings.value("WebView/FullScreen", false).toBool());
    if (startUrl.isEmpty()) {
        WebTab * tab = addTab(NULL);
        QString language = qApp->property("language").toString();
        tab->setUrl(QUrl(QString("local://")+language+"/"));
        ui->tabWidget->setCurrentIndex(0);
    }
    else {
        WebTab * tab = addTab(NULL);
        tab->setUrl(QUrl(startUrl));
        ui->tabWidget->setCurrentIndex(0);
    }
}

void MainWindow::saveSettings()
{
    QSettings settings;
    settings.setValue("WebView/Geometry", QRect(pos(), size()));
    QList<QVariant> vals;
    settings.setValue("WebView/MenuBarVisible", menuBar()->isVisible());
    settings.setValue("WebView/FullScreen", isFullScreen());
    settings.sync();
}

WebTab * MainWindow::addTab(QWebPage * page)
{
    WebTab * tab;
    if (page) {
        tab = new WebTab(page, this);
    }
    else {
        tab = new WebTab(this);
    }
    connect(tab,SIGNAL(iconChanged()), this, SLOT(tabIconChanged()));
    connect(tab,SIGNAL(titleChanged(QString)), this, SLOT(tabTitleChanged(QString)));
    connect(tab,SIGNAL(newPageCreated(QWebPage*)), this, SLOT(createNewTab(QWebPage*)));
    ui->tabWidget->addTab(tab, tr("New Tab"));
    ui->actionClose_tab->setEnabled(ui->tabWidget->count()>1);
    return tab;
}

void MainWindow::tabChanged(int no)
{
    const QString title = ui->tabWidget->widget(no)->windowTitle();
    setWindowTitle(tr("%1 - PictoMir").arg(title));
}

void MainWindow::tabCloseRequest(int no)
{
    QWidget * w = ui->tabWidget->widget(no);
    w->deleteLater();
}

void MainWindow::tabTitleChanged(const QString &t)
{
    QWidget * w = qobject_cast<QWidget*>(sender());
    w->setWindowTitle(t);
    ui->tabWidget->setTabText(ui->tabWidget->indexOf(w), t);
    setWindowTitle(tr("%1 - PictoMir Navigator").arg(t));
}

void MainWindow::tabIconChanged()
{
    WebTab * wt = qobject_cast<WebTab*>(sender());
    const QIcon i = wt->icon();
    int index = ui->tabWidget->indexOf(wt);
    ui->tabWidget->setTabIcon(index, i);
}

MainWindow::~MainWindow()
{
    delete ui;
}

} // namespace Webkit
