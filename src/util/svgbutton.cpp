#include "util/svgbutton.h"
#include "core/platform_configuration.h"

namespace Util {

SvgButton::SvgButton(const QString &iconFileName,
                     const QString &baseElementId,
                     QGraphicsItem *parent)
                         : QGraphicsObject(parent)
                         , s_baseElementId(baseElementId)
{
//    setCacheMode(QGraphicsItem::NoCache);
    const QString defPath = Core::PlatformConfiguration::resourcesPath()+iconFileName;
//    Q_ASSERT_X(QFile::exists(defPath)
//               , "SvgButton::SvgButton(...)"
//               , QString(QString("File not exists: ")+defPath).toLocal8Bit());
    m_iconRenderer = new QSvgRenderer(defPath, this);
    connect(m_iconRenderer, SIGNAL(repaintNeeded()), this, SLOT(updateAnimation()));
    setAcceptHoverEvents(true);

    an_scale = new QPropertyAnimation(this, "size", this);
    an_scale->setDuration(0);

    an_move = new QPropertyAnimation(this, "pos", this);
    an_move->setDuration(0);

    animation = new QParallelAnimationGroup(this);
    animation->addAnimation(an_scale);
    animation->addAnimation(an_move);
    e_state = Default;

    b_pressed = false;
    setSize(QSize(0,0));
}


void SvgButton::updateAnimation()
{
//    qDebug() << "Update SVG animation";
    update();
}

void SvgButton::enable(const QPointF &target, const QSizeF &newSize)
{
    QGraphicsObject::setEnabled(true);
    animation->stop();
    b_enabled = true;
    an_move->setStartValue(pos());
    an_move->setEndValue(target);
    an_scale->setStartValue(size());
    an_scale->setEndValue(newSize);
    animation->start();
//    setVisible(true);
}

void SvgButton::disable(const QPointF &target, const QSizeF &newSize)
{
    QGraphicsObject::setEnabled(false);
    animation->stop();
    b_enabled = false;
    an_move->setStartValue(pos());
    an_move->setEndValue(target);
    an_scale->setStartValue(size());
    an_scale->setEndValue(newSize);
    animation->start();
    if (e_state & Hovered) {
        e_state ^= Hovered;
    }
}

void SvgButton::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if (b_enabled) {
        b_pressed = true;
        event->accept();
    }
    else {
        event->ignore();
    }
}

void SvgButton::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    if (b_enabled && b_pressed) {
        b_pressed = false;
        event->accept();
        emit clicked();
    }
    else {
        event->ignore();
    }
}

QString SvgButton::currentElementId() const
{
    if (e_state & Attended & Hovered) {
        return s_baseElementId+".attended.hovered";
    }
    else if (e_state & Attended) {
        return s_baseElementId+".attended";
    }
    else if (e_state & Hovered) {
        return s_baseElementId+".hovered";
    }
    else {
        return s_baseElementId+".default";
    }
}

void SvgButton::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
    if (b_enabled) {
        e_state |= Hovered;
        update();
        event->accept();
    }
    else {
        event->ignore();
    }
}

void SvgButton::hoverMoveEvent(QGraphicsSceneHoverEvent *event)
{
    if (b_enabled) {
        e_state |= Hovered;
        update();
        event->accept();
    }
    else {
        event->ignore();
    }
}

void SvgButton::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
    if (b_enabled) {
        if (e_state & Hovered)
            e_state ^= Hovered;
        update();
        event->accept();
    }
    else {
        event->ignore();
    }
}

void SvgButton::setAttended(bool v)
{
    if (b_enabled) {
        if (v) {
            e_state |= Attended;
        }
        else {
            if (e_state & Attended)
                e_state ^= Attended;
        }
        update();
    }
}

QRectF SvgButton::boundingRect() const
{
    return QRectF(QPointF(0,0), size());
}

void SvgButton::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    const QString elementId = currentElementId();
    m_iconRenderer->render(painter, elementId, boundingRect());
}

QSizeF SvgButton::size() const
{
    return sz_size;
}

void SvgButton::setSize(const QSizeF &v)
{
    prepareGeometryChange();
    sz_size = v;
    if (v.height()==0 || v.width()==0) {
        setVisible(false);
    }
    else {
        setVisible(true);
    }
    update();
}

}
