INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

HEADERS += \
  cookiedialog.h \
  cookieexceptionsdialog.h \
  cookieexceptionsmodel.h \
  cookiejar.h \
  cookiemodel.h

SOURCES += \
  cookiedialog.cpp \
  cookieexceptionsmodel.cpp \
  cookiemodel.cpp \
  cookieexceptionsdialog.cpp \
  cookiejar.cpp

FORMS += \
    cookies.ui \
    cookiesexceptions.ui

HEADERS += \
    searchlineedit.h \
    autosaver.h \
    edittableview.h \
    lineedit.h \
    lineedit_p.h \
    clearbutton.h \
    searchbutton.h

SOURCES += \
    searchlineedit.cpp \
    autosaver.cpp \
    edittableview.cpp \
    lineedit.cpp \
    clearbutton.cpp \
    searchbutton.cpp

include($$PWD/networkcookiejar/networkcookiejar.pri)

