#include <QtGui>
#include "desktop/functionwidget.h"
#include "core/pictomirfunction.h"

#include "core/actiontype.h"
#include "core/actionmodel.h"
#include "core/actionview.h"
#include "desktop/actionprogramview.h"
#include "util/svgbutton.h"
#include "core/enums.h"
#include "core/actionmanager.h"
#include "core/platform_configuration.h"

using namespace Core;

namespace Desktop {

FWMCell::FWMCell(FunctionWidget *fw
                 , PictomirFunction *f
                 , ActionType actionType
                 , int index
                 , QGraphicsItem *parent)
    : QGraphicsObject(parent)
    , f_ptr(f)
    , fw_ptr(fw)
    , e_actionType(actionType)
    , i_index(index)
    , b_mousePressed(false)
    , b_hovered(false)
    , b_hasItemInside(false)
{
    setAcceptDrops(true);
    setAcceptHoverEvents(true);
    QColor start;
    QColor end;
    if (actionType==ActionPlain) {
        start = QColor(0,128,0,96);
        end = QColor("lightgreen");
    }
    if (actionType==ActionCondition) {
        start = QColor(128,0,0,64);
        end = QColor("lightred");
    }
    if (actionType==ActionRepeat) {
        start = QColor(0,0,128,64);
        end = QColor("lightblue");
    }
    setColor(start);
    an_hover = new QPropertyAnimation(this, "color", this);
    an_unhover = new QPropertyAnimation(this, "color", this);
    an_hover->setStartValue(start);
    an_hover->setEndValue(end);
    an_unhover->setStartValue(end);
    an_unhover->setEndValue(start);
    an_hover->setDuration(400);
    an_unhover->setDuration(400);
    createAnimationPath();
    createHighlighterObjects();
    setHighlighterPixmap(0);

    an_highlighter = new QPropertyAnimation(this, "highlighterPixmap", this);
    an_highlighter->setLoopCount(-1);
    an_highlighter->setStartValue(0);
    an_highlighter->setEndValue(7);
    an_highlighter->setDuration(500);

    an_unhighlighter = new QPropertyAnimation(this, "highlighterOpacity", this);
    an_unhighlighter->setStartValue(1.0);
    an_unhighlighter->setEndValue(0.0);
    an_unhighlighter->setDuration(400);
    connect(an_unhighlighter, SIGNAL(finished()), this, SLOT(handleHighlightAnimationFinished()));



    m_viewInside = NULL;


}

void FWMCell::highlight()
{
    setHighlighterPixmap(0);
    px_highlighter->setPixmap(b_errorHighlight? v_errorHighlighters[0] : v_plainHighlighters[0]);
    px_highlighter->setOpacity(1.0);
    px_highlighter->setVisible(true);
    an_highlighter->start();
}

void FWMCell::unhighlight()
{
    setHighlighterOpacity(0.0);
    b_errorHighlight = false;
}

void FWMCell::createHighlighterObjects()
{
    b_errorHighlight = false;
    QSvgRenderer plain(Core::PlatformConfiguration::resourcesPath()+"/ui_desktop/highlighter_yellow.svg");
    QSvgRenderer error(Core::PlatformConfiguration::resourcesPath()+"/ui_desktop/highlighter_red.svg");
    Q_ASSERT(plain.isValid());
    Q_ASSERT(error.isValid());

    v_plainHighlighters.resize(8);
    v_errorHighlighters.resize(8);

    for (int i=0; i<8; i++) {
        QImage img1(plain.boundsOnElement(QString("frame%1").arg(i)).size().toSize(), QImage::Format_ARGB32);
        QImage img2(error.boundsOnElement(QString("frame%1").arg(i)).size().toSize(), QImage::Format_ARGB32);
        img1.fill(0);
        img2.fill(0);
        QPainter p1(&img1);
        QPainter p2(&img2);
        plain.render(&p1,QString("frame%1").arg(i));
        error.render(&p2,QString("frame%1").arg(i));
        v_plainHighlighters[i] = QPixmap::fromImage(img1);
        v_errorHighlighters[i] = QPixmap::fromImage(img2);
    }

    px_highlighter = new QGraphicsPixmapItem(this);
    px_highlighter->setPixmap(v_plainHighlighters[0]);
    px_highlighter->setPos(1,1);
    px_highlighter->setVisible(false);

}

void FWMCell::setHighlighterError(bool v)
{
    b_errorHighlight = v;
    px_highlighter->setPixmap(v? v_errorHighlighters[i_highlightPixmap] : v_plainHighlighters[i_highlightPixmap]);
}

void FWMCell::setHighlighterOpacity(qreal value)
{
    r_highlighterOpacity = value;
    px_highlighter->setOpacity(value);
}

void FWMCell::setHighlighterPixmap(int frameNo)
{
    i_highlightPixmap = frameNo;
    px_highlighter->setPixmap(b_errorHighlight? v_errorHighlighters[frameNo] : v_plainHighlighters[frameNo]);

}

void FWMCell::createAnimationPath()
{
    if (e_actionType==ActionPlain) {
        p_animationPath.addRoundedRect(QRectF(6, 6, 28, 28), 10, 10);
    }
    else if (e_actionType==ActionCondition) {
        QVector<QPoint> points(4);
        points[3] = QPoint(20, 6);
        points[2] = QPoint(6,20);
        points[1] = QPoint(20,34);
        points[0] = QPoint(34,20);
        p_animationPath.addPolygon(QPolygonF(points));
        p_animationPath.closeSubpath();
    }
    else if (e_actionType==ActionRepeat) {
        p_animationPath.addEllipse(6, 6, 28, 28);
    }
    else {
        qFatal("Unknown action type!");
    }
}

void FWMCell::handleHighlightAnimationFinished()
{
    an_highlighter->stop();
    b_errorHighlight = false;
    i_highlightPixmap = 0;
    px_highlighter->setVisible(false);
}

FWMCell::~FWMCell()
{
    if (m_viewInside) {
        delete m_viewInside;
    }
    delete an_hover;
    delete an_unhover;
    delete an_highlighter;
    delete an_unhighlighter;

    delete px_highlighter;
}


void FWMCell::paint(QPainter *p,
                    const QStyleOptionGraphicsItem *,
                    QWidget *)
{
    p->save();
    p->setBrush(cl_color);
    p->setPen(Qt::NoPen);
    bool antialiasing = scene()->views()[0]->renderHints().testFlag(QPainter::Antialiasing);
    p->setRenderHint(QPainter::Antialiasing, antialiasing);
    if (e_actionType==ActionPlain)
        p->drawRoundedRect(2,2,36,36,10,10);
    else if (e_actionType==ActionRepeat)
        p->drawEllipse(2,2,36,36);
    else if (e_actionType==ActionCondition) {
        QVector<QPoint> points(4);
        points[0] = QPoint(20,0);
        points[1] = QPoint(0,20);
        points[2] = QPoint(20,40);
        points[3] = QPoint(40,20);
        p->drawPolygon(points);
    }
    //    p->setPen(Qt::SolidLine);
    //    p->drawPath(p_animationPath);
    p->restore();
}

bool FWMCell::canInsertItem(const ActionModel *model) const
{
    if (!model)
        return false;
    ActionType outerType = model->actionType();
    ActionType myType = e_actionType;
    bool typeMatch = outerType==myType;
    bool locked = fw_ptr->isLocked();
    return typeMatch && !b_hasItemInside && !locked;
}

bool FWMCell::canInsertActiveItem() const
{
    return canInsertItem(f_ptr->actionManager()->activeModel());
}

void FWMCell::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
    if (canInsertActiveItem()) {
        b_hovered = true;
        an_unhover->stop();
        an_hover->start();
        event->accept();
    }
    else {
        b_hovered = false;
        event->ignore();
    }
}

void FWMCell::dragEnterEvent(QGraphicsSceneDragDropEvent *event)
{
    if (!fw_ptr->isLocked()) {
        fw_ptr->unhighlight();
    }
    if (event->mimeData()->property("mimetype").toString()!="ActionItem") {
        return;
    }
    int modelId = event->mimeData()->property("modelId").toInt();
    ActionModel* model = f_ptr->actionManager()->modelById(modelId);
    if (canInsertItem(model)) {
        b_hovered = true;
        an_unhover->stop();
        an_hover->start();
        event->accept();
    }
    else {
        b_hovered = false;
        event->ignore();
    }
}

void FWMCell::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
    if (b_hovered) {
        an_hover->stop();
        an_unhover->start();
        event->accept();
        b_hovered = false;
    }
    else {
        event->ignore();
    }
}

void FWMCell::dragLeaveEvent(QGraphicsSceneDragDropEvent *event)
{
    if (!fw_ptr->isLocked()) {
        fw_ptr->unhighlight();
    }
    if (b_hovered) {
        an_hover->stop();
        an_unhover->start();
        event->accept();
        b_hovered = false;
    }
    else {
        event->ignore();
    }
}


void FWMCell::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if (fw_ptr->isLocked()) {
        event->ignore();
        return;
    }
    if (!fw_ptr->isLocked()) {
        fw_ptr->unhighlight();
    }
    b_mousePressed = true;
    event->accept();
}

void FWMCell::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    if (fw_ptr->isLocked()) {
        event->ignore();
        return;
    }
    if (!fw_ptr->isLocked()) {
        fw_ptr->unhighlight();
    }
    if (b_mousePressed && canInsertActiveItem()) {
        b_mousePressed = false;
        insertActiveItem();
        event->accept();
    }
    else if (b_hasItemInside) {
        b_mousePressed = false;
        event->accept();
        emit imitateActionClicked(m_viewInside, event->button()==Qt::RightButton);
    }
    else {
        b_mousePressed = false;
        event->ignore();
    }
}

void FWMCell::dropEvent(QGraphicsSceneDragDropEvent *event)
{
    if (fw_ptr->isLocked()) {
        event->ignore();
        return;
    }
    if (event->mimeData()->property("mimetype").toString()!="ActionItem") {
        return;
    }
    int modelId = event->mimeData()->property("modelId").toInt();
    ActionModel* model = f_ptr->actionManager()->modelById(modelId);
    if (canInsertItem(model)) {
        insertItem(model);
        event->accept();
        b_hovered = false;
        an_hover->stop();
        an_unhover->start();
    }
    else {
        event->ignore();
    }
}

void FWMCell::insertActiveItem()
{
    ActionModel *model = f_ptr->actionManager()->activeModel();
    insertItem(model);
}

void FWMCell::insertItem(ActionModel *model)
{
    if (i_index==-2)
        f_ptr->setRepeater(model);
    if (i_index==-1)
        f_ptr->setCondition(model);
    if (i_index>=0)
        f_ptr->setAction(i_index, model);

    ActionProgramView *view = new ActionProgramView(model, this);
    qreal x_offset = (40 - view->graphicsItem()->boundingRect().width())/2;
    qreal y_offset = (40 - view->graphicsItem()->boundingRect().height())/2;
    view->graphicsItem()->setPos(x_offset, y_offset);
    connect(view->graphicsItem(), SIGNAL(clicked(bool)), fw_ptr, SLOT(handleActionClicked(bool)));
    connect(view->graphicsItem(), SIGNAL(dragged()), fw_ptr, SLOT(handleActionDragged()));
    f_ptr->actionManager()->setActiveView(NULL, true);
    b_hasItemInside = true;

    m_viewInside = view;
    f_ptr->setModified(true);
}

void FWMCell::unsetItemInside()
{
    b_hasItemInside = false;
    m_viewInside = NULL;
    f_ptr->setModified(true);
}

CurtainItem::CurtainItem(const QSize &size, QGraphicsItem *parent)
    : QGraphicsObject(parent)
    , sz_size(size)
{
    an_close = new QPropertyAnimation(this);
    an_close->setTargetObject(this);
    an_close->setPropertyName("openingState");
    an_close->setStartValue(1.0);
    an_close->setEndValue(0.0);
    an_close->setDuration(500);

    an_open = new QPropertyAnimation(this);
    an_open->setTargetObject(this);
    an_open->setPropertyName("openingState");
    an_open->setStartValue(0.0);
    an_open->setEndValue(1.0);
    an_close->setDuration(500);
    connect (an_open, SIGNAL(finished()), this, SLOT(handleOpenAnimationFinished()));
    r_state = 0.0;

    b_closed = false;

}

void CurtainItem::handleOpenAnimationFinished()
{
    b_closed = false;
}

QRectF CurtainItem::boundingRect() const
{
    //    if (b_closed)
    return QRectF(0, 0, sz_size.width(), sz_size.height());
    //    else
    //        return QRectF(0, 0, sz_size.width(), 30);
}

void CurtainItem::setOpeningState(qreal v)
{
    r_state = v;
    update(0,0,sz_size.width(),sz_size.height());
}

void CurtainItem::openCurtain()
{
    b_closed = true;
    an_close->stop();
    an_open->start();
}

void CurtainItem::closeCurtain()
{
    an_open->stop();
    an_close->start();
}

void CurtainItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    int mh = int(r_state*(sz_size.height()-30));
    painter->setPen(Qt::SolidLine);
    QLinearGradient gr(0,1,0,0);
    gr.setCoordinateMode(QGradient::ObjectBoundingMode);
    gr.setColorAt(0.0, QColor("gray"));
    gr.setColorAt(1.0, QColor("black"));
    painter->setBrush(gr);
    QVector<QPoint> points(8);
    points[0] = QPoint(0, mh);
    points[1] = QPoint(0, mh+30);
    points[2] = QPoint(sz_size.width(), mh+30);
    points[3] = QPoint(sz_size.width(), mh);
    points[4] = QPoint(sz_size.width()-8, mh);
    points[5] = QPoint(sz_size.width()-8, mh+30-8);
    points[6] = QPoint(8, mh+30-8);
    points[7] = QPoint(8, mh);
    QPolygon polygon(points);
    painter->drawPolygon(polygon);
    painter->setBrush(QColor("black"));
    painter->drawRect(0,0,8,mh);
    painter->drawRect(sz_size.width()-8,0,8,mh);
    painter->setBrush(QColor(0,0,0,100));
    painter->drawRect(8,0,sz_size.width()-16,mh+30-8);
}

TextItem::TextItem(const QString &txt, int maxW, QGraphicsItem *parent)
    : QGraphicsTextItem(txt, parent)
    , i_maxW(maxW)
{
    QFont fnt = font();
    QFontMetrics fm(fnt);
    setDefaultTextColor(QColor("white"));
    int tw = fm.width(txt);
    if (tw > maxW-16) {
        animation = new QPropertyAnimation(this);
        animation->setTargetObject(this);
        animation->setPropertyName("x");
        animation->setStartValue(8);
        animation->setDuration(5000);
        animation->setKeyValueAt(0.5, maxW-tw-8);
        animation->setEndValue(8);
        animation->setLoopCount(-1);
        //        animation->start();
    }
}



FunctionWidget::FunctionWidget(PictomirFunction *f, QGraphicsItem *parent) :
    FunctionWidgetInterface(parent)
{
    b_locked = false;
    setAcceptedMouseButtons(Qt::LeftButton | Qt::RightButton);
    setAcceptHoverEvents(true);
    setEnabled(true);
    connect (f, SIGNAL(programChanged()), this, SLOT(loadProgramFromModel()));
    m_highlightedCell = 0;
    m_function = f;
    int w = f->size().width();
    w = w * 40 + (w-1)*4;


    int h = f->size().height();
    int y = 40;
    if (f->modifiers()!=PlainFunction) {
        int x = 8;
        if (f->modifiers() & RepeatFunction) {
            FWMCell *item = new FWMCell(this, f,ActionRepeat,-2,this);
            item->setPos(x, y);
            x += 44;
            l_cells << item;
            connect (item, SIGNAL(imitateActionClicked(ActionProgramView*,bool)),
                     this, SLOT(handleActionClicked(ActionProgramView*,bool)));
        }
        if (f->modifiers() & ConditionFunction) {
            FWMCell *item = new FWMCell(this, f, ActionCondition, -1, this);
            item->setPos(x, y);

            l_cells << item;
            connect (item, SIGNAL(imitateActionClicked(ActionProgramView*,bool)),
                     this, SLOT(handleActionClicked(ActionProgramView*,bool)));
        }
        y += 48;

        h += 1;
    }
    h = h * 40 + (h-1)*4;


    QPainterPath linesPath;


    int index = 0;
    linesPath.moveTo(4, y+20);
    for (int row=0; row<f->size().height(); row++) {
        linesPath.quadTo(4, y, 4, y+20);
        int x = 8;
        for (int col=0; col<f->size().width(); col++) {
            FWMCell *item = new FWMCell(this, f, ActionPlain, index, this);
            index += 1;
            item->setPos(x, y);
            x += 44;

            l_cells << item;
            connect (item, SIGNAL(imitateActionClicked(ActionProgramView*,bool)),
                     this, SLOT(handleActionClicked(ActionProgramView*,bool)));
        }
        linesPath.lineTo(x, y+20);
        y += 44;
    }

    linesPath.lineTo(linesPath.currentPosition()+QPointF(-16, -6));
    linesPath.moveTo(linesPath.currentPosition()+QPointF(16, 6));
    linesPath.lineTo(linesPath.currentPosition()+QPointF(-16, 6));
    linesPath.closeSubpath();

    if (m_function->modifiers()==(RepeatFunction|ConditionFunction))
        w = qMax(w, 86);

//    lines = new QGraphicsPathItem(linesPath, this);
//    lines->setPen(QPen(QColor(0,0,0,40), 2));
//    lines->setZValue(-1.0);

    g_curtain = new CurtainItem(QSize(w+15, h+48), this);
    g_curtain->setPos(0, 0);


    setRect(0,0,w+15,h+48);
    setPen(Qt::SolidLine);
    setBrush(QColor(255,255,255,200));


    QGraphicsRectItem *r = new QGraphicsRectItem(0,0,w+15,24,this);

    r->setPen(Qt::SolidLine);
    QLinearGradient gr(0,1,0,0);
    gr.setCoordinateMode(QGradient::ObjectBoundingMode);
    gr.setColorAt(0.0, QColor("darkgreen"));
    gr.setColorAt(1.0, QColor(0,10,0));
    r->setBrush(gr);


    g_heading = new TextItem(f->title(), w, this);
    g_heading->setPos(8,0);
    setFunctionTitle(f->title());

    loadProgramFromModel();



}

void FunctionWidget::setFunctionTitle(const QString &title)
{
    g_heading->setPlainText(title);
    qreal w = rect().width()-6;
    QFontMetricsF fm(g_heading->font());
    qreal tw = fm.width(title);
    qreal ratio = w / tw;
    qreal scX = qMin(ratio, 1.0);
    QTransform trans;
    trans.scale(scX, 1.0);
    g_heading->setTransform(trans);
    if (ratio<1.0) {
        g_heading->setPos(1, 0);
    }
    else {
        g_heading->setPos(8, 0);
    }

}

PictomirFunction* FunctionWidget::model()
{
    return m_function;
}



void FunctionWidget::handleActionClicked(ActionProgramView *view, bool rightClick)
{
    Q_CHECK_PTR(view);
    if (!b_locked && m_highlightedCell) {
        unhighlight();
    }
    if (!rightClick) {
        if (m_function->actionManager()->activeView()==view) {
            m_function->actionManager()->setActiveView(NULL, true);
        }
        else {
            m_function->actionManager()->setActiveView(view, true);
        }
    }
    else {
        FWMCell *cell = qobject_cast<FWMCell*>(view->parentObject());
        Q_CHECK_PTR(cell);
        if (cell->index()==-2) {
            m_function->setRepeater(NULL);
        }
        else if (cell->index()==-1) {
            m_function->setCondition(NULL);
        }
        else {
            m_function->setAction(cell->index(), NULL);
        }
        if (m_function->actionManager()->activeView()==view) {
            m_function->actionManager()->setActiveView(NULL, true);
        }
        cell->unsetItemInside();
        view->dispose();
    }
    emit activationChanged();
}

void FunctionWidget::handleActionClicked(bool rightClick)
{
    ActionProgramView* view = qobject_cast<ActionProgramView*>(sender());
    handleActionClicked(view, rightClick);
}

void FunctionWidget::handleActionDragged()
{
    ActionProgramView* view = qobject_cast<ActionProgramView*>(sender());
    handleActionClicked(view, true);
}

void FunctionWidget::connectSignalActivationChanged(QObject *obj, const char *method)
{
    connect (this, SIGNAL(activationChanged()), obj, method, Qt::DirectConnection);
}

void FunctionWidget::loadProgramFromModel()
{
    for (int i=0; i<l_cells.size(); i++) {
        FWMCell *cell = l_cells[i];
        int index = cell->index();
        ActionModel *model = NULL;
        if (index==-2)
            model = m_function->repeater();
        else if (index==-1)
            model = m_function->condition();
        else
            model = m_function->action(index);
        if (model)
            cell->insertItem(model);
    }
}

void FunctionWidget::unlock()
{
    g_curtain->closeCurtain();
    b_locked = false;
}

void FunctionWidget::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsRectItem::mousePressEvent(event);
    if (m_highlightedCell && b_locked) {
        m_highlightedCell->unhighlight();
    }
}

void FunctionWidget::lock()
{
    g_curtain->openCurtain();
    if (m_highlightedCell) {
        m_highlightedCell->unhighlight();
    }
    m_highlightedCell = NULL;
    b_locked = true;
}

void FunctionWidget::highlightPlainCell(int index)
{
    if (m_highlightedCell) {
        m_highlightedCell->unhighlight();
    }
    FWMCell *cell = NULL;
    for (int i=0; i<l_cells.size(); i++) {
        if (l_cells[i]->index()==index) {
            cell = l_cells[i];
            break;
        }
    }
    Q_CHECK_PTR(cell);
    m_highlightedCell = cell;
    cell->highlight();
}

void FunctionWidget::highlightErrorCell(int index)
{
    FWMCell *cell = NULL;
    for (int i=0; i<l_cells.size(); i++) {
        if (l_cells[i]->index()==index) {
            cell = l_cells[i];
            break;
        }
    }
    if (m_highlightedCell!=cell) {
        if (m_highlightedCell)
            m_highlightedCell->unhighlight();
        m_highlightedCell = cell;
        m_highlightedCell->setHighlighterError(true);
        m_highlightedCell->highlight();
    }
    else {
        m_highlightedCell->setHighlighterError(true);
    }
}

void FunctionWidget::highlightConditionalValue(bool value)
{
    Q_CHECK_PTR(m_highlightedCell);
    m_highlightedCell->setHighlighterError(value);
}

void FunctionWidget::unhighlight()
{
    if (m_highlightedCell) {
        m_highlightedCell->unhighlight();
        m_highlightedCell = NULL;
    }

}

FunctionWidget::~FunctionWidget()
{
    for (int i=0; i<l_cells.size(); i++) {
        delete l_cells[i];
    }
    delete g_curtain;
    delete g_heading;
//    delete lines;
}
}
