#include "addalgorhitmdialog.h"
#include "ui_addalgorhitmdialog.h"

namespace Creator {

AddAlgorhitmDialog::AddAlgorhitmDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddAlgorhitmDialog)
{
    ui->setupUi(this);
}

AddAlgorhitmDialog::~AddAlgorhitmDialog()
{
    delete ui;
}

QSize AddAlgorhitmDialog::programSize() const
{
    return QSize(ui->width->value(), ui->height->value());
}

bool AddAlgorhitmDialog::hasCondition() const
{
    return ui->condition->checkState()==Qt::Checked;
}

bool AddAlgorhitmDialog::hasRepeater() const
{
    return ui->repeater->checkState()==Qt::Checked;
}

void AddAlgorhitmDialog::setProgramSize(const QSize &sz)
{
    ui->width->setValue(sz.width());
    ui->height->setValue(sz.height());
}

void AddAlgorhitmDialog::setRepeater(bool v)
{
    ui->repeater->setChecked(v);
}

void AddAlgorhitmDialog::setCondition(bool v)
{
    ui->condition->setChecked(v);
}

AddAlgorhitmDialog* AddAlgorhitmDialog::instance()
{
    if (!m_instance) {
        m_instance = new AddAlgorhitmDialog();
    }
    return m_instance;
}

AddAlgorhitmDialog* AddAlgorhitmDialog::m_instance = NULL;

}
