#include "cr_programeditor.h"
#include "core/actionmanager.h"
#include "core/pictomirprogram.h"
#include "core/pictomirfunction.h"
#include "desktop/functionwidget.h"
#include "desktop/dockwidget.h"
#include "schema/sch_program.h"
#include "schema/sch_algorhitm.h"
#include "creator/addalgorhitmdialog.h"
#include "robot25d/default_plugin.h"

#include "core/platform_configuration.h"


namespace Creator {


ProgramEditor::ProgramEditor(QWidget *parent) : QGraphicsView(parent)
{
    init();
}

void ProgramEditor::init()
{
    setObjectName("Creator__ProgramEditor");
    setMinimumWidth(420);
    setMinimumHeight(400);
    setBackgroundBrush(palette().brush(QPalette::Window));
    sch_game = 0;
    i_taskIndex = 0;
    m_program = 0;
    m_actionManager = new Core::ActionManager(this);
    m_module = new Robot25D::Plugin();
    m_actionManager->createActions(QList<Robot25D::Plugin*>() << m_module);
    setScene(new QGraphicsScene);
    g_dock = new Desktop::DockWidget;
    g_dock->setRect(QRectF(0,0,400,90));
    g_dock->setTableColor(QColor("darkgray"));
    g_dock->setTextColor(QColor("black"));
    scene()->addItem(g_dock);
    g_dock->setPos(0,0);
    g_dock->setActionManager(m_actionManager);

    a_add = new QAction(tr("Add algorhitm"), this);
    connect(a_add, SIGNAL(triggered()), this, SLOT(addAlgorhitm()));
    menu_edit = new QMenu(tr("Change algorhitm"));
    menu_remove = new QMenu(tr("Remove algorhitm"));

    btn_addAlgorhitm = new QPushButton(0);
    btn_addAlgorhitm->setText(tr("Add algorhitm..."));
    btn_addAlgorhitm->setIcon(QIcon(Core::PlatformConfiguration::resourcesPath()+"/ui_creator/list-add.png"));
    btn_addAlgorhitm->setIconSize(QSize(22,22));
    g_addAlgorhitm = new QGraphicsProxyWidget;
    g_addAlgorhitm->setWidget(btn_addAlgorhitm);
    scene()->addItem(g_addAlgorhitm);

    connect(btn_addAlgorhitm, SIGNAL(clicked()), this, SLOT(addAlgorhitm()));
}

void ProgramEditor::setProgram(Schema::Game * game, int taskIndex)
{
    Q_CHECK_PTR(game);
    Q_ASSERT(taskIndex>=0);
    Q_ASSERT(taskIndex<game->tasks.size());
    sch_game = game;
    i_taskIndex = taskIndex;
    Schema::Program pr = game->tasks[taskIndex].program;
    setProgram(pr);
    updateWindowTitle();
}

void ProgramEditor::updateWindowTitle()
{
    setWindowTitle(sch_game->title + " : " + sch_game->tasks[i_taskIndex].title);
}


void ProgramEditor::setProgram(const Schema::Program &program)
{
    if (m_program)
        m_program->deleteLater();
    foreach (Desktop::FunctionWidget *fw, l_functions) {
        scene()->removeItem(fw);
        fw->deleteLater();
    }
    m_program = new Core::PictomirProgram(program, m_module, m_actionManager, this);
    for (int i=0; i<m_program->functionsCount(); i++) {
        Core::PictomirFunction *f = m_program->function(i);
        Desktop::FunctionWidget * fw = new Desktop::FunctionWidget(f);
        addAlgorhitmToScene(fw);
    }
    relayout();
    updateMenus();
    updateDock();
    updateTitles();
}

void ProgramEditor::addAlgorhitmToScene(Desktop::FunctionWidget *fw)
{
    QToolButton * btnChange = new QToolButton();
    btnChange->setIcon(QIcon(Core::PlatformConfiguration::resourcesPath()+"/ui_creator/edit-table-cell-merge.png"));
    btnChange->setIconSize(QSize(32,32));
    QGraphicsProxyWidget *gChange = new QGraphicsProxyWidget;
    gChange->setWidget(btnChange);
    scene()->addItem(gChange);
    l_changeGButtons << gChange;
    l_changeButtons << btnChange;
    connect(btnChange, SIGNAL(clicked()), this, SLOT(editAlgorhitm()));

    QToolButton * btnRemove = new QToolButton();
    btnRemove->setIcon(QIcon(Core::PlatformConfiguration::resourcesPath()+"/ui_creator/edit-delete.png"));
    btnRemove->setIconSize(QSize(32,32));
    QGraphicsProxyWidget *gRemove = new QGraphicsProxyWidget;
    gRemove->setWidget(btnRemove);
    scene()->addItem(gRemove);
    l_removeGButtons << gRemove;
    l_removeButtons << btnRemove;
    connect(btnRemove, SIGNAL(clicked()), this, SLOT(removeAlgorhitm()));

    l_functions << fw;
    scene()->addItem(fw);
}

void ProgramEditor::addAlgorhitm(const QSize &size, bool rep, bool cond)
{
    Core::FunctionModifiers props = Core::PlainFunction;
    if (rep)
        props |= Core::RepeatFunction;
    if (cond)
        props |= Core::ConditionFunction;
    m_program->addAlgorhitm(size, props);
    Desktop::FunctionWidget * fw = new Desktop::FunctionWidget(m_program->function(m_program->functionsCount()-1));
    addAlgorhitmToScene(fw);
    relayout();
    updateMenus();
    updateDock();
    updateTitles();
}

void ProgramEditor::removeAlgorhitm(int index)
{
    QMessageBox::StandardButton r = QMessageBox::question(0,
                                                          tr("Remove algorhitm"),
                                                          tr("Are you sure to REMOVE this algorhitm and its contents?"),
                                                          QMessageBox::Yes | QMessageBox::No,
                                                          QMessageBox::No);
    if (r==QMessageBox::Yes) {
        Desktop::FunctionWidget * fw = l_functions[index];
        scene()->removeItem(fw);
        fw->deleteLater();
        l_functions.removeAt(index);
        l_removeButtons[index]->deleteLater();
        l_removeButtons.removeAt(index);
        scene()->removeItem(l_removeGButtons[index]);
        l_removeGButtons.removeAt(index);
        l_changeButtons[index]->deleteLater();
        l_changeButtons.removeAt(index);
        scene()->removeItem(l_changeGButtons[index]);
        l_changeGButtons.removeAt(index);
        m_program->removeAlgorhitm(index);
        relayout();
        updateMenus();
        updateDock();
        updateTitles();
    }
}

void ProgramEditor::editAlgorhitm(int index)
{
    AddAlgorhitmDialog *d = AddAlgorhitmDialog::instance();
    d->setProgramSize(m_program->function(index)->size());
    bool rep = m_program->function(index)->modifiers() & Core::RepeatFunction;
    bool cond = m_program->function(index)->modifiers() & Core::ConditionFunction;
    d->setRepeater(rep);
    d->setCondition(cond);

    if (d->exec()==QDialog::Accepted) {
        QSize newSize = d->programSize();
        bool newRep = d->hasRepeater();
        bool newCond = d->hasCondition();
        Core::FunctionModifiers fm = Core::PlainFunction;
        if (newRep)
            fm = Core::RepeatFunction;
        if (newCond)
            fm = fm | Core::ConditionFunction;
        m_program->function(index)->modifyAlgorhitm(newSize, fm);
        scene()->removeItem(l_functions[index]);
        l_functions[index]->deleteLater();
        l_functions[index] = new Desktop::FunctionWidget(m_program->function(index));
        scene()->addItem(l_functions[index]);
        relayout();
        updateMenus();
        updateDock();
        updateTitles();
    }
}

void ProgramEditor::sync()
{
    Schema::Program current = m_program->data();
    sch_game->tasks[i_taskIndex].program = current;
}

void ProgramEditor::closeEvent(QCloseEvent *event)
{
    sync();
    event->accept();
}

void ProgramEditor::addAlgorhitm()
{
    AddAlgorhitmDialog *d = AddAlgorhitmDialog::instance();
    if (d->exec()==QDialog::Accepted) {
        QSize newSize = d->programSize();
        bool newRep = d->hasRepeater();
        bool newCond = d->hasCondition();
        addAlgorhitm(newSize, newRep, newCond);
    }
}

void ProgramEditor::relayout()
{
    qreal y = 100;
    for (int i=0; i<l_functions.size(); i++) {
        l_functions[i]->setPos(0, y);
        l_changeGButtons[i]->setPos(l_functions[i]->boundingRect().width()+10, y);
        l_removeGButtons[i]->setPos(l_functions[i]->boundingRect().width()+l_changeGButtons[i]->boundingRect().width()+20, y);
        y += l_functions[i]->boundingRect().height()+10;
    }
    g_addAlgorhitm->setPos(0, y+20);
}

void ProgramEditor::updateMenus()
{
    menu_edit->clear();
    menu_remove->clear();
//    for (int i=0; i<l_editActions.size(); i++) {
//        l_editActions[i]->deleteLater();
//    }
//    for (int i=0; i<l_removeActions.size(); i++) {
//        l_removeActions[i]->deleteLater();
//    }
    l_editActions.clear();
    l_removeActions.clear();
    for (int i=0; i<l_functions.size(); i++) {
        const QString algTable = trUtf8(" АБВГДЕЖЗИКЛМНОПРСТУФ", "letters");
        const QString title = i==0?
                              trUtf8("Главный") :
                              trUtf8("Алгоритм %1").arg(algTable[i]);
        QAction * ed = menu_edit->addAction(tr("Change \"%1\"").arg(title), this, SLOT(editAlgorhitm()));
        l_editActions << ed;
        if (i>0) {
            QAction * rm = menu_remove->addAction(tr("Remove \"%1\"").arg(title), this, SLOT(removeAlgorhitm()));
            l_removeActions << rm;
        }
    }
}

void ProgramEditor::updateDock()
{
    bool cond = false;
    bool rep  = false;
    for (int i=0; i<m_program->functionsCount(); i++) {
        cond |= m_program->function(i)->modifiers().testFlag(Core::ConditionFunction);
        rep  |= m_program->function(i)->modifiers().testFlag(Core::RepeatFunction);
    }
    int funcs= m_program->functionsCount()-1;
    m_actionManager->setEnabledActionsList(m_module, true, cond, rep, funcs);
    g_dock->updateVisiblity();
}

void ProgramEditor::updateTitles()
{
    const QString algTable = trUtf8(" АБВГДЕЖЗИКЛМНОПРСТУФ", "letters");
    for (int i=0; i<l_functions.size(); i++) {
        const QString title = i==0?
                              trUtf8("Главный") :
                              trUtf8("Алгоритм %1").arg(algTable[i]);
        l_functions[i]->setFunctionTitle(title);
    }
}

void ProgramEditor::removeAlgorhitm()
{
    if (QString(sender()->metaObject()->className())=="QAction") {
        QAction * a = qobject_cast<QAction*>(sender());
        Q_CHECK_PTR(a);
        int index = l_removeActions.indexOf(a);
        Q_ASSERT(index>=0);
        removeAlgorhitm(index);
    }
    else if (QString(sender()->metaObject()->className())=="QToolButton") {
        QToolButton * b = qobject_cast<QToolButton*>(sender());
        Q_CHECK_PTR(b);
        int index = l_removeButtons.indexOf(b);
        Q_ASSERT(index>=0);
        removeAlgorhitm(index);
    }
}

void ProgramEditor::editAlgorhitm()
{
    if (QString(sender()->metaObject()->className())=="QAction") {
        QAction * a = qobject_cast<QAction*>(sender());
        Q_CHECK_PTR(a);
        int index = l_editActions.indexOf(a);
        Q_ASSERT(index>=0);
        editAlgorhitm(index);
    }
    else if (QString(sender()->metaObject()->className())=="QToolButton") {
        QToolButton * b = qobject_cast<QToolButton*>(sender());
        Q_CHECK_PTR(b);
        int index = l_changeButtons.indexOf(b);
        Q_ASSERT(index>=0);
        editAlgorhitm(index);
    }
}

} // namespace Creator
