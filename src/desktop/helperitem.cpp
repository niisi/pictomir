#include "helperitem.h"

#define BG_OPACITY 0.4

namespace Desktop {

HelperItem::HelperItem(QGraphicsItem * parent)
    : QObject(0)
    , QGraphicsRectItem(parent)
{
    setBrush(Qt::NoBrush);
    setPen(Qt::NoPen);
    b_enabled = true;
    setAcceptsHoverEvents(true);
    setOpacity(BG_OPACITY);
    g_border = new QGraphicsPathItem(this);
    g_border->setPen(QPen(QColor(255,255,255,64), 2));
    g_border->setBrush(QColor(255,255,255,32));
}

HelperItem::~HelperItem()
{
}

void HelperItem::setBorder(const QSize &blockSize)
{
    prepareGeometryChange();
    QPainterPath path;
    path.moveTo(0, 12);
    path.arcTo(0,0,24,24,180,-90);
    path.lineTo(blockSize.width()-12, 0);
    path.arcTo(blockSize.width()-24,0,24,24,90,-90);
    path.lineTo(blockSize.width(), blockSize.height()-12);
    path.arcTo(blockSize.width()-24,blockSize.height()-24,24,24,0,-90);
    path.lineTo(12, blockSize.height());
    path.arcTo(0,blockSize.height()-24,24,24,-90,-90);
    path.lineTo(0, 12);
    path.closeSubpath();

    g_border->setPath(path);
    setRect(0,0,blockSize.width(),blockSize.height());
}

void HelperItem::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
    event->accept();
    setOpacity(b_enabled? 1.0 : BG_OPACITY);
}

void HelperItem::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
    event->accept();
    setOpacity(BG_OPACITY);
}


bool HelperItem::isEnabled() const
{
    return b_enabled;
}

void HelperItem::setEnabled(bool v)
{
    b_enabled = v;
    setOpacity(v? 1.0 : BG_OPACITY);
}


} // namespace Desktop
