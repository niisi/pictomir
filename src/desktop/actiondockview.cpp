#include "desktop/actiondockview.h"
#include "core/actionmodel.h"
#include "core/actionmanager.h"

using namespace Core;
namespace Desktop {

ActionDockView::ActionDockView(ActionModel *model, QGraphicsItem *parent)
    : QGraphicsSvgItem(parent)
{
    m_model = model;
    setSharedRenderer(model->m_renderer);
    b_mouseClick = false;
    setAcceptHoverEvents(true);
    b_active = false;
    an_jumping = new QPropertyAnimation(this);
    an_jumping->setPropertyName("y");
    an_jumping->setTargetObject(this);
    an_jumping->setKeyValueAt(0.5, 5);
    an_jumping->setStartValue(35);
    an_jumping->setEndValue(35);
    an_jumping->setDuration(800);
    an_jumping->setLoopCount(-1);

    an_stopJumping = new QPropertyAnimation(this);
    an_stopJumping->setPropertyName("y");
    an_stopJumping->setTargetObject(this);
    an_stopJumping->setEndValue(35);
    an_stopJumping->setDuration(150);
    an_stopJumping->setLoopCount(1);

}

void ActionDockView::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if (m_model->manager()->isLocked()) {
        event->ignore();
        return;
    }
    b_mouseClick = true;
    event->accept();
}

void ActionDockView::setActive(bool v) {
    if (b_active && !v) {
        an_jumping->stop();
        an_stopJumping->setStartValue(an_jumping->currentValue());
        an_stopJumping->start();
    }
    b_active = v;
    if (b_active) {
        an_jumping->start();
    }
}

void ActionDockView::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    if (m_model->manager()->isLocked() || !b_mouseClick) {
        b_mouseClick = false;
        event->ignore();
        return;
    }
    event->accept();
    emit clicked();
}

#ifndef QT_NO_CURSOR
void ActionDockView::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    if ( QLineF(event->screenPos(), event->buttonDownScreenPos(Qt::LeftButton))
        .length() < QApplication::startDragDistance() ) {
        return;
    }
    b_mouseClick = false;
    QDrag *drag = new QDrag(event->widget());
    QPixmap px = QPixmap::fromImage(m_model->image());
    drag->setPixmap(px);
#ifndef Q_WS_X11
    drag->setHotSpot(QPoint(px.width()/2, px.height()/2));
#endif
    QMimeData *data = new QMimeData;
    data->setProperty("mimetype","ActionItem");
    data->setProperty("modelId", m_model->modelId());
    drag->setMimeData(data);
    if (m_model->manager()->activeView()!=this) {
        m_model->manager()->setActiveView(NULL, true);
    }
    drag->exec();
    emit dragged();
}



void ActionDockView::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
    if (m_model->manager()->isLocked()) {
        event->ignore();
        return;
    }
    event->accept();
    emit hovered(m_model->text());
}

void ActionDockView::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
    if (m_model->manager()->isLocked()) {
        event->ignore();
        return;
    }
    event->accept();
    emit hovered("");
}
#endif // QT_NO_CURSOR

ActionModel* ActionDockView::model()
{
    return m_model;
}

int ActionDockView::modelId() const
{
    return m_model->modelId();
}

bool ActionDockView::isEnabled() const
{
    return m_model->isEnabled();
}

QSize ActionDockView::originalSize() const
{
    return m_model->m_renderer->defaultSize();
}

QSize ActionDockView::currentSize() const
{
    return boundingRect().size().toSize();
}
}
