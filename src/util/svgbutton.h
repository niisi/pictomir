#ifndef SVGBUTTON_H
#define SVGBUTTON_H

#include <QtCore>
#include <QtGui>
#include <QtSvg>

namespace Util {

class SvgButton:
        public QGraphicsObject
{
    Q_OBJECT;
    Q_PROPERTY(QSizeF size READ size WRITE setSize);
    Q_ENUMS(State);
public:
    enum State { Default = 0x00, Hovered = 0x01, Attended = 0x02 };
    Q_DECLARE_FLAGS(SuperState, State);
    explicit SvgButton(  const QString &iconFileName, const QString &baseElementId, QGraphicsItem *parent = 0);
    QSizeF size() const;
    void setSize(const QSizeF &v);
    QRectF boundingRect() const;
public slots:
    void enable(const QPointF &target, const QSizeF &newSize);
    void disable(const QPointF &target, const QSizeF &newSize);
    void setAttended(bool v);
signals:
    void clicked();
protected:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
    void hoverLeaveEvent(QGraphicsSceneHoverEvent *event);
    void hoverMoveEvent(QGraphicsSceneHoverEvent *event);
protected slots:
    void updateAnimation();

private:
    QString s_baseElementId;
    QString currentElementId() const;
    QPropertyAnimation *an_scale;
    QPropertyAnimation *an_move;
    QParallelAnimationGroup *animation;
    bool b_enabled;
    bool b_pressed;
    QSvgRenderer *m_iconRenderer;
    QSizeF sz_size;
    SuperState e_state;
};

Q_DECLARE_OPERATORS_FOR_FLAGS(SvgButton::SuperState);

}

#endif // SVGBUTTON_H
