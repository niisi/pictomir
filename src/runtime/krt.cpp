#include "krt.h"
#include "krt_p.h"

namespace Runtime {

KRT::KRT(QObject *parent)
        : QObject(parent)
{
    initialize();
}

void KRT::initialize()
{
    d = new KRT_private(this);
}

void KRT::connectSignalINTCallback(QObject *object, const char *method)
{
    Q_CHECK_PTR( object );
    connect(d, SIGNAL(INT_callback(quint16,QStack<QVariant> *)),
            object, method, Qt::DirectConnection);
}

void KRT::init(const QStringList &functions)
{
    foreach ( const QString &function, functions ) {
        QStringList instructions = function.split("\n");
        d->instructions << instructions;
    }
    FunctionData entryPoint;
    entryPoint.instructions = d->instructions[0];
    entryPoint.ip = 0;
    d->callStack.push(entryPoint);
}

void KRT::clear()
{
    qDebug() << "KRT: clear";
    d->callStack.clear();
    d->dataStack.clear();
    d->instructions.clear();
    d->heap.clear();
}

void KRT::executeNextInstruction()
{
    quint32 current_ip;
    quint32 instructions_count;
    QString instruction;

    current_ip = d->callStack.top().ip;
    instructions_count = d->callStack.top().instructions.count();

    if (current_ip >= instructions_count) {
        d->callStack.pop();
        return;
    }

    instruction = d->callStack.top().instructions.at(current_ip);
    d->callStack.top().ip++;
    d->processInstruction(instruction);
}

bool KRT::hasMoreInstructions() const
{
    return !d->callStack.isEmpty();
}

QStack<QString> KRT::backtraceInfo() const
{
    qDebug() << "KRT: request backtrace info";
    QStack<QString> result;
    for (int i=0; i<d->callStack.size(); i++) {
        result.push(d->callStack.at(i).backtraceInfo);
    }
    return result;
}

KRT_private::KRT_private(QObject *parent)
        : QObject(parent)
{

}

void KRT_private::processInstruction(const QString &instruction)
{
    if (instruction.isEmpty())
        return;
    QStringList lexems = instruction.split(" ");
    QString instr = lexems[0];
    QStringList arguments = lexems.mid(1,lexems.size()-1);
    if (instr=="add") {
        ADD(arguments);
    }
    else if (instr=="sub") {
        SUB(arguments);
    }
    else if (instr=="mul") {
        MUL(arguments);
    }
    else if (instr=="div") {
        DIV(arguments);
    }
    else if (instr=="neg") {
        NEG(arguments);
    }
    else if (instr=="cmp") {
        CMP(arguments);
    }
    else if (instr=="exg") {
        EXG(arguments);
    }
    else if (instr=="copy") {
        COPY(arguments);
    }
    else if (instr=="int") {
        INT(arguments);
    }
    else if (instr=="jsr") {
        JSR(arguments);
    }
    else if (instr=="rts") {
        RTS(arguments);
    }
    else if (instr=="jmp") {
        JMP(arguments);
    }
    else if (instr=="jnz") {
        JNZ(arguments);
    }
    else if (instr=="jz") {
        JZ(arguments);
    }
    else if (instr=="pop") {
        POP(arguments);
    }
    else if (instr=="push") {
        PUSH(arguments);
    }
    else if (instr=="bti") {
        BTI(arguments);
    }
    else {
        qFatal("Execute instruction: unknown command");
    }
}

QMetaMethod KRT_private::findMethod(const QString &name) const
{
    QString s, n;
    QRegExp rxName("(.+)\\(.*");
    Q_ASSERT(rxName.isValid());
    for (int i=0; i<metaObject()->methodCount(); i++) {
        s = QString::fromAscii(metaObject()->method(i).signature());
        int brPos = s.indexOf("(");
        if (brPos==-1)
            n = s;
        else
            n = s.left(brPos);
        if (n==name) {
            return metaObject()->method(i);
        }
    }
    qFatal("Method not found!");
    return QMetaMethod(); // to avoid compiler warning
}

quint8 KRT_private::registerByName(const QString &regName) const
{
    Q_ASSERT(regName.length()==1);
    Q_ASSERT(regName[0] >= 'A');
    Q_ASSERT(regName[0] <= 'Z');
    ushort offset = QChar('A').unicode();
    ushort code = regName[0].unicode();
    Q_ASSERT(code>=offset);
    ushort difference = ushort(code-offset);
    Q_ASSERT(difference<26);
    return quint8(difference);
}

ArgumentInfo KRT_private::extractArgumentInfo(const QString &argument) const
{
    ArgumentInfo result;
    Q_ASSERT ( argument.length()>=1 );
    bool ok = true;
    if ((argument[0]>='A')&&(argument[0]<='Z')) {
        result.location = Register;
        result.register_no = registerByName(argument);
    }
    else if (argument[0]=='$') {
        result.location = Heap;
        result.ptr = argument.mid(1).toUInt(&ok,16);
    }
    else if (argument[0]=='@') {
        result.location = Local;
        result.ptr = argument.mid(1).toUInt(&ok,16);
    }
    else {
        result.location = Constant;
    }
    Q_ASSERT(ok);
    return result;
}

QVariant KRT_private::extractArgumentValue(const QString &argument) const
{
    Q_ASSERT (argument.length()>=1);
    ArgumentInfo info = extractArgumentInfo(argument);
    QVariant result;
    if (info.location==Register) {
        result = callStack.top().registers[info.register_no];
    }
    else if (info.location==Heap) {
        Q_ASSERT (int(info.ptr)<heap.size());
        result = heap.at(info.ptr);
    }
    else if (info.location==Local) {
        Q_ASSERT (int(info.ptr)<callStack.top().locals.size());
        result = callStack.top().locals.at(info.ptr);
    }
    else {
        Q_ASSERT ( (argument[0]=='i') ||
                   (argument[0]=='r') ||
                   (argument[0]=='c') ||
                   (argument[0]=='s') ||
                   (argument[0]=='b') );
        bool ok = true;
        QByteArray ba1, ba2;
        QString tmp;
        if (argument[0]=='i') {
            result = argument.mid(1).toInt(&ok);
        }
        else if (argument[0]=='r') {
            result = argument.mid(1).toDouble(&ok);
        }
        else if (argument[0]=='b') {
            result = argument.mid(1).trimmed()!="0";
        }
        else if (argument[0]=='c') {
            ba1 = argument.mid(1).trimmed().toAscii();
            Q_ASSERT ( !ba1.isEmpty() );
            ba2 = QByteArray::fromHex(ba1);
            Q_ASSERT ( !ba2.isEmpty() );
            tmp = QString::fromUtf8(ba2);
            Q_ASSERT(tmp.length()==1);
            result = QVariant(QChar(tmp[0]));
        }
        else if (argument[0]=='s') {
            ba1 = argument.mid(1).trimmed().toAscii();
            if (ba1.isEmpty()) {
                result = "";
            }
            else {
                ba2 = QByteArray::fromHex(ba1);
                Q_ASSERT ( !ba2.isEmpty() );
                tmp = QString::fromUtf8(ba2);
                result = tmp;
            }
        }
    }
    return result;
}

void KRT_private::setArgumentValue(const QString &argument, const QVariant &value)
{
    ArgumentInfo info = extractArgumentInfo(argument);
    Q_ASSERT (info.location!=Constant);
    if (info.location==Register) {
        callStack.top().registers[registerByName(argument)] = value;
    }
    else if (info.location==Heap) {
        Q_ASSERT(int(info.ptr)<heap.size());
        heap[info.ptr] = value;
    }
    else if (info.location==Local) {
        Q_ASSERT(int(info.ptr)<callStack.top().locals.size());
        callStack.top().locals[info.ptr] = value;
    }
}

void KRT_private::ADD(const QStringList &arguments)
{
    Q_ASSERT(arguments.size()==2);
    QVariant dest = extractArgumentValue(arguments[0]);
    QVariant src = extractArgumentValue(arguments[1]);
    Q_ASSERT(dest.isValid());
    Q_ASSERT(src.isValid());

    if (dest.type()==QVariant::Int && src.type()==QVariant::Int)
        dest = dest.toInt()+src.toInt();
    else if (dest.type()==QVariant::Double && (src.type()==QVariant::Double || src.type()==QVariant::Int))
        dest = dest.toDouble()+src.toDouble();
    else if (dest.type()==QVariant::Bool && src.type()==QVariant::Bool)
        dest = dest.toBool() || src.toBool();
    else if (dest.type()==QVariant::String && (src.type()==QVariant::String || src.type()==QVariant::Char))
        dest = dest.toString()+src.toString();
    else
        qFatal("ADD: uncompatible types");
    setArgumentValue(arguments[0],dest);
}

void KRT_private::MUL(const QStringList &arguments)
{
    Q_ASSERT(arguments.size()==2);
    QVariant dest = extractArgumentValue(arguments[0]);
    QVariant src = extractArgumentValue(arguments[1]);
    Q_ASSERT(dest.isValid());
    Q_ASSERT(src.isValid());

    if (dest.type()==QVariant::Int && src.type()==QVariant::Int)
        dest = dest.toInt()*src.toInt();
    else if (dest.type()==QVariant::Double && (src.type()==QVariant::Double || src.type()==QVariant::Int))
        dest = dest.toDouble()*src.toDouble();
    else if (dest.type()==QVariant::Bool && src.type()==QVariant::Bool)
        dest = dest.toBool() && src.toBool();
    else
        qFatal("MUL: uncompatible types");
    setArgumentValue(arguments[0],dest);
}

void KRT_private::SUB(const QStringList &arguments)
{
    Q_ASSERT(arguments.size()==2);
    QVariant dest = extractArgumentValue(arguments[0]);
    QVariant src = extractArgumentValue(arguments[1]);
    Q_ASSERT(dest.isValid());
    Q_ASSERT(src.isValid());

    if (dest.type()==QVariant::Int && src.type()==QVariant::Int)
        dest = dest.toInt()-src.toInt();
    else if (dest.type()==QVariant::Double && (src.type()==QVariant::Double || src.type()==QVariant::Int))
        dest = dest.toDouble()-src.toDouble();
    else
        qFatal("SUB: uncompatible types");
    setArgumentValue(arguments[0],dest);
}


void KRT_private::DIV(const QStringList &arguments)
{
    Q_ASSERT(arguments.size()==2);
    QVariant dest = extractArgumentValue(arguments[0]);
    QVariant src = extractArgumentValue(arguments[1]);
    Q_ASSERT(dest.isValid());
    Q_ASSERT(src.isValid());

    if (dest.type()==QVariant::Int && src.type()==QVariant::Int)
        dest = dest.toInt()/src.toInt();
    else if (dest.type()==QVariant::Double && (src.type()==QVariant::Double || src.type()==QVariant::Int))
        dest = dest.toDouble()/src.toDouble();
    else
        qFatal("DIV: uncompatible types");
    setArgumentValue(arguments[0],dest);
}

void KRT_private::NEG(const QStringList &arguments)
{
    Q_ASSERT(arguments.size()==1);
    QVariant dest = extractArgumentValue(arguments[0]);
    Q_ASSERT(dest.isValid());

    if (dest.type()==QVariant::Int)
        dest = - dest.toInt();
    else if (dest.type()==QVariant::Double)
        dest = - dest.toDouble();
    else if (dest.type()==QVariant::Bool)
        dest = !dest.toBool();
    else
        qFatal("NEG: uncompatible types");
    setArgumentValue(arguments[0],dest);
} 


void KRT_private::CMP(const QStringList &arguments)
{
    Q_ASSERT(arguments.size()==2);
    QVariant dest = extractArgumentValue(arguments[0]);
    QVariant src = extractArgumentValue(arguments[1]);
    Q_ASSERT(dest.isValid());
    Q_ASSERT(src.isValid());
    int value = 0;

    if (dest.type()==QVariant::Int && src.type()==QVariant::Int) {
        if (dest.toInt()<src.toInt())
            value = 1;
        else if (dest.toInt()==src.toInt())
            value = 0;
        else
            value = -1;
    }
    else if (dest.type()==QVariant::Double && src.type()==QVariant::Double)  {
        if (dest.toDouble()<src.toDouble())
            value = 1;
        else if (dest.toDouble()==src.toDouble())
            value = 0;
        else
            value = -1;
    }
    else if (dest.type()==QVariant::String && src.type()==QVariant::String) {
        if (dest.toString()<src.toString())
            value = 1;
        else if (dest.toString()==src.toString())
            value = 0;
        else
            value = -1;
    }
    else if (dest.type()==QVariant::Char && src.type()==QVariant::Char) {
        if (dest.toChar()<src.toChar())
            value = 1;
        else if (dest.toChar()==src.toChar())
            value = 0;
        else
            value = -1;
    }
    else if (dest.type()==QVariant::Bool && src.type()==QVariant::Bool) {
        if (dest.toBool() && !src.toBool())
            value = -1;
        else if (!dest.toBool() && src.toBool())
            value = 1;
        else
            value = 0;
    }
    else
        qFatal("CMP: uncompatible types");
    setArgumentValue("A",value);
}

void KRT_private::EXG(const QStringList &arguments)
{
    Q_ASSERT(arguments.size()==2);
    ArgumentInfo i0 = extractArgumentInfo(arguments[0]);
    ArgumentInfo i1 = extractArgumentInfo(arguments[1]);
    QVariant v0 = extractArgumentValue(arguments[0]);
    QVariant v1 = extractArgumentValue(arguments[1]);
    Q_ASSERT(i0.location!=Constant);
    Q_ASSERT(i1.location!=Constant);
    setArgumentValue(arguments[1],v0);
    setArgumentValue(arguments[0],v1);
}

void KRT_private::COPY(const QStringList &arguments)
{
    Q_ASSERT(arguments.size()==2);
    ArgumentInfo i0 = extractArgumentInfo(arguments[0]);
    QVariant v1 = extractArgumentValue(arguments[1]);
    Q_ASSERT(i0.location!=Constant);
    setArgumentValue(arguments[0],v1);
}

void KRT_private::INT(const QStringList &arguments)
{
    Q_ASSERT(arguments.size()==1);
    QVariant no = extractArgumentValue(arguments[0]);
    Q_ASSERT(no.canConvert(QVariant::UInt));
    INT_finished = false;
    emit INT_callback(quint16(no.toUInt()), &(dataStack));

}

void KRT_private::JSR(const QStringList &arguments)
{
    Q_ASSERT(arguments.size()==1);
    QVariant no = extractArgumentValue(arguments[0]);
    Q_ASSERT(no.canConvert(QVariant::Int));
    int func_no = no.toInt();
    Q_ASSERT(func_no>=0);
    Q_ASSERT(func_no<instructions.size());
    FunctionData fd;    
    fd.instructions = instructions.at(func_no);
    fd.ip = 0;
    callStack.push(fd);
}

void KRT_private::RTS(const QStringList &)
{
    callStack.pop();
}

void KRT_private::JMP(const QStringList &arguments)
{
    Q_ASSERT(arguments.size()==1);
    QVariant v = extractArgumentValue(arguments[0]);
    Q_ASSERT(v.canConvert(QVariant::UInt));
    quint32 ip = quint32(v.toUInt());
    callStack.top().ip = ip;
}

void KRT_private::JZ(const QStringList &arguments)
{
    Q_ASSERT(arguments.size()==1);
    QVariant v = extractArgumentValue(arguments[0]);
    Q_ASSERT(v.canConvert(QVariant::UInt));
    quint32 ip = quint32(v.toUInt());
    QVariant a = extractArgumentValue("A");
    if (a.toInt()==0)
        callStack.top().ip = ip;
}

void KRT_private::JNZ(const QStringList &arguments)
{
    Q_ASSERT(arguments.size()==1);
    QVariant v = extractArgumentValue(arguments[0]);
    Q_ASSERT(v.canConvert(QVariant::UInt));
    quint32 ip = quint32(v.toUInt());
    QVariant a = extractArgumentValue("A");
    if (a.toInt()!=0)
        callStack.top().ip = ip;
}

void KRT_private::PUSH(const QStringList &arguments)
{
    Q_ASSERT(arguments.size()==1);
    QVariant v = extractArgumentValue(arguments[0]);
    dataStack.push(v);
}

void KRT_private::BTI(const QStringList &arguments)
{
    Q_ASSERT(arguments.size()==1);
    QVariant v = extractArgumentValue(arguments[0]);
    callStack.top().backtraceInfo = v.toString();
}

void KRT_private::POP(const QStringList &arguments)
{
    Q_ASSERT(arguments.size()==1);
    ArgumentInfo i = extractArgumentInfo(arguments[0]);
    Q_ASSERT(i.location!=Constant);
    Q_ASSERT(dataStack.size()>0);
    QVariant v = dataStack.pop();
    setArgumentValue(arguments[0],v);
}

}
