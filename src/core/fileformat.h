#ifndef FILEFORMAT_H
#define FILEFORMAT_H

namespace Core {
    enum FileFormat {
        FileTextFormat,
        FileBinFormat,
        FileJsonFormat,
        FileXmlFormat,
        FileAgkXmlFormat
    };
}

#endif // FILEFORMAT_H
