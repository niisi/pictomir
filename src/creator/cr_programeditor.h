#ifndef CR_PROGRAMEDITOR_H
#define CR_PROGRAMEDITOR_H

#include <QtCore>
#include <QtGui>

#include "schema/sch_game.h"
#include "schema/sch_task.h"
#include "schema/sch_program.h"

namespace Core {
    class PictomirProgram;
    class ActionManager;
}

namespace Desktop {
    class DockWidget;
    class FunctionWidget;
}

namespace Robot25D {
    class Plugin;
}

namespace Creator {

class ProgramEditor : public QGraphicsView
{
    Q_OBJECT
public:
    explicit ProgramEditor(QWidget *parent = 0);
    void init();
    inline QAction * actionAdd() { return a_add; }
    inline QMenu * editMenu() { return menu_edit; }
    inline QMenu * removeMenu() { return menu_remove; }

public slots:
    void updateWindowTitle();
    inline Schema::Game * game() { return sch_game; }
    void setProgram(Schema::Game * game, int index);
    inline void setTaskIndex(int index) { i_taskIndex = index; }
    inline int taskIndex() const { return i_taskIndex; }
    inline void checkTaskIndex(const QMap<int,int> map) { if (map.contains(i_taskIndex)) i_taskIndex = map[i_taskIndex]; }
    void setProgram(const Schema::Program &program);
    void sync();

    void addAlgorhitm();
    void removeAlgorhitm();
    void editAlgorhitm();

protected slots:
    void addAlgorhitm(const QSize &size, bool rep, bool cond);
    void editAlgorhitm(int index);
    void removeAlgorhitm(int index);
    void relayout();
    void updateMenus();
    void updateDock();
    void updateTitles();

private:
    void addAlgorhitmToScene(Desktop::FunctionWidget * fw);
    void closeEvent(QCloseEvent *event);

    QMenu* menu_edit;
    QMenu* menu_remove;
    QAction* a_add;
    Schema::Game * sch_game;
    int i_taskIndex;
    QList<QAction*> l_editActions;
    QList<QAction*> l_removeActions;
    Core::ActionManager * m_actionManager;
    Robot25D::Plugin * m_module;
    Core::PictomirProgram * m_program;
    Desktop::DockWidget * g_dock;
    QList<Desktop::FunctionWidget*> l_functions;
    QList<QGraphicsProxyWidget*> l_removeGButtons;
    QList<QGraphicsProxyWidget*> l_changeGButtons;
    QList<QToolButton*> l_removeButtons;
    QList<QToolButton*> l_changeButtons;
    QGraphicsProxyWidget *g_addAlgorhitm;
    QPushButton * btn_addAlgorhitm;

};

} // namespace Creator

#endif // CR_PROGRAMEDITOR_H
