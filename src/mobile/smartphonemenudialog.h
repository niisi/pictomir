#ifndef SMARTPHONEMENUDIALOG_H
#define SMARTPHONEMENUDIALOG_H

#include <QtCore>
#include <QtGui>

namespace Ui {
    class SmartphoneMenuDialog;
}

namespace Mobile {

class SmartphoneMenuDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SmartphoneMenuDialog(QWidget *parent = 0);
    void init(const QStringList &items, int selectedNo);
    ~SmartphoneMenuDialog();
    int selectedItemIndex() const;
protected slots:
    void handleItemClicked(QListWidgetItem *item);
private:
    Ui::SmartphoneMenuDialog *ui;
};

}

#endif // SMARTPHONEMENUDIALOG_H
