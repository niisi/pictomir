#ifndef PLATFORM_CONFIGURATION_H
#define PLATFORM_CONFIGURATION_H

#include <QtCore>

namespace Core {

class PlatformConfiguration
{
public:

    inline static QString imagesPath()
    {
        return resourcesPath();
    }

    inline static QString cssPath()
    {
        return resourcesPath();
    }

    inline static QString resourcesPath()
    {
#ifdef Q_OS_WINCE
        return ":/resources/";
#endif
#ifdef ANDROID
        return ":/resources/";
#endif
        return shareRoot()+"/resources/";


    }

    inline static QString shareRoot()
    {
#ifdef Q_OS_WINCE
        return QCoreApplication::applicationDirPath()+"/";
#endif
#ifdef Q_OS_MAC
        return QDir::cleanPath(QCoreApplication::applicationDirPath()+"/../Resources/share/pictomir/");
#endif
        return QDir::cleanPath(QCoreApplication::applicationDirPath()+"/../share/pictomir/");
    }

    inline static QString tasksPath()
    {
        return shareRoot()+"/Tasks/";
    }

    inline static QString translatorsPath()
    {
        return shareRoot()+"/Languages/";
    }

    inline static QString envPath()
    {
        return shareRoot()+"/Environments/";
    }

    inline static QString modulesPath()
    {
#ifdef Q_OS_WINCE
	return QCoreApplication::applicationDirPath()+"/";
#endif
#ifdef Q_OS_MAC
        return QCoreApplication::applicationDirPath()+
                "/../PlugIns/";
#endif
        QString lib = "lib";
#ifndef Q_OS_FREEBSD
        if (QSysInfo::WordSize==64) {
            lib = "lib64";
        }
#endif
#ifndef Q_OS_MAC
        return QCoreApplication::applicationDirPath()+
                "/../"+lib+"/pictomir/modules/";
#endif
    }

    inline static QString aboutPath() {
        return shareRoot()+"/HyperText/about/index.html";
    }

    inline static QString helpPath() {
        return shareRoot()+"/HyperText/help/index.html";

    }
};

}

#endif // PLATFORM_CONFIGURATION_H
