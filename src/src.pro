# -------------------------------------------------
# Project created by QtCreator 2009-07-06T15:36:24
# -------------------------------------------------
QT += svg xml network script webkit
win32-g++:QMAKE_CXXFLAGS += -Werror
#unix:QMAKE_CXXFLAGS += -Werror -Woverloaded-virtual -Wundef -Wall
#macx:QMAKE_CXXFLAGS -= -Werror -std=c++0x
android:QMAKE_CXXFLAGS -= -Werror -std=c++0x
TARGET = ../bin/pictomir
win32: TARGET = ../../bin/pictomir

!equals(PWD, $${OUT_PWD}) {
    TARGET = pictomir
    win32: TARGET = ../pictomir
}
wince*: {
	TARGET = ../../bin-wince/pictomir
	message("Target platform is WindowsCE")
}

TEMPLATE = app
INCLUDEPATH += .



SOURCES += main.cpp
HEADERS += version.def

unix {
    target.path = /bin
    INSTALLS += target
}

include(3rd-party/cookiejar/cookiejar.pri)

include(core/core.pri)
include(schema/schema.pri)
include(desktop/desktop.pri)
include(httpservice/httpservice.pri)
include(interfaces/interfaces.pri)
#include(mobile/mobile.pri)
include(robot25d/robot25d.pri)
include(runtime/runtime.pri)
include(util/util.pri)
include(webkit/webkit.pri)
include(creator/creator.pri)

TRANSLATIONS = ../share/pictomir/Languages/russian.ts

macx {
    ICON = ../mac/pictomir.icns
}

win32 {
    
}

OTHER_FILES += \
    ../CHANGES.txt
