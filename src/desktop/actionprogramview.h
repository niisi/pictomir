#ifndef ACTIONPROGRAMVIEW_H
#define ACTIONPROGRAMVIEW_H

#include <QtCore>
#include <QtGui>
#include <QtSvg>
#include "core/actionview.h"



namespace Desktop {

class ActionProgramView :
        public QGraphicsSvgItem,
        public Core::ActionView
{
    Q_OBJECT;
    Q_PROPERTY(qreal pulse READ pulse WRITE setPulse);
public:
    explicit ActionProgramView(Core::ActionModel *model, QGraphicsItem *parent=0);
    Core::ActionModel *model();
    void setLongClickAsRightClick(bool v);
    int modelId() const;
    inline QGraphicsSvgItem* graphicsItem() { return this; }
    bool isEnabled() const;
    QSize originalSize() const;
    QSize currentSize() const;
    inline qreal pulse() const { return r_pulse; }
    void setPulse(qreal value);
public slots:
    void setActive(bool active);
    void dispose();
signals:
    void clicked(bool rightClick);
    void dragged();
protected slots:
    void handleFadeOutFinished();
private:
    virtual void mouseReleaseEvent ( QGraphicsSceneMouseEvent * event );
    virtual void mousePressEvent ( QGraphicsSceneMouseEvent * event );
#ifndef QT_NO_CURSOR
    virtual void mouseMoveEvent( QGraphicsSceneMouseEvent * event );
#endif
    Core::ActionModel *m_model;
    bool b_mouseClick;
    QPropertyAnimation *an_pulsing;
    QPropertyAnimation *an_stopPulsing;
    QPropertyAnimation *an_fadeOut;
    QPropertyAnimation *an_fadeIn;
    bool b_active;
    qreal r_pulse;
    QPointF pnt_basePosition;
    QPointF pnt_currentPosition;


};

}
#endif // ACTIONPROGRAMVIEW_H
