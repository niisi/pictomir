#include "core/actionmodel.h"
#include "core/actionview.h"
#include "core/actionmanager.h"
#include "desktop/dockwidget.h"
#include "desktop/actiondockview.h"

using namespace Core;

namespace Desktop {

DockWidget::DockWidget(QGraphicsItem *parent)
    : QObject(0)
    , QGraphicsRectItem(parent)
{
    setPen(Qt::NoPen);
    i_dockWidth = 600;
    m_actionManager = NULL;

    an_textSliding = new QPropertyAnimation(this);
    an_textSliding->setStartValue(10);
    an_textSliding->setDuration(300);
    an_textSliding->setLoopCount(1);

    g_overText = new QGraphicsTextItem();
    g_overText->setParentItem(this);

    QFont fnt;
#ifdef Q_OS_WIN32
    fnt.setFamily("Times");
#else
    fnt.setFamily("serif");
#endif
    fnt.setPointSize(14);
    fnt.setStyleHint(QFont::Times, QFont::PreferDefault);
    fnt.setItalic(true);

    g_overText->setFont(fnt);
    g_overText->setVisible(false);
    g_overText->setDefaultTextColor(QColor("white"));

    an_textSliding->setTargetObject(g_overText);
    an_textSliding->setPropertyName("x");

    QLinearGradient gr(0,1,0,0);
    gr.setCoordinateMode(QGradient::ObjectBoundingMode);

    QColor c1 = QColor("white");
    QColor c2 = QColor("lightgreen");
    gr.setColorAt(0.0, c1);
    gr.setColorAt(1.0, c2);


    QLinearGradient gr2(0,1,0,0);
    gr2.setCoordinateMode(QGradient::ObjectBoundingMode);
    gr2.setColorAt(1.0, QColor(0,0,0,0));
    gr2.setColorAt(0.1, QColor("lightgray"));
    g_background = new QGraphicsRectItem(this);
    g_background->setBrush(Qt::NoBrush);
    g_background->setBrush(QColor(255,255,255,32));
    g_background->setPen(Qt::NoPen);
    QVector<QPointF> points(6);
    points[0] = QPoint(0,height()-10);
    points[1] = QPoint(30,50);
    points[2] = QPoint(width()-30, 50);
    points[3] = QPoint(width(),height()-10);
    points[4] = QPoint(width(),height());
    points[5] = QPoint(0,height());
    g_table = new QGraphicsPolygonItem(this);
    g_table->setPolygon(QPolygonF(points));
    g_table->setBrush(gr2);
    g_table->setPen(Qt::NoPen);
}

void DockWidget::setTableColor(const QColor &c)
{
    QLinearGradient gr2(0,1,0,0);
    gr2.setCoordinateMode(QGradient::ObjectBoundingMode);
    gr2.setColorAt(1.0, QColor(0,0,0,0));
    gr2.setColorAt(0.1, c);
    g_table->setBrush(gr2);
    g_table->update();
}

void DockWidget::setTextColor(const QColor &c)
{
    g_overText->setDefaultTextColor(c);
}

int DockWidget::width() const
{
    const QRectF rf = rect();
    return rf.toRect().width();
}

int DockWidget::height() const
{
    const QRectF rf = rect();
    return rf.toRect().height();
}

void DockWidget::setDrawBackground(bool v)
{
    Q_UNUSED(v);
    update();
}

void DockWidget::setActionManager(Core::ActionManager *m)
{
    m_actionManager = m;
}

Core::ActionManager * DockWidget::actionManager()
{
    return m_actionManager;
}

void DockWidget::addSpacer()
{
    l_spacers << l_items.size();
}

void DockWidget::relayoutDock()
{
    Q_ASSERT ( l_actions.size() == l_items.size() );
    int actual_width = 0;
    for (int i=0; i<l_actions.size(); i++ ) {
        actual_width += l_items[i]->originalSize().width();
        if (l_spacers.contains(i))
            actual_width += 25;
    }
    int maximum_width = i_dockWidth-2*25-8*(l_items.count()-1);
    bool needToScale = false;
    qreal scaleFactor = 1.0;
    if (actual_width>maximum_width) {
        scaleFactor = qreal(maximum_width)/qreal(actual_width);
        needToScale = true;
    }
    if (needToScale) {
        for (int i=0; i<l_items.count(); i++) {
            qreal restoreScale = qreal(l_items.at(i)->originalSize().width()) / qreal(l_items.at(i)->currentSize().width());
            l_items.at(i)->graphicsItem()->scale(restoreScale, restoreScale);
            l_items.at(i)->graphicsItem()->scale(scaleFactor, scaleFactor);
        }
        actual_width = 0;
        for (int i=0; i<l_items.count(); i++) {
            actual_width += l_items.at(i)->currentSize().width();
        }
        actual_width += 8*(l_items.count()-1)+l_spacers.count()*25;
    }
    int freeSpace = i_dockWidth-2*25-actual_width;
    int left = freeSpace/2;
    for (int i=0; i<l_items.count(); i++ ) {
        if (l_spacers.contains(i))
            left += 25;
        l_items[i]->graphicsItem()->setPos(left,l_items[i]->graphicsItem()->y());
        left += l_items[i]->currentSize().width();
        left += 8;
    }
}

void DockWidget::updateVisiblity()
{
    l_actions.clear();
    for (int i=0; i<l_items.size(); i++) {
        l_items[i]->graphicsItem()->deleteLater();
    }
    l_items.clear();
    l_spacers.clear();
    QList<ActionModel*> repeaters;
    QList<ActionModel*> conditions;
    QList<ActionModel*> moduleActions;
    QList<ActionModel*> functions;
    repeaters = m_actionManager->models(true, ActionRepeat);
    conditions = m_actionManager->models(true, ActionCondition);
    moduleActions = m_actionManager->models(true, ActionPlain, Module);
    functions = m_actionManager->models(true, ActionPlain, Functions);
    for (int i=0; i<repeaters.size(); i++) {
        l_items << new ActionDockView(repeaters[i]);
    }
    addSpacer();
    for (int i=0; i<conditions.size(); i++) {
        l_items << new ActionDockView(conditions[i]);
    }
    for (int i=0; i<moduleActions.size(); i++) {
        l_items << new ActionDockView(moduleActions[i]);
    }
    addSpacer();
    for (int i=0; i<functions.size(); i++) {
        l_items << new ActionDockView(functions[i]);
    }
    l_actions = repeaters + conditions + moduleActions + functions;
    for (int i=0; i<l_items.size(); i++) {
        ActionDockView *item = l_items[i];
        item->setPos(0, 35);
        connect (item, SIGNAL(clicked()), this, SLOT(handleItemClicked()));
        connect (item, SIGNAL(hovered(QString)), this, SLOT(handleItemText(QString)));
//        scene()->addItem(item);
        item->setParentItem(this);
        item->setVisible(true);
    }
    relayoutDock();
}


void DockWidget::setRect(const QRectF &rect)
{
    i_dockWidth = rect.toRect().width();
    relayoutDock();
    QVector<QPointF> points(6);
    points[0] = QPoint(0,rect.height()-10);
    points[1] = QPoint(30,50);
    points[2] = QPoint(rect.width()-30, 50);
    points[3] = QPoint(rect.width(),rect.height()-10);
    points[4] = QPoint(rect.width(),rect.height());
    points[5] = QPoint(0,rect.height());
    g_table->setPolygon(QPolygonF(points));
    g_background->setRect(rect.x()+25, rect.y(), rect.width()-50, rect.height());
    QGraphicsRectItem::setRect(rect);

}

void DockWidget::connectSignalTouched(QObject *obj, const char *method)
{
    connect (this, SIGNAL(touched()), obj, method, Qt::DirectConnection);
}

void DockWidget::connectSignalActivationChanged(QObject *obj, const char *method)
{
    connect (this, SIGNAL(activationChanged()), obj, method, Qt::DirectConnection);
}

void DockWidget::handleItemClicked()
{
    ActionDockView *item = qobject_cast<ActionDockView*>(sender());
    Q_CHECK_PTR(item);

    if (m_actionManager->activeView()==item) {
        m_actionManager->setActiveView(NULL, true);
    }
    else {
        m_actionManager->setActiveView(item, true);
    }
    emit touched();
    emit activationChanged();
}


void DockWidget::handleItemText(const QString &text)
{
    ActionDockView *item = qobject_cast<ActionDockView*>(sender());
    Q_CHECK_PTR(item);
    Q_CHECK_PTR(an_textSliding);
    if (text.isEmpty()) {
        g_overText->setVisible(false);
    }
    else {
        g_overText->setPlainText(text);
        g_overText->setVisible(true);
        int x = item->x() + item->boundingRect().width()/2 - g_overText->boundingRect().width() / 2;
        if (x<10)
            x = 10;
        if ( x+g_overText->boundingRect().width()/2 > width()-10)
            x = width()-10-g_overText->boundingRect().width()/2;
//        qDebug() << x;
        an_textSliding->setStartValue(an_textSliding->endValue());
        an_textSliding->setEndValue(x);
        an_textSliding->start();
    }

}
}
