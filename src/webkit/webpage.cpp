#include <QtWebKit>
#include "webpage.h"
#include "pageglobalobject.h"

namespace Webkit {

WebPage::WebPage(QObject *parent)
    : QWebPage(parent)
{
    obj = new PageGlobalObject(this);
    connect(mainFrame(), SIGNAL(javaScriptWindowObjectCleared()), this, SLOT(setJavaScriptObjects()));
    connect(mainFrame(), SIGNAL(loadStarted()), obj, SLOT(reload()));
    connect(mainFrame(), SIGNAL(loadFinished(bool)), obj, SLOT(prepare()));
}



void WebPage::setJavaScriptObjects()
{
    mainFrame()->addToJavaScriptWindowObject("pictomir", obj);
}


QObject * WebPage::createPlugin(const QString &classid, const QUrl &url, const QStringList &paramNames, const QStringList &paramValues)
{
    if (classid=="robot") {
        return createRobot(url, paramNames, paramValues);
    }
    else if (classid=="shelf") {
        return createShelf(url, paramNames, paramValues);
    }
    else if (classid=="form") {
        return createForm(url, paramNames, paramValues);
    }
    else {
        return NULL;
    }
}

QObject * WebPage::createRobot(const QUrl &url, const QStringList &paramNames, const QStringList &paramValues)
{
    Q_UNUSED(url);
    QString groupName;
    QUrl envUrl;
    for (int i=0; i<paramNames.size(); i++) {
        if (paramNames[i]=="env") {
            QUrl relative(paramValues[i].trimmed());
            if (relative.isRelative()) {
                envUrl = currentFrame()->url().resolved(relative);
            }
            else {
                envUrl = relative;
            }
        }
        else if (paramNames[i]=="group") {
            groupName = paramValues[i].trimmed();
        }
    }
    return obj->createRobot(groupName, envUrl);
}

QObject * WebPage::createShelf(const QUrl &url, const QStringList &paramNames, const QStringList &paramValues)
{
    Q_UNUSED(url);
    bool rep = true;
    bool cond = true;
    int fc = 0;
    QString groupName;
    QString color;
    int tw = -1;
    for (int i=0; i<paramNames.size(); i++) {
        QString val = paramValues[i].trimmed().toLower();
        bool bv = val=="1" || val=="yes" || val=="true";
        if (paramNames[i]=="conditions") {
            cond = bv;
        }
        else if (paramNames[i]=="repeaters") {
            rep = bv;
        }
        else if (paramNames[i]=="functions") {
            fc = val.toInt();
        }
        else if (paramNames[i]=="group") {
            groupName = paramValues[i].trimmed();
        }
        else if (paramNames[i]=="table_width") {
            QString w = paramValues[i];
            w.remove("px");
            tw = w.toInt();
        }
        else if (paramNames[i]=="table_color") {
            color = paramValues[i];
        }
    }
    return obj->createShelf(groupName, true, cond, rep, fc, tw, QColor(color));
}

QObject * WebPage::createForm(const QUrl &url, const QStringList &paramNames, const QStringList &paramValues)
{
    Q_UNUSED(url);
    int w = 0;
    int h = 0;
    bool rep = false;
    bool cond = false;
    int no = 0;
    QStringList initial;
    QString groupName;
    QStringList hints;
    QString title;
    for (int i=0; i<paramNames.size(); i++) {
        QString val = paramValues[i].trimmed().toLower();
        bool bv = val=="1" || val=="yes" || val=="true";
        if (paramNames[i]=="condition") {
            cond = bv;
        }
        else if (paramNames[i]=="repeater") {
            rep = bv;
        }
        else if (paramNames[i]=="group") {
            groupName = paramValues[i].trimmed();
        }
        else if (paramNames[i]=="program") {
            QString p = paramValues[i].simplified();
            p.replace(","," ");
            p.remove("\"");
            p.remove("'");
            initial = p.split(" ", QString::KeepEmptyParts);
        }
        else if (paramNames[i]=="number") {
            no = paramValues[i].toInt();
        }
        else if (paramNames[i]=="w") {
            w = paramValues[i].toInt();
        }
        else if (paramNames[i]=="h") {
            h = paramValues[i].toInt();
        }
        else if (paramNames[i]=="title") {
            title = paramValues[i].simplified();
        }
        else if (paramNames[i]=="hint") {
            hints << paramValues[i].trimmed();
        }
    }
    return obj->createForm(groupName, no, cond, rep, w, h, title, hints, initial);
}

QWebPage * WebPage::createWindow(WebWindowType type)
{
    Q_UNUSED(type);
    WebPage * page = new WebPage();
    emit newPageCreated(page);
    return page;
}

}
