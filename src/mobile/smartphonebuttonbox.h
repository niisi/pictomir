#ifndef SMARTPHONEBUTTONBOX_H
#define SMARTPHONEBUTTONBOX_H

#include <QtCore>
#include <QtGui>
#include <QtSvg>

namespace Util {
    class SvgButton;
}

namespace Mobile {

class SmartphoneButtonBox
    : public QGraphicsView
{
    Q_OBJECT;
public:
    explicit SmartphoneButtonBox(const QStringList &buttons, QWidget *parent = 0);
public slots:
    void setButtonsEnabled(const QList<bool> &buttons);
    void setDockPosition(Qt::DockWidgetArea);
    void relayout();
    void setCurrentIcon(int index);
protected:
    void resizeEvent(QResizeEvent *event);
    void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
signals:
    void iconPressed(int index);
private:
    QList<Util::SvgButton*> l_buttons;
    QList<int> l_spacers;
    Qt::DockWidgetArea e_currentPosition;
    qreal r_offset;
    bool b_pressed;
    QPointF pnt_pressed;
    QGraphicsItem* g_pressed;
    int i_currentIcon;
};

}

#endif // SMARTPHONEBUTTONBOX_H
