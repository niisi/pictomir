#include <QtWebKit>
#include "webkit/webtab.h"
#include "ui_webtab.h"
#include "util/contentprovider.h"
#include "webkit/webpage.h"
#include "core/platform_configuration.h"

namespace Webkit {



WebTab::WebTab(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Tab)
{
    ui->setupUi(this);
    ui->webView->setPage(new WebPage);
    init();
}

WebTab::WebTab(QWebPage *page, QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Tab)

{
    ui->setupUi(this);
    ui->webView->setPage(page);
    init();
}

void WebTab::init() {
    connect(ui->addressEdit, SIGNAL(returnPressed()), this, SLOT(handleAddressEntered()));
    QNetworkAccessManager * nam = Util::ContentProvider::instance()->sharedNetworkAccessManager();
    ui->webView->page()->setNetworkAccessManager(nam);
    ui->webView->settings()->setAttribute(QWebSettings::DeveloperExtrasEnabled, true);
    ui->webView->settings()->setAttribute(QWebSettings::AutoLoadImages, true);
    ui->webView->settings()->setAttribute(QWebSettings::PluginsEnabled, true);

    connect(ui->webView,SIGNAL(titleChanged(QString)), this, SIGNAL(titleChanged(QString)));
    connect(ui->webView,SIGNAL(urlChanged(QUrl)), this, SLOT(handleLinkChanged(QUrl)));
    connect(ui->webView,SIGNAL(iconChanged()), this, SIGNAL(iconChanged()));

    QAction * home = new QAction(this);
    home->setShortcut(QKeySequence("Alt+Home"));
    connect(home, SIGNAL(triggered()), this, SLOT(goHome()));
    addAction(home);

    const QSize iconSize(16,16);
    const QSize btnSize(22,22);

    ui->btnBack->setIcon(QIcon(Core::PlatformConfiguration::resourcesPath()+"/ui_webkit/icons/go-back.png"));
    ui->btnBack->setIconSize(iconSize);
    ui->btnBack->resize(btnSize);
    ui->btnForward->setIcon(QIcon(Core::PlatformConfiguration::resourcesPath()+"/ui_webkit/icons/go-forward.png"));
    ui->btnForward->setIconSize(iconSize);
    ui->btnForward->resize(btnSize);
    ui->btnReload->setIcon(QIcon(Core::PlatformConfiguration::resourcesPath()+"/ui_webkit/icons/reload.png"));
    ui->btnReload->setIconSize(iconSize);
    ui->btnReload->resize(btnSize);
    ui->btnStop->setIcon(QIcon(Core::PlatformConfiguration::resourcesPath()+"/ui_webkit/icons/stop.png"));
    ui->btnStop->setIconSize(iconSize);
    ui->btnStop->resize(btnSize);

    ui->btnGo->setIcon(QIcon(Core::PlatformConfiguration::resourcesPath()+"/ui_webkit/icons/enter.png"));
    ui->btnGo->setIconSize(iconSize);
    ui->btnGo->resize(btnSize);
    ui->btnStop->setVisible(false);

    WebPage * wp = qobject_cast<WebPage*>(ui->webView->page());
    connect(wp, SIGNAL(newPageCreated(QWebPage*)), this, SIGNAL(newPageCreated(QWebPage*)));
    ui->btnForward->setVisible(false);

    QAction *ctrlL = new QAction(this);
    ctrlL->setShortcut(QKeySequence("Ctrl+L"));
    connect(ctrlL, SIGNAL(triggered()), ui->addressEdit, SLOT(setFocus()));
}

void WebTab::setAddressLineFocus()
{
    ui->addressEdit->setFocus();
}

void WebTab::goHome()
{
    QString language = qApp->property("language").toString();
    QString homeUrl = QString("local://")+language+"/";
    goAddress(homeUrl);
}

void WebTab::setUrl(const QUrl &u)
{
    ui->webView->load(u);
}

QUrl WebTab::url() const
{
    return ui->webView->url();
}

void WebTab::handleLinkChanged(const QUrl &url)
{
    const QString link = url.toString();
    ui->addressEdit->setText(link);
}


WebTab::~WebTab()
{
    delete ui;
}

void WebTab::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

QIcon WebTab::icon() const
{
    return ui->webView->icon();
}

void WebTab::handleAddressEntered()
{
    QString addr = ui->addressEdit->text().trimmed();
    goAddress(addr);
}

void WebTab::goAddress(const QString &addressText)
{
    QString addr = addressText;
    while (addr.endsWith("//")) {
        addr = addr.left(addr.length()-1);
    }
    if (addr.endsWith(":")) {
        addr += "/";
    }
    if (addr.endsWith(":/")) {
        addr += "/";
    }
    if (addr.isEmpty() || addr=="local://") {
        addr = "local://default/";
    }
    else if (!addr.startsWith("http://")&&!addr.startsWith("file://")&&!addr.startsWith("local://")) {
        if (addr.contains(" ") || !addr.contains(".")) {
            addr = "http://www.google.com/search?aq=f&sourceid=chrome&ie=UTF-8&q="+addr;
        }
        else {
            addr = "http://"+addr;
        }
    }
    QUrl url(addr);
    ui->webView->load(url);
}



} // namespace Webkit
