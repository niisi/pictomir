#include "sch_algorhitm.h"

namespace Schema {

extern bool parceJSON(const QScriptValue &value, Algorhitm &algorhitm)
{
    if (!value.isObject() ||
            !value.property("width").isNumber() ||
            !value.property("height").isNumber())
    {
        return false;
    }
    int w = value.property("width").toInteger();
    int h = value.property("height").toInteger();
    algorhitm.size = QSize(w,h);
    algorhitm.commands = QVector<Command>(w*h, CmdNone);
    if (value.property("hint").isArray()) {
        int len = value.property("hint").property("length").toInteger();
        for (int i=0; i<len; i++) {
            algorhitm.hints << value.property("hint").property(i).toString();
        }
    }
    if (value.property("hint").isString()) {
        algorhitm.hints << value.property("hint").toString();
    }
    if (value.property("repeater").isBool()) {
        algorhitm.repeater = value.property("repeater").toBool();
    }
    else {
        algorhitm.repeater = false;
    }
    if (value.property("condition").isBool()) {
        algorhitm.condition = value.property("condition").toBool();
    }
    else {
        algorhitm.condition = false;
    }

    if (value.property("data").isArray()) {
        int len = value.property("data").property("length").toInteger();
        len = qMin(algorhitm.commands.size(), len);
        for (int i=0; i<len; i++) {
            Command cmd;
            if ( parceJSON(value.property("data").property(i), cmd) )
                algorhitm.commands[i] = cmd;
        }
    }
    algorhitm.repeaterCommand = CmdNone;
    algorhitm.conditionCommand = CmdNone;
    if (algorhitm.repeater && value.property("repeater_data").isNumber()) {
        algorhitm.repeaterCommand = Command(value.property("repeater_data").toUInt16());
    }
    else if (algorhitm.repeater) {
        parceJSON(value.property("repeater_data"), algorhitm.repeaterCommand);
    }
    if (algorhitm.condition) {
        parceJSON(value.property("condition_data"), algorhitm.conditionCommand);
    }
    return true;
}

extern bool parceAGKXML(const QDomNode &spec, const QDomNode &content, Algorhitm &algorhitm)
{
    QDomElement algSpecElement = spec.toElement();
    int w = algSpecElement.attribute("width").toInt();
    int h = algSpecElement.attribute("height").toInt();
    if (w<=0 || h<=0) {
        return false;
    }
    algorhitm.size = QSize(w, h);
    algorhitm.commands = QVector<Command>(w*h, CmdNone);
    const QString repSpec = algSpecElement.attribute("repeat").toLower().trimmed();
    const QString condSpec = algSpecElement.attribute("condition").toLower().trimmed();
    if (repSpec=="true" || repSpec=="1") {
        algorhitm.repeater = true;
    }
    else {
        algorhitm.repeater = false;
    }
    if (condSpec=="true" || condSpec=="1") {
        algorhitm.condition = true;
    }
    else {
        algorhitm.condition = false;
    }
    const QString rep = content.toElement().attribute("repeat");
    const QString cond = content.toElement().attribute("condition");
    algorhitm.repeaterCommand = CmdNone;
    algorhitm.conditionCommand = CmdNone;
    Command cmd;
    if (parceAGKXML(rep, cmd)) {
        algorhitm.repeaterCommand = cmd;
    }
    if (parceAGKXML(cond, cmd)) {
        algorhitm.conditionCommand = cmd;
    }
    QDomNodeList rows = content.toElement().elementsByTagName("row");
    int maxRows = qMin(algorhitm.size.height(), rows.count());
    for (int rowNo=0; rowNo<maxRows; rowNo++) {
        QDomNode row = rows.at(rowNo);
        QString textData = "";
        for (int i=0; i<row.childNodes().count(); i++) {
            QDomNode child = row.childNodes().at(i);
            if (child.isText())
                textData += child.toText().data() + "\n";
        }
        textData = textData.trimmed();
        QStringList cols = textData.split(QRegExp("\\s+"));
        int maxCols = qMin(algorhitm.size.width(), cols.size());
        for (int colNo=0; colNo<maxCols; colNo++) {
            const QString sc = cols[colNo];
            int index = rowNo * algorhitm.size.width() + colNo;
            Command cmd;
            if (parceAGKXML(sc, cmd)) {
                algorhitm.commands[index] = cmd;
            }
        }
    }
    return true;
}

extern QString generateAGKXML(const Algorhitm &algorhitm)
{
    QString result = "<alg";
    if (algorhitm.repeater) {
        result += " repeat=\""+generateAGKXML(algorhitm.repeaterCommand)+"\"";
    }
    if (algorhitm.condition) {
        result += " condition=\""+generateAGKXML(algorhitm.conditionCommand)+"\"";
    }
    result += ">\n";
    int cmdNo = 0;
    for (int i=0; i<algorhitm.size.height(); i++) {
        result += "\t<row>";
        for (int j=0; j<algorhitm.size.width(); j++, cmdNo++) {
            result += generateAGKXML(algorhitm.commands[cmdNo]);
            if (j<algorhitm.size.width()-1)
                result += " ";
        }
        result += "</row>\n";
    }
    result += "</alg>\n";
    return result;

}


extern QString generateKumBody(const Algorhitm &algorithm)
{
    QString result;
    if (algorithm.repeater) {
        result += QString::fromUtf8("нц");
        if (RepForever == algorithm.repeaterCommand)
            result += "\n";
        else if (Rep2 == algorithm.repeaterCommand)
            result += QString::fromUtf8(" 2 раз\n");
        else if (Rep3 == algorithm.repeaterCommand)
            result += QString::fromUtf8(" 3 раз\n");
        else if (Rep4 == algorithm.repeaterCommand)
            result += QString::fromUtf8(" 4 раз\n");
        else if (Rep5 == algorithm.repeaterCommand)
            result += QString::fromUtf8(" 5 раз\n");
        else if (Rep6 == algorithm.repeaterCommand)
            result += QString::fromUtf8(" 6 раз\n");
        else
            result += QString::fromUtf8(" 1 раз\n");
    }
    if (algorithm.conditionCommand) {
        result += QString::fromUtf8("если ");
        if (CondIsWall == algorithm.conditionCommand)
            result += QString::fromUtf8("впереди стена");
        else if (CondNoWall == algorithm.conditionCommand)
            result += QString::fromUtf8("впереди не стена");
        else if (CondWasAction == algorithm.conditionCommand)
            result += QString::fromUtf8("клетка закрашена");
        else if (CondNotAction == algorithm.conditionCommand)
            result += QString::fromUtf8("клетка не закрашена");
        else
            result += QString::fromUtf8("да");
        result += QString::fromUtf8(" то\n");
    }

    for (int i=0; i<algorithm.commands.size(); i++) {
        QString command;
        const Command cmd = algorithm.commands.at(i);
        if (CmdGoForward == cmd)
            command = QString::fromUtf8("вперед");
        else if (CmdTurnLeft == cmd)
            command = QString::fromUtf8("повернуть налево");
        else if (CmdTurnRight == cmd)
            command = QString::fromUtf8("повернуть направо");
        else if (CmdDoAction == cmd)
            command = QString::fromUtf8("закрасить");
        else if (CmdCall1 == cmd)
            command = QString::fromUtf8("Команда А");
        else if (CmdCall2 == cmd)
            command = QString::fromUtf8("Команда Б");
        else if (CmdCall3 == cmd)
            command = QString::fromUtf8("Команда В");
        else if (CmdCall4 == cmd)
            command = QString::fromUtf8("Команда Г");
        else if (CmdCall5 == cmd)
            command = QString::fromUtf8("Команда Д");
        else if (CmdCall6 == cmd)
            command = QString::fromUtf8("Команда Е");
        result += command;
        if (i < algorithm.commands.size()-1)
            result += "\n";
    }

    if (algorithm.conditionCommand) {
        result += QString::fromUtf8("\nвсе\n");
    }
    if (algorithm.repeater) {
        result += QString::fromUtf8("\nкц\n");
    }
    return result.trimmed();
}

extern QString generateJSON(const Algorhitm &algorhitm)
{
    const QString template0 = "{\n\t\"width\": %1,\n\t\"height\": %2,\n\t\"condition\": %3,\n\t\"condition_data\": %4,\n\t\"repeater\": %5,\n\t\"repeater_data\": %6,\n\t\"hint\": %7,\n\t\"data\": %8\n}";
    int width = algorhitm.size.width();
    int height = algorhitm.size.height();
    bool cond = algorhitm.condition;
    bool rep = algorhitm.repeater;
    QString condData = generateJSON(algorhitm.conditionCommand);
    QString repData = generateJSON(algorhitm.repeaterCommand);
    QString hint;
    if (algorhitm.hints.size()==0) {
        hint = "null";
    }
    if (algorhitm.hints.size()==1) {
        hint = "\""+algorhitm.hints.first()+"\"";
    }
    if (algorhitm.hints.size()>1) {
        hint += "[ ";
        for (int i=0; i<algorhitm.hints.size(); i++) {
            hint += "\""+algorhitm.hints[i]+"\"";
            if (i<algorhitm.hints.size()-1) {
                hint += ", ";
            }
        }
        hint += " ]";
    }
    QString data = "[ ";
    if (!data.isEmpty())
        data += "\n\t\t";
    for (int i=0; i<algorhitm.commands.size(); i++) {
        Command cmd = algorhitm.commands[i];
        data += generateJSON(cmd);
        if (i<algorhitm.commands.size()-1) {
            data += "\n\t\t,";
        }
    }
    if (!data.isEmpty())
        data += "\n\t";
    data += " ]";
    QString all = template0
            .arg(width)
            .arg(height)
            .arg(cond? "true" : "false")
            .arg(condData)
            .arg(rep? "true" : "false")
            .arg(repData)
            .arg(hint)
            .arg(data);
    return all;
}

extern bool isEqual(const Algorhitm &a, const Algorhitm &b)
{
    if (a.size!=b.size)
        return false;
    if (a.repeater!=b.repeater)
        return false;
    if (a.condition!=b.condition)
        return false;
    if (a.repeaterCommand!=b.repeaterCommand)
        return false;
    if (a.conditionCommand!=b.conditionCommand)
        return false;
    if (a.commands.size()!=b.commands.size())
        return false;
    for (int i=0; i<a.commands.size(); i++) {
        if (a.commands[i]!=b.commands[i])
            return false;
    }
    return true;
}

} // namespace Schema
