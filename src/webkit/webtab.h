#ifndef WEBTAB_H
#define WEBTAB_H

#include <QtGui>
#include <QtWebKit>

namespace Webkit {

namespace Ui {
    class Tab;
}

class WebTab
        : public QWidget
{
    Q_OBJECT

public:
    explicit WebTab(QWidget *parent = 0);
    explicit WebTab(QWebPage * page, QWidget * parent = 0);
    QUrl url() const;
    void setUrl(const QUrl &u);
    QIcon icon() const;
    void setAddressLineFocus();
    ~WebTab();
public slots:
    void goHome();
    void goAddress(const QString &addressLine);
signals:
    void titleChanged(const QString &title);
    void iconChanged();
    void newPageCreated(QWebPage * w);

protected:
    void init();
    void changeEvent(QEvent *e);

protected slots:
    void handleAddressEntered();
    void handleLinkChanged(const QUrl &url);



private:
    Ui::Tab *ui;
};


} // namespace Webkit
#endif
