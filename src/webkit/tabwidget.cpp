#include "tabwidget.h"
#include <QtGui>

namespace Webkit {

class TabBar :
        public QTabBar
{
    Q_OBJECT;
public:
    explicit TabBar(QWidget *parent = 0);
    QSize sizeHint() const;
signals:
    void newTabRequest();
protected:
    void mouseDoubleClickEvent(QMouseEvent *e);
};


void TabBar::mouseDoubleClickEvent(QMouseEvent *e)
{
    if (tabAt(e->pos())==-1) {
        e->accept();
        emit newTabRequest();
    }
    else {
        e->ignore();
    }
}

QSize TabBar::sizeHint() const
{
    QSize sh = QTabBar::sizeHint();
//    sh.setHeight(32);
    return sh;
}

TabBar::TabBar(QWidget *parent)
    : QTabBar(parent)
{
//    setFixedHeight(32);
}


TabWidget::TabWidget(QWidget *parent) :
    QTabWidget(parent)
{
    TabBar * tb = new TabBar(this);
    setTabBar(tb);
    connect(tb, SIGNAL(newTabRequest()), this, SIGNAL(newTabRequest()));

}

void TabWidget::tabInserted(int index)
{
    Q_UNUSED(index);
    setTabsClosable(count()>1);
}

void TabWidget::tabRemoved(int index)
{
    Q_UNUSED(index);
    setTabsClosable(count()>1);
}

} // namespace Webkit

#include "tabwidget.moc"

