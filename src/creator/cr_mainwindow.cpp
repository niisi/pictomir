#include "cr_mainwindow.h"
#include "ui_cr_mainwindow.h"
#include "core/pictomir.h"
#include "core/platform_configuration.h"
#include "cr_tasklistwindow.h"
#include "cr_environmenteditor.h"
#include "cr_programeditor.h"
#include "cr_hinteditor.h"

#include "schema/sch_game.h"
#include "schema/sch_task.h"
#include "schema/sch_program.h"
#include "schema/sch_environment.h"

#include "util/aboutdialog.h"


namespace Creator {

MainWindow::MainWindow(Core::PictomirCommon * pm, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_pictomir(pm)
{
    i_newGameIndex = 1;
    ui->setupUi(this);
    connect(ui->actionNew, SIGNAL(triggered()), this, SLOT(newGame()));
    connect(ui->actionLoad, SIGNAL(triggered()), this, SLOT(loadGame()));
    connect(ui->actionQuit, SIGNAL(triggered()), this, SLOT(close()));

    connect(ui->actionShowEnvEditor, SIGNAL(triggered(bool)), this, SLOT(showEnvEditor(bool)));
    connect(ui->actionShowProgramEditor, SIGNAL(triggered(bool)), this, SLOT(showProgramEditor(bool)));
    connect(ui->actionShowHintEditor, SIGNAL(triggered(bool)), this, SLOT(showHintEditor(bool)));

    connect(ui->menuGame, SIGNAL(aboutToShow()), this, SLOT(prepareGameMenu()));
    connect(ui->menuTask, SIGNAL(aboutToShow()), this, SLOT(prepareTaskMenu()));
    connect(ui->menuEnvrironment, SIGNAL(aboutToShow()), this, SLOT(prepareEnvironmentMenu()));
    connect(ui->menuProgram, SIGNAL(aboutToShow()), this, SLOT(prepareProgramMenu()));
    connect(ui->menuHint, SIGNAL(aboutToShow()), this, SLOT(prepareHintMenu()));

    connect(ui->actionUsage, SIGNAL(triggered()), this, SLOT(showHelp()));


    connect(qApp, SIGNAL(aboutToQuit()), this, SLOT(saveState()));
    restoreState();

    const QString lang = qApp->property("language").toString();
    m_about = new Util::AboutDialog(Core::PlatformConfiguration::resourcesPath()+"/creator_icon.png",
                                  Core::PlatformConfiguration::resourcesPath()+"/about_creator_"+lang+".html", this);

    connect(ui->actionAbout, SIGNAL(triggered()), m_about, SLOT(exec()));
}

void MainWindow::showHelp()
{
    QProcess::startDetached(QCoreApplication::applicationFilePath(),
                            QStringList() << "--ui=navigator" << QString("local://%1/creator.pms.html").arg(qApp->property("language").toString()));
}



void MainWindow::saveState()
{
    QSettings settings;
    settings.setValue("Creator/MainWindow/Rect", QRect(pos(), size()));
}

void MainWindow::restoreState()
{
    QSettings settings;
    QRect r = settings.value("Creator/MainWindow/Rect",QRect(0,0,800,600)).toRect();
    resize(r.size());
    move(r.topLeft());
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::showEnvEditor(bool v)
{
    QPair<Schema::Game*, int> tp = resolveTask();
    QMdiSubWindow * ww = NULL;
    EnvironmentEditor * ee = NULL;
    foreach (QMdiSubWindow * w, ui->mdiArea->subWindowList()) {
        if (w->widget()->objectName()=="Creator__EnvironmentEditor") {
            ee = qobject_cast<EnvironmentEditor*>(w->widget());
            if (ee->game()==tp.first && ee->taskIndex()==tp.second) {
                ww = w;
                break;
            }
        }
    }
    if (!ww && v) {
        ee = new EnvironmentEditor(this);
        ee->setEnvironment(tp.first, tp.second);
        TaskListWindow *tlw = resolveTaskListWindow(tp.first);
        Q_CHECK_PTR(tlw);
        ww = ui->mdiArea->addSubWindow(ee);
        connect(tlw, SIGNAL(taskMoved(QMap<int,int>)), ee, SLOT(checkTaskIndex(QMap<int,int>)));
        connect(tlw, SIGNAL(taskTitleChanged()), ee, SLOT(updateWindowTitle()));
        connect(tlw, SIGNAL(syncRequest()), ee, SLOT(sync()));
        connect(tlw, SIGNAL(closeRequest()), ww, SLOT(close()));
    }
    if (ww && !v) {
        ww->close();
    }
    if (v) {
        if (!ww->isVisible())
            ww->show();
        else
            ww->raise();
    }
}

void MainWindow::showProgramEditor(bool v)
{
    QPair<Schema::Game*, int> tp = resolveTask();
    QMdiSubWindow * ww = NULL;
    ProgramEditor * ee = NULL;
    foreach (QMdiSubWindow * w, ui->mdiArea->subWindowList()) {
        if (w->widget()->objectName()=="Creator__ProgramEditor") {
            ee = qobject_cast<ProgramEditor*>(w->widget());
            if (ee->game()==tp.first && ee->taskIndex()==tp.second) {
                ww = w;
                break;
            }
        }
    }
    if (!ww && v) {
        ee = new ProgramEditor(this);
        TaskListWindow *tlw = resolveTaskListWindow(tp.first);
        Q_CHECK_PTR(tlw);
        ww = ui->mdiArea->addSubWindow(ee);
        connect(tlw, SIGNAL(taskMoved(QMap<int,int>)), ee, SLOT(checkTaskIndex(QMap<int,int>)));
        connect(tlw, SIGNAL(taskTitleChanged()), ee, SLOT(updateWindowTitle()));
        connect(tlw, SIGNAL(syncRequest()), ee, SLOT(sync()));
        connect(tlw, SIGNAL(closeRequest()), ww, SLOT(close()));
        ee->setProgram(tp.first, tp.second);
    }
    if (ww && !v) {
        ww->close();
    }
    if (v) {
        if (!ww->isVisible())
            ww->show();
        else
            ww->raise();
    }
}

void MainWindow::showHintEditor(bool v)
{
    QPair<Schema::Game*, int> tp = resolveTask();
    QMdiSubWindow * ww = NULL;
    HintEditor * ee = NULL;
    foreach (QMdiSubWindow * w, ui->mdiArea->subWindowList()) {
        if (w->widget()->objectName()=="Creator__HintEditor") {
            ee = qobject_cast<HintEditor*>(w->widget());
            if (ee->game()==tp.first && ee->taskIndex()==tp.second) {
                ww = w;
                break;
            }
        }
    }
    if (!ww && v) {
        ee = new HintEditor(this);
        TaskListWindow *tlw = resolveTaskListWindow(tp.first);
        Q_CHECK_PTR(tlw);
        ww = ui->mdiArea->addSubWindow(ee);
        connect(tlw, SIGNAL(taskMoved(QMap<int,int>)), ee, SLOT(checkTaskIndex(QMap<int,int>)));
        connect(tlw, SIGNAL(taskTitleChanged()), ee, SLOT(updateWindowTitle()));
        connect(tlw, SIGNAL(syncRequest()), ee, SLOT(sync()));
        connect(tlw, SIGNAL(closeRequest()), ww, SLOT(close()));
        ee->setHint(tp.first, tp.second);
    }
    if (ww && !v) {
        ww->close();
    }
    if (v) {
        if (!ww->isVisible())
            ww->show();
        else
            ww->raise();
    }
}

TaskListWindow* MainWindow::resolveTaskListWindow(const Schema::Game *gamePtr)
{
    TaskListWindow * result = NULL;
    foreach (QMdiSubWindow * w, ui->mdiArea->subWindowList()) {
        if (w->widget()->objectName()=="Creator__TaskListWindow") {
            TaskListWindow * tlw = qobject_cast<TaskListWindow*>(w->widget());
            Q_CHECK_PTR(tlw);
            if (tlw->gamePtr()==gamePtr) {
                result = tlw;
            }
        }
    }
    return result;
}

QPair<Schema::Game*, int> MainWindow::resolveTask() const
{
    QMdiSubWindow * win = ui->mdiArea->currentSubWindow();
    Q_CHECK_PTR(win);
    Q_CHECK_PTR(win->widget());
    QWidget * w = win->widget();
    const QString winType = QString(w->objectName());
    QPair<Schema::Game*, int> result(0,0);
    if (winType=="Creator__TaskListWindow") {
        TaskListWindow * tlw = qobject_cast<TaskListWindow*>(w);
        Q_CHECK_PTR(tlw);
        result.first = tlw->gamePtr();
        result.second = tlw->taskIndex();
    }
    else if (winType=="Creator__EnvironmentEditor") {
        EnvironmentEditor * ee = qobject_cast<EnvironmentEditor*>(w);
        Q_CHECK_PTR(ee);
        result.first = ee->game();
        result.second = ee->taskIndex();
    }
    else if (winType=="Creator__ProgramEditor") {
        ProgramEditor * pe = qobject_cast<ProgramEditor*>(w);
        Q_CHECK_PTR(pe);
        result.first = pe->game();
        result.second = pe->taskIndex();
    }
    else if (winType=="Creator__HintEditor") {
        HintEditor * ee = qobject_cast<HintEditor*>(w);
        Q_CHECK_PTR(ee);
        result.first = ee->game();
        result.second = ee->taskIndex();
    }
    Q_CHECK_PTR(result.first);
    Q_ASSERT(result.second>=0);
    Q_ASSERT(result.second<result.first->tasks.size());
    return result;
}

void MainWindow::newGame()
{
    Schema::Game game;
    Schema::Task task;
    task.title = tr("Untitled task %1").arg(1);
    Schema::Program program;
    Schema::Algorhitm algorhitm;
    algorhitm.size = QSize(2, 2);
    algorhitm.repeater = false;
    algorhitm.condition = false;
    algorhitm.commands = QVector<Schema::Command>(4);
    program << algorhitm;
    task.program = program;
    Schema::Environment env;
    env.size = QSize(2, 2);
    env.direction = Schema::Environment::South;
    env.position = QPoint(0, 0);
    task.environment = env;
    game.tasks << task;
    game.title = tr("New game %1").arg(i_newGameIndex);
    game.license = "Creative Commons BY";
    i_newGameIndex ++ ;
    game.index = 0;

    TaskListWindow * tlw = new TaskListWindow(this);
    connect(tlw, SIGNAL(requestEnvironmentEditor(bool)), this, SLOT(showEnvEditor(bool)));
    connect(tlw, SIGNAL(requestProgramEditor(bool)), this, SLOT(showProgramEditor(bool)));
    connect(tlw, SIGNAL(requestHintEditor(bool)), this, SLOT(showHintEditor(bool)));
    tlw->setGame(game,"");

    QMdiSubWindow * w = ui->mdiArea->addSubWindow(tlw);
    w->show();

}


void MainWindow::saveGame()
{
    TaskListWindow * tlw = resolveTaskListWindow(resolveTask().first);
    Q_CHECK_PTR(tlw);
    tlw->saveGame();
}

void MainWindow::saveGameAs()
{
    TaskListWindow * tlw = resolveTaskListWindow(resolveTask().first);
    Q_CHECK_PTR(tlw);
    tlw->saveGameAs();
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    QStringList unsavedGames;
    QList<TaskListWindow*> unsavedWindows;
    foreach (QMdiSubWindow * sub, ui->mdiArea->subWindowList()) {
        if (sub->widget()->objectName()=="Creator__TaskListWindow") {
            TaskListWindow * tlw = qobject_cast<TaskListWindow*>(sub->widget());
            Q_CHECK_PTR(tlw);
            if (tlw->isChanged()) {
                unsavedGames << "\t"+tlw->gameTitle();
                unsavedWindows << tlw;
            }
        }
    }
    bool allowClose = true;
    if (unsavedGames.count()>0) {
        allowClose = false;
        const QString message = tr("The following games has unsaved changes:\n%1\nSave them?\n").arg(unsavedGames.join("\n"));
        QMessageBox::StandardButton answer = QMessageBox::question(this, tr("Save changes"), message, QMessageBox::Yes|QMessageBox::No|QMessageBox::Cancel);
        if (answer==QMessageBox::Yes) {
            allowClose = true;
            foreach (TaskListWindow * t, unsavedWindows) {
                allowClose = allowClose && t->saveGame();
            }
        }
        else if (answer==QMessageBox::No) {
            allowClose = true;
        }
    }
    if (allowClose) {
        event->accept();
    }
    else {
        event->ignore();
    }
}

void MainWindow::loadGame()
{
    QSettings settings;
    QString lastDir = settings.value("Creator/Game/LastDir", Core::PlatformConfiguration::tasksPath()).toString();
    const QString filter = tr("Pictomir tasks, version >= 0.12.x (*.pm.json);;Pictomir tasks, version < 0.12.x (*.pm.xml)");
    const QString fileName = QFileDialog::getOpenFileName(this, tr("Load game..."), lastDir, filter);
    if (!fileName.isEmpty()) {
        lastDir = fileName;
        settings.setValue("Creator/Game/LastDir", QFileInfo(lastDir).dir().absolutePath());
        QString error;
        Schema::Game game;
        game = loadGameFromFile(fileName, error);
        if (error.isEmpty()) {
            TaskListWindow * tlw = new TaskListWindow(this);
            tlw->setGame(game, fileName);
            connect(tlw, SIGNAL(requestEnvironmentEditor(bool)), this, SLOT(showEnvEditor(bool)));
            connect(tlw, SIGNAL(requestProgramEditor(bool)), this, SLOT(showProgramEditor(bool)));
            connect(tlw, SIGNAL(requestHintEditor(bool)), this, SLOT(showHintEditor(bool)));
            QMdiSubWindow * w = ui->mdiArea->addSubWindow(tlw);
            w->show();
        }
        else {
            QMessageBox::critical(this, tr("Can't load game"), error);
        }
    }
}

Schema::Game MainWindow::loadGameFromFile(const QString &fileName, QString &error)
{
    Schema::Game result;
    if (fileName.endsWith(".xml")) {
        QDomDocument doc;
        QFile f(fileName);
        QByteArray ba;
        if (f.open(QIODevice::ReadOnly)) {
            ba = f.readAll();
            f.close();
        }
        QString xmlError;
        int xmlLine, xmlCol;
        if (!doc.setContent(ba,&xmlError,&xmlLine,&xmlCol)) {
            error = QString("XML error at line %1, col. %2:\n%3").
                    arg(xmlLine).arg(xmlCol).arg(xmlError);
        }

        else if (doc.documentElement().nodeName()=="game") {
            QDomNode gameNode = doc.documentElement();
            if (!Schema::parceAGKXML(gameNode, result, false)) {
                error = "Error parsing AGKXML!!!";
            }
        }
        else {
            error = "Not PictoMir XML!";
        }
    }
    else if (fileName.endsWith(".js") || fileName.endsWith(".json")) {
        QString jsData;
        QFile f(fileName);
        if (f.open(QIODevice::ReadOnly|QIODevice::Text)) {
            QTextStream ts(&f);
            ts.setCodec("UTF-8");
            ts.setAutoDetectUnicode(true);
            jsData = "currentGame = "+ts.readAll()+";";
            f.close();
        }
        QScriptEngine engine;
        engine.evaluate("var initialGame = null; var currentGame = null;");
        QScriptValue value = engine.evaluate(jsData);
        if (engine.hasUncaughtException()) {
             int line = engine.uncaughtExceptionLineNumber();
             qDebug() << "uncaught exception at line" << line << ":" << value.toString();
        }
        if (value.isError()) {
            error = value.toString();
        }
        else if (value.isObject()) {
            if (!Schema::parceJSON(value, result)) {
                error = "Error pasing JSON!";
            }
        }
    }
    if (error.isEmpty()) {
        // Move to the top of recent files list
        QSettings settings;
        QStringList recent = settings.value("Creator/RecentGames").toStringList();
        recent.removeAll(fileName);
        recent.prepend(fileName);
        while (recent.size()>5) {
            recent.pop_back();
        }
        settings.setValue("Creator/RecentGames", recent);
        settings.sync();
    }
    return result;
}

void MainWindow::prepareGameMenu()
{
    if (ui->mdiArea->currentSubWindow()==0) {
        ui->actionSave->setEnabled(false);
        ui->actionSave_as->setEnabled(false);
    }
    else {
        TaskListWindow * tlw = resolveTaskListWindow(resolveTask().first);
        ui->actionSave->setEnabled(true);
        ui->actionSave_as->setEnabled(true);
        ui->actionSave->disconnect();
        ui->actionSave_as->disconnect();
        connect(ui->actionSave, SIGNAL(triggered()), tlw, SLOT(saveGame()));
        connect(ui->actionSave_as, SIGNAL(triggered()), tlw, SLOT(saveGameAs()));
    }
    QSettings settings;
    QStringList recent = settings.value("Creator/RecentGames").toStringList();
    QStringList actual;
    QMenu * m = new QMenu(this);
    foreach (const QString s, recent) {
        if (QFile::exists(s)) {
            QAction * a = m->addAction(QFileInfo(s).fileName(), this, SLOT(loadRecentGame()));
            a->setProperty("fileName", s);
            actual << s;
        }
    }
    ui->actionRecent_games->setEnabled(actual.size()>0);
    ui->actionRecent_games->setMenu(m);
}

void MainWindow::loadRecentGame()
{
    const QString fileName = sender()->property("fileName").toString();
    QString error;
    Schema::Game game = loadGameFromFile(fileName, error);
    if (!error.isEmpty()) {
        QMessageBox::critical(this, tr("Can't load game"), error);
    }
    if (error.isEmpty()) {
        TaskListWindow * tlw = new TaskListWindow(this);
        tlw->setGame(game, fileName);
        connect(tlw, SIGNAL(requestEnvironmentEditor(bool)), this, SLOT(showEnvEditor(bool)));
        connect(tlw, SIGNAL(requestProgramEditor(bool)), this, SLOT(showProgramEditor(bool)));
        connect(tlw, SIGNAL(requestHintEditor(bool)), this, SLOT(showHintEditor(bool)));
        QMdiSubWindow * w = ui->mdiArea->addSubWindow(tlw);
        w->show();
    }
}

void MainWindow::prepareTaskMenu()
{
    TaskListWindow * tlw = NULL;
    if (ui->mdiArea->currentSubWindow()) {
        if (ui->mdiArea->currentSubWindow()->widget()->objectName()=="Creator__TaskListWindow") {
            tlw = qobject_cast<TaskListWindow*>(ui->mdiArea->currentSubWindow()->widget());
        }
    }
    if (tlw == NULL) {
        ui->actionClone->setEnabled(false);
        ui->actionRemove->setEnabled(false);
        ui->actionRename->setEnabled(false);
        ui->actionMove_up->setEnabled(false);
        ui->actionMove_down->setEnabled(false);
    }
    else {
        ui->actionClone->setEnabled(true);
        ui->actionRemove->setEnabled(tlw->actionRemove()->isEnabled());
        ui->actionRename->setEnabled(true);
        ui->actionMove_up->setEnabled(tlw->actionUp()->isEnabled());
        ui->actionMove_down->setEnabled(tlw->actionUp()->isEnabled());

        ui->actionClone->disconnect();
        ui->actionRemove->disconnect();
        ui->actionRename->disconnect();
        ui->actionMove_up->disconnect();
        ui->actionMove_down->disconnect();

        connect(ui->actionClone, SIGNAL(triggered()), tlw->actionClone(), SLOT(trigger()));
        connect(ui->actionRemove, SIGNAL(triggered()), tlw->actionRemove(), SLOT(trigger()));
        connect(ui->actionRename, SIGNAL(triggered()), tlw->actionRename(), SLOT(trigger()));
        connect(ui->actionMove_up, SIGNAL(triggered()), tlw->actionUp(), SLOT(trigger()));
        connect(ui->actionMove_down, SIGNAL(triggered()), tlw->actionDown(), SLOT(trigger()));
    }
}

void MainWindow::prepareEnvironmentMenu()
{
    EnvironmentEditor * ed = NULL;
    bool showEditorEnabled = false;
    bool showEditorChecked = false;
    if (ui->mdiArea->currentSubWindow()) {
        showEditorEnabled = true;
        if (ui->mdiArea->currentSubWindow()->widget()->objectName()=="Creator__EnvironmentEditor") {
            ed = qobject_cast<EnvironmentEditor*>(ui->mdiArea->currentSubWindow()->widget());
            showEditorChecked = true;
        }
        else {
            QPair<Schema::Game*,int> ct = resolveTask();
            foreach (const QMdiSubWindow * w, ui->mdiArea->subWindowList()) {
                if (w->widget()->objectName()=="Creator__EnvironmentEditor") {
                    EnvironmentEditor * ww = qobject_cast<EnvironmentEditor*>(w->widget());
                    if (ww->game()==ct.first) {
                        showEditorChecked = true;
                        break;
                    }
                }
            }
        }
    }
    if (ed==NULL) {
        ui->actionShowEnvEditor->setEnabled(showEditorEnabled);
        ui->actionShowEnvEditor->setChecked(showEditorChecked);

        ui->actionCreate_new->setEnabled(false);
        ui->actionImport_from_Kumir->setEnabled(false);
    }
    else {
        ui->actionShowEnvEditor->setEnabled(true);
        ui->actionShowEnvEditor->setChecked(true);

        ui->actionCreate_new->setEnabled(true);
        ui->actionImport_from_Kumir->setEnabled(true);

        ui->actionCreate_new->disconnect();
        ui->actionImport_from_Kumir->disconnect();

        connect(ui->actionCreate_new, SIGNAL(triggered()), ed->actionNew(), SLOT(trigger()));
        connect(ui->actionImport_from_Kumir, SIGNAL(triggered()), ed->actionImport(), SLOT(trigger()));
        connect(ui->actionExport_to_Kumir, SIGNAL(triggered()), ed->actionExport(), SLOT(trigger()));
    }
}

void MainWindow::prepareProgramMenu()
{
    ProgramEditor * ed = NULL;
    bool showEditorEnabled = false;
    bool showEditorChecked = false;
    if (ui->mdiArea->currentSubWindow()) {
        showEditorEnabled = true;
        if (ui->mdiArea->currentSubWindow()->widget()->objectName()=="Creator__ProgramEditor") {
            ed = qobject_cast<ProgramEditor*>(ui->mdiArea->currentSubWindow()->widget());
            showEditorChecked = true;
        }
        else {
            QPair<Schema::Game*,int> ct = resolveTask();
            foreach (const QMdiSubWindow * w, ui->mdiArea->subWindowList()) {
                if (w->widget()->objectName()=="Creator__ProgramEditor") {
                    ProgramEditor * ww = qobject_cast<ProgramEditor*>(w->widget());
                    if (ww->game()==ct.first) {
                        showEditorChecked = true;
                        break;
                    }
                }
            }
        }
    }
    if (ed==NULL) {
        ui->actionShowProgramEditor->setEnabled(showEditorEnabled);
        ui->actionShowProgramEditor->setChecked(showEditorChecked);

        ui->actionAdd_algorhitm->setEnabled(false);
        ui->actionChange_algorhitm->setEnabled(false);
        ui->actionRemove_algorhitm->setEnabled(false);

    }
    else {
        ui->actionShowProgramEditor->setEnabled(true);
        ui->actionShowProgramEditor->setChecked(true);

        ui->actionAdd_algorhitm->setEnabled(true);
        ui->actionChange_algorhitm->setEnabled(true);
        ui->actionRemove_algorhitm->setEnabled(true);

        ui->actionRemove_algorhitm->disconnect();

        connect(ui->actionAdd_algorhitm, SIGNAL(triggered()), ed->actionAdd(), SLOT(trigger()));
        ui->actionChange_algorhitm->setMenu(ed->editMenu());
        ui->actionRemove_algorhitm->setMenu(ed->removeMenu());
    }
}

void MainWindow::prepareHintMenu()
{
    HintEditor * ed = NULL;
    bool showEditorEnabled = false;
    bool showEditorChecked = false;
    if (ui->mdiArea->currentSubWindow()) {
        showEditorEnabled = true;
        if (ui->mdiArea->currentSubWindow()->widget()->objectName()=="Creator__HintEditor") {
            ed = qobject_cast<HintEditor*>(ui->mdiArea->currentSubWindow()->widget());
            showEditorChecked = true;
        }
        else {
            QPair<Schema::Game*,int> ct = resolveTask();
            foreach (const QMdiSubWindow * w, ui->mdiArea->subWindowList()) {
                if (w->widget()->objectName()=="Creator__HintEditor") {
                    HintEditor * ww = qobject_cast<HintEditor*>(w->widget());
                    if (ww->game()==ct.first) {
                        showEditorChecked = true;
                        break;
                    }
                }
            }
        }
    }
    if (ed==NULL) {
        ui->actionShowHintEditor->setEnabled(showEditorEnabled);
        ui->actionShowHintEditor->setChecked(showEditorChecked);

        ui->actionClear_hint->setEnabled(false);
        ui->actionSet_plain_text_as_hint->setEnabled(false);
        ui->actionLoad_picture_as_hint->setEnabled(false);

    }
    else {
        ui->actionShowHintEditor->setEnabled(true);
        ui->actionShowHintEditor->setChecked(true);

        ui->actionClear_hint->setEnabled(ed->actionClear()->isEnabled());
        ui->actionSet_plain_text_as_hint->setEnabled(ed->actionSetText()->isEnabled());
        ui->actionLoad_picture_as_hint->setEnabled(ed->actionSetPicture()->isEnabled());

        ui->actionClear_hint->disconnect();
        ui->actionSet_plain_text_as_hint->disconnect();
        ui->actionLoad_picture_as_hint->disconnect();

        connect(ui->actionClear_hint, SIGNAL(triggered()), ed->actionClear(), SLOT(trigger()));
        connect(ui->actionSet_plain_text_as_hint, SIGNAL(triggered()), ed->actionSetText(), SLOT(trigger()));
        connect(ui->actionLoad_picture_as_hint, SIGNAL(triggered()), ed->actionSetPicture(), SLOT(trigger()));

    }
}



} // namespace Creator
