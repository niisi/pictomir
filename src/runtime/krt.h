#ifndef KRT_H
#define KRT_H

#include <QtCore>

namespace Runtime {

class KRT : 
    public QObject
{
    Q_OBJECT;
    public:
    KRT(QObject *parent=0);
    void initialize();
    void init(const QStringList &functions);
    bool hasMoreInstructions() const;
    void clear();
    void connectSignalINTCallback(QObject *object, const char *method);
    QStack<QString> backtraceInfo() const;

    public slots:
    void executeNextInstruction();
    private:
    class KRT_private *d;
};

}

#endif
