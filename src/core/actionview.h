#ifndef ACTIONVIEW_H
#define ACTIONVIEW_H

#include <QtCore>
#include <QtGui>
#include <QtSvg>

namespace Core {

class ActionModel;

class ActionView
{
public:
    virtual QGraphicsSvgItem* graphicsItem() = 0;
    virtual ActionModel* model() = 0;
    virtual int modelId() const = 0;
    virtual bool isEnabled() const = 0;
    virtual QSize originalSize() const = 0;
    virtual QSize currentSize() const = 0;
    virtual void setActive(bool v) = 0;
};

}
#endif // ACTIONVIEW_H
