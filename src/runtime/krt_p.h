#ifndef KRT_PRIVATE_H
#define KRT_PRIVATE_H

#include <QtCore>
#include "functiondata.h"

namespace Runtime {

enum ArgumentLocation {
    Register,
    Heap,
    Local,
    Constant
};

struct ArgumentInfo {
    QVariant::Type type;
    ArgumentLocation location;
    quint32 ptr;
    quint8 register_no;
    inline ArgumentInfo() {
        type = QVariant::Invalid;
        location = Constant;
        ptr = 0;
        register_no = 0;
    }
};

class KRT_private
        : QObject
{
    friend class KRT;
    Q_OBJECT
    public:
    KRT_private(QObject *parent);
    void processInstruction(const QString &instruction);
    mutable bool INT_finished;

    protected slots:
    void ADD(const QStringList &arguments);
    void SUB(const QStringList &arguments);
    void MUL(const QStringList &arguments);
    void DIV(const QStringList &arguments);
    void NEG(const QStringList &arguments);
    void CMP(const QStringList &arguments);
    void EXG(const QStringList &arguments);
    void COPY(const QStringList &arguments);
    void INT(const QStringList &arguments);
    void JSR(const QStringList &arguments);
    void RTS(const QStringList &arguments);
    void JMP(const QStringList &arguments);
    void JNZ(const QStringList &arguments);
    void JZ(const QStringList &arguments);
    void POP(const QStringList &arguments);
    void PUSH(const QStringList &arguments);
    void BTI(const QStringList &arguments);

    protected:
    QMetaMethod findMethod(const QString &name) const;
    quint8 registerByName(const QString &regName) const;
    ArgumentInfo extractArgumentInfo(const QString &argument) const;
    QVariant extractArgumentValue(const QString &argument) const;
    void setArgumentValue(const QString &argument, const QVariant &value);

    public:
    QVector<QVariant> heap;
    QStack<FunctionData> callStack;
    QStack<QVariant> dataStack;
    QList<QStringList> instructions;

    signals:
    void INT_callback(quint16 sigNo, QStack<QVariant> *data_stack);
};

}

#endif
