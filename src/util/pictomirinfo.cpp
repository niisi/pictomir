#include "pictomirinfo.h"

namespace Util {

PictomirInfo::PictomirInfo(QObject *parent)
        : QObject(parent)
{
}

QString PictomirInfo::version() const
{
    return QCoreApplication::applicationVersion();
}

QString PictomirInfo::revision() const
{
    return QCoreApplication::instance()->property("applicationRevision").toString();
}

PictomirInfo* PictomirInfo::m_instance = NULL;

PictomirInfo* PictomirInfo::instance()
{
    if (m_instance==NULL)
        m_instance =  new PictomirInfo();
    return m_instance;
}

}
